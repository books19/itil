<map version="freeplane 1.9.13">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="ITIL 2011" FOLDED="false" ID="ID_1567096635" CREATED="1657801620360" MODIFIED="1657830798650" STYLE="oval">
<font SIZE="10"/>
<hook NAME="MapStyle" background="#ffffff">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_icon_for_attributes="true" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_716601714" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_716601714" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#4e85f8" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="7" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Estrategia del Servicio" POSITION="right" ID="ID_1255148570" CREATED="1657801663187" MODIFIED="1657830798651">
<edge COLOR="#ff0000"/>
<font SIZE="10"/>
<node TEXT="Prefacio" FOLDED="true" ID="ID_1903146672" CREATED="1658172741232" MODIFIED="1658174083156" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;En Estrategia es importante ver las cosas distantes como si estuvieran cerca y tomar una vista distanciada de las cosas cercanas&quot;
    </p>
    <p>
      
    </p>
    <p>
      -- Miyamoto Musashi
    </p>
  </body>
</html></richcontent>
<node TEXT="Proposito" ID="ID_1187582642" CREATED="1658173656787" MODIFIED="1658173673400"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El principal proposito de la fase estrategia del servicio del ciclo de vida del servicio es implementar y gestionar una correcta estrategia generalizada para IT, basada sobre la estrategia del negocio general de la organizacion, para que los servicios IT apropiados puedan ser proveidos para cumplir con las necesidades actuales y futuras del negocio. Por tanto el pensamiento estrategico debe ser aplicado a la gestion del servicio y la gestion del servicio debe en si misma ser considerada como un activo estrategico de una organizacion IT.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Proceso" ID="ID_9476143" CREATED="1658173026277" MODIFIED="1658173482307" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estrategia del servicio acuerda y define el portafolio de servicio y cualquier nuevas adiciones a este, y provee la entrada para el diseno del servicio para que los servicios IT apropiados puedan ser designados y entregados para cumplir con las salidas de negocio requeridas.
    </p>
    <p>
      
    </p>
    <p>
      Establecer una estrategia viable es el primer paso pero tambien es esencial comunicar satisfactoriamente a todos los clientes, personal interno y proveedores para asegurar que el diseno, transicion y operacion de los servicios IT cumplen consistentemente las saidas del negocio requeridas. Esto implica una colaboracion cercana con la mejora continua del servicio para implementar el monitoreo, medicion, analisis, reporteria e implementacion de acciones correctivas apropiadas. De esta forma los servicios IT estan siendo proveidos; los procesos, herrameintas y proveedores que los entregan y operan; y la estrategia en general por si misma se ajusta al proposito (o sera actualizada de ser requerido).
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Pensamiento Academico" ID="ID_1388057569" CREATED="1658173488187" MODIFIED="1658173625202" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estrategia requiere pensamiento academico, pero no deberia mantenerse como un ejercicio academico, esta debe ser ser aplicada al mundo de los negocios reales en orden de influenciar y obtener resultados de negocio exitosos. La Estrategia de Servicio ITIL provee una guia practica en como la estrategia de servicio debe ser formulada y aplicada para proveer valor al negocio.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="1. Introduccion" FOLDED="true" ID="ID_1973324050" CREATED="1657976492507" MODIFIED="1657976499806">
<node TEXT="ITIL (Information Technology Infrastructure Library)" ID="ID_1928819085" CREATED="1657976592172" MODIFIED="1658172734119" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      ITIL es parte de una suite de publicaciones de buenas practicas para la gestion de servicio IT (ITSM). ITIL provee una guia para los proveedores de servicio en la provision de servicio IT de calidad, y los procesos, funciones y otras capacidades necesarias para soportarlo.
    </p>
    <p>
      
    </p>
    <p>
      ITIL no es un estandar que debe ser seguido, es una guia que deberia ser leida y entendida, y usada para crear valor para el proveedor de servicio y sus clientes. Las organizaciones estan motivadas para adoptar las mejores practicas ITIL y adaptarlas a sus especificos ambientes de trabajo para cumplir con sus necesidades.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Estandar" ID="ID_28981393" CREATED="1657978822196" MODIFIED="1657979104482" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      ITIL es un framework para ITSM. ISO/IEC 20000 es un estandar formal y universal para las organizaciones que buscan que sus capacidades de gestion sean auditadas y certificadas. Mientras ISO/IEC 20000 es un estandar a ser alcanzado y mantenido, ITIL ofrece un banco de conocimiento util para conseguir el estandar.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Fases del ciclo de vida del servicio" FOLDED="true" ID="ID_397154729" CREATED="1657801683257" MODIFIED="1657979958975" TEXT_SHORTENED="true">
<font SIZE="10"/>
<richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El framework ITIL esta basado en las cinco fases del ciclo de vida de servicio como se muestra en la figura, con una publicacion principal que provee una guia de buenas practicas para cada fase. Esta guia incluye los principios clave, los procesos y actividades requeridas, la organizacion y roles, la tecnologia, los retos asociados, factores criticos de exito y riesgos.
    </p>
    <p>
      
    </p>
    <p>
      El ciclo de vida del servicio usa un diseno hub-and-spoke, con la estrategia del servicio en el centro, y el diseno, la trancision y operacion del servicio como fases que la envuelven como radios &quot;spokes&quot;. La mejora continua del servicio rodea y soporta todas las fases del ciclo de vida del servicio. Cada fase del ciclo de vida ejerce influencia en las otras y confia en las otras por entradas y retroalimentacion. De esta manera, un conjunto constante de controles y equilibrios en todo el ciclo de vida del servicio garantiza que, a medida que la demanda del negocio cambia con la necesidad del negocio, los servicios pueden adaptarse y responder de manera efectiva.
    </p>
  </body>
</html></richcontent>
<node TEXT="" ID="ID_1034593028" CREATED="1657801781725" MODIFIED="1657895465697">
<hook URI="img/Ciclo%20de%20vida%20servicio%20ITIL.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="1.1 Overview" FOLDED="true" ID="ID_1164680760" CREATED="1657979968107" MODIFIED="1657979978343">
<node TEXT="1.1.1 Proposito y objetivos de la estrategia del servicio" FOLDED="true" ID="ID_1217770298" CREATED="1657801921351" MODIFIED="1657980031300" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El proposito de la estrategia del servicio es definir la perspectiva, planes y patrones que un proveedor de servicio necesita para ejecutar para conocer las salidas de la organizacion del negocio.
    </p>
  </body>
</html></richcontent>
<font SIZE="10"/>
<node TEXT="Objetivos" ID="ID_524661387" CREATED="1657980073813" MODIFIED="1657995483559" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los objetivos de la estrategia del servicio incluye proveer:
    </p>
    <ul>
      <li>
        Un entendimiento de que es la estrategia
      </li>
      <li>
        Una clara identificacion de la definicion de servicios y los clientes que la usan
      </li>
      <li>
        La habilidad para definir como el valor es creado y entregado
      </li>
      <li>
        Un medio para identificar oportunidades para proveer servicios y como explotarlas
      </li>
      <li>
        Una claro model de provision de servicios, que articula como los servicios seran entregados y creados, y a quien seran entregados y para que proposito.
      </li>
      <li>
        El medio para entender la capacidad organizacional requerida para entregar la estrategia.
      </li>
      <li>
        Documentacion y coordinacion de como los activos de servicio son utilizados para entregar servicios, y como optimizar su rendimiento.
      </li>
      <li>
        Procesos que definen la estrategia de la organizacion, cuyos servicios alcanzaran la estrategia, que nivel de inversion sera requerido, y que niveles de demanda, y los medios para asegurar que una relacion de trabajo existe entre el cliente y el proveedor de servicio.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="1.1.2 Alcance" FOLDED="true" ID="ID_965934786" CREATED="1657980515103" MODIFIED="1657980833877" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      ITIL Estrategia del servicio es realizada para ser usada tanto para proveedores de servicio internos como externos, e incluye una guia para las organizaciones la cuales requieren ofrecer servicios de IT como un negocio rentable, asi como a aquellas que requieren ofrecer servicios IT a otras unidades de negocio dentro de la misma organizacion sin fines de lucro.
    </p>
  </body>
</html></richcontent>
<node TEXT="Aspectos de la estrategia del servicio" ID="ID_1540819184" CREATED="1657980743735" MODIFIED="1657980831819" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Definir una estrategia por la cual un proveedor de servicio entregara servicios para cumplir con las salidas de negocio de sus clientes.
      </li>
      <li>
        Definir una estrategia sobre como gestionar esos servicios.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="1.1.3 Uso" ID="ID_924272983" CREATED="1657980838321" MODIFIED="1657993585167" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estrategia del servicio de ITIL provee acceso a las mejores practicas probadas basadas en las habilidades y conocimiento de trabajadores de la industria experimentados en adoptar un enfoque estandarizado y controlado para la gestion de servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="1.1.4 Valor al negocio" FOLDED="true" ID="ID_1102464298" CREATED="1657980844425" MODIFIED="1657993749835" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Seleccionar y adoptar la mejor practica como se recomienda en ITIL asistira a las organizaciones en la entrega de beneficios significativos. El adoptar e implementar un enfoque estandar y consistente para la estrategia del servicio permitira:
    </p>
  </body>
</html></richcontent>
<node ID="ID_1572138770" CREATED="1657993752887" MODIFIED="1657994732712"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Soportar la habilidad para enlazar las actividades realizadas por el proveedor del servicio con las salidas que son criticas a clientes internos y externos. Como resultado, el proveedor de servicio sera visto como que contribuye valor de la organizacion (no solo como costo).
      </li>
      <li>
        Permite al proveedor del servicio tener un claro entendimiento de que tipos y niveles de servicio seran existos para sus clientes y luego organizarse a si mismo de forma optima para entregar soporte a esos servicios. El proveedor de servicio lograra esto mediante un proceso de definir estrategias y servicios, asegurando un enfoque consistente y repetible para definir como el valor sera construido y entregado que sea accesible a todos los interesados.
      </li>
      <li>
        Permitira al proveedor del servicio responder rapida y efectivamente a cambios en el ambiente de negocio, asegurando una ventaja competitiva en el tiempo.
      </li>
      <li>
        Soportar la creacion y mantenimiento de un portafolio de servicios cuantificados que permitira al negocio alcanzar un retorno positivo de la inversion en servicios.
      </li>
      <li>
        Facilitar la comunicacion funcional y transparente entre el cliente y el proveedor de servicio, asi que los dos tengan un entendimiento consistente de lo que es requerido y como sera entregado.
      </li>
      <li>
        Provera los medios al proveedor de servicio para organizarse a si mismo para que pueda proveer servicios de una manera eficiente y efectiva.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="1.1.5 Audiencia objetivo" FOLDED="true" ID="ID_1642501958" CREATED="1657980863962" MODIFIED="1657995420997">
<node TEXT="Ejecutivos y gerentes" ID="ID_429021466" CREATED="1657995339197" MODIFIED="1657995429992" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estrategia de servicio ITIL esta dirigido a ejecutivos y gerentes responsables de definir la estrategia de un proveedor de servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Clientes y lideres de unidad de negocio" ID="ID_1522087296" CREATED="1657995349551" MODIFIED="1657995429200" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Clientes y lideres de unidades de negocio quienes interactuan con proveedores de servicio como parte de sus responsabilidades se beneficiaran de esta publicacion al recibir una perspectiva sobre como los proveedores de servicio trabajan, y como ellos pueden facilitar una interfaz mas constructiva con el proveedor de servicio. Mientras mas un cliente entienda el contexto de la provision de servicio, mejor podran articular sus necesidades de forma que el proveedor de servicio pueda satisfacerlas.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Duenos de proceso y profesionales" ID="ID_629421295" CREATED="1657995363082" MODIFIED="1657995428445" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estrategia de servicio ITIL tambien sera valiosa a los duenos de procesos mas estrategicos tales como la gestion de la estrategia de servicios IT, gestion del portafolio del servicio, gestion de la demanda, gestion financiera y gestion de la relacion del negocio. Los profesionales de la gestion del servicio que trabajan como parte del ciclo de vida del servicio encuentran util esta publicacion al identificar el contexto para las actividades que estan realizando, las cuales ayudaran a mejorar la habilidad para definir y entregar servicios de calidad, y construir estructuras apropiadas para gestionar esos servicios.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="1.2 Contexto" FOLDED="true" ID="ID_1346579371" CREATED="1657980885555" MODIFIED="1657995813849" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Cada una de las fases del ciclo de vida del servicio tiene las capacidades para dirigir el impacto en el rendimiento del proveedor del servicio. Del core se espera que provea estructura, estabilidad y fortaleza a las capacidades de gestion de servicio, con principios, metodos y herramientas durables. Esto sirve para proteger inversiones y proveer la base necesaria para medir, aprender y mejorar.
    </p>
  </body>
</html></richcontent>
<node TEXT="1.2.1 Estrategia del servicio" FOLDED="true" ID="ID_829805421" CREATED="1657980892564" MODIFIED="1657996387104" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Al centro del ciclo de vida del servicio esta la estrategia del servicio. La creacion de valor inicia aqui entendiendo los objetivos organizacionales y necesidades del cliente. Cada activo organizacional incluyendo las personas, procesos y productos deben soportar la estrategia.
    </p>
  </body>
</html></richcontent>
<node TEXT="Activo estrategico" ID="ID_47291433" CREATED="1657996361799" MODIFIED="1657996389478" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estrategia del servicio ITIL provee una guia de como ver la gestion del servicio no solo como una capacidad organizacional sino como un activo estrategico. Esto describe los principios que apuntalan la practica de la gestion de servicio la cual es utili para desarrollar las politicas de gestion del servicio, guias y procesos a traves del ciclo de vida del servicio ITIL. La gestion de la relacion con el negocio, la gestion de la demanda, la gestion financiera, el desarrollo organizaciona y los riesgos estrategicos tambien forman parte entre otros topicos.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Definir objetivos y expectativas" ID="ID_419126155" CREATED="1657996398164" MODIFIED="1657996697525" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las organizaciones deben usar la estrategia de servicio ITIL para definir objetivos y expectativas de rendimiento a traves de servir a los clientes y espacios de mercados, e identificar, seleccionar y priorizar oportunidades. La estrategia de servicio es asegurar que las organizaciones estan en una posicion para manejar los costos y riesgos asociados con sus portafolios de servicio, y estan listas no solo para la efectividad operacional sino para un rendimiento distinto.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Organizaciones que ya estan usando ITIL" ID="ID_1782160533" CREATED="1657996728276" MODIFIED="1657997009975" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las organizaciones que ya estan usando ITIL pueden usar la estrategia de servicio ITIL para guiar una revision estrategica de sus capacidades de gestion de servicio basadas en ITIL y mejorar la alineacion entre estas capacidades y las estrategias de su negocio. La estrategia del servicio ITIL alienta a los lectores a parar y pensar algo que tiene que ser hecho primero de ser pensado como.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="1.2.2 Diseno del servicio" FOLDED="true" ID="ID_1379368976" CREATED="1657980917191" MODIFIED="1658149790481" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Para que los servicios proveean valor al negocio, estos deben ser disenados con los objetivos del negocio en mente. El diseno abarca a toda la organizacion que entrega y soporta los servicios. El diseno del servicio es la fase en el ciclo de vida que convierte la estrategia del servicio en un plan para la entregar los objetivos del negocio.
    </p>
  </body>
</html></richcontent>
<node TEXT="Guia para el diseno" ID="ID_122480341" CREATED="1658149801745" MODIFIED="1658150097981" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El diseno del servicio ITIL provee la guia para el diseno y desarrollo de servicios y practicas de gestion de servicio. Cubre los principios de diseno y metodos para convertir objetivos estrategicos en portafolios de servicios y activos de servicio. El alcance del diseno del servicio ITIL no se limita a solo nuevos servicios. Incluye los cambios y mejoras necesarias para incrementar o mantener el valor a los clientes sobre el ciclo de vida de los servicios, la continuidad de los servicios, cumplimiento de los niveles de servicio, y cumplimiento de estandares y regulaciones. Guia a las organizaciones en como desarrollar habilidades de diseno para la gestion de servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Otros topicos" ID="ID_1761150091" CREATED="1658150099380" MODIFIED="1658150216726" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El servicio de diseno ITIL incluye:
    </p>
    <ul>
      <li>
        Coordinacion del diseno
      </li>
      <li>
        Gestion del catalogo de servicio
      </li>
      <li>
        Gestion de la disponibilidad
      </li>
      <li>
        Gestion de la capacidad
      </li>
      <li>
        Gestionn de la continuidad del servicio IT
      </li>
      <li>
        Gestion de la seguridad de la informacion
      </li>
      <li>
        Gestion de proveedores
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="1.2.3 Transicion del servicio" FOLDED="true" ID="ID_56979081" CREATED="1657980934500" MODIFIED="1658155092076" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La transicion del servicio ITIL provee una guia para el desarrollo y mejora de las capacidades para introducir nuevos servicios y cambios a los ya existentes hacia los ambientes soportados. Describe como trancisionar una organizacion de un estado a otro mientras se controla el riesgo y se soporta el conocimiento organizacional para el soporte de las decisiones. Esto asegura que el valor o valores identificados en la estrategia del servicio, y codificado en el diseno del servicio, sean efectivamente transicionados para que puedan ser ejecutadas en la operacion del servicio.
    </p>
  </body>
</html></richcontent>
<node TEXT="Best practice" ID="ID_1427927918" CREATED="1658155106618" MODIFIED="1658155834024" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La transicion del servicio ITIL describe las buenas practicas en planeacion y soporte de transicion, gestion de cambios, gestion de la configuracion y activos del servicio, gestion del despliegue y liberacion, validacion del servicio y pruebas, gestion del conocimiento y evaluacion del cambio. Provee la guia en la gestion de la complejidad relacionada a cambios a los servicios y procesos de gestion de servicio, previniendo las consecuencias indeseadas mientras se permite la innovacion.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Sistema de Gestion del Conocimiento" ID="ID_616983957" CREATED="1658155837767" MODIFIED="1658156066694" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La Trancision del servicio tambien introduce el sistema de gestion del conocimiento, el cual puede soportar el aprendizaje organizacional y ayudar a mejorar la eficiencia y efectividad en general de todas las fases del ciclo de vida del servicio. Esto permitira a las personas beneficiarse del conocimiento y experiencia de otros, toma decisiones soportadas en la informacion, y mejorar la gestion de los servicios.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="1.2.4 Operacion del servicio" FOLDED="true" ID="ID_934348594" CREATED="1657980942944" MODIFIED="1658156196502" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La Operacion del servicio describe las buenas practicas para la gestion de servicios en ambientes soportados. Incluye la guia para alcanzar la efectividad y eficiencia en la entrega y soporte de servicios para asegurar el valor al cliente, los usuarios y el proveedor de servicio.
    </p>
  </body>
</html></richcontent>
<node TEXT="Mantener la estabilidad" ID="ID_1256109754" CREATED="1658156200738" MODIFIED="1658156679951" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los objetivos estrategicos son por ultimo ejecutados por la operacion del servicio, por tanto la hace una capacidad critica. La operacion del servicio ITIL provee una guia de como mantener la estabilidad en la operacion del servicio, permitiendo cambios en el diseno, escala, alcance y niveles de servicio. Las organizaciones estan proveidas con las guias de proceso detalladas, metodos y herramientas para ser usadas en dos perspectivas de control mayores: Reactiva y Proactiva.
    </p>
    <p>
      
    </p>
    <p>
      Los gerentes y trabajadores estan proveidos con el conocimiento que les permite hacer mejores decisiones en areas tales como la gestion de la disponibilidad de servicios, control de la demanda, optimizar la utilizacion de la capacidad, calendarizacion de las operaciones, y evitar y resolver los incidentes de servicio y la gestion de problemas.
    </p>
    <p>
      
    </p>
    <p>
      El soporte de la operacion de servicio de nuevos modelos y arquitecturas como servicios compartidos, utilidad computacional, servicios web y comercio movil, tambien son descritos en este contexto.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Otros topicos" ID="ID_1705988238" CREATED="1658156683166" MODIFIED="1658156850999" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Otros topicos en la operacion de servicio ITIL incluye los procesos de:
    </p>
    <ul>
      <li>
        Gestion de eventos,
      </li>
      <li>
        Gestion de incidentes,
      </li>
      <li>
        Gestion de requerimientos,
      </li>
      <li>
        Gestion de problemas y
      </li>
      <li>
        Gestion de accesos
      </li>
    </ul>
    <p>
      Asi como las funciones de:
    </p>
    <ul>
      <li>
        Mesa de servicio,
      </li>
      <li>
        Gestion tecnica,
      </li>
      <li>
        Gestion de operaciones IT,
      </li>
      <li>
        Gestion de aplicaciones.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="1.2.5 Mejora continua del servicio" FOLDED="true" ID="ID_1167462032" CREATED="1657980959231" MODIFIED="1658156961513" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Ls mejora continua del servicio ITIL provee una guia en la creacion y mantenimiento del valor para los clientes a traves de una mejor estrategia, diseno, trancision y operacion de servicios. Esta combina los principios, practicas y metodos desde la gestion de la calidad, gestion de cambios y mejora de la capacidad.
    </p>
  </body>
</html></richcontent>
<node TEXT="Mejora incremental y a gran escala" ID="ID_1965205461" CREATED="1658156964933" MODIFIED="1658157283349" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La mejora continua del servicio ITIL describe la mejor practica para alcanzar las mejoras incrementales y a gran escala en la calidad del servicio, eficiencia operacional, y continuidad del negocio, y para asegurar que el portafolio de servicios continua alineado a las necesidades del negocio. Se guia para el enlace entre los esfuerzos de mejora y las salidas con la estrategia del servicio, diseno, trancision y operacion. Se establece un sistema de lazo de retroalimentacion cerrado, basado en el ciclo PDCA (Planificar-Hacer-Evaluar-Actuar). La retroalimentacion desde cualquier fase del ciclo de vida del servicio puede ser usada para identificar las oportunidades de mejora para cualquier fase del ciclo de vida del servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Otros topicos" ID="ID_1733651090" CREATED="1658157274085" MODIFIED="1658157362853" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Otros topicos de la mejora continua del servicio ITIL incluye los procesos de:
    </p>
    <ul>
      <li>
        Medicion del servicio,
      </li>
      <li>
        Demostracion del valor con metricas,
      </li>
      <li>
        Desarrollo de lineas base,
      </li>
      <li>
        Madurez de los activos.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="1.3 ITIL en relacion a otras publicaciones" FOLDED="true" ID="ID_1627341171" CREATED="1657980968717" MODIFIED="1658157706342" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      ITIL es parte de un portafolio de publicaciones de buenas practicas (conocido como buenas practicas de gestion o BMP:Best Management Practice) dedicadas a ayudar a las organizaciones e individuos a gestionar proyectos, programas y servicios de forma consistente y efectiva.
    </p>
    <p>
      
    </p>
    <p>
      ITIL puede ser usado en armonia con otros productos BMP, y estandares internaciones o internos a la organizacion. Donde sea lo apropiado, la guia BMP esta soportada por el esquema de calificacion y entrenamiento acreditado y servicios de consultoria. Todas las guias BMP estan adaptadas para el uso de organizaciones individuales.
    </p>
  </body>
</html></richcontent>
<node TEXT="Relacion de ITIL con BMP" FOLDED="true" ID="ID_646645107" CREATED="1658157710248" MODIFIED="1658157718055">
<node TEXT="" ID="ID_1931473483" CREATED="1658157752885" MODIFIED="1658157763619">
<hook URI="img/Relacion%20ITIL%20con%20BMP.png" SIZE="0.7802341" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Publicaciones BMP" FOLDED="true" ID="ID_1022829526" CREATED="1658157799873" MODIFIED="1658157811789">
<node TEXT="Gestion de Portafolios (MoP)" ID="ID_207428622" CREATED="1658157814843" MODIFIED="1658158141860" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La gestion del portafolio se dedica a los problemas a la par de como realizar los proyectos y programas &quot;correctos&quot; en el contexto de los objetivos estrategicos de la organizacion, y como hacerlos &quot;correctamente&quot; en terminos de alcanzar la entrega y beneficios a un nivel colectivo.
    </p>
    <p>
      
    </p>
    <p>
      MoP engloba la consideracion de los principios sobre el cual se basa la efectiva gestion del portafolio; las practicas clave en la definicion del portafolio y los ciclos de entrega, incluyendo ejemplos de como estan siendo aplicados a la vida real; y una guia de como implementar la gestion del portafolio y el progreso sustentable en una amplia variedad de organizaciones.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Gestion del Riesgo (M_o_R" ID="ID_1456538459" CREATED="1658158144021" MODIFIED="1658158325934" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      M_o_R ofrece un marco de trabajo efectivo para tomar decisiones basadas en la informacion acerca de riesgos que afectan los objetivos de rendimiento. El marco permite a las organizaciones analizar riesgos minuciosamente (seleccionando las respuestas correctas a amenazas y oportunidades creadas por la incertidumbre) y por tanto mejora su entrega de servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Gestion del Valor (MoV)" ID="ID_1228455195" CREATED="1658158327154" MODIFIED="1658158670875" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      MoV provee una guia universal y multi sector aplicable en como maximizar el valor en una via que toma en cuenta las prioridades de la organizacion, diferenciando las necesidades de los interesados, y al mismo tiempo, usa los recursos de la manera mas eficiente y efectiva posible.
    </p>
    <p>
      
    </p>
    <p>
      Ayuda a las organizaciones a poner su lugar metodos efectivos para entregar valor mejorado por medio de su portafolio, programas, proyectos y actividades operacionales para cumplir con los retos de los ambientes cada ves mas competitivos y recursos restringidos.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Gestion exitosa de Programas (MSP)" ID="ID_116133316" CREATED="1658158346856" MODIFIED="1658158825167" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      MSP Provee un marco de trabajo que permite alcanzar salidas de alta calidad en los cambios y beneficios que fundalmente afectan el modo que las organizaciones trabajan. Uno de los temas principales de MSP es que un programa debe anadir mas valor que el proveido por la suma de sus proyectos y mayores actividades que lo constituyen.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Gestion exitosa de Proyectos con PRINCE2" ID="ID_1946385212" CREATED="1658158365507" MODIFIED="1658159001291" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      PRINCE2 (Proyectos en ambientes controlados V2) es una metodologia estructurada para ayudar en la efectiva gestion de proyectos via productos claramente definidos. Lo temas clave que se caracterizan en PRINCE2 son la dependencia en un caso de negocio viable confirmando la entrega de beneficios cuantificables que estan alineados a los objetivos y estrategia del negocio, mientras se asegura la gestion de riesgos, costos y calidad.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Oficinas de Portafolio, Programas y Proyectos (P3O)" ID="ID_1076592377" CREATED="1658158391960" MODIFIED="1658159148830" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      P2O provee una guia aplicable de forma universal, que incluye principios, procesos y tecnicas, para establecer satisfactoriamente, desarrollar y mantener las estructuras de soporte apropiadas. Estas estructuras facilitaran la entrega de objetivos de negocio (portafolios), programas y proyectos dentro del tiempo, costo, calidad y otras restricciones organizacionales.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="1.4 Porque ITIL es tan exitoso" FOLDED="true" ID="ID_1455701433" CREATED="1657981049865" MODIFIED="1658159828309" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      ITIL abarca un enfoque practico a la gestion de servicio: &quot;Hacer lo que funciona&quot;. Y lo que funciona es adaptar un marco de trabajo comun de practicas que unifica todas las areas de la provision de servicios IT a traves de un unico objetivo: &quot;Entregar valor al negocio&quot;.
    </p>
    <p>
      
    </p>
    <p>
      La siguiente lista define las caracteristicas clave de ITIL que contribuyen a su exito global:
    </p>
  </body>
</html></richcontent>
<node TEXT="Caracteristicas clave de ITIL" FOLDED="true" ID="ID_472885313" CREATED="1658159377733" MODIFIED="1658159393663">
<node TEXT="Proveedor neutral" ID="ID_808523185" CREATED="1658159402441" MODIFIED="1658159595198" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las practicas de gestion de servicio ITIL son aplicables a cualquier organizacion IT ya que estas no estan basadas en ninguna plataforma de tecnologia en particular o tipo de industria. ITIL es propiedad del gobierno de Inglaterra UK, y no esta atada a ninguna practica o solucion de propiedad comercial.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="No prescriptivo" ID="ID_238565362" CREATED="1658159416595" MODIFIED="1658159760898" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      ITIL ofrece practicas robustas, maduras y probadas en el tiempo que tienen aplicabilidad a todos los tipos de organizaciones de servicios. Esta continua siendo util y relevante en el sector publico y privado, proveedores de servicio internos y externos, empresas pequenas, medianas y grandes, y dentro de cualquier ambiente tecnico. Las organizaciones deben adoptar ITIL y adaptarse a esta para cumplir las necesidades de la organizacion ITIL y sus clientes.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Buena Practica" ID="ID_774904070" CREATED="1658159422755" MODIFIED="1658159817785" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      ITIL representa el aprendizaje de experiencias y liderazgo de pensamiento de los proveedores de servicio mejores en su clase.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Entregar beneficios" ID="ID_717550371" CREATED="1658159849431" MODIFIED="1658160200170" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      ITIL es existoso porque describe practicas que permiten a las organizaciones entregar beneficios, retorno de la inversion y exito sustentable. ITIL es adoptado por las organizaciones para:
    </p>
    <ul>
      <li>
        Entregar valor a sus clientes a traves de servicios
      </li>
      <li>
        Integrar la estrategia para servicios con la estrategia de negocio y las necesidades del cliente.
      </li>
      <li>
        Medir, monitorear y optimizar los servicios IT y el rendimiento del proveedor de servicios.
      </li>
      <li>
        Gestionar la inversion IT y el presupuesto
      </li>
      <li>
        Gestionar Riesgos
      </li>
      <li>
        Gestionar Conocimiento
      </li>
      <li>
        Gestionar las capacidades y recursos para entregar servicios efectivamente y eficientemente.
      </li>
      <li>
        Permitir la adopcion de un enfoque estandar para la gestion de servicio a lo largo de la empresa.
      </li>
      <li>
        Cambiar la cultura organizacional para soportar el alcance de un exito sustentable.
      </li>
      <li>
        Mejorar la interaccion y relacion con los clientes.
      </li>
      <li>
        Coordinar la entrega de bienes y servicios a lo largo de la red de valor.
      </li>
      <li>
        Optimizar y reducir costos.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="1.5 Resumen de capitulos" FOLDED="true" ID="ID_776255532" CREATED="1657981075388" MODIFIED="1657981090886">
<node TEXT="Capitulo 2: Gestion del servicio como una practica" ID="ID_1241525181" CREATED="1658160257876" MODIFIED="1658170778993" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Este capitulo explica los conceptos de gestion de servicio y los servicios, y describe como estos pueden ser utilizados para crear valor. Este tambien resume un numero de conceptos genericos de ITIL que el resto de la publicacion depende.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Capitulo 3: Principios de la estrategia del servicio" ID="ID_816410395" CREATED="1658160278401" MODIFIED="1658171493812" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Este capitulo describe la terminologia y principios clave los cuales forman parte de los bloques de construccion de las mejores practicas de la estrategia del servicio. Estos principios son las politicas y los aspectos de la gobernanza de la fase de la estrategia del servicio del ciclo de vida que ancla los procesos tacticos y actividades para alcanzar sus objetivos. Este concluye con una tabla que muestra las mayores entradas y salidas para la fase de estrategia del servicio del ciclo de vida.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Capitulo 4: Procesos de la estrategia del servicio" ID="ID_628362166" CREATED="1658160296137" MODIFIED="1658171554090" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Implementa los procesos y actividades en los cuales la efectiva estrategia del servicio depende y como estas se integran con otras fases del ciclo de vida.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Capitulo 5: Estrategia del servicio, governanza, arquitectura y estrategias de implementacion ITSM" ID="ID_650887346" CREATED="1658160313221" MODIFIED="1658171752291" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Trata sobre algunas de las mayores intrefaces estrategicas entre la estrategia del servicio, el contexto del negocio y la gestion del servicio. Especificamente, muestra la interfaz entre la estrategia organizacional en general, la estrategia del servicio del proveedor de servicio, y la estrategia del servicio IT del proveedor de servicio. Este tambien considera la relacion entre la gestion del servicio, la arquitectura empresarial y el desarrollo de aplicaciones.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Capitulo 6: Organizacion para la estrategia del servicio" ID="ID_507751439" CREATED="1658160348914" MODIFIED="1658171872377" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Identifica los roles organizacionales y responsabilidades que son necesarias para gestionar la fase de estrategia del servicio en el ciclo de vida y los procesos. Estos roles son proveidos como lineas guia y pueden ser combinadas para ajustarse a una variedad de estructuras organizacionales. Se provee ejemplos de estructuras organizacionales tambien.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Capitulo 7: Consideraciones de la tecnologia" ID="ID_1533085435" CREATED="1658160367062" MODIFIED="1658172010333" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las practicas de gestion de servicio ITIL ganan momentum cuando se aplica el tipo correcto de automatizacion tecnica. Este capitulo provee recomendaciones para el uso de la tecnologia en la estrategia del servicio y los requerimientos basicos que un proveedor de servicio necesita considerar cuando elija las herramientas de gestion de servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Capitulo 8: Implementando la estrategia del servicio" ID="ID_1426667769" CREATED="1658160384277" MODIFIED="1658172112873" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Para organizaciones nuevas a ITIL, o para aquellos que desean mejorar su madurez y capacidad de servicio, este capitulo describe las vias efectivas para implementar la fase de estrategia del servicio en el ciclo de vida.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Capitulo 9: Retos, riesgos y factores criticos de exito" ID="ID_183562652" CREATED="1658160397421" MODIFIED="1658172214622" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Es importante para cualquier organizacion entender los retos, riesgos y factores criticos de exito que podrian influenciar su exito. Este capitulo discute los ejemplos tipicos para la fase de estrategia del servicio del ciclo de vida.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Apendice A: Valor presente de una anualidad" ID="ID_245009811" CREATED="1658160416695" MODIFIED="1658160429949"/>
<node TEXT="Apendice B: Description de los tipos de activos" ID="ID_1200645243" CREATED="1658160430563" MODIFIED="1658172290001" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Este apendice describe los tipos de activos clave de la gestion, organizacion, procesos, conocimiento, personas, informacion, aplicaciones, infraestructura y capital financiero.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Apendice C: Estrategia de servicio y la nube" ID="ID_1898982296" CREATED="1658160444694" MODIFIED="1658172357436" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Aqui se incluye un breve resumen de los temas mayores y asuntos relacionados al area compleja de los servicios en la nube.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Apendice D: Guia relacionada" ID="ID_247784069" CREATED="1658160460672" MODIFIED="1658172450826" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Este contiene una lista de algunos de muchos metodos externos, practicas y marcos de trabajo que se alinean bien con las buenas practicas de ITIL. Las notas son proveidas sobre como estas se integran en el ciclo de vida de servicio ITIL, y cuando y como estas son utililes.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Apendice E: Analisis y Gestion de Riesgos" ID="ID_1647037089" CREATED="1658160477588" MODIFIED="1658172555849" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Este apendice contiene informacion basica acerca de los bastantes enfoques comunmente utilizados para el analisis y gestion de riesgo.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Apendice F: Ejemplos de entradas y salidas a lo largo del ciclo de vida del servicio" ID="ID_767382538" CREATED="1658160503188" MODIFIED="1658172557572" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Este apendice identifica algunas de las mayores entradas y salidas entre cada fase del cliclo de vida del servicio.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="2. Gestion del servicio como una practica" FOLDED="true" ID="ID_1596914969" CREATED="1657827690226" MODIFIED="1657847883819">
<font SIZE="10"/>
<node TEXT="2.1 Servicios y Gestion de Servicios" FOLDED="true" ID="ID_1982800121" CREATED="1657827737187" MODIFIED="1657847894956">
<font SIZE="10"/>
<node TEXT="2.1.1 Servicios" ID="ID_1740205586" CREATED="1657832976085" MODIFIED="1658178285334">
<node TEXT="Definicion" ID="ID_283571850" CREATED="1657827783168" MODIFIED="1658178256963" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <font size="2">Los servicios son los medios para la entrega de valor a los clientes facilitando resultados que los clientes desean obtener sin la propiedad de costos y riesgos especificos.</font>
    </p>
  </body>
</html></richcontent>
<font SIZE="10"/>
</node>
<node TEXT="Outcome (Resultado o Salida)" ID="ID_574181822" CREATED="1657830616381" MODIFIED="1658178263022" TEXT_SHORTENED="true">
<font SIZE="10"/>
<richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El resultado de llevar a cabo una actividad, siguiendo un proceso, o entregando un servicio IT, etc. El termino es usado para referirse a resultado esperados asi como a resultados obtenidos.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Servicio IT" ID="ID_851473507" CREATED="1657831457014" MODIFIED="1658178240143" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un servicio proveido por proveedor de servicios IT.
    </p>
    <p>
      
    </p>
    <p>
      Un servicio IT es una combinacion de tecnologias de la informacion, personas y procesos.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Clasificacion de Servicio segun su relacion con el cliente" ID="ID_1759042357" CREATED="1657832042196" MODIFIED="1658497710694" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        <b>Servicios Core</b>: Entrega los resultados basicos esperados por el usuario. Representa el valor que el cliente espera y por el cual esta pagando. Los servicios de Core reunen la proposicion de valor para el cliente y provee la base para su utilizacion y satisfaccion continua.
      </li>
      <li>
        <b>Servicios Habilitadores</b>: Son aquellos servicios necesarios para que un servicio core sea entregado. Podrian o no podrian ser visibles al cliente, pero el cliente no los percibe como un servicio como tal. Son &quot;factores basicos&quot; los cuales habilitan al usuario para que reciba el servicio &quot;real&quot; core.
      </li>
      <li>
        <b>Servicios Mejorados</b>: Son aquellos servicios anadidos al servicio core para hacerlo mas atractivo o tentativo al cliente. Los servicios mejorados no son esenciales para la entrega de un servicio core, y son anadidos como un factor de atraccion, el cual provoca que el cliente use mas el servicio core (o lo elija sobre otros competidores).
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Paquete de servicio" ID="ID_531714413" CREATED="1657832628384" MODIFIED="1658178245681" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un paquete de servicio es una colecion de dos o mas servicios que han sido combinados para ofrecer una solucion a un tipo especifico de necesidad del cliente o para apuntar a un resultado especifico del negocio.
    </p>
    <p>
      
    </p>
    <p>
      Un paquete de servicios puede consistir de una combinacion de servicios core, servicios habilitadores y servicios mejorados.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Paquetes de niveles de servicio" ID="ID_372208838" CREATED="1657832860126" MODIFIED="1658178248218" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Cuando un paquete de servicio necesita ser diferenciado por tipo de cliente, uno o mas componentes pude ser cambiado u ofrecido a diferentes niveles de utilidad y garantia, para crear opciones de servicio.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="2.1.2 Gestion de Servicio" FOLDED="true" ID="ID_815542099" CREATED="1657833019508" MODIFIED="1658178289542">
<node TEXT="Definicion" ID="ID_749838354" CREATED="1657833178472" MODIFIED="1658178297218" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La gestion de servicios es un conjunto de capacidades organizacionales especializadas para proveer valor a los clientes en la forma de servicios.
    </p>
    <p>
      
    </p>
    <p>
      Mientras mas maduras son las capacidades del proveedor de servicios, mayor el su habilidad para producir servicios de calidad consistentes que cumplan las necesidades del cliente de una manera oportuna y a un costo-beneficio efectivo.
    </p>
    <p>
      
    </p>
    <p>
      El acto de transformar las capacidades y recursos en servicios valiosos es el centro de la gestion de servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Proveedor de servicio" ID="ID_1692581905" CREATED="1657833419387" MODIFIED="1658178301424" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una organizacion que provee servicios a uno o mas clientes internos o externos.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="2.1.3 Gestion de Servicio IT (ITSM)" FOLDED="true" ID="ID_807063608" CREATED="1657845710629" MODIFIED="1657847915313">
<node TEXT="Definicion" ID="ID_1201675321" CREATED="1657836192372" MODIFIED="1657845778425" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La implementacion y gestion de servicios de IT de calidad que proporciona las necesidades del negocio. ITSM es realizado por los proveedores de servicios IT a traves de un conjunto de personas, procesos y tecnologia de la informacion.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Provedor de servicio IT" ID="ID_1571783201" CREATED="1657845732217" MODIFIED="1657845779410" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un proveedor de servicio que provee servicios IT a clientes internos o externos.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Factores de servicio IT" ID="ID_1628585939" CREATED="1657846655515" MODIFIED="1657846781012" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una buena relacion entre un proveedor de servicio IT y sus clientes depende en que el cliente este recibiendo un servicio IT que cubre sus necesidades, a un aceptable nivel de servicio a un costo que el cliente puede permitirse.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="2.1.4 Proveedores de Servicio" FOLDED="true" ID="ID_1144534678" CREATED="1657846793139" MODIFIED="1657847924345">
<node TEXT="Tipos de proveedores de servicio" ID="ID_931550562" CREATED="1657846812653" MODIFIED="1657847063650"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        <b>Tipo I - Proveedor de servicio Interno</b>: Un proveedor de servicio interno que esta dentro de una unidad de negocio. Puede haber varios proveedores Tipo I dentro de una organizacion.
      </li>
      <li>
        <b>Tipo II - Unidad de Servicios Compartida</b>: Un proveedor de servicio interno que provee servicios IT compartido entre mas de una unidad de negocio.
      </li>
      <li>
        <b>Tipo III - Proveedor de servicios externo</b>: Un proveedor de servicio que provee servicios IT a clientes externos.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="2.1.5 Interesados en la gestion de servicio" FOLDED="true" ID="ID_1188308480" CREATED="1657847163106" MODIFIED="1657847931708">
<node TEXT="Interesados Internos" ID="ID_535996660" CREATED="1657847251007" MODIFIED="1657847292991" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Funciones
      </li>
      <li>
        Grupos
      </li>
      <li>
        Equipos que entregan servicios
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Interesados Externos" ID="ID_1907858454" CREATED="1657847294718" MODIFIED="1657847599345" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        <b>Clientes</b>: Aquellos que compran los bienes o servicios. El cliente de un proveedor de servicios IT es la persona o grupo que define y acuerda los niveles de servicio objetivo.
      </li>
      <li>
        <b>Usuarios</b>: Aquellos quienes usan el servicio dia a dia. Los usuarios son diferentes que clientes, ya que los clientes no usan los servicios directamente.
      </li>
      <li>
        <b>Proveedores</b>: Terceras partes son responsables de proveer bienes y servicios que son requeridos para entregar los servicios IT. Ejemplo: hardware, software, redes, proveedores de telecomunicaciones, y organizaciones outsourcings.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Tipos de Clientes" FOLDED="true" ID="ID_1415354091" CREATED="1657847741213" MODIFIED="1657847744483">
<node TEXT="Clientes Internos" ID="ID_156018864" CREATED="1657847632800" MODIFIED="1657847730525"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Son clientes que trabajan en el mismo negocio que el proveedor de servicio IT, ej: departamento de marketing.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Clientes Externos" ID="ID_1836599263" CREATED="1657847641086" MODIFIED="1657847846998"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Son clientes que trabajan para un negocio diferente que el proveedor de servicio IT. Los clientes externos tipicamente compran servicios a un proveedor de servicios por medio de un contrato o acuerdo de compromiso legal.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="2.1.6 Utilidad y garantia" FOLDED="true" ID="ID_763896233" CREATED="1657847934666" MODIFIED="1657847946956">
<node TEXT="Valor del servicio" ID="ID_789054111" CREATED="1657847959529" MODIFIED="1657848565602" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El valor de un servicio se podria considerar como el nivel al cual dicho servicio cumple lo esperado por el cliente. Usualmente se puede medir por cuanto el cliente esta dispuesto a pagar por el servicio, en lugar de el costo que el proveedor del servicio tiene proveyendolo o algun otro atributo intrinseco del servicio en si.
    </p>
    <p>
      
    </p>
    <p>
      El valor del servicio no esta determinado por el proveedor sino por la persona que lo recibe. Los servicios contribuyen valor a una organizacion solo cuando el valor es percibido como mayor que el costo en obtener el servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Componentes del valor der servicio" FOLDED="true" ID="ID_329254012" CREATED="1657848308174" MODIFIED="1657849525533" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Desde la perspectiva del cliente, el valor consiste en alcanzar los objetivos de negocio. El valor de un servicio es creado combinando dos elementos principales:
    </p>
    <p>
      
    </p>
    <ul>
      <li>
        <b>Utilidad</b>: Se ajusta al proposito.
      </li>
      <li>
        <b>Garantia</b>: Se ajusta al uso.
      </li>
    </ul>
    <p>
      <i>Utilidad es que hace el servicio, la garantia es como el servicio es entregado.</i>
    </p>
  </body>
</html></richcontent>
<node TEXT="" ID="ID_1979073184" CREATED="1657849409015" MODIFIED="1657895512310">
<hook URI="img/Creacion%20de%20valor.png" SIZE="0.74812967" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Utilidad" ID="ID_99532838" CREATED="1657848559630" MODIFIED="1657849152098" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La utilidad es la funcionalidad ofrecida por un producto o servicio para cumplir una necesidad en particular, o en otras palabra &quot;lo que hace el servicio&quot;, &quot;se ajusta al proposito&quot;.
    </p>
    <p>
      
    </p>
    <p>
      La utilidad puede representar cualquier atributo de un servicio que elimina, o reduce el efecto de restricciones en realizar una tarea.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Garantia" ID="ID_170262437" CREATED="1657848765329" MODIFIED="1657849151309" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La garantia es asegurar que un producto o servicio cumplira los requerimientos acordados. Esto puede ser un acuerdo formal como un acuerdo de nivel de servicio o contrato.
    </p>
    <p>
      
    </p>
    <p>
      La garantia se refiere a la habilidad de un servicio para estar disponible cuando se lo necesite, para proveer la capacidad requerida, y proveer la confiabilidad requerida in los terminos de continuidad y seguridad. En resumen la garantia es &quot;Como el servicio es entregado&quot;, &quot;Se ajusta al uso&quot;.
    </p>
    <p>
      
    </p>
    <p>
      Por tanto representa cualquier atributo de un servicio que incrementa el potencial de un negocio para realizar una tarea. Se refiere a cualquier medio para que dicha utilidad este disponible a los usuarios.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Definition of a service" ID="ID_724669476" CREATED="1657851352502" MODIFIED="1657851578655" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La informacion sobre las salidas deseadas del negocio, oportunidades, clientes, utilidad y garantia del servicio son usados para desarrollar la de definicion de un servicio.
    </p>
    <p>
      
    </p>
    <p>
      Usando una definicion basada en la salida ayuda a asegurar que los gerentes planifiquen y ejecuten todos los aspectos de la gestion del servicio desde la perspectiva de que es valioso para el cliente.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="2.1.7 Mejores practicas en el dominio publico" FOLDED="true" ID="ID_200046406" CREATED="1657851583437" MODIFIED="1657851598864">
<node TEXT="" ID="ID_1111224631" CREATED="1657851949882" MODIFIED="1657895530510">
<hook URI="img/Sources%20of%20ITSM%20best%20practices.png" SIZE="0.7884363" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="2.2 Conceptos basicos" ID="ID_1640769578" CREATED="1657852015724" MODIFIED="1657852022917">
<node TEXT="2.2.1 Activos, recursos y capacidades" FOLDED="true" ID="ID_583270109" CREATED="1657852026913" MODIFIED="1657852040606">
<node TEXT="Asset" ID="ID_139246402" CREATED="1657855143593" MODIFIED="1657855538724" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Cualquier recurso o capacidad.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Customer Asset" ID="ID_1122581107" CREATED="1657855148071" MODIFIED="1657855537931" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Cualquier recurso o capacidad usada por un cliente para alcanzar una salida de negocio.
    </p>
    <p>
      
    </p>
    <p>
      Sin activos del cliente, no existe definicion de valor del servicio. El rendimiento de los activos del cliente es por tanto de primera importancia para la gestion de servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Service Asset" ID="ID_916208015" CREATED="1657855152786" MODIFIED="1657855537067" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Cualquier recurso o capacidad usada por un proveedor de servicio para entregar servicios a un cliente.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Tipos de activos" FOLDED="true" ID="ID_1985054720" CREATED="1657855339855" MODIFIED="1657855981353" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        <b>Recursos</b>: Entradas directas para produccion
      </li>
      <li>
        <b>Capacidades</b>: Representa la habilidad de la organizacion para coordinar, controlar e implementar recursos para producir valor. Las capacidades generalmente dependen de la experiencia, amplio conocimiento, informacion que esta dentro de organizacion de la gente, los sistemas, procesos y tecnologias. Adquirir recursos es facil comparado a adquirir capacidades.
      </li>
    </ul>
    <p>
      Las capacidades por si mismas no pueden producir valor sin los recursos adecuados y apropiados. La capacidad productiva de un proveedor de servicio depende en los recursos bajo su control. Las capacidades son usadas para desarrollar, implementar y coordinar la capacidad productiva.
    </p>
    <p>
      
    </p>
    <p>
      Ej: Las capacidades de gestion de la capacidad y gestion de la disponibilidad son usadas para gestionar la actuacion y utilizacion de procesos, aplicaciones e infraestructura, asegurando que los niveles de servicio sean entregados efectivamente.
    </p>
  </body>
</html></richcontent>
<node TEXT="" ID="ID_440773046" CREATED="1657855629437" MODIFIED="1657895545558">
<hook URI="img/Capacidades%20y%20Recursos.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="2.2.2 Procesos" FOLDED="true" ID="ID_1690834596" CREATED="1657855991468" MODIFIED="1657856160121" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un&#160;&#160;proceso es un conjunto estructurado de actividades disenadas para cumplir un objetivo especifico. Un procesos toma una o mas entradas definidas y las convierte en salidas definidas.
    </p>
    <p>
      
    </p>
    <p>
      Los Procesos definen acciones, dependencias y sequencia. Los procesos bien definidos pueden mejorar la productividad dentro y a traves de las organizaciones y funciones.
    </p>
  </body>
</html></richcontent>
<node TEXT="Caracteristicas" ID="ID_1619812935" CREATED="1657856161596" MODIFIED="1657856964276" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        <b>Mesurabilidad</b>: Se debe tener la habilidad para medir el proceso de una manera relevante. Es manejada por el rendimiento. Los gerentes quieren medir costos, calidad y otras variables mientras los trabajadores estan preocupados de la duracion y la productividad.
      </li>
      <li>
        <b>Resultados Especificos</b>: La razon que un proceso exista es para entregar un resultado especifico. Este resultado debe ser individualmente identificable y medible.
      </li>
      <li>
        <b>Clientes</b>: Cada proceso entrega sus resutados primarios a un cliente o interesado. Los clientes podrian ser internos o externos a la organizacion, pero el procesos debe cumplir sus espectativas.
      </li>
      <li>
        <b>Responsivo a ciertos disparadores</b>: Mientras un proceso podria estar en curso o iterando, este deberia ser trazable a un disparador especifico.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Objetivos del proceso" ID="ID_1123267649" CREATED="1657856623546" MODIFIED="1657856965956" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un proceso esta organizado alrededor de un conjunto de objetivos. Las principales salidas de un proceso deberia ser manejado por los objetivos y deberia incluir metricas del proceso, reportes y mejoras del proceso.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Salidas de un proceso" ID="ID_22890140" CREATED="1657856719084" MODIFIED="1657856966724" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La salida producida por un proceso debe estar conforme a las normas que se derivan de los objetivos del negocio.
    </p>
    <p>
      
    </p>
    <p>
      Si los productos son conforme al conjunto de normas, el proceso puede ser considerado <b>efectivo</b>&#160;(porque puedes ser repetido, medido y gestionado, y alcanza las salidas requeridas).
    </p>
    <p>
      
    </p>
    <p>
      Si las actividades de los procesos son llevadas a cabo con un minimo de recursos, el proceso puede ser llamado<b>&#160;eficiente</b>.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Entradas de un proceso" ID="ID_1563533573" CREATED="1657856918128" MODIFIED="1657856967578" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las entradas son data o informacion usada por el proceso y podria ser la salida de otro proceso.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Disparadores (Triggers)" ID="ID_1549255440" CREATED="1657856975172" MODIFIED="1657857102358" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un proceso o una actividad dentro de un proceso es iniciada por un disparador. Un disparador podria ser la llegada de una entrada u otro evento. Ej: La falla de un server podria disparar los procesos de gestion de eventos y de gestion de incidentes.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Roles, Responsabilidades, herramientas y controles de gestion" ID="ID_1612170420" CREATED="1657857102983" MODIFIED="1657857278877" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un proceso podria incluir alguno de los roles, responsabilidades, herramientas y controles de gestion requeridos para entregar las salidas confiablemente.
    </p>
    <p>
      
    </p>
    <p>
      Un proceso podria definir politicas, estandares, guias base, actividades e instrucciones de trabajo de ser necesario.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Documentacion" ID="ID_217917372" CREATED="1657857297049" MODIFIED="1657857510810" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los procesos una vez definidos deben ser documentados y controlados. Una vez bajo control, estos pueden ser repetidos y gestionados. La medicion y metricas de un proceso pueden ser construidas en proceso para controlar y mejorar el proceso.
    </p>
    <p>
      
    </p>
    <p>
      El analisis de procesos, resultados y metricas deben ser incorporados en los reportes de gestion regulares y mejoras del proceso.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Modelo de proceso" FOLDED="true" ID="ID_1402940813" CREATED="1657857504391" MODIFIED="1657857508160">
<node TEXT="" ID="ID_79584424" CREATED="1657857514238" MODIFIED="1657857522675">
<hook URI="img/Modelo%20de%20Proceso.png" SIZE="0.83916086" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="2.2.3 Organizacion para la gestion de servicio" FOLDED="true" ID="ID_1524990983" CREATED="1657857541204" MODIFIED="1657857568723">
<node TEXT="2.2.3.1 Funciones" FOLDED="true" ID="ID_162184950" CREATED="1657886121589" MODIFIED="1657886371365" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una funcion es un equipo o grupo de personas y las herramientas u otros recursos que ellos utilizan para realizar una o mas actividades o procesos.
    </p>
    <p>
      
    </p>
    <p>
      En grandes organizaciones una funcion podria estar repartida entre varios departamentos, equipos y grupos, o podria ser una unica unidad organizacional (ej: mesa de servicio). En pequenas organizaciones, una persona o grupo pueden realizar multiples funciones, ej: un departamento de gestion tecnica podria incorporar la funcion de mesa de servicio.
    </p>
  </body>
</html></richcontent>
<node TEXT="Estructuras" ID="ID_1967287332" CREATED="1657886489367" MODIFIED="1657888075050">
<node TEXT="Grupo" ID="ID_1355838674" CREATED="1657886502628" MODIFIED="1657887597999" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un grupo es un cierto numero de personas que son similares de alguna forma. En ITIL, los grupos se refieren a personas quienes realizan actividades similares, aun si trabajan en diferentes tecnologias o reportan a diferentes estructuras organizacionales o companias.
    </p>
    <p>
      
    </p>
    <p>
      Los grupos usualmente no son estructuras organizacionales formales, pero son muy utiles a la hora de definir procesos en comun a traves de la organizacion, ej: asegurar que toda la gente quienes resuelven incidentes completen el guardado del incidente de la misma forma.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Equipo" ID="ID_326693616" CREATED="1657886874853" MODIFIED="1657887597245" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un equipo es un tipo mas formal de grupo. Son personas que trabajan juntos para alcanzar un objetivo en comun, pero no necesariamente en la misma estructura organizacional. Los miembros del equipo pueden estar ubicados en el mismo sitio, o trabajar desde multiples ubicaciones y operar de modo virtual.
    </p>
    <p>
      
    </p>
    <p>
      Los equipos son utiles para la colaboracion, o para tratar con situaciones de naturaleza transicional o temporal. Ej: Equipos de proyecto, equipos de desarrollo de aplicaciones (usualmente gente de diferentes unidades de negocios) y equipos de resolucion de incidentes o problemas.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Departamento" ID="ID_269336366" CREATED="1657886507420" MODIFIED="1657887596381" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Son estructuras organizacionales formales que existen para realizar un conjunto especifico de actividades definidas de forma continua.
    </p>
    <p>
      
    </p>
    <p>
      Los departamentos tienen una estructura de reporte jer&#225;rquica con jefes que son responsables de la ejecucion de las actividades y tambien de la gestion del dia a dia del staff del departamento.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Division" ID="ID_382631863" CREATED="1657886512089" MODIFIED="1657887586733" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Se refiere a un numero de departamentos que han sido agrupados usualmente por geografia o por una linea de producto. Una division es normalmente autonoma.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Funciones" ID="ID_1736725255" CREATED="1657887624910" MODIFIED="1657887627539">
<node TEXT="Service Desk" ID="ID_1107797460" CREATED="1657888013915" MODIFIED="1657889208269" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El punto unico de contacto para los usuarios cuando hay una disrupcion del servicio, para requerimientos de servicio, o algunas categorias de requerimientos para cambio (RFC). La mesa de servicio provee un punto de comunicacion a los usuarios y un punto de coordinacion de muchos grupos y procesos de IT.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Gerencia Tecnica" ID="ID_1089192729" CREATED="1657888019975" MODIFIED="1657889207619" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Provee recursos y habilidades tecnicas detalladas necesarias para soportar la operacion en curso de los servicios IT y la gestion de la infraestructura IT.
    </p>
    <p>
      
    </p>
    <p>
      La gestion tecnica tambien juega un rol importante en el diseno, pruebas, liberacion y mejora de los servicios IT.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Gerencia de Operaciones IT" ID="ID_617645156" CREATED="1657888026731" MODIFIED="1657889206881" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Ejecuta las actividades operacionales del dia a dia necesarias para la gestion de los servicios IT y el soporte de la infraestructura IT. Esto se realiza segun los estandares de eficiencia definidos en la fase de diseno del servicio.
    </p>
    <p>
      
    </p>
    <p>
      La gestion de operaciones IT tiene dos sub-funciones que son generalmente organizacionalmente distintas. Estas son el control de operaciones IT y la gestion de las instalaciones (facilities).
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Gerencia de Aplicaciones" ID="ID_1161493124" CREATED="1657888039144" MODIFIED="1657889206094" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Es responsable de la gestion de las aplicaciones a traves de su ciclo de vida. La funcion de gestion de aplicaciones soporta y mantinene a las aplicaciones operacionales y tambien juega un rol importante en el diseno, pruebas y mejora de las aplicaciones que forman parte de los servicios IT.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="2.2.3.2 Roles" FOLDED="true" ID="ID_412145094" CREATED="1657889324353" MODIFIED="1657889528943" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un rol es un conjunto de responsabilidades, actividades y permisos autorizados a una persona o equipo. Un rol es definido en un proceso o funcion. Una persona o equipo podria tener multiples roles, ej: Los roles de administrador de configuraciones y administrador de cambios podria ser realizado por la misma persona.
    </p>
  </body>
</html></richcontent>
<node TEXT="Realizar una tarea" ID="ID_1938363135" CREATED="1657889644249" MODIFIED="1657889834276"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una persona como parte de su asignacion de trabajo, en realizar una sola tarea podria representar la participacion en mas de un proceso.
    </p>
    <p>
      
    </p>
    <p>
      Ej: Un analista de tecnologia quien envio un RFC para anadir memoria a un servidor para resolver un problema de rendimiento debera participar en actividades del proceso de gestion de cambios y al mismo tiempo tomar parte de actividades de los procesos de gestion de la capacidad y gestion de problemas.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="2.2.3.3 Comportamiento y cultura organizacional" FOLDED="true" ID="ID_1847125192" CREATED="1657889851553" MODIFIED="1657890100112" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La cultura organizacional es el conjunto de valores compartidos y normas que controlan las iteracciones del proveedor de servicio con todos los interesados, incluyendo clientes, usuarios, proveedores, staff interno, etc.
    </p>
    <p>
      
    </p>
    <p>
      Los valores organizacionales don modos deseados de comportamiento que afectan su cultura. Ej: altos estandares, respeto al cliente, respeto por la tradicion y la autoridad, actuar cuidadosamente y de forma conservadora y ser austero.
    </p>
  </body>
</html></richcontent>
<node TEXT="Red de valor" ID="ID_1206729333" CREATED="1657890483913" MODIFIED="1657890585216" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los proveedores de servicio altamente eficientes continuamente se alinean a la red de valor para eficiencia y efectividad. La cultura es transmitida a traves de la red de valor al staff mediante socializacion, programas de entrenamiento, historias, ceremonias y el lenguaje corporativo.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Restricciones" ID="ID_1500134021" CREATED="1657890203198" MODIFIED="1657890586246" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Restricciones como la gobernanza, capacidades, estandares, recursos, valores y etica juegan un rol importante en la cultura y comportamiento de la organizacion.
    </p>
    <p>
      
    </p>
    <p>
      La cultura organizacional tambien puede ser afectada por la estructura o estilos de gestion con un impacto positivo o negativo en la eficiencia.
    </p>
    <p>
      
    </p>
    <p>
      Las estructuras organizacionales y los estilos de gestion contribuyen al comportamiento de la gente, los procesos, la tecnologia y los socios. Estos son aspetos importantes en la adopcion de las practicas de gestion de servicios e ITIL.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Cambios" ID="ID_1950947138" CREATED="1657890587198" MODIFIED="1657890760537" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los cambios relacionados a los programas de gestion de servicio afectaran a la cultura organizacional por lo que es importante preparar planes de comunicacion, entrenamiento, politicas y procedimientos para alcanzar el rendimiento de las salidas esperado.
    </p>
    <p>
      
    </p>
    <p>
      Establecer el cambio cultural es tambien un factor importante para el trabajo colaborativo entre diferentes personas de la gestion del servicio.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="2.2.4 Portafolio de servicios" FOLDED="true" ID="ID_579768257" CREATED="1657890767755" MODIFIED="1657894713756" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El portafolio de servicios es el conjunto completo de servicios que es gestionado por un proveedor de servicio y este representa los compromisos del proveedor de servicios y las inversiones a traves de todos los clientes y espacios de mercado. Tambien representa los compromisos contractuales actuales, nuevos servicios en desarrollo y planes de mejora de servicios en curso iniciados por la mejora continua del servicio.
    </p>
    <p>
      
    </p>
    <p>
      El portafolio podria incluir servicios de terceras partes, los cuales son una parte integral de la oferta de servicio a los clientes.
    </p>
    <p>
      
    </p>
    <p>
      El portafolio de servicios representa todos los recursos actualmente compromentidos o que estan siendo entregados en diferentes fases del ciclo de vida del servicio. Es una base de datos o documento estructurado en tres partes:
    </p>
    <p>
      
    </p>
    <ul>
      <li>
        Tuberia de servicios
      </li>
      <li>
        Catalogo de servicios
      </li>
      <li>
        Servicios retirados
      </li>
    </ul>
  </body>
</html></richcontent>
<node TEXT="Figura" FOLDED="true" ID="ID_1406840469" CREATED="1657895382560" MODIFIED="1657895387164">
<node TEXT="" ID="ID_607793524" CREATED="1657895389659" MODIFIED="1657895396432">
<hook URI="img/Portafolio%20de%20servicios.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Tuberia de servicios" ID="ID_1845122761" CREATED="1657894268146" MODIFIED="1657895107346" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Todos los servicios que estan bajo consideracion o en desarrollo, pero que no estan todavia disponibles a los clientes. Este incluye mayores oportunidades de inversion que tienen que ser atribuibles a la prestacion de servicios, y el valor que sera realizado. El servicio de tuberia provee una vision de negocio de los posibles servicios futuros y es parte del portafolio de servicios que no esta normalmente publicado a los clientes.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Catalogo de servicio" FOLDED="true" ID="ID_1635168643" CREATED="1657894599880" MODIFIED="1657895106826" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Todos los servicios de IT en vivo, incluyendo aquellos disponibles para implementacion. Esta es la unica parte del portafolio de servicios publicada a los clientes, y es usada para soportar las ventas y la entrega de servicios IT. Esto incluye una vista de cara al cliente de los servicios de IT en uso, como se debe usarlos, los procesos de negocio que habilitan, y los niveles y calidad del servicio que el cliente puede esperar para cada servicio.
    </p>
    <p>
      
    </p>
    <p>
      El catalogo de servicio tambien incluye informacion acerca de servicios de soporte requeridos por el proveedor de servicio para entregar los servicios de cara al cliente. Informacion acerca de los servicios pueden solo entrar en el catalogo de servicio despues de que una debida diligencia ha sido realizada en los costos y riesgos relacionados.
    </p>
  </body>
</html></richcontent>
<node TEXT="Servicios de cara al cliente" ID="ID_319832003" CREATED="1657895127674" MODIFIED="1657895306663" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los servicios de IT que son visibles al cliente. Estos son servicios normales que soportan los procesos de negocio del cliente y facilitan una o mas salidas deseadas del cliente.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Servicios de soporte" ID="ID_1752607074" CREATED="1657895137907" MODIFIED="1657895303740" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Son los servicios IT que soportan o &quot;apuntalan&quot; los servicios de cara al cliente. Estos son tipicamente invisibles al cliente, pero son esenciales para la entrega de los servicios IT de cara al cliente.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Servicios retirados" ID="ID_568855083" CREATED="1657895038170" MODIFIED="1657895106173" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Todos los servicios que estan en fase de salida o ya han sido retirados. Los servicios retirados no estan disponibles a nuevos clientes o contratos a menos que exista un caso de negocio especial.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="2.2.5 Gestion del conocimiento y la SKMS" FOLDED="true" ID="ID_17010710" CREATED="1657895582582" MODIFIED="1657896150758" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Conocimiento e informacion de calidad permite a las personas realizar actividades de los procesos y soportar el flujo de informacion entre las faces del ciclo de vida del servicio y los procesos.
    </p>
    <p>
      
    </p>
    <p>
      El entendimiento, definicion, establecimiento y mantenimiento de la informacion es responsabilidad del proceso de gestion del conocimiento.
    </p>
  </body>
</html></richcontent>
<node TEXT="Implementacion" ID="ID_422984328" CREATED="1657896138467" MODIFIED="1657897192032" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La implementacion de un <b>sistema de gestion del conocimiento del servicio (SKMS)</b>&#160;permite el soporte de decisiones efectivas y reduce los riesgos que aparecen por la falta de los mecanismos apropiados.
    </p>
    <p>
      
    </p>
    <p>
      Las herramientas podrian variar para la implementacion, pero los datos, la informacion y el conocimiento deben ser estar interrelacionados a lo largo de la organizacion. Un sistema de gestion documental y/o un sistema de gestion de la configuracion (CMS) puede ser utilizado para empezar con la implementacion de la SKMS.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Arquitectura" FOLDED="true" ID="ID_876197227" CREATED="1657896258602" MODIFIED="1657896272385">
<node TEXT="Figura" FOLDED="true" ID="ID_635953934" CREATED="1657896156937" MODIFIED="1657896314736">
<node TEXT="" ID="ID_892877702" CREATED="1657896206596" MODIFIED="1657896214923">
<hook URI="img/Arquitectura%20SKMS.png" SIZE="0.7751938" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Capa presentacion" ID="ID_1165246813" CREATED="1657896274404" MODIFIED="1657896667320" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Permite la busqueda, navegacion, descarga, actualizacion, subscripcion y colaboracion. Las diferentes vistas sobre otras capas son apropiadas para diferentes audiencias. Cada vista debe ser protegida para asegurar que solo las personas autorizadas pueden ver o modificar el conocimiento subyacente, informacion o data.
    </p>
  </body>
</html></richcontent>
<font BOLD="true"/>
</node>
<node TEXT="Capa procesamiento del conocimiento" ID="ID_156841036" CREATED="1657896282741" MODIFIED="1657896666553" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Es donde la informacion es convertida en conocimiento util lo cual permite la toma de decisiones.
    </p>
  </body>
</html></richcontent>
<font BOLD="true"/>
</node>
<node TEXT="Capa integracion de la informacion" ID="ID_1365194786" CREATED="1657896294553" MODIFIED="1657896665967" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Provee la informacion de forma integrada que puede ser obtenida desde data de multiples fuentes de la capa de datos.
    </p>
  </body>
</html></richcontent>
<font BOLD="true"/>
</node>
<node TEXT="Capa de datos" ID="ID_556403598" CREATED="1657896301995" MODIFIED="1657896665263" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Incluye las herramientas para descubrimiento y recoleccion de datos, e item de datos de tipo estructurada y no estructurada.
    </p>
  </body>
</html></richcontent>
<font BOLD="true"/>
</node>
</node>
<node TEXT="Componentes" ID="ID_570694581" CREATED="1657896859508" MODIFIED="1657897225341" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Esta arquitectura es aplicable a muchos de los sistemas de gestion de la informacion en ITIL. Un componente primario de la SKMS es el portafolio de servicios, otros ejemplos incluye el <b>sistema de gestion de la configuracion (CMS)</b>, el <b>sistema de informacion de gestion de la disponibilidad (AMIS)</b>, y el <b>sistema de informacion de la gestion de la capacidad (CMIS)</b>.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="2.3 Gobernanza y Sistemas de Gestion" FOLDED="true" ID="ID_435549725" CREATED="1657897235960" MODIFIED="1657897317956">
<node TEXT="2.3.1 Gobernanza" FOLDED="true" ID="ID_1480629000" CREATED="1657897273490" MODIFIED="1657898765527" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Gobernanza es el area global unica que une a IT con el negocio, y los servicios son una via de asegurar que la organizacion pueda ejecutar esa gobernanza. La Gobernanza es lo que define las direcciones comunes, politicas y reglas que tanto el negocio como al IT usan para conducir el negocio.
    </p>
    <p>
      
    </p>
    <p>
      Muchas estrategias de ITSM fallan debido a que tratan de construir una estructura o procesos de acuerdo a como les gustaria que sea la organizacion en lugar de trabajar dentro de las estructuras de gobernanza existentes.
    </p>
  </body>
</html></richcontent>
<node TEXT="Definicion" ID="ID_1588197586" CREATED="1657898441146" MODIFIED="1657898763283" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La gobernanza asegura que las politicas y estrategia esta siendo implementada, y que los procesos requeridos estan siendo realizados correctamente. La Gobernanza incluye la definicion de roles y responsabilidades, midiendo y reportanto, y tomando acciones para resolver cualquier problema identificado.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Alcance" ID="ID_472665664" CREATED="1657898543050" MODIFIED="1657898762439" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La Gobernanza aplica un enfoque gestionado de forma consistente a todos los niveles de la organizacion - primero asegurando que la estrategia implementada es clara, luego definiendo las politicas por las cuales la estrategia sera alcanzada. Las politicas tambien definen los limites, o que es lo que no deberia ser parte de las operaciones de la organizacion.
    </p>
    <p>
      
    </p>
    <p>
      La Gobernanza necesita poder evaluar, dirigir y monitorear la estrategia, politicas y planes. El estandar internacional ISO/IEC 38500 describe la gobernanza corporativa de IT.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="2.3.2 Sistemas de gestion" FOLDED="true" ID="ID_1512895940" CREATED="1657897284543" MODIFIED="1657899781517" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un sistema es un numero de cosas relacionadas que trabajan en conjunto para alcanzar un objetivo general. Los sistemas deben ser auto regulados para agilidad y menor perdida de tiempo. Para logra esto, las relaciones dentro del sistema deben influenciar unos en otros por el bien de todos.
    </p>
    <p>
      
    </p>
    <p>
      Los componentes claves de un sistema son la estructura y los procesos que trabajan en conjunto.
    </p>
  </body>
</html></richcontent>
<node TEXT="Enfoque de sistemas" ID="ID_333401675" CREATED="1657899077872" MODIFIED="1657899289646" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un enfoque de sistemas en la gestion de servicio asegura el aprendizaje y mejora a traves de la vista de la gran imagen de los servicios y la gestion de servicio. Este extiende el horizonte de gestion y provee un enfoque sustentable a largo plazo.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Beneficios" FOLDED="true" ID="ID_1877602895" CREATED="1657899094833" MODIFIED="1657899501108" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Entendiendo la estructura del sistema, las interconexiones entre los activos y los componentes del servicio, y como los cambios en un area afectaran todo el sistema y sus partes constituyentes sobre el tiempo, un proveedor de servicio puede entregar beneficios como:
    </p>
  </body>
</html></richcontent>
<node ID="ID_1344487387" CREATED="1657899454966" MODIFIED="1657899471990"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Habilidad para adaptarse a los cambios necesarios de los clientes y mercados.
      </li>
      <li>
        Rendimiento sustentable
      </li>
      <li>
        Mejor enfoque para la gestion de servicios, riesgos, costos y entrega de valor.
      </li>
      <li>
        Gestion de servicio efectiva y eficiente.
      </li>
      <li>
        Enfoque simplificado que es mas facil para las personas de usar.
      </li>
      <li>
        Menos conflicto entre procesos.
      </li>
      <li>
        Menos trabajo duplicado y burocracia.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Sistema de Gestion" FOLDED="true" ID="ID_745807311" CREATED="1657899501549" MODIFIED="1657899813981" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Muchos negocios han adoptado estandares de gestion de sistemas como ventaja competitiva y para asegurar un enfoque consistente en la implementacion de la gestion de servicios a traves de la red o cadena de valor. La implementacion de sistemas de gestion tambien provee soporte para la gobernanza.
    </p>
  </body>
</html></richcontent>
<node TEXT="Definicion" ID="ID_1310986441" CREATED="1657899634703" MODIFIED="1657899973676" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un sistema de gestion (ISO 9001) es un marco de trabajo de politicas, procesos, funciones, estandares, guias basicas y herramientas que aseguran a una organizacion o parte de la organizacion a que puedan alcanzar sus objetivos.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Estandares" ID="ID_1263747868" CREATED="1657899798857" MODIFIED="1657900292787" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un sistema de gestion de una organizacion puede adoptar multiples sistemas estandares de gestion, tales como:
    </p>
    <ul>
      <li>
        Sistema de gestion de la calidad (ISO 9001)
      </li>
      <li>
        Sistema de gestion ambiental (ISO 14000)
      </li>
      <li>
        Sistema de gestion de servicio IT (ISO/IEC 20000)
      </li>
      <li>
        Sistema de gestion de la seguridad de la informacion (ISO/IEC 27001)
      </li>
      <li>
        Sistema de gestion de activos de software (ISO/IEC 19770)
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="PDCA" FOLDED="true" ID="ID_57030003" CREATED="1657900017570" MODIFIED="1657900253416" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los estandares de gestion ISO usan el ciclo de Deming PDCA (Plan-Do-Check-Act). El enfoque de ciclo de vida del servicio de ITIL acoge a PDCA con una interpretacion mejorada. La guia ITIL reconoce la necesidad de dirigir la gobernanza, el diseno organizacional y los sistemas de gestion desde la estrategia de negocio, la estrategia de servicio y los requerimientos del servicio.
    </p>
  </body>
</html></richcontent>
<node TEXT="" ID="ID_511537363" CREATED="1657911457779" MODIFIED="1657911463617">
<hook URI="img/pdca.png" SIZE="0.7623888" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="ISO/IEC 20000" FOLDED="true" ID="ID_1533045358" CREATED="1657900293916" MODIFIED="1657900902395" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      ISO/IEC 20000 es un estandar internacional para buenas practicas de ITSM. La parte 1 especifica los requerimientos para el proveedor de servicio para planificar, establecer, implementar, operar, monitorear, revisar, mantener y mejorar un sistema de gestion de servicio (SMS). La implementacion e integracion coordinada de un SMS, para cumplir los requerimientos de la parte 1, provee al control en curso, mayor efectividad, eficiencia y oportunidades para la mejora continua. Esto asegura que el proveedor de servicio:
    </p>
  </body>
</html></richcontent>
<node TEXT="Ventajas" ID="ID_277515942" CREATED="1657900904238" MODIFIED="1657900927187" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Entiende y cumple los requerimientos del servicio para lograr la satisfaccion del cliente.
      </li>
      <li>
        Establece la politica y objetivos para la gestion de servicio.
      </li>
      <li>
        Disena y entrega cambios y servicios que anaden valor al cliente.
      </li>
      <li>
        Monitorea, mide y revisa el rendimiento del SMS y los servicios.
      </li>
      <li>
        Continuamente mejora el SMS y los servicios basado en mediciones objetivas.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Desicion estrategica" ID="ID_1226402175" CREATED="1657900683985" MODIFIED="1657900889615" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los proveedores de servicio alrededor del mundo han establecido un SMS satisfactoriamente para dirigir y controlar sus actividades de gestion de servicio. La adopcion de un SMS deberia ser una decision estrategica de la organizacion.
    </p>
    <p>
      
    </p>
    <p>
      Una de las rutas mas comunes para una organizacion para cumplir con los requerimientos de ISO/IEC 20000 es adoptar las buenas practicas de gestion de servicio ITIL.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Certificacion" ID="ID_706427221" CREATED="1657900939846" MODIFIED="1657901128942" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La Certificacion ISO/IEC 20000-1 acreditada por un organismo de certificacion muestra que un proveedor de servicios esta comprometido en la entrega de valor a sus clientes y la mejora continua del servicio. Esto demuestra la existencia de un SMS efectivo que satisface los requerimientos de una auditoria externa. La certificacion da al proveedor de servicio una ventaja competitiva en marketing. Muchas organizaciones especifican como requisito el cumplimiento de ISO/IEC 20000 en sus contratos y acuerdos.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</node>
<node TEXT="2.4 El ciclo de vida del servicio" FOLDED="true" ID="ID_1484757254" CREATED="1657901142150" MODIFIED="1657911415710" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los servicios y procesos describen como las cosas cambian, mientras que la estructura describe como estos estan conectados. La estructura ayuda a determinar el comportamiento correcto requerido para la gestion del servicio.
    </p>
  </body>
</html></richcontent>
<node TEXT="Estructura" ID="ID_1789783166" CREATED="1657911042119" MODIFIED="1657911596282" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estructura describe como los procesos, personas, tecnologia y socios estan conectados. La estructura es esencial para organizar la informacion. Sin estructura, nuestra conocimiento en la gestion del servicio es meramente una coleccion de observaciones, practicas y metas conflictivas. La estructura del ciclo de vida del servicio es un marco&#160; de organizacion, soportado por la estructura organizacional, el portafolio del servicio y los modelos de servicio dentro de una organizacion.
    </p>
    <p>
      
    </p>
    <p>
      La estructura puede influenciar o determinar el comportamiento de la organizacion y las personas. Alterar la estructura de la gestion del servicio puede ser mas efectiva que simplemente controlar eventos discretos.
    </p>
    <p>
      
    </p>
    <p>
      Si estructura, es dificil aprender de la experiencia. Es dificil usar el pasado para educar para el futuro. Podemos aprender de la experiencia pero es necesario tambien confrontar directamente muchas de las consecuencias importantes de nuestras acciones.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="2.4.1 Especializacion y coordinacion a traves del ciclo de vida" FOLDED="true" ID="ID_1094260873" CREATED="1657911605971" MODIFIED="1657913147564" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La especializacion y coordinacion es necesario en un enfoque de ciclo de vida. La especializacion permite a los expertos focalizarse en los componentes del servicio pero los componentes del servicio tambien necesitan trabajar juntos para generar valor. La especializacion combinada con la coordinacion ayuda a gestionar la expertise, mejorar el enfoque y reducir la superposicion y vacios en los procesos. La especializacion y coordinacion juntos ayudan a crear una arquitectura colaborativa y agil que maximiza la utilizacion de los activos.
    </p>
  </body>
</html></richcontent>
<node TEXT="Coordinacion" ID="ID_1798512400" CREATED="1657912936374" MODIFIED="1657913145616" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La coordinacion a traves del ciclo de vida crea un ambiente enfocado en el negocio y en las salidas del cliente en lugar de solo objetivos de IT y proyectos. La coordinacion tambien es esencial entre los grupos funcionales, a traves de la red de valor, y entre procesos y tecnologia.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Feedback" ID="ID_591388752" CREATED="1657913062344" MODIFIED="1657913144874" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La retroalimentacion y el control entre los activos organizacionales ayudan a conseguir la eficiencia operacional, la efectividad organizacional y las economias de escala.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="2.4.2 Procesos a traves del ciclo de vida del servicio" FOLDED="true" ID="ID_1781545348" CREATED="1657913150789" MODIFIED="1657913166496">
<node TEXT="Procesos" FOLDED="true" ID="ID_259397834" CREATED="1657918027530" MODIFIED="1657918031937">
<node ID="ID_999477313" CREATED="1657919090877" MODIFIED="1657919881226"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Core ITIL lifecycle publication
          </p>
        </th>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Processes described in the publication
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" rowspan="5" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ITIL Service Strategy
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Strategy management for IT services
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Service portfolio management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Financial management for IT services
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Demand management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Business relationship management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" rowspan="8" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ITIL Service Design
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Design coordination
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Service catalogue management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Service level management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Availability management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Capacity management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            IT service continuity management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Information security management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Supplier management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" rowspan="7" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ITIL Service Transition
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Transition planning and support
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Change management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Service asset and configuration management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Release and deployment management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Service validation and testing
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Change evaluation
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Knowledge management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" rowspan="5" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ITIL Service Operation
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Event management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Incident management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Request fulfilment
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Problem management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Access management
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ITIL Continual Service Improvement
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Seven-step improvement process
          </p>
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Efectividad de la gestion de servicio" ID="ID_1475056938" CREATED="1657919950496" MODIFIED="1657975413934" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La gestion de servicio es mas efectiva si la gente tiene un claro entendimiento de como los procesos interactuan a traves del ciclo de vida del servicio, dentro de la organizacion y con otras partes (usuarios, clientes, proveedores).
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Integracion de procesos" FOLDED="true" ID="ID_864071581" CREATED="1657920380487" MODIFIED="1657975432844" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La integracion de procesos a traves del ciclo de vida del servicio depende de que el dueno del servicio, los duenos de los procesos, los trabajadores del proceso y otros interesados entienda:
    </p>
  </body>
</html></richcontent>
<node ID="ID_780940051" CREATED="1657920156256" MODIFIED="1657920324553"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        El contexto de uso, alcance, proposito y limites de cada proceso.
      </li>
      <li>
        Las estrategias, politicas y estandares que aplican al proceso y a la gestion de interfaces entre procesos.
      </li>
      <li>
        Las autoridades y responsabilidades involucradas en cada proceso.
      </li>
      <li>
        La informacion proveida por cada proceso que fluye desde un proceso a otro; quien lo produce; y como este es usado por los procesos integrados.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Integracion de servicios" ID="ID_908931269" CREATED="1657920474670" MODIFIED="1657921486662" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La integracion de los procesos de gestion de servicio depende en el flujo de informacion a traves de los procesos y limites organizacionales. Esto a su vez depende en la implementacion de la tecnologia de apoyo y la gestion de sistemas de informacion a traves de los limites organizacionales, en lugar de solo silos. Si los procesos de gestion de servicio son implementados, seguidos o cambiados en aislamiento, ellos pueden llegar a convertirse en gastos burocraticos que no entregan valor por dinero. Ellos podrian danar o negar la operacion o valor de otros procesos o servicios.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Procesos" ID="ID_53076095" CREATED="1657920831525" MODIFIED="1657921486037" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Cada proceso tiene un alcance claro con un conjunto estructurado de actividades que transforma entradas para entregas salidas confiables. Una interfaz de proceso es el limite del proceso. La integracion de procesos es el enlace de procesos asegurando que la informacion fluye de un proceso a otro efectivamente y eficientemente. Si es que existe compromiso para la integracion del proceso, los procesos son generalmente mas facil de implementar y habran menos conflictos entre procesos.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Fases del ciclo de vida" FOLDED="true" ID="ID_1094954277" CREATED="1657921021888" MODIFIED="1657921485370" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las fases del ciclo de vida trabajan juntos como un sistema integrado para soportar por ultimo los objetivos de gestion de servicio para la realizacion del valor del negocio. Cada fase es interdependiente como se muestra en la figura.
    </p>
  </body>
</html></richcontent>
<node TEXT="" ID="ID_335904536" CREATED="1657921148192" MODIFIED="1657921198211">
<hook URI="img/Integracion%20a%20traves%20del%20ciclo%20de%20vida%20del%20servicio.png" SIZE="0.77021825" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="SKMS" ID="ID_300512508" CREATED="1657921321919" MODIFIED="1657921484666" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La SKMS posibilita la integracion a traves de las fases del ciclo de vida del servicio. Este provee acceso controlado y seguro al conocimiento, informacion y data que es necesaria para gestionar y entregar servicios. El portafolio de servicio representa todos los activos actualmente comprometidos o siendo liberados en varias fases del ciclo de vida.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Estrategia del servicio" ID="ID_33493182" CREATED="1657921515136" MODIFIED="1657921790338" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estrategia del servicio establece las politicas y principios que proveen una guia para el ciclo de vida completo. El portafolio de servicio esta definido en esta fase del ciclo de vida, y servicios nuevos o cambiados estan alquilados.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Diseno del servicio" ID="ID_1974465432" CREATED="1657921638281" MODIFIED="1657921789756" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Durante la fase de diseno del servicio, todo lo necesario para transicionar y operar el servicio nuevo o cambiado es documentado en un paquete de diseno de servicio. Esta fase del ciclo de vida tambien disena todo lo necesario para crear, transicionar y operar los servicios, incluyendo los sistemas de gestion de informacion y herramientas, arquitecturas, procesos, metodos de media y metricas.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Transicion del servicio" ID="ID_1307963598" CREATED="1657921792160" MODIFIED="1657975234837" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La transicion del servicio asegura que los requerimientos de la estrategia del servicio, desarrollados en el diseno del servicio, sean efectivamente realizados en la operacion del servicio mientras los riesgos de falla y disrupcion son controlados.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Operacion del servicio" ID="ID_1276041707" CREATED="1657921816501" MODIFIED="1657975238245" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La fase del operacion del servicio del ciclo de vida del servicio lleva a cabo las actividades y procesos requeridos para entregar los servicios acordados. Durante esta etapa del ciclo de vida, el valor definido en la estrategia de servicio es realizada.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Mejora continua del servicio" ID="ID_1061697720" CREATED="1657921822733" MODIFIED="1657975335534" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La mejora continua del servicio actua en conjunto con todas las otras fases del ciclo de vida. Todos los procesos, actividades, roles, servicios y tecnologias deben ser medidos y estar sujetas a mejora continua.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Multiples fases" FOLDED="true" ID="ID_15488673" CREATED="1657975474408" MODIFIED="1657976344422" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La mayoria de procesos y funciones de ITIL tienen actividades a lo largo de multiples fases del ciclo de vida del servicio. Por ejemplo:
    </p>
  </body>
</html></richcontent>
<node ID="ID_1198065238" CREATED="1657975561558" MODIFIED="1657975906415"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        La validacion del servicio y el proceso de pruebas podria disenar pruebas durante la fase de diseno del servicio y realizar esas pruebas durante la transicion del servicio.
      </li>
      <li>
        La funcion de gestion tecnica podria proveer entradas para decisiones estrategicas acerca de tecnologia, asi como asistir en el diseno y transicion de componentes de infraestructura.
      </li>
      <li>
        Lo gerentes de relaciones con el negocio podrian asistir en la obtencion de requerimientos detallados durante la fase diseno del servicio del ciclo de vida, o tomar parte en la gestion de incidentes mayores durante la fase de operacion del servicio.
      </li>
      <li>
        Todas las fases del ciclo de vida contribuyen al proceso de mejora en siete pasos.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Realimentacion continua" FOLDED="true" ID="ID_1368921907" CREATED="1657975969326" MODIFIED="1657976343781" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La fortaleza del ciclo de vida del servicio se soporta en la realimentacion continua a traves de cada fase del ciclo de vida. Esta realimentacion asegura que la optimizacion del servicio es gestionada desde una perspectiva de negocio y es medida en terminos del valor que el negocio obtiene de los servicios en un momento dado durante el ciclo de vida del servicio.
    </p>
    <p>
      
    </p>
    <p>
      El ciclo de vida del servicio es no lineal en su diseno. A cualquier punto en el ciclo de vida del servicio, el proceso de monitoreo, analisis y retroalimentacion entre cada fase genera decisiones acerca de la necesidad de correcciones menores en el curso o mayores iniciativas de mejoras en el servicio.
    </p>
  </body>
</html></richcontent>
<node TEXT="" ID="ID_272431680" CREATED="1657976332438" MODIFIED="1657976338725">
<hook URI="img/Continual%20service%20improvement.png" SIZE="0.7623888" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Automatizacion" ID="ID_269446958" CREATED="1657976359375" MODIFIED="1657976447420" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Adoptar la tecnologia apropiada para automatizar los procesos y proveer gestion con informacion que soporte los procesos es tambien importante para una gestion de servicio efectiva y eficiente.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node TEXT="3. Principios de Estrategia del Servicio" FOLDED="true" ID="ID_506954769" CREATED="1658173723566" MODIFIED="1658174070782" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;La gente no quiere taladros de cuarto de pulgada. Ellos quieren agujeros de cuarto de pulgada.&quot;
    </p>
    <p>
      
    </p>
    <p>
      -- Professor Emerito Theodore Levitt, Harvard Business School
    </p>
  </body>
</html></richcontent>
<node TEXT="3.1 Estrategia" FOLDED="true" ID="ID_723782042" CREATED="1658174110132" MODIFIED="1658174152598" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;La escencia de la estrategia es escoger que no hacer&quot;
    </p>
    <p>
      
    </p>
    <p>
      -- Porter, 1996
    </p>
  </body>
</html></richcontent>
<node TEXT="Caso de estudio: Servicios de Seguridad" FOLDED="true" ID="ID_726836680" CREATED="1658174159851" MODIFIED="1658174752375">
<node TEXT="Problema" ID="ID_1442131041" CREATED="1658174741182" MODIFIED="1658174759125" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Hace algun tiempo en el 2001, un proveedor de servicio de seguridad de red global perdio uno de sus mayores clientes debido a aspectos de la calidad que materialmente estaba afectando los ingresos y ganancias. Los altos ejecutivos pedian que algo debia hacerse -&#160;&#160;ya sea cortar costos o encontrar un&#160;&#160;cliente de reemplazo.
    </p>
    <p>
      
    </p>
    <p>
      Mientras se buscaba un cliente de reemplazo, las operaciones del servicio obedientemente redujeron costos. La calidad del servicio se vio impactada, incitando que tres clientes recientemente adquiridos se vayan, afectando mas negativamente los ingresos y ganancias.
    </p>
    <p>
      
    </p>
    <p>
      Los altos ejecutivos nuevamente pedian que algo debia hacerse, ya sea reducir costos o encontrar clientes de reemplazo.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Solucion" ID="ID_1210446902" CREATED="1658174759742" MODIFIED="1658175549800" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Sorprendentemente, la solucion fue suspender nuevas ventas. El gerente de tecnologias de la informacion (CIO: Chief Information Officer) entendio que:
    </p>
    <ul>
      <li>
        Las operaciones de servicio estaban atrapadas en un ciclo vicioso con consecuencias desastrosas al largo plazo.
      </li>
      <li>
        Los clientes se fueron saliendo debido a la debilidad estrategica. Los clientes diferenciaron el valor de los servicios de seguridad a traves de la calidad del servicio. Las estrategias basadas en costos y tecnologia son incorrectas.
      </li>
    </ul>
    <p>
      Reenfocando el personal y presupuesto en las operaciones de servico, la organizacion reparo y reconstruyo sus capacidades de calidad distintivas para mantener los clientes. Se detuvo la rotacion de clientes.
    </p>
    <p>
      
    </p>
    <p>
      La solucion aunque dolorosa al corto plazo, permitio al proveedor romper el ciclo vicioso y pavimentar una estrategia a largo plazo para reganar clientes. El descubrimiento contra intuitivo estuvo basado en (a) un gran punto de vista de la imagen de los servicios, y (b) la percepcion de desempeno superior vs alternativas competitivas.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Concepto" ID="ID_1253952076" CREATED="1658175562238" MODIFIED="1658175744489" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Al mas simple nivel, una estrategia es un plan que define como una organizacion cumplira un conjunto designado de objetivos. Como se vio en esta publicacion, las estrategias rara vez son tan simples como un simple plan. Una estrategia es un conjunto complejo de actividades planificadas en las cuales la organizacion busca moverse de una situacion a otra en respuesta a un numero de variables internas y externas.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Estrategia de Servicio" ID="ID_1243820929" CREATED="1658175746223" MODIFIED="1658176002223" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una Estrategia de Servicio especificamente define como un proveedor de servicio usara los servicios para alcanzar los resultados de negocio de sus clientes, por tanto permitiendo al proveedor del servicio (interno o externo) cumplir sus objetivos.
    </p>
    <p>
      
    </p>
    <p>
      La Estrategia del Servicio ITIL define los conceptos necesarios para definir una estrategia de servicio exitosa, pero especificamente se enfoca en definir una estrategia de servicio IT.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Estrategia IT" ID="ID_1847617204" CREATED="1658175875241" MODIFIED="1658176002769" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una Estrategia IT se enfoca en como una organizacion usara y organizara la tecnologia para cumplir con sus objetivos de negocio. Una estrategia IT tipicamente incluye una estrategia de servicio IT.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.1.1 Aspectos fundamentales de la estrategia" FOLDED="true" ID="ID_212928219" CREATED="1658178316608" MODIFIED="1658178464534"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Carl vo Clausewitz dice que &quot;Todo en estrategia es muy simple, pero no significa que todo sea muy facil&quot;.
    </p>
  </body>
</html></richcontent>
<node TEXT="El pensamiento estrategico y la accion puede ser dificil por las siguientes razones:" ID="ID_800591829" CREATED="1658178470729" MODIFIED="1658178904219" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Definir y ejecutar aun la mas simple de las estrategias significa problemas complejos como el impacto organizacional, incertidumbre y conflicto de prioridades y objetivos. La experiencia y codigos de conducta usualmente no son suficiente para tratar con estos problemas.
      </li>
      <li>
        Estos representan analisis complejos de modelos para analizar los patrones actuales, tendencias proyectadas futuras y luego el estimado de probabilidad de que cada tendencia se convierta en realidad.
      </li>
      <li>
        Estas estrategias se enfocan en todos los factores respecto a la organizacion y su ambiente y las interacciones entre ellas. El alcance de aun las estrategias mas simples pueden parecer intimidantes, pero todavia es importante tomar esas areas en cuenta.
      </li>
      <li>
        Desde que los estrategas estan tratando con la incertidumbre, ellos podrian invertir un significante esfuerzo investigando los principios subyacentes de la estrategia, solo para encontrar que hay mucha incertidumbre que tienen que recurrir a la teoria basica.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="3.1.1.1 Usando la teoria para soportar la estrategia" ID="ID_1513042018" CREATED="1658178906304" MODIFIED="1658179329879" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La teoria es usualmente descontinuada debido a asociaciones abstractas o impracticas. La teoria, sin embargo, es la base de la mejor practica. Los ingenieros usan la teoria para resolver problemas practicos. Los bancos de inversion usan la teoria de portafolio para validar inversiones. Los metodos clave de Six Sigma se basan en las teorias de probabilidad y estadistica.
    </p>
    <p>
      
    </p>
    <p>
      Los gerentes confian en modelos de mapas mentales que los aseguran que posiblemente obtendran los resultados deseados. Los problemas ocurren cuando usan el modelo mental erroneo para el problema que estan manejando. Lo que aparece que no tiene solucion o aleatorio usualmente luce asi debido a que no se entiende el proceso o sistema. Sin los principios subyacentes, no es posible explicar porque una solucion perfectamente buena falla en una instancia despues de tremendo exito en otra.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.1.1.2 La estrategia debe permitir a los proveedores de servicio entregar valor" FOLDED="true" ID="ID_1207589576" CREATED="1658178927542" MODIFIED="1658181422065">
<node TEXT="Estrategia de valor unico" ID="ID_723088050" CREATED="1658181406711" MODIFIED="1658181570028" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un buen modelo de negocio describe los medios para cumplir con los objetivos de la organizacion. Sin embargo sin una estrategia que en alguna manera haga al proveedor de servicio de un valor unico para el cliente, hay poco que hacer para prevenir que el proveedor sea desplazado, degradando su mision o su entrada en el mercado. Una estrategia de servicio por tanto define un enfoque unico para la entrega de mejor valor. La necesidad de tener una estrategia de servicio no esta limitada a proveedores de servicio quienes son empresas comerciales. Los proveedores de servicio internos solo necesitan a lo mucho tener una perspectiva clara, posicionamiento y planes para asegurar que siguen siendo relevantes a las estrategias del negocio para sus empresas.
    </p>
    <p>
      
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Busqueda mejora continua" ID="ID_580310137" CREATED="1658181443571" MODIFIED="1658181569502" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los clientes continuamente buscan mejorar sus modelos y estrategias. Ellos quieren soluciones que rompan las estrategias de desempeno, y alcanzar la mas alta calidad de sus resultados de los procesos de negocio con poco o ningun incremento en costo. Tales soluciones estan usualmente disponibles a traves de productos y servicios innovadores. Si tales soluciones no estan disponibles dentro del ambito de control existente del cliente, contratos de servicio o red de valor, ellos se ven obligados a buscar en algun otro lado.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="La perspectiva de valor puede cambiar con el tiempo" ID="ID_369494814" CREATED="1658181446151" MODIFIED="1658181568822" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los proveedores de servicio no deberian tener como ganada su posicion y rol dentro de los planes de sus clientes aun si ellos tienen la ventaja de ser titulares. El valor de los servicios desde la perspectiva de los clientes podria cambiar sobre el tiempo debido a condiciones, eventos y factores fuera del control del proveedor. Una vista estrategica de la gestion de servicio significa un enfoque considerado cuidadosamente de las relaciones con el cliente y un estado de preparacion en el tratamiento de la incertidumbre en el valor que identifica esa relacion.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.1.1.3 Un enfoque basico para decidir una estrategia" FOLDED="true" ID="ID_920154115" CREATED="1658178955759" MODIFIED="1658178969987">
<node TEXT="Decision de la estrategia" ID="ID_1142853715" CREATED="1658181614575" MODIFIED="1658181679136"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Como decidiria la estrategia para servir a los clientes?
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Fuerzas competitivas" ID="ID_1590768952" CREATED="1658181693257" MODIFIED="1658184416944" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Primeramente, reconocer que existen otras organizaciones cuyo objetivo es competir con su organizacion. Aun las agencias del gobierno estan sujetas a fuerzas competitivas. Mientras el valor que ellos crean puede ser a veces dificil de definir y medir, esas fuerzas demandan lo que una organizacion debe realizar para que su mision sea mejor que las alternativas tan eficientemente como sea posible.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Valor diferenciado o unico" ID="ID_521985747" CREATED="1658181885281" MODIFIED="1658184415826" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Segundo, decidir un objetivo o estado final que diferencie el valor de que hacer, o como hacerlo, tal que los clientes no perciban que un mayor valor podria ser generado de alguna otra alternativa. La forma de valor podria ser monetario, si existe mayores ganancias o baja en costos, o social, como salvar vidas o recollectar impuestos. La difenciacion puede venir en la forma de barreras de entrada, tales como el conocimiento de su organizacion sobre el negocio de su cliente o la amplitud de su oferta de servicio. O este pordria estar en la forma de aumento en los costos de cambio, como estructuras de menor costo generadas a traves de la especializacion o la contratacion de servicios. <b>De cualquier manera es una forma de hacerlo mejor siendo diferente</b>. Esto es usualmente expresado en la vision y mision, lo cual es importante en la articulacion de como el proveedor de servicio se diferencia a si mismo y provee valor unico a sus clientes.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Premisa basica" ID="ID_274514141" CREATED="1658182880828" MODIFIED="1658184415020" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La premisa basica de la estrategia de servicios es que los proveedores de servicio deben cumplir objetivos definidos en terminos de resultados comerciales de sus clientes mientras estan sujetos a un sistema de restricciones. En un mundo de recursos y capacidades limitados, deben mantener sus posiciones frente a alternativas de la competencia. Al comprender las ventajas y desventajas involucradas en sus elecciones estrategicas, como servicios para ofrecer o mercados para servir, una organizacion puede servir mejor a sus clientes y superar a sus competidores.
    </p>
    <p>
      
    </p>
    <p>
      La meta de una estrategia de servicio puede ser resumida de una forma muy simple: Mejor desempeno vs alternativas competitivas.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.1.1.4 La gestion del servicio como un activo estrategico" FOLDED="true" ID="ID_1787492009" CREATED="1658184433926" MODIFIED="1658184459601">
<node TEXT="Caso de estudio: Uso de la gestion del servicio como un activo estrategico" FOLDED="true" ID="ID_1529646328" CREATED="1658184471066" MODIFIED="1658186000712">
<node TEXT="Problema" ID="ID_1824299396" CREATED="1658185985736" MODIFIED="1658186018640"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En algun momento a mediados de la decada de 1990, un gerente de linea de un proveedor de servicios de internet (ISP) lider, noto una gran cantidad de incremento de trafico en las carpetas de tablon de anuncios de dos analistas de stock de satiras. El ISP habia adoptado la perspectiva estrategica de &quot;La conectividad del consumidor primero en cualquier momento y en cualquier lugar&quot;.
    </p>
    <p>
      
    </p>
    <p>
      En lugar de advertir a los suscriptores sobre el aumento anormal en el uso de la capacidad, el administrador tomo un camino alternativo.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Solucion" ID="ID_1938004450" CREATED="1658185991063" MODIFIED="1658186224463"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El Gerente uso la gestion de servicios como un activo estrategico. En lugar de advertir a los suscriptores sobre el marcado aumento en el uso de la capacidad, el administrador ofrecio a los irreverentes analistas la oportunidad de crear su propio sitio. El sitio ahora llamado Motley Fool, continua siendo un destino muy visitado para obtener asesoramiento financiero. El gerente de linea finalmente se convirtio en presidente de programacion.
    </p>
    <p>
      
    </p>
    <p>
      El gerente entendio la intencion estrategica del proveedor de servicios: una conectividad de consumo mas profunda o una distribucion mas amplia.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Estrategias exitosas" ID="ID_1610990398" CREATED="1658186233378" MODIFIED="1658186666752" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las estrategias exitosas se basan en la capacidad para aprovechar las el conjunto de capacidades distintas para ofrecer un valor superior a los clientes a traves de los servicios. Estas capacidades se consideran activos estrategicos porque un proveedor de servicios puede depender de ellas para el su exito. El exito proviene no solo de brindar valor a los clientes, sino tambien de poder generar retornos sobre las inversiones. Los activos estrategicos son conjuntos cuidadosamente desarrollados de elementos tangibles e intangibles, sobre todo conocimientos, experiencia, sistemas y procesos. La gestion de servicios es un activo estrategico porque constituye las capacidades centrales de los proveedores de servicios. La gestion de servicios actua como un sistema operativo para los activos de servicio al implementarlos de manera efectiva para brindar servicios.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.1.1.5 La estrategia sintetiza dinamicas opuestas" FOLDED="true" ID="ID_578170771" CREATED="1658186670829" MODIFIED="1658186755483">
<node TEXT="Curso de accion futuro" ID="ID_437188350" CREATED="1658186893079" MODIFIED="1658188232615" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estrategia de servicio es a veces pensada como un curso de accion futuro. Cuando se pide a los altos directivos que elaboren una estrategia, la respuesta frecuente es un plan estrategico que detalla como la organizacion pasa de su estado actual a un estado futuro deseado. Pero hay deficiencias en esta comprension de la estrategia del servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Futuro vs Presente" ID="ID_1866136287" CREATED="1658187035559" MODIFIED="1658187582331" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La primera dinamica opuesta es futuro vs presente. El ritmo del cambio empresarial se esta acelerando, sin importar que tan grande o especifica sea la organizacion o en que industria compita. Las oportunidades surgen mientras otras desaparecen. Lo que era un buen plan hoy, puede convertirse en una responsabilidad manana. Una estrategia de servicio resuelve grandes problemas para que el personal pueda ocuparse de los pequenos detalles, por ejemplo, como proporcionar los servicios de mejor manera, en lugar de debatir que servicios ofrecer. Pero centrarse en un plan estrategico limita la capacidad de la organizacion para responder a las condiciones cambiantes. Las organizaciones que confian mucho en la consistencia y los procedimientos formales, por ejemplo, pueden perder flexibilidad, la capacidad de innovar o la capacidad de adaptarse rapidamente a condiciones imprevistas. Resulta que un enfoque de planificacion, si bien es necesario, es insuficiente: Una estrategia de servicio requiere mas que un plan o una direccion.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Efectividad operacional vs mejoras en la funcionalidad que conducen a una mayor ventaja competitiva" ID="ID_543454807" CREATED="1658187588133" MODIFIED="1658188209260" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La segunda dinamica es la eficacia operativa frente a las mejoras en la funcionalidad que conducen a una mayor ventaja competitiva. Las organizaciones que tienen un enfoque constante en mejorar la eficacia operativa a menudo no pueden mejorar su ventaja competitiva. Si bien la eficacia operativa es absolutamente necesaria, a menudo no es suficiente para seguir siendo competitivo. Por ejemplo, una organizacion puede lograr defectos minimos en la calidad, pero si no brinda el servicio que el cliente espera, no podra retener a sus clientes. Una estrategia de servicio explica como un proveedor de servicios se desempenara mejor en lo que hace o en como lo hace, no solo en comparacion consigo mismo, sino con las alternativas de la competencia. Si la estrategia de un proveedor se centra en la eficacia operativa a expensas del caracter distintivo, no prosperara por mucho tiempo. Tarde o temprano, toda organizacion se encuentra con competidores. Esto es cierto incluso para las organizaciones del sector publico, donde el enfoque unilateral en la eficiencia o eficacia puede hacer que su organizacion reconsidere la subcontratacion de IT a una organizacion que pueda diferenciarse mejor en el sector publico.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Captura de valor" FOLDED="true" ID="ID_1287350086" CREATED="1658188218909" MODIFIED="1658188794879" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La tercera dinamica es la &quot;captura de valor&quot; cuando se lanzan innovaciones frente al valor capturado durante las operaciones en curso. Los planes no son adecuados para proporcionar la informacion continua necesaria para mantener una capacidad de captura de valor. La captura de valor es la parte de la creacion de valor que un proveedor puede conservar. Si bien un proveedor de servicios puede crear valor a traves del caracter distintivo, es posible que no pueda conservar nada de el, ya que cada innovacion es costosa. Ademas, las condiciones para capturar valor no duran indefinidamente. Hay una ventana muy pequena entre el momento en que se lanza la innovacion y el momento en que el siguiente competidor tienen la misma capacidad. Cuantos mas competidores, menores seran los costos y mas dificil sera capturar valor. Cada innovacion se convierte en una operacion estandar despues de que se implementa, y la estrategia debe encontrar un equilibrio entre afianzar la innovacion actual y buscar la proxima oportunidad.
    </p>
  </body>
</html></richcontent>
<node TEXT="Nota sobre la captura de valor" LOCALIZED_STYLE_REF="default" ID="ID_1470277620" CREATED="1658188810193" MODIFIED="1658189081112">
<icon BUILTIN="idea"/>
<richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La captura de valor podria no siempre ser posible, especialmente en organizaciones del sector publico. En esos casos es importante enfocarse en demostrar valor en dinero, para asegurar la asignacion continuada de presupuesto para cubrir la innovacion y la mejora en curso.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Fracaso estrategico" FOLDED="true" ID="ID_1896601197" CREATED="1658189232150" MODIFIED="1658192161214" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El fracaso estrategico es a menudo el resultado de que una organizacion falle en reconocer y gestionar esas fuerzas dinamicas opuestas. Para un ejecutivo de IT, ser un estratega significa no solo tener pundos de vista opuestos, sino tener la capacidad de sintetizarlos. Incluyen la capacidad de reaccionar y predecir, adaptarse y planificar. De hecho, los proveedores de servicios de alto rendimiento tienen la habilidad de combinar marcos de referencia al momento de elaborar la estrategia de servicios.
    </p>
  </body>
</html></richcontent>
<node TEXT="" ID="ID_1392675072" CREATED="1658192077126" MODIFIED="1658192150143">
<hook URI="img/Balance%20entre%20dinamicas%20estrategicas%20opuestas.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="3.1.1.6 La estrategia como medio para superar a competidores" ID="ID_361088113" CREATED="1658189239988" MODIFIED="1658192543852" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los proveedores de servicio deben cumplir los objetivos definidos en terminos de resultados comerciales de sus clientes mientras estan sujetos a un sistema de restricciones. Al comprender las ventajas y desventajas involucradas en sus elecciones estrategicas, como servicios para ofrecer o mercados que servir, una organizacion puede servir mejor a sus clientes y superar a sus competidores. El objetivo de una estrategia de servicio puede resumirse como un desempeno superior frente a las alternativas de la competencia.
    </p>
    <p>
      
    </p>
    <p>
      Una estrategia de servicio de alto rendimiento, por tanto, que permite a un proveedor de servicios superar constantemente a las alternativas de la competencia a lo largo del tiempo, a lo largo de los ciclos comerciales, las interrupciones de la industria y los cambios en el liderazgo. Comprende tanto la capacidad de tener exito hoy como el posicionamiento para el futuro.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.1.1.7 Gobierno y organizaciones sin fines de lucro" FOLDED="true" ID="ID_1728230685" CREATED="1658189275857" MODIFIED="1658193353417">
<node TEXT="Recursos y capacidades limitadas" ID="ID_292714403" CREATED="1658193285122" MODIFIED="1658193392237" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      A primera vista, prodria parecer que las organizaciones gubernamentales y sin fines de lucro no se ven afectadas por las presiones de la competencia y los mercados. La etica de los servicios del sector social consiste en ayudar a las personas, no en golpearlas. Sin embargo, las organizaciones gubernamentales y sin fines de lucro tambien deben operar con recursos y capacidades limitados y restringidos. Las partes interesadas y los clientes exigen la mayor rentabilidad social posible por el dinero invertido. Eventualmente, estos electores consideraran alternativas competitivas.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Necesidad demandante" ID="ID_1456177896" CREATED="1658193311333" MODIFIED="1658193391450" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estrategia de una organizacion gubernamental o sin fines de lucro, al igual que la de sus contrapartes comerciales, explica como su enfoque de servicio unico brindara mejores resultados para la sociedad. Cuando la necesidad para los servicios del sector social es demasiado demandante, un desempeno superior frente a las alternativas competitivas es muy importante. Ninguna empresa comercial puede tener exito si intenta ser todo para todas las personas. Del mismo modo, los gobiernos y organizaciones sin fines de lucro deben elegir lo que haran y , o lo que es igualmente importante, lo que que no haran.
    </p>
    <p>
      
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Competencia con el sector privado" ID="ID_1413493678" CREATED="1658193332103" MODIFIED="1658193390823" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Finalmente, cabe senalar que las organizaciones gubernamentales y sin fines de lucro pueden competir con las organizaciones del sector privado, y lo hacen. Los formuladores de politicas a menudo buscan economias de modelos de entrega alternativos que pueden conducir a la subcontratacion de elementos o servicios completos. Por tanto, incluso los gobiernos y las organizaciones sin fines de lucro deben verse a si mismos en un entorno competitivo constante.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="3.1.2 Las cuatro Ps de la estrategia" FOLDED="true" ID="ID_943462275" CREATED="1658189306721" MODIFIED="1658189323978">
<node TEXT="Las cuatro Ps" FOLDED="true" ID="ID_287313387" CREATED="1658193435913" MODIFIED="1658232412574" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Mitnzberg (1994) introdujo cuatro formas de estrategia que deberian presentarse en cualquier estrategia que sea definida. Estas se ilustran en la figura 3.2 (after Simons, 1995)
    </p>
    <p>
      
    </p>
    <p>
      Las cuatro Ps son:
    </p>
  </body>
</html></richcontent>
<node TEXT="Figura" FOLDED="true" ID="ID_1525370269" CREATED="1658193554242" MODIFIED="1658193556963">
<node TEXT="" ID="ID_646268058" CREATED="1658193558756" MODIFIED="1658193604542">
<hook URI="img/Cuatro%20Ps%20de%20la%20estrategia.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Perspectiva" ID="ID_677094350" CREATED="1658193611129" MODIFIED="1658232095342" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Describe la vision y direccion de la organizacion. Una perspectiva estrategica articula cual es el negocio de la organizacion, como este interactua con el cliente y como sus servicios y productos seran proveidos. Una perspectiva cimenta la difenciacion de un proveedor de servicio en las mentes de sus empleados y clientes.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Posiciones" ID="ID_996413104" CREATED="1658193618233" MODIFIED="1658232092138" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Describe c&#243;mo el proveedor de servicios pretende competir contra otros proveedores de servicios en el mercado.&#160;&#160;La posici&#243;n se refiere a los atributos y capacidades que tiene el proveedor de servicios que lo diferencia de sus competidores.&#160;&#160;Las posiciones pueden basarse en valor o bajo costo, servicios especializados o la prestaci&#243;n de una gama de servicios inclusiva, conocimiento del entorno de un cliente o variables de la industria.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Planes" ID="ID_1212909076" CREATED="1658193621498" MODIFIED="1658232091509" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Describe c&#243;mo el proveedor de servicios har&#225; la transici&#243;n de su situaci&#243;n actual a la situaci&#243;n deseada.&#160;&#160;Los planes describen las actividades que el proveedor de servicios deber&#225; realizar para poder lograr su perspectiva y posiciones.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Patrones" ID="ID_1493423135" CREATED="1658193627632" MODIFIED="1658232090796" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Describe las acciones continuas y repetibles que un proveedor de servicios deber&#225; realizar para continuar cumpliendo sus objetivos estrat&#233;gicos.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Orden de adopcion" ID="ID_1536500204" CREATED="1658232128901" MODIFIED="1658232277903" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un proveedor de servicios puede comenzar con cualquiera de las cuatro P y evolucionar a otra.&#160;&#160;Por ejemplo, un proveedor de servicios podr&#237;a comenzar con una perspectiva: una visi&#243;n y direcci&#243;n para la organizaci&#243;n.&#160;&#160;El proveedor de servicios podr&#237;a entonces decidir adoptar una posici&#243;n articulada a trav&#233;s de pol&#237;ticas, capacidades y recursos.&#160; Esta posici&#243;n puede lograrse mediante la ejecuci&#243;n de un plan cuidadosamente elaborado.&#160;&#160;Una vez logrado, el proveedor de servicios puede mantener su posici&#243;n a trav&#233;s de una serie de decisiones y acciones bien entendidas a lo largo del tiempo: un patr&#243;n.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="La secuencia" ID="ID_346197834" CREATED="1658232330687" MODIFIED="1658232403751" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La secuencia no es tan importante como usar las cuatro Ps.&#160;&#160;De esta forma, la estrategia puede ajustarse para permitir que el proveedor de servicios pueda manejar las estrategias que ya se est&#225;n ejecutando junto con las que reci&#233;n se est&#225;n concibiendo.&#160;&#160;Tambi&#233;n asegurar&#225; que se mantenga un equilibrio entre el panorama general y la ejecuci&#243;n detallada.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.1.2.1 Perspectiva" FOLDED="true" ID="ID_104185775" CREATED="1658189338581" MODIFIED="1658237334527" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La perspectiva de una organizaci&#243;n expresa c&#243;mo se ve a s&#237; misma en t&#233;rminos de su contexto. Describe qu&#233; es la organizaci&#243;n, qu&#233; hace, para qui&#233;n lo hace y c&#243;mo funciona, y lo hace de una manera que facilita la comunicaci&#243;n interna y externa.
    </p>
    <p>
      
    </p>
    <p>
      La perspectiva recuerda a los empleados, clientes y proveedores acerca de las creencias, valores y prop&#243;sito de la organizaci&#243;n. Establece una direcci&#243;n general para la organizaci&#243;n y articula c&#243;mo cumplir&#225; su prop&#243;sito.
    </p>
  </body>
</html></richcontent>
<node TEXT="Algunos ejemplos de perspectiva incluyen:" ID="ID_1224476957" CREATED="1658234801754" MODIFIED="1658234997406"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Conc&#233;ntrese en el usuario y todo lo dem&#225;s seguir&#225;.
      </li>
      <li>
        Se trata de crecimiento, innovaci&#243;n y dependencia de la tecnolog&#237;a, liderados por las mejores personas en cualquier lugar.
      </li>
      <li>
        La conectividad del consumidor es lo primero, en cualquier momento y en cualquier lugar.
      </li>
      <li>
        (Nuestro) prop&#243;sito es mejorar la calidad de vida de las comunidades a las que servimos.
      </li>
      <li>
        Seremos el mejor proveedor de servicios de su clase en (nuestra) industria.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="O considere a los proveedores de servicios del mundo real que tienen una perspectiva de:" ID="ID_985687813" CREATED="1658235000979" MODIFIED="1658235211248"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        &quot;Operaciones administrativas altamente eficientes&quot; durante el surgimiento de la subcontrataci&#243;n de servicios.
      </li>
      <li>
        &quot;Proveedor de servicios de bajo costo&quot; durante el surgimiento de mano de obra calificada en el exterior.
      </li>
      <li>
        &quot;Experiencia de tecnologia especifica&quot; con el surgimiento de sistemas y software abiertos.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.1.2.2 Posicion" FOLDED="true" ID="ID_463354756" CREATED="1658189351294" MODIFIED="1658239336108" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estrategia como posici&#243;n se expresa como distinci&#243;n en la mente de los clientes.&#160;Esto a menudo significa competir en el mismo espacio que otros pero con una propuesta de valor diferenciada que sea atractiva para el cliente. Ya sea que se trate de ofrecer una amplia gama de servicios a un tipo particular de cliente, o de siendo la opci&#243;n de menor costo, es una posici&#243;n estrat&#233;gica.
    </p>
  </body>
</html></richcontent>
<node TEXT="Hay cuatro tipos generales de puestos, y estos se analizan en detalle en la sección 4.1.5.9.  Para facilitar la referencia, aquí se proporciona una breve descripción general de cada uno:" FOLDED="true" ID="ID_1301641351" CREATED="1658237378765" MODIFIED="1658239315743">
<node TEXT="Posicionamiento basado en la variedad" ID="ID_80470948" CREATED="1658239169112" MODIFIED="1658239326445" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El proveedor de servicios se diferencia por ofrecer una gama limitada de servicios a una variedad de clientes con necesidades variadas.&#160;Por ejemplo, una compa&#241;&#237;a de telefon&#237;a m&#243;vil ofrece una variedad de paquetes predefinidos seg&#250;n el tiempo y el tipo de uso.&#160;Los clientes eligen el paquete que m&#225;s les conviene, aunque los clientes pueden usar el mismo paquete de manera diferente.
    </p>
  </body>
</html></richcontent>
<font BOLD="true"/>
</node>
<node TEXT="Posicionamiento basado en necesidades" ID="ID_1207937831" CREATED="1658239212694" MODIFIED="1658239325841" TEXT_SHORTENED="true">
<font BOLD="true"/>
<richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El proveedor de servicios se diferencia por ofrecer una amplia gama de servicios a un n&#250;mero reducido de clientes.&#160;Este tipo de posicionamiento, tambi&#233;n llamado &quot;intimidad con el cliente&quot;, donde el proveedor de servicios identifica oportunidades en un cliente, desarrolla servicios para ellos y luego contin&#250;a desarrollando servicios para nuevas oportunidades, o simplemente continua proveyendo servicios valiosos para mantener a otros competidores fuera. La relaci&#243;n con el proveedor de servicio y el conocimiento del cliente es clave en la posici&#243;n basada en necesidades.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Posicionamiento basado en el acceso" ID="ID_770461181" CREATED="1658239275531" MODIFIED="1658239325198" TEXT_SHORTENED="true">
<font BOLD="true"/>
<richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El proveedor de servicios ofrece servicios altamente personalizados a un mercado objetivo muy espec&#237;fico, generalmente en funci&#243;n de la ubicaci&#243;n, el inter&#233;s especial o alguna otra categor&#237;a.&#160;Normalmente, s&#243;lo las personas de ese grupo tendr&#225;n acceso al servicio.&#160;Por ejemplo, los accesorios de motocicleta de marca s&#243;lo se pueden comprar en tiendas que vendan esa marca de motocicleta.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Posicionamiento basado en la demanda" ID="ID_330229847" CREATED="1658239302711" MODIFIED="1658239324457" TEXT_SHORTENED="true">
<font BOLD="true"/>
<richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Este es un tipo emergente de posicionamiento en el que el proveedor de servicios utiliza un enfoque basado en la variedad para atraer a una amplia gama de clientes, pero permite que cada cliente personalice exactamente qu&#233; componentes del servicio utilizar&#225; y cu&#225;nto va a utilizar.&#160;Los proveedores de servicios en l&#237;nea est&#225;n explorando esta forma de posicionamiento en el momento de escribir este art&#237;culo.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="3.1.2.3 Plan" ID="ID_981494436" CREATED="1658189359086" MODIFIED="1658240700733" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La forma m&#225;s tangible de una estrategia es un conjunto de documentos denominado plan estrat&#233;gico, y en muchas organizaciones esto es lo que se conoce como &quot;la estrategia&quot;. El plan contiene detalles sobre c&#243;mo la organizaci&#243;n lograr&#225; sus objetivos estrat&#233;gicos y cu&#225;nto est&#225; dispuesta a invertir para lograrlo.
    </p>
    <p>
      
    </p>
    <p>
      Dado que el futuro es incierto, los planes suelen contener varios escenarios, cada uno de los cuales contiene una respuesta estrat&#233;gica y un nivel de inversi&#243;n. A lo largo del a&#241;o, los planes se comparan con los hechos reales y se realizan ajustes para adaptarse al escenario emergente.
    </p>
    <p>
      
    </p>
    <p>
      Algunos planes son de alto nivel, como la estrategia general, mientras que otros son m&#225;s detallados, como los planes de ejecuci&#243;n para un nuevo servicio o proceso en particular. Todos los planes est&#225;n coordinados y siguen el mismo marco estrat&#233;gico. En la secci&#243;n 4.1.5.10 se incluyen m&#225;s detalles sobre la elaboraci&#243;n de planes estrat&#233;gicos.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.1.2.4 Patron" ID="ID_539794270" CREATED="1658189368212" MODIFIED="1658240919281" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los patrones son las formas en que una organizaci&#243;n se organiza para cumplir sus objetivos. Estos patrones podr&#237;an ser jerarqu&#237;as organizacionales, procesos, colaboracion inter-departamental, servicios, etc.
    </p>
    <p>
      
    </p>
    <p>
      Algunos patrones implican la forma en que la organizaci&#243;n interact&#250;a con sus clientes y proveedores. Los patrones son importantes porque aseguran que el proveedor de servicios no reaccione continuamente a la demanda de una manera nueva cada vez. Los patrones permiten al proveedor de servicios predecir c&#243;mo se cumplir&#225; una estrategia y qu&#233; inversi&#243;n se necesitar&#225;. En algunos casos, la organizaci&#243;n definir&#225; los patrones que necesita en su estrategia y luego requerir&#225; que todos cumplan con los patrones. En otros casos, los patrones que han tenido &#233;xito en el pasado se formalizar&#225;n en la estrategia de la organizaci&#243;n (estrategias emergentes). Puede encontrar m&#225;s informaci&#243;n sobre el establecimiento de patrones y estrategias emergentes en la secci&#243;n 4.1.5.11.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.1.2.5 La relacion entre las cuatro Ps" FOLDED="true" ID="ID_1959053553" CREATED="1658189378636" MODIFIED="1658241137826" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Como se indic&#243; en la secci&#243;n 3.1.2, la perspectiva y la posici&#243;n de un proveedor de servicios le permitir&#225;n desarrollar planes que, si se ejecutan, garantizar&#225;n que el proveedor de servicios logre sus objetivos estrat&#233;gicos.
    </p>
    <p>
      
    </p>
    <p>
      Sin embargo, la planificaci&#243;n se ocupa del futuro, que es cualquier cosa menos seguro. Con la mejor intenci&#243;n, ning&#250;n plan puede ser completamente confiable. Los cambios en la organizaci&#243;n, sus clientes y sus respectivos entornos pueden afectar la ejecuci&#243;n exitosa de un plan. En lugar de apegarse r&#237;gidamente a un plan que ya no es v&#225;lido, el proveedor de servicios debe modificar el plan, aplazarlo y abandonarlo. En algunos casos, es posible que deban fusionar planes o incluso crear otros nuevos.
    </p>
    <p>
      
    </p>
    <p>
      La Figura 3.3 ilustra la naturaleza din&#225;mica de los planes identificados por Mintzberg. En este diagrama, se ejecuta un plan (aunque con algunos ajustes), se aplaza otro y surge otro m&#225;s para responder a un cambio que impacta en la estrategia. El resultado neto del plan ejecutado m&#225;s el plan emergente son los nuevos patrones para la organizaci&#243;n.
    </p>
    <p>
      
    </p>
    <p>
      De esta forma, la estrategia no es una aplicaci&#243;n r&#237;gida de planes en un entorno cambiante, sino un proceso de adaptaci&#243;n continua mediante el cual la empresa sigue siendo relevante para su entorno cambiante.
    </p>
  </body>
</html></richcontent>
<node TEXT="" ID="ID_1975434209" CREATED="1658241204142" MODIFIED="1658241210022">
<hook URI="img/Planes%20resultantes%20de%20la%20estrategia%20en%20patrones.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
</node>
<node TEXT="3.2 Clientes y Servicios" FOLDED="true" ID="ID_1908614662" CREATED="1658189401916" MODIFIED="1658189412505">
<node TEXT="3.2.1 Clientes" FOLDED="true" ID="ID_263189200" CREATED="1658189414319" MODIFIED="1658255149828" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El concepto de clientes se introdujo en la secci&#243;n 2.1.5, pero aqu&#237; se repite y ampl&#237;a.
    </p>
    <p>
      
    </p>
    <p>
      Los clientes son aquellos que compran bienes o servicios. El cliente de un proveedor de servicios de TI es la persona o grupo que define y acuerda los objetivos de nivel de servicio. El t&#233;rmino tambi&#233;n se usa a veces de manera informal para referirse al usuario; por ejemplo, 'Esta es una organizaci&#243;n enfocada en el cliente'.
    </p>
    <p>
      
    </p>
    <p>
      Los usuarios son aquellos que utilizan un servicio de TI en el d&#237;a a d&#237;a. Aunque algunos clientes tambi&#233;n son usuarios, la funci&#243;n de un usuario es distinta de la de un cliente, ya que algunos clientes no utilizan el servicio de TI directamente. En contextos como el comercio minorista, se utiliza el t&#233;rmino &quot;consumidor&quot; en lugar de usuario.
    </p>
  </body>
</html></richcontent>
<node TEXT="3.2.1.1 ¿En qué se diferencian los clientes de los usuarios y consumidores?" FOLDED="true" ID="ID_1357919299" CREATED="1658189424536" MODIFIED="1658242515307">
<node TEXT="Quien es el cliente?" ID="ID_1755636864" CREATED="1658242330034" MODIFIED="1658242368245" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los proveedores de servicios de TI a menudo hablan en t&#233;rminos gen&#233;ricos sobre el cliente como &quot;cualquier persona que recibe un servicio de alg&#250;n tipo&quot;. Es importante que todo el personal de TI trate a las personas a las que presta servicios como clientes. Esto asegura que los miembros del personal de TI demuestren un buen comportamiento de servicio al cliente y que tanto los usuarios como los clientes est&#233;n satisfechos con el nivel de servicio que reciben. Esto, a su vez, promueve la retenci&#243;n de clientes.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Quien es el usuario?" ID="ID_349365039" CREATED="1658242401633" MODIFIED="1658242410970" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Sin embargo, no todas las personas que reciben un servicio son en realidad clientes. Si bien esto no deber&#237;a marcar una diferencia en la forma en que TI los trata, es una distinci&#243;n muy importante cuando se trata de responder a solicitudes y demandas. No todos los usuarios tienen la autoridad o la responsabilidad de solicitar servicios, exigir un determinado nivel de servicio o solicitar cambios en los servicios existentes. En el contexto de la venta al por menor, el papel del consumidor se entiende bien. Por ejemplo, la publicidad de juguetes est&#225; dirigida al consumidor (ni&#241;os) y no al cliente (padres). Los comerciales de juguetes se transmiten durante la programaci&#243;n de televisi&#243;n para ni&#241;os. Las tiendas de juguetes est&#225;n dise&#241;adas para hacer que los juguetes sean accesibles y atractivos para los ni&#241;os, no necesariamente para las personas que realmente hacen la compra. Adem&#225;s, el dise&#241;o de los juguetes se realiza con aportes significativos de los ni&#241;os, mientras que se consulta a los padres sobre la seguridad de los precios y si el juguete es cultural y moralmente apropiado.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Comercio minorista" ID="ID_128745629" CREATED="1658242440346" MODIFIED="1658242450285" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En el comercio minorista, el consumidor es poderoso y desempe&#241;a un papel muy influyente, a menudo m&#225;s que el papel del cliente. Por ejemplo, un cliente puede hacer una compra en nombre de una familia (los consumidores), pero si a la familia no le gusta el art&#237;culo, es poco probable que el cliente lo compre de nuevo. Los consumidores forman parte de grupos focales destinados al desarrollo de productos y servicios.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Satisfacer las necesidades de los usuarios" ID="ID_1921149766" CREATED="1658242462548" MODIFIED="1658242497527" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La misma din&#225;mica opera con los servicios de TI. Si bien los clientes realmente pagan y negocian el nivel de los servicios con el proveedor de servicios, por lo general lo hacen en nombre de una serie de usuarios (consumidores). Es fundamental que el proveedor de servicios se comprometa activamente con estos usuarios para garantizar que los servicios satisfagan las necesidades tanto de los clientes como de los usuarios. La interacci&#243;n formal tanto con los clientes como con los usuarios debe dise&#241;arse en procesos como la gesti&#243;n del nivel de servicio, la gesti&#243;n de las relaciones comerciales, la gesti&#243;n de la cartera de servicios, etc.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Consumidores individuales" ID="ID_122012728" CREATED="1658242502916" MODIFIED="1658242523453" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Algunos proveedores de servicios se especializan en brindar servicios a consumidores individuales, que tambi&#233;n son los clientes (por ejemplo, proveedores de telefon&#237;a o proveedores de conectividad a Internet de banda ancha inal&#225;mbrica). El n&#250;mero de clientes suele ser grande y los servicios muy estandarizados. Esto significa que la interacci&#243;n con los clientes tambi&#233;n est&#225; estandarizada y depende de un amplio marketing y soporte centralizado.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.2.1.2 Clientes internos y externos" FOLDED="true" ID="ID_1513428615" CREATED="1658189458372" MODIFIED="1658242974390" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Independientemente de la coherencia con la que se trate a los clientes y consumidores, no todos son iguales. Existe una diferencia entre los clientes que trabajan en la misma organizaci&#243;n que el proveedor de servicios de TI y los clientes que trabajan para otra organizaci&#243;n.
    </p>
  </body>
</html></richcontent>
<node TEXT="Clientes Internos" ID="ID_24472214" CREATED="1658242936952" MODIFIED="1658242972088" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los clientes internos son personas o departamentos que trabajan en la misma organizaci&#243;n que el proveedor de servicios. Por ejemplo, el departamento de marketing es un cliente interno de la organizaci&#243;n de TI porque utiliza servicios de TI. Tanto el jefe de marketing como el CIO reportan al director ejecutivo (CEO). Si TI cobra por sus servicios, el dinero pagado es una transacci&#243;n interna en el sistema de contabilidad de la organizaci&#243;n, es decir, no es un ingreso real.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Clientes Externos" ID="ID_1353610181" CREATED="1658242957831" MODIFIED="1658242971424" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los clientes externos son personas que no est&#225;n empleadas por la organizaci&#243;n, u organizaciones que son entidades legales independientes, que compran servicios del proveedor de servicios en los t&#233;rminos de un contrato o acuerdo legalmente vinculante. Cuando el proveedor de servicios cobra por los servicios, se les paga con 'dinero real, o a trav&#233;s de un intercambio de servicios o productos. Por ejemplo, una l&#237;nea a&#233;rea podr&#237;a obtener servicios de consultor&#237;a de una gran empresa de consultor&#237;a. Las dos terceras partes del valor del contrato se pagan en efectivo y la tercera parte se paga en boletos a&#233;reos por un valor equivalente.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Trato con clientes externos" ID="ID_1835842437" CREATED="1658243049960" MODIFIED="1658243111534" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Muchas organizaciones de TI que tradicionalmente brindan servicios a clientes internos descubren que est&#225;n tratando directamente con clientes externos debido a los servicios en l&#237;nea que brindan. Es importante que la estrategia de servicio identifique claramente c&#243;mo la organizaci&#243;n de TI interact&#250;a con estos clientes y qui&#233;n posee y administra la relaci&#243;n con ellos.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Acuerdo de nivel de servicio" ID="ID_893940161" CREATED="1658243150990" MODIFIED="1658245883423" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Es muy importante tener en cuenta que tanto los clientes internos como los externos deben recibir el nivel de servicio acordado, con los mismos niveles de servicio al cliente. Sin embargo, la forma en que se dise&#241;an, gestionan, entregan y mejoran los servicios suele ser bastante diferente. La tabla 3.1 muestra las principales diferencias entre clientes internos y externos.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Diferencia entre cliente interno y cliente externo" FOLDED="true" ID="ID_964442633" CREATED="1658243202798" MODIFIED="1658245861851" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La diferencia entre clientes internos y externos es un tema a lo largo de esta publicaci&#243;n, y las diferencias entre ellos ser&#225;n referenciados y descritos en mas detalle en varios contextos. A continuacion se muestra la tabla 3.1 Diferencias entre clientes internos y clientes externos:
    </p>
  </body>
</html></richcontent>
<node TEXT="Fondos" ID="ID_1886491240" CREATED="1658243323527" MODIFIED="1658245059758" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 100%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Clientes Internos
          </p>
        </th>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Clientes Externos
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            La financiaci&#243;n de los servicios de TI se proporciona internamente, por lo que la TI es un costo que debe recuperarse.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            En las organizaciones comerciales, el cliente interno debe usar el servicio de TI para generar ingresos que deben cubrir el costo del servicio de TI y todos los dem&#225;s costos.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Muchos proveedores de servicios de TI internos no cuantifican el costo de los servicios de TI individuales ni los vinculan con fuentes de ingresos externas. Estos proveedores de servicios se arriesgan a una situaci&#243;n en la que m&#250;ltiples clientes internos demandan servicios de un grupo limitado de personal y tecnolog&#237;a, creyendo que ya han pagado por ellos a trav&#233;s de la asignaci&#243;n central de presupuesto de TI. La competencia resultante por los recursos de TI puede da&#241;ar la capacidad de la organizaci&#243;n para lograr sus objetivos estrat&#233;gicos.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            En organizaciones gubernamentales o sin fines de lucro, el servicio de TI tiene que respaldar actividades que aseguren que los donantes o los organismos presupuestarios gubernamentales asignen fondos a la organizaci&#243;n, que se utilizan para cubrir los costos de TI y otros.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            La capacidad de brindar servicio a m&#250;ltiples clientes se financia con los ingresos incrementales obtenidos de cada nuevo contrato. A medida que aumentan los ingresos, tambi&#233;n aumenta la financiaci&#243;n del personal y la tecnolog&#237;a para prestar el servicio. Los clientes externos financian el servicio directamente en forma de ingresos. TI se convierte en un generador de ingresos para la organizaci&#243;n. El costo del servicio, m&#225;s un margen, debe recuperarse del cliente.
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="Enlace a la estrategia y objetivos del negocio" ID="ID_1478587648" CREATED="1658243347404" MODIFIED="1658245035776" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 100%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Clientes Internos
          </p>
        </th>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Clientes Externos
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El proveedor de servicios tiene los mismos objetivos organizacionales generales y la misma estrategia que sus clientes. Idealmente, el proveedor de servicios y el cliente trabajan juntos para brindar servicios externos y optimizar la eficiencia y eficacia operativa.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El t&#233;rmino 'cliente' se usa para garantizar que los resultados comerciales se coloquen en primer lugar y que el proveedor de servicios priorice sus actividades adecuadamente, pero el t&#233;rmino 'colega' puede ser m&#225;s preciso para describir c&#243;mo se relacionan entre s&#237; dos grupos internos.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Es &#250;til para el proveedor de servicios comprender qu&#233; resultados quiere lograr el cliente, de modo que el servicio pueda dise&#241;arse adecuadamente para cumplir con los niveles esperados de rendimiento y funcionalidad.&#160;&#160;Esto asegurar&#225; la satisfacci&#243;n y retenci&#243;n del cliente.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Sin embargo, los objetivos y estrategias del proveedor de servicios y del cliente son diferentes. Los objetivos del cliente son fijados por sus ejecutivos y son adecuados para su negocio. El objetivo del proveedor de servicios es mantener su negocio proporcionando servicios de TI. El proveedor de servicios y el cliente suelen estar en diferentes negocios, de lo contrario ser&#237;an competidores.
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="Contabilidad" ID="ID_189175508" CREATED="1658243375133" MODIFIED="1658245022435" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 100%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Clientes Internos
          </p>
        </th>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Clientes Externos
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El costo del servicio es el factor principal.&#160;&#160;Es posible generar un margen de 'beneficio' en los servicios, pero esto siempre est&#225; sujeto a las pol&#237;ticas de gesti&#243;n financiera de la empresa.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El objetivo de brindar servicios a los clientes internos es proporcionar un equilibrio &#243;ptimo entre el costo y la calidad del servicio, para apoyar a la organizaci&#243;n en el logro de sus objetivos.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El costo del servicio normalmente no se revela al cliente. El precio del servicio es el factor principal.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            En una organizaci&#243;n comercial, el proveedor del servicio debe asegurarse de que el precio del servicio sea m&#225;s alto que el costo del servicio. El objetivo del proveedor de servicios es maximizar la rentabilidad sin dejar de ser competitivo con los precios.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            En las organizaciones gubernamentales y sin fines de lucro, el objetivo de la prestaci&#243;n de servicios es lograr los objetivos de la organizaci&#243;n mientras se cubren los costos en parte mediante donaciones o impuestos, y en parte recuperando una parte de los costos de los clientes.
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="Involucramiento en el diseno del servicio" ID="ID_501272298" CREATED="1658243382932" MODIFIED="1658245015952" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 100%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Clientes Internos
          </p>
        </th>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Clientes Externos
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El dise&#241;o del servicio depende de las pol&#237;ticas de la empresa, las limitaciones de recursos y el rendimiento esperado de la inversi&#243;n.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los clientes tienden a estar involucrados en las especificaciones de dise&#241;o detalladas, que a menudo cubren tanto la funcionalidad como la capacidad de administraci&#243;n del servicio, ya que ambos afectan el nivel de inversi&#243;n requerido.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Cuando un cliente compra servicios predefinidos, es posible que se le solicite informaci&#243;n sobre su experiencia, que se utiliza para mejorar el dise&#241;o del servicio.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Si cada servicio est&#225; dise&#241;ado para cada cliente espec&#237;fico, el enfoque est&#225; en la funcionalidad del servicio y el precio para garantizar que funcione seg&#250;n las expectativas.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los clientes pueden involucrarse en el trabajo de dise&#241;o durante el posicionamiento basado en las necesidades y la demanda.&#160;&#160;De lo contrario, los clientes normalmente no se involucran en el trabajo de dise&#241;o y el proveedor de servicios normalmente no se involucra en el c&#225;lculo del retorno de la inversi&#243;n del cliente.
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="Involucramiento en la transicion y operacion del servicio" ID="ID_1325242686" CREATED="1658243400835" MODIFIED="1658245287660" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 100%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Clientes Internos
          </p>
        </th>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Clientes Externos
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los clientes a menudo participan en la creaci&#243;n, prueba y despliegue de servicios.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los cambios deben ser evaluados y autorizados tanto por los clientes como por los administradores de TI.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los clientes participan en la definici&#243;n de los procedimientos, mecanismos y cronogramas de implementaci&#243;n.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            La participaci&#243;n del cliente en la gesti&#243;n de cambios est&#225; claramente documentada en el contrato, junto con cl&#225;usulas sobre c&#243;mo los cambios afectar&#225;n el precio del servicio. Las solicitudes de cambio son evaluadas por el cliente en t&#233;rminos de impacto y precio.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Por lo general, los clientes no participan en el dise&#241;o detallado y las pruebas de los servicios, y tienen poca visibilidad de los procesos mediante los cuales se realizan.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            La participaci&#243;n en la implementaci&#243;n suele estar cuidadosamente planificada y los clientes reciben capacitaci&#243;n sobre c&#243;mo ejecutar sus actividades de implementaci&#243;n.
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="Disparadores de mejora" ID="ID_473015443" CREATED="1658243418142" MODIFIED="1658245572219" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 100%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Clientes Internos
          </p>
        </th>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Clientes Externos
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Las mejoras son impulsadas por el impacto en el negocio, espec&#237;ficamente optimizando el balance entre costo y calidad y la habilidad de ayudar a las unidades de negocios a alcanzar sus objetivos.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Las mejoras tambi&#233;n apuntan a mejorar la forma en que se dise&#241;an, entregan y administran los servicios. Los clientes suelen estar involucrados en los detalles de los planes de mejora del servicio, ya que tienen habilidades que podr&#237;an ayudar a TI a convertirse en mejores proveedores de servicios.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Las mejoras son impulsadas por la necesidad de retener a los clientes que contribuyen a la rentabilidad del proveedor de servicios y de seguir siendo competitivos en el mercado. Los cambios que impactan en los precios y la rentabilidad impulsan medidas que reducen los costos al tiempo que brindan niveles de servicio competitivos.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los clientes no suelen participar en la definici&#243;n y ejecuci&#243;n de los planes de mejora del servicio, sino que se centran en los resultados esperados. La forma en que el proveedor de servicios logra esos resultados generalmente no es importante para el cliente.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Al mismo tiempo, debe tenerse en cuenta que los clientes pueden participar en la definici&#243;n de los planes de mejora del servicio, especialmente cuando la relaci&#243;n con el proveedor del servicio (normalmente un subcontratista) es dif&#237;cil.
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="¿Puede un cliente equivocarse alguna vez?" ID="ID_1758981954" CREATED="1658245894592" MODIFIED="1658246492747" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La capacitaci&#243;n en servicio al cliente ha promocionado frases como 'El cliente es el rey' y 'El cliente siempre tiene la raz&#243;n'. Los proveedores de servicios de TI bien intencionados se han tomado estas frases en serio, solo para encontrar que sus organizaciones tienen algunas dificultades porque entregaron el servicio exacto que su 'cliente' solicit&#243;.
    </p>
    <p>
      
    </p>
    <p>
      La realidad de la situaci&#243;n es que hay circunstancias en las que el cliente no tiene raz&#243;n, o en las que el cliente deja de ser cliente. Un cliente deja de serlo cuando se niega a pagar por un servicio o si requiere que el proveedor del servicio haga algo ilegal, o cuando utiliza el servicio para fines ilegales. Por ejemplo, los compradores se convierten en delincuentes cuando roban art&#237;culos de una tienda.
    </p>
    <p>
      
    </p>
    <p>
      La administraci&#243;n de TI se vuelve c&#243;mplice del fraude cuando sigue las instrucciones del cliente para destruir ciertos registros. TI es negligente si accede a proporcionar un servicio que es contrario a sus objetivos comerciales o perjudicial para la rentabilidad del negocio (incluso si un cliente les dijo que lo hicieran).
    </p>
    <p>
      
    </p>
    <p>
      Aqu&#237; hay otra distinci&#243;n muy importante entre los clientes internos y externos y es qu&#233; factores se tienen en cuenta al decidir si se debe prestar un nuevo servicio. Por ejemplo, un cliente est&#225; involucrado en un trabajo de investigaci&#243;n y desarrollo sensible y confidencial. Requieren que todo el procesamiento se realice en un centro de datos seguro, que debe construirse en una ubicaci&#243;n virtualmente inaccesible que hayan identificado. Este es un requisito extremadamente costoso y requerir&#225; que el proveedor de servicios realice varias inversiones nuevas.
    </p>
    <p>
      
    </p>
    <p>
      El proveedor de servicios inicia una investigaci&#243;n y descubre una ubicaci&#243;n alternativa que cumple con todos los requisitos del cliente y es igual de segura. La alternativa reducir&#237;a los costos del nuevo servicio en un 25 % . El cliente insiste en seguir adelante con el plan original.&#160;&#160;&#191;Tiene raz&#243;n el cliente?
    </p>
    <p>
      
    </p>
    <p>
      La respuesta no es sencilla, ya que puede haber otros factores involucrados. Sin embargo, si el cliente es un cliente externo, el proveedor de servicios probablemente firmar&#225; el contrato y luego comenzar&#225; a dise&#241;ar y construir la nueva instalaci&#243;n. Este nuevo servicio requiere una inversi&#243;n significativa, pero si la empresa es legal y posible, y si el cliente est&#225; dispuesto a pagar lo suficiente para que el proveedor del servicio obtenga ganancias, parece una buena oportunidad. La clave aqu&#237; es que el resultado final se basa en los ingresos externos generados por el cliente.
    </p>
    <p>
      
    </p>
    <p>
      Si el cliente es un cliente interno, el proveedor de servicios tiene la responsabilidad de cuestionar la decisi&#243;n con m&#225;s detalle, ya que ambos son responsables del dinero que se utilizar&#225; para hacer las inversiones. Es dinero que pertenece a la organizaci&#243;n para la que ambos trabajan, y no son ingresos. &#191;Qu&#233; resultados comerciales generar&#225; esta inversi&#243;n y c&#243;mo esta ubicaci&#243;n espec&#237;fica lograr&#225; estos objetivos de manera m&#225;s efectiva que la ubicaci&#243;n m&#225;s econ&#243;mica? En este caso, el proveedor de servicios tiene un importante papel de control y equilibrio que desempe&#241;ar, mientras que el cliente debe demostrar cu&#225;l ser&#225; el retorno de la inversi&#243;n y justificar ante la alta direcci&#243;n por qu&#233; la alternativa m&#225;s barata no es aceptable.
    </p>
    <p>
      
    </p>
    <p>
      Adem&#225;s, esta distinci&#243;n impone responsabilidades al cliente interno. El cliente es responsable de proporcionar la financiaci&#243;n adecuada para el nivel de servicio requerido. Se comprometen a no utilizar competidores externos siempre que los servicios se presten seg&#250;n lo requerido.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Nota importante sobre servicio al cliente" ID="ID_740321699" CREATED="1658246575093" MODIFIED="1658246726489" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estrategia de servicio de ITIL no sugiere ni por un momento que las organizaciones de TI deban comenzar a cuestionar la idea de un buen servicio al cliente. Los clientes, ya sean internos o externos, son la raz&#243;n de existir de un proveedor de servicios. Un proveedor de servicios de TI responsable debe hacer todo lo que est&#233; a su alcance para garantizar que los servicios se acuerden, financien y entreguen adecuadamente para garantizar la satisfacci&#243;n del cliente.
    </p>
    <p>
      
    </p>
    <p>
      Sin embargo, un proveedor de servicios responsable tambi&#233;n act&#250;a con la debida diligencia al aceptar prestar el servicio a un cliente, para asegurarse de que se trata de una solicitud leg&#237;tima de una persona autorizada. Si un ejercicio de diligencia debida no es posible, un proveedor de servicios responsable bien puede especificar que no ha llevado a cabo la &quot;diligencia debida&quot; (a menudo porque el cliente no puede o no est&#225; dispuesto a proporcionar suficiente acceso a la informaci&#243;n y el tiempo para lograr una verdadera diligencia debida, o&#160; no est&#225; dispuesto a financiar un verdadero ejercicio de diligencia debida ). Tambi&#233;n se debe tener en cuenta que es responsabilidad legal del cliente asegurarse de que se obedezcan las leyes locales. Esto se convierte en un problema con elementos como el almacenamiento local, por ejemplo, la legislaci&#243;n de protecci&#243;n de datos var&#237;a de pa&#237;s a pa&#237;s y, a menudo, es el cliente, no el proveedor de servicios, quien es responsable ante la ley.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.2.1.3 Unidades de negocio como clientes" ID="ID_630896275" CREATED="1658189479985" MODIFIED="1658254464791" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En el modelo de gesti&#243;n de servicios de TI, la relaci&#243;n entre una organizaci&#243;n de TI y otras unidades de negocio se caracteriza como una relaci&#243;n de proveedor de servicios con el cliente. Este es un modelo &#250;til para explicar las interacciones entre diferentes partes de una organizaci&#243;n y tambi&#233;n para explicar la naturaleza din&#225;mica de los resultados y actividades de la organizaci&#243;n de TI.
    </p>
    <p>
      
    </p>
    <p>
      Es apropiado que TI vea a otras unidades de negocio como clientes, por lo que los objetivos de negocio son la m&#225;xima prioridad. Si bien el modelo de proveedor de servicios-cliente es constructivo, el rol de TI en el negocio es mucho m&#225;s complejo.
    </p>
    <p>
      
    </p>
    <p>
      Te&#243;ricamente, TI y las dem&#225;s unidades de negocios est&#225;n todas enfocadas en lograr los mismos objetivos. Sin embargo, a menudo sucede que TI tiene que tratar con unidades de negocios que tienen objetivos diferentes u opuestos. A veces se trata de una estrategia corporativa general deliberada, otras veces es simplemente una implementaci&#243;n deficiente de la estrategia corporativa. En cualquier caso, la organizaci&#243;n de TI puede encontrarse entre las demandas en conflicto de la 'corporaci&#243;n' como un todo y cada unidad de negocios individual. La estrategia debe identificar claramente c&#243;mo hacer frente a estas situaciones, y obtener el apoyo de los ejecutivos de la organizaci&#243;n para las t&#225;cticas utilizadas para hacer frente a las demandas conflictivas. En algunos casos, esto significar&#225; adaptarse a los requisitos conflictivos (que necesitar&#225;n financiamiento adicional), y en otros significar&#225; que TI a veces tiene que imponer objetivos corporativos a las unidades de negocios que no desean cumplir.
    </p>
    <p>
      
    </p>
    <p>
      A medida que la TI madura y la tecnolog&#237;a se vuelve m&#225;s omnipresente, m&#225;s organizaciones de TI se encuentran entregando servicios generadores de ingresos a clientes externos. Cada vez m&#225;s, TI es tanto un proveedor de servicios interno como una unidad comercial estrat&#233;gica por derecho propio.
    </p>
    <p>
      
    </p>
    <p>
      La estrategia de servicio de ITIL explora esta doble funci&#243;n en cada cap&#237;tulo e identifica c&#243;mo una serie de organizaciones se enfrentan a los desaf&#237;os que plantea.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.2.1.4 Otras organizaciones IT como clientes" FOLDED="true" ID="ID_1577167102" CREATED="1658189503339" MODIFIED="1658254890554" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Algunas organizaciones de TI alientan a su personal a ver a cada organizaci&#243;n de TI como un cliente.&#160;&#160;El motivo de hacer esto es garantizar un trabajo de mayor calidad y unas interacciones m&#225;s positivas entre los departamentos.&#160;&#160;Si bien el objetivo es encomiable es importante evitar cualquier comportamiento negativo que resulte de una relaci&#243;n artificial cliente-proveedor.
    </p>
    <p>
      
    </p>
    <p>
      Por ejemplo, en una organizaci&#243;n los departamentos de alojamiento de aplicaciones y redes dedicaron mucho tiempo y energ&#237;a a negociar y tratar de mejorar los niveles de servicio. El impacto de estas negociaciones sobre la calidad y el costo del servicio fue insignificante, pero la relaci&#243;n entre los dos departamentos se volvi&#243; contradictoria. El 'cliente' acus&#243; al 'proveedor de servicios' de mala calidad y falta de comprensi&#243;n de la forma en que trabajaba el 'cliente'. El 'proveedor de servicios' se quej&#243; de que el 'cliente' era demasiado exigente, considerando el hecho de que no pagaba por el servicio. Ninguno de los departamentos consider&#243; nunca al cliente real al que se supon&#237;a que ambos deb&#237;an brindar soporte y la calidad del servicio comenz&#243; a sufrir debido a que los incidentes y problemas quedaron sin resolver, y los proyectos conjuntos se retrasaron.
    </p>
    <p>
      
    </p>
    <p>
      La gesti&#243;n de la calidad entre los diferentes departamentos del proveedor de servicios se logra mejor centr&#225;ndose tanto en el cliente real como en los objetivos comerciales asociados. Los requisitos espec&#237;ficos de cada departamento se pueden especificar y medir en procedimientos operativos est&#225;ndar y acuerdos de nivel operativo, reforzados por un fuerte liderazgo de sus gerentes.
    </p>
  </body>
</html></richcontent>
<node TEXT="¿Quién es el verdadero cliente?" ID="ID_1659148021" CREATED="1658254908480" MODIFIED="1658255028491"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En muchas organizaciones, especialmente en las empresas relacionadas con la fabricaci&#243;n, los clientes se definen como cualquier persona que recibe un resultado de otra persona. El objetivo es hacer que cada persona sea responsable de la calidad en cada paso del proceso. El proveedor tendr&#225; m&#225;s cuidado en la producci&#243;n de la salida, ya que el cliente no aceptar&#225; un trabajo de calidad inferior.
    </p>
    <p>
      
    </p>
    <p>
      En realidad, este enfoque solo funciona si todos los eslabones de la cadena de procesos se gestionan de acuerdo con un objetivo final com&#250;n. El juicio sobre lo que constituye un buen servicio no lo definen los individuos en el proceso; se define por los resultados comerciales deseados que se lograr&#225;n si todo el proceso funciona correctamente.
    </p>
    <p>
      
    </p>
    <p>
      Lo mismo ocurre con varias organizaciones de TI que trabajan juntas. Si bien pueden verse como clientes, el objetivo general de la organizaci&#243;n siempre debe ser lo m&#225;s importante en su relaci&#243;n. Esto puede lograrse concertando apropiadamente procesos construidos y acuerdos internos.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.2.1.5 IT como un proveedor de servicios externo" ID="ID_63267243" CREATED="1658189518626" MODIFIED="1658255142950" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La mayor&#237;a de los ejemplos utilizados anteriormente se refieren a TI como un proveedor de servicios interno, pero muchas organizaciones est&#225;n en el negocio de brindar servicios de TI. La estrategia de servicios de ITIL tambi&#233;n trata sobre proveedores de servicios que brindan servicios, como proveedores de servicios de Internet, servicios de hospedaje y empresas de subcontrataci&#243;n. A lo largo de esta publicaci&#243;n, se hace referencia tanto a proveedores de servicios 'internos' como 'externos'.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.2.2 Servicios" FOLDED="true" ID="ID_1592189919" CREATED="1658189538673" MODIFIED="1658255430571" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un servicio es un medio para entregar valor a los clientes facilitando los resultados que los clientes desean lograr sin tener que asumir costos y riesgos espec&#237;ficos.
    </p>
    <p>
      
    </p>
    <p>
      Esta definici&#243;n establece el contexto para la gesti&#243;n de TI. La principal caracter&#237;stica de TI es que se enfoca en brindar un conjunto de servicios que contribuyen al valor para sus clientes. No se enfoca tanto en la salida como en lo que el cliente puede hacer con la salida.
    </p>
    <p>
      
    </p>
    <p>
      El enfoque de gesti&#243;n de TI como un conjunto de servicios es diferente del enfoque en el que TI se gestiona como un tipo de entorno de fabricaci&#243;n que genera un conjunto de &quot;productos&quot; o salidas. Las diferencias clave entre estos enfoques se resumen en la Tabla 3.2.
    </p>
    <p>
      
    </p>
    <p>
      Tambi&#233;n se debe tener en cuenta que muchos servicios se venden o se ponen a disposici&#243;n cuando se entrega un producto. Por ejemplo, el precio de un servidor incluye el env&#237;o y el servicio de instalaci&#243;n. Tambi&#233;n algunos 'productos' se fabrican a partir de materias primas, pero se entregan en tiempo real ya veces se denominan servicios, por ejemplo, electricidad. En t&#233;rminos de las definiciones dadas en la Tabla 3.2, estos no son ni productos ni servicios, sino que se les conoce como utilities.
    </p>
    <p>
      
    </p>
    <p>
      Por lo tanto, el enfoque apropiado para administrar TI es el enfoque de administrar un conjunto de servicios, en lugar de un conjunto de productos.&#160;&#160;Aunque puede haber una serie de productos utilizados para prestar servicios (por ejemplo, PC de escritorio), el objetivo final de TI no es producirlos. M&#225;s bien, implementar&#225;n productos como activos que se utilizan para brindar servicios. <b>Estos activos se gestionan como componentes de servicios, no como servicios en s&#237; mismos</b>.
    </p>
  </body>
</html></richcontent>
<node TEXT="Tabla 3.2 Diferencias entre servicios productos manufacturados" ID="ID_865755359" CREATED="1658255447518" MODIFIED="1658261117624" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 100%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Servicios
          </p>
        </th>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Productos manufacturados
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los servicios son interacciones din&#225;micas entre proveedor de servicios y cliente.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los productos son entidades f&#237;sicas que se producen procesando materias primas o ensamblando componentes.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los servicios se entregan en tiempo real a medida que los clientes los necesitan y los utilizan.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los productos se crean con anticipaci&#243;n y se almacenan antes de su distribuci&#243;n al cliente. La producci&#243;n Just In Time busca reducir la cantidad de tiempo entre la producci&#243;n y la entrega, pero se basa en la capacidad de acceder a los componentes necesarios, ensamblarlos y entregar el art&#237;culo terminado al cliente.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los servicios se producen y consumen al mismo tiempo y no pueden separarse de sus proveedores.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los productos son producidos por una entidad y pueden ser almacenados, distribuidos y vendidos por diferentes entidades en diferentes momentos.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            La salida de un servicio es vol&#225;til, a menudo cambia en tiempo real seg&#250;n el entorno del cliente.&#160;&#160;Por ejemplo, incluso una transacci&#243;n simple puede variar en tama&#241;o y salida dependiendo de la entrada de un usuario.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El resultado de la producci&#243;n es predecible, y los productos no deben desviarse de una norma predefinida mas alla de niveles espec&#237;ficos, o el producto ni siquiera se entrega al cliente.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            La forma en que se prestan los servicios puede variar con cada iteraci&#243;n de la prestaci&#243;n del servicio. Por ejemplo, en un entorno virtualizado, una transacci&#243;n puede procesarse en cualquier n&#250;mero de dispositivos y enrutarse a trav&#233;s de cualquier componente de red. Esto significa que transacciones similares a menudo se procesan de manera diferente.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            La mayor&#237;a de los productos se fabrican utilizando una ruta fija a trav&#233;s de una l&#237;nea de producci&#243;n fija con equipos espec&#237;ficos. Aunque hay algunos casos en los que se utilizan m&#233;todos de producci&#243;n variables, cada tipo de producto generalmente sigue exactamente la misma ruta a trav&#233;s de la f&#225;brica.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El &#233;xito de un servicio solo se puede determinar si el cliente ha sido capaz de lograr el resultado deseado. El resultado es secundario (consulte la secci&#243;n 3.2.2.1).
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El &#233;xito del producto est&#225; determinado por la calidad y la entrega del producto en s&#237;.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El valor de un servicio s&#243;lo se percibe cuando un cliente realmente lo utiliza.&#160;&#160;No retiene ning&#250;n valor despu&#233;s de que se haya utilizado y no se puede volver a vender despu&#233;s de que se haya utilizado.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El valor se crea y se realiza cada vez que un producto cambia de manos. Un producto conserva su valor a lo largo del tiempo y puede comprarse y venderse varias veces durante su vida &#250;til.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El valor de un servicio se produce en la relaci&#243;n entre el cliente y el proveedor del servicio. Si cualquiera de las partes deja la relaci&#243;n, el servicio no tiene valor, ya que no se puede entregar.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El valor de un producto se lleva en el producto mismo. Incluso si el productor cierra, el producto a&#250;n conservar&#225; parte (si no todo) de su valor.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            La calidad de un servicio suele definirse por el nivel de satisfacci&#243;n del cliente en funci&#243;n de su experiencia subjetiva del servicio.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            La calidad se basa primero en si el producto cumple con ciertos criterios f&#237;sicos predefinidos, y solo luego en la experiencia del cliente de si el producto hace lo que afirma el proveedor.
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="¿Existen aspectos de TI que se entregan como productos en lugar de servicios?" ID="ID_996635640" CREATED="1658260758620" MODIFIED="1658260868847" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Algunas organizaciones encuentran &#250;til categorizar ciertos componentes de TI como productos en lugar de servicios. Por ejemplo, los usuarios cuentan con computadoras de escritorio. Cada computadora de escritorio se construye y configura de acuerdo con especificaciones estrictas para cada tipo de usuario, y debe mantenerse en una l&#237;nea de base de configuraci&#243;n aprobada para que funcione correctamente. Sin embargo, es importante recordar que estas computadoras son simplemente un medio para que el proveedor de servicios brinde servicios al usuario. No son servicios en s&#237; mismos. Si un proveedor de TI s&#243;lo construye y entrega computadoras, no es un proveedor de servicios, sino un fabricante o revendedor de computadoras.
    </p>
    <p>
      
    </p>
    <p>
      Por lo tanto, si bien TI puede ver ciertos elementos de configuraci&#243;n como productos, estos deben estar vinculados a los servicios que se usan para proporcionar y los resultados que el cliente logra al usarlos.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.2.2.1 Salidas" FOLDED="true" ID="ID_38798315" CREATED="1658189550171" MODIFIED="1658261878019" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una salida es el resultado de llevar a cabo una actividad, seguir un proceso o entregar un servicio de TI, etc. El t&#233;rmino se usa para referirse a las salidas previstos, as&#237; como a las salidas reales.&#160;&#160;Tenga en cuenta que en esta publicaci&#243;n, las salidas se denominan 'resultados comerciales' y 'resultados del cliente'.
    </p>
  </body>
</html></richcontent>
<node TEXT="No hay una diferencia real en el resultado real, pero el contexto suele ser diferente:" ID="ID_73900720" CREATED="1658261313318" MODIFIED="1658261410529" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Los resultados empresariales suelen referirse al contexto de los clientes internos, donde el resultado para el cliente representa los objetivos empresariales generales tanto de la unidad empresarial como del proveedor de servicios.
      </li>
      <li>
        Los resultados del cliente generalmente se refieren al contexto de proveedores de servicios externos, donde los resultados del proveedor de servicios se basan en los resultados del cliente, pero son diferentes.&#160;Por ejemplo, el resultado principal del proveedor de servicios externo es brindar un servicio rentable.&#160;Su cliente utiliza ese servicio para lograr un resultado espec&#237;fico que es importante para &#233;l, pero que no forma parte del negocio del proveedor del servicio.&#160;Si el cliente logra su resultado, estar&#225; dispuesto a pagarle al proveedor de servicios para que contin&#250;e brindando el servicio, lo que permitir&#225; que el proveedor de servicios logre el resultado deseado de rentabilidad.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="La Definicion de servicio es salidas" ID="ID_1737871172" CREATED="1658261801851" MODIFIED="1658261872221" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La definici&#243;n de un servicio se refiere espec&#237;ficamente a resultados y no a productos.&#160;&#160;La diferencia es importante. Cuando un proveedor de servicios se enfoca &#250;nicamente en los resultados, puede demostrar que brind&#243; un nivel espec&#237;fico de servicio, pero no sabe si el servicio realmente logr&#243; los resultados esperados. Esto a menudo se caracteriza por situaciones en las que el proveedor de servicios de TI cumple con sus acuerdos de nivel de servicio de manera constante, pero tiene una baja calificaci&#243;n de satisfacci&#243;n del cliente. En otras palabras, TI es capaz de entregar un resultado consistente, pero no permite que los clientes alcancen los resultados esperados.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Centrarse en alcanzar resultados" ID="ID_1517742614" CREATED="1658261899796" MODIFIED="1658262251581" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Centrarse exclusivamente en lograr resultados tambi&#233;n dificulta que el proveedor de servicios realice un seguimiento de los cambios en el entorno del cliente y ajuste los servicios en consecuencia.&#160;&#160;Adem&#225;s, el potencial de insatisfacci&#243;n del cliente es mucho mayor, ya que cualquier queja sobre el servicio que no cumple los requerimientos es justificada con la respuesta estandar &quot;pero nosotros entregamos lo que dice el acuerdo de nivel de servicio&quot;. En su lugar, el proveedor de servicio debe continuamente tomar en cuenta si el servicio esta cumpliendo las necesidades del cliente, especialmente cuando esas necesidades cambian. Esta es una de las razones por las que se debe realizar revisiones de los niveles de servicio frecuentes.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="El negocio puede realizar actividades que cumplen objetivos de negocio" ID="ID_445291326" CREATED="1658262348038" MODIFIED="1658262584833" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los resultados se logran cuando la empresa es capaz de realizar actividades que cumplen los objetivos empresariales. Los resultados pueden verse limitados por la presencia de restricciones; por ejemplo, los procesos manuales tardan demasiado y son demasiado costosos para que la organizaci&#243;n sea competitiva. Los servicios mejoran el rendimiento de las actividades clave y reducen los l&#237;mites de las limitaciones. El resultado es un aumento en la posibilidad de lograr los resultados deseados. Mientras que algunos servicios mejoran el rendimiento de las tareas (por ejemplo, el correo electr&#243;nico), otros tienen un impacto m&#225;s directo. Estos servicios pueden realizar la tarea por s&#237; mismos; por ejemplo, un servicio de pedidos en l&#237;nea automatizado que reemplaza los procesos manuales que consumen mucho tiempo.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Los resultados del negocio son definidos en terminos medibles y practicos" FOLDED="true" ID="ID_195329562" CREATED="1658262734667" MODIFIED="1658262999372" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los resultados del negocio se definen en t&#233;rminos pr&#225;cticos y mensurables. Aunque algunos resultados comerciales se refieren a resultados estrat&#233;gicos (p. ej., hacer crecer el negocio en un 15 % durante los pr&#243;ximos tres a&#241;os), la mayor&#237;a de los resultados a los que se hace referencia en esta publicaci&#243;n son m&#225;s t&#225;cticos y operativos.
    </p>
  </body>
</html></richcontent>
<node TEXT="Los ejemplos incluyen:" ID="ID_70601986" CREATED="1658262820396" MODIFIED="1658262992931"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Una unidad de negocio procesa una orden de venta.
      </li>
      <li>
        Se entrega un producto o servicio a un cliente.
      </li>
      <li>
        Se paga el salario de un empleado.
      </li>
      <li>
        Se env&#237;a un informe financiero a un organismo regulador.
      </li>
      <li>
        Una persona de ventas puede obtener informaci&#243;n sobre un cliente y usarla para identificar oportunidades.
      </li>
      <li>
        Se expide una licencia de conducir a un miembro del p&#250;blico.
      </li>
      <li>
        Se recaudan impuestos.
      </li>
      <li>
        Un conductor puede pagar un peaje y conducir en una carretera de peaje.
      </li>
      <li>
        La carga se env&#237;a en un barco o avi&#243;n.
      </li>
      <li>
        Un pasajero de una aerol&#237;nea hace una reserva y recibe una tarjeta de embarque.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Definir y documentar las salidas" ID="ID_1154220208" CREATED="1658263012273" MODIFIED="1658263843749" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La tarea de definir y documentar los resultados recae en las unidades de negocio que los alcanzan. La gesti&#243;n de relaciones con el negocio puede ayudar a definirlos y comunicarlos al proveedor de servicios. Incluso cuando el proveedor del servicio es una empresa diferente, es &#250;til para ellos comprender para qu&#233; usar&#225;n los clientes el servicio. Esta los asiste priorizando sus actividades, ajustando sus servicios sobre el tiempo y contribuyendo a la satisfacci&#243;n de los clientes.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Tabla 3.3 Diferencia entre salidas y resultados" FOLDED="true" ID="ID_80994403" CREATED="1658263848621" MODIFIED="1658264333416" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Para resumir esta discusion, ejemplos de las diferencias entre salidas y resultados se proveen en la tabla 3.3
    </p>
  </body>
</html></richcontent>
<node ID="ID_297362520" CREATED="1658263947776" MODIFIED="1658264327811"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 100%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Salidas
          </p>
        </th>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Resultados
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Lo que entrega el proveedor de servicio, ej: Un reporte estandar.
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Lo que el cliente hace con la salida, ej: El reporte es usado para dar seguimiento los niveles de inventario.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Medido en termino de rendimiento (ej: uptime)
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Medido en terminos de si el resultado fue alcanzado.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Fue entregado el servicio?
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Podria el servicio ser utilizado para alcanzar el resultado?
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Cumplimiento a un est&#225;ndar o nivel de desempe&#241;o
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El Resultado podr&#237;a necesitar niveles variables de desempe&#241;o
          </p>
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="3.2.2.2 Responsabilidad de costos y riesgos especificos" FOLDED="true" ID="ID_1730881539" CREATED="1658189564748" MODIFIED="1658350445448" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los clientes est&#225;n preocupados por lo que les costar&#225; un servicio y qu&#233; tan confiable ser&#225;. Sin embargo, las buenas relaciones con los clientes no dependen de que el cliente conozca todos los gastos y las medidas de mitigaci&#243;n de riesgos realizadas por el proveedor de servicios.
    </p>
  </body>
</html></richcontent>
<node TEXT="Interes principal en resultados" ID="ID_1939636682" CREATED="1658266268370" MODIFIED="1658353913885" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El cliente est&#225; principalmente interesado en el resultado que recibir&#225;, y no necesita preocuparse por los costos espec&#237;ficos y los riesgos espec&#237;ficos en los que tendr&#225; que incurrir el proveedor de servicios para entregar el servicio. El cliente solo estar&#225; expuesto a la suma total del precio del servicio, que incluir&#225; todos los costos del proveedor y las medidas de mitigaci&#243;n de riesgos (y cualquier margen de beneficio si corresponde). El cliente puede entonces juzgar el valor del servicio bas&#225;ndose en una comparaci&#243;n de precio y confiabilidad con el resultado deseado.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="El triangulo del servicio" FOLDED="true" ID="ID_153967125" CREATED="1658353920406" MODIFIED="1658363409333" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una buena relaci&#243;n entre un proveedor de servicios y sus clientes depende de que el cliente reciba un servicio que satisfaga sus necesidades, a un nivel aceptable de rendimiento y a un costo que pueda pagar. El proveedor de servicios debe determinar c&#243;mo lograr un equilibrio entre estas tres &#225;reas, y comunicarse con el cliente si hay algo que les impide brindar el servicio requerido al nivel requerido de rendimiento o precio. Esto se ilustra en la Figura 3.4,
    </p>
  </body>
</html></richcontent>
<node TEXT="Figura 3.4 El triangulo del servicio" FOLDED="true" ID="ID_200121753" CREATED="1658364135080" MODIFIED="1658364711804" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La Figura 3.4 muestra c&#243;mo cada servicio se basa en un equilibrio entre el precio, la funcionalidad (lo que hace el servicio) y el rendimiento. Los clientes que deseen pagar menos dinero tendr&#225;n que sacrificar el rendimiento o la funcionalidad, o ambos. Por el contrario, si desean mejorar el rendimiento o aumentar la funcionalidad, tendr&#225;n que pagar m&#225;s. Cada cliente requerir&#225; un equilibrio diferente entre estas tres &#225;reas. Por ejemplo, un cliente puede querer un servicio que solo haga una cosa, pero debe ser altamente disponible y confiable. Otro cliente puede querer un servicio que admita una variedad de actividades, pero no es cr&#237;tico para el negocio, por lo que el precio puede reducirse al tener un rendimiento m&#225;s bajo.
    </p>
  </body>
</html></richcontent>
<node TEXT="" ID="ID_1651740596" CREATED="1658363354899" MODIFIED="1658363365557">
<hook URI="img/The%20service%20triangle.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Reducir costo" ID="ID_1116668111" CREATED="1658364712968" MODIFIED="1658365550830" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Es importante tener en cuenta que solo reducir el rendimiento o la funcionalidad por s&#237; solo no resulta necesariamente en una disminuci&#243;n significativa del precio. Reducir el rendimiento disminuir&#225; la cantidad de dinero gastado en sistemas de mayor rendimiento, pero puede aumentar el costo del soporte y mantenimiento reactivos. La disminuci&#243;n de la funcionalidad puede ahorrar costos de desarrollo y adquisici&#243;n, pero si el nivel de rendimiento sigue siendo alto, el costo unitario del servicio ser&#225; mayor. La mayor&#237;a de las reducciones de costos resultan de una reducci&#243;n tanto en la funcionalidad como en el rendimiento.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Requerimientos" ID="ID_1064096427" CREATED="1658365877998" MODIFIED="1658366155014" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Aparte de indicar sus requisitos y seleccionar el equilibrio &#243;ptimo entre precio, funcionalidad y rendimiento, los clientes no deben participar en la especificaci&#243;n de los detalles t&#233;cnicos de mitigaci&#243;n de riesgos y gastos. Es responsabilidad del proveedor en trabajar en la forma mas eficiente y efectiva de proveer el servicio. Si el cliente quiere un costo m&#225;s bajo, entonces ellos deben estar preparados para reducir sus requerimientos de funcionalidad o desempe&#241;o, o sino renunciar totalmente al servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Garantía y seguridad" ID="ID_167989626" CREATED="1658366211060" MODIFIED="1658366388694" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Hay ocasiones en las que un proveedor de servicios necesita tranquilizar al cliente sobre el nivel de rendimiento o seguridad de un producto o servicio. En estos casos, el proveedor de servicios puede proporcionar alguna informaci&#243;n para tranquilizar al cliente. Por ejemplo, los autom&#243;viles son probados por organizaciones de garant&#237;a de seguridad y reciben una calificaci&#243;n de seguridad. Los clientes consideran la calificaci&#243;n y algunas caracter&#237;sticas b&#225;sicas&#160;de seguridad cuando compran un coche, pero ellos no tienen que entender todos los factores de mitigaci&#243;n de riesgo que el fabricante del coche ha dise&#241;ado y a puesto en el coche.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Ejemplo" ID="ID_1303955640" CREATED="1658366430760" MODIFIED="1658366636367" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un ejemplo de esto es una unidad comercial que necesita un terabyte de almacenamiento seguro para respaldar su sistema de compras en l&#237;nea. Desde una perspectiva estrat&#233;gica, quiere que el personal, el equipo, las instalaciones y la infraestructura para un terabyte de almacenamiento permanezcan dentro de su alcance de control. Sin embargo, no quiere ser responsable de todos los costos y riesgos asociados, reales o nominales, reales o percibidos. Afortunadamente, existe un grupo de TI dentro de la empresa con conocimiento especializado y experiencia en sistemas de almacenamiento a gran escala, y la confianza para controlar los costos y riesgos asociados. La unidad de negocio se compromete a pagar por el servicio de almacenamiento prestado por el grupo bajo unas especificaciones y condiciones espec&#237;ficas.
    </p>
    <p>
      
    </p>
    <p>
      La unidad de negocios sigue siendo responsable del cumplimiento de las &#243;rdenes de compra en l&#237;nea. No se responsabiliza por la operaci&#243;n y mantenimiento de configuraciones tolerantes a fallas de dispositivos de almacenamiento, fuentes de alimentaci&#243;n dedicadas y redundantes, personal calificado, ni la seguridad del per&#237;metro del edificio, gastos administrativos, seguros, cumplimiento de normas de seguridad, medidas de contingencia o la optimizaci&#243;n problema de la capacidad ociosa por aumentos inesperados de la demanda. La complejidad del dise&#241;o, las incertidumbres operativas y las compensaciones t&#233;cnicas asociadas con el mantenimiento de sistemas de almacenamiento confiables y de alto rendimiento generan costos y riesgos que la unidad comercial simplemente no est&#225; dispuesta a asumir. El proveedor de servicios asume la propiedad y asigna esos costos y riesgos a cada unidad de almacenamiento utilizada por la empresa y cualquier otro cliente del servicio de almacenamiento.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="3.2.2.3 Servicios internos y externos" FOLDED="true" ID="ID_1647264751" CREATED="1658189592410" MODIFIED="1658375461767" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Así como hay clientes internos y externos, existen servicios internos y externos. Los servicios internos se entregan entre departamentos o unidades de negocios en la misma organización. Los servicios externos se entregan a clientes externos.
    </p>
    <p>
      
    </p>
    <p>
      La razón para diferenciar entre servicios internos y externos es diferenciar entre los servicios que respaldan una actividad interna y aquellos que realmente logran resultados comerciales. Es posible que la diferencia no parezca significativa al principio, ya que la actividad para prestar los servicios suele ser similar. Sin embargo, es importante reconocer que los servicios internos deben estar vinculados a los servicios externos antes de poder comprender y medir su contribución a los resultados comerciales. Esto es especialmente importante cuando se mide el retorno de la inversión de los servicios (ver sección 3.6.1).
    </p>
    <p>
      
    </p>
    <p>
      La figura 3.5 muestra la diferencia entre servicios internos y externos para un proveedor de servicios de TI. Estos se describen con más detalle en las siguientes dos secciones.
    </p>
  </body>
</html></richcontent>
<node TEXT="Figura 3.5 Servicios Internos y Externo" FOLDED="true" ID="ID_999924459" CREATED="1658375625315" MODIFIED="1658375651886">
<node TEXT="Servicios Internos y Externos" ID="ID_1806761272" CREATED="1658375659470" MODIFIED="1658375689623">
<hook URI="ITIL2011_files/png_9809817072476648062.png" SIZE="0.7614213" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Servicios TI" ID="ID_154649389" CREATED="1658375465400" MODIFIED="1658375710487" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un servicio de TI es un servicio proporcionado a uno o más clientes por un proveedor de servicios de TI. Un servicio de TI se basa en el uso de tecnología de la información y apoya los procesos comerciales del cliente. Se compone de una combinación de personas, procesos y tecnología.
    </p>
    <p>
      
    </p>
    <p>
      Hay tres tipos de servicios de TI, como se muestra en la Tabla 3.4.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Tabla 3.4 Tipos de servicio TI" FOLDED="true" ID="ID_1989454672" CREATED="1658375712269" MODIFIED="1658375734591">
<node ID="ID_132222518" CREATED="1658375808671" MODIFIED="1658377488425" FORMAT="STANDARD_FORMAT"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 100%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Tipo de Servicio
          </p>
        </th>
        <th valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Definición
          </p>
        </th>
        <th valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Descripción
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            <b>Servicio de apoyo</b>, a veces llamado servicio de infraestructura, aunque a menudo son más amplios que solo infraestructura.
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Un servicio que no utiliza directamente la empresa, pero que el proveedor de servicios de TI requiere para poder proporcionar otros servicios de TI, por ejemplo, servicios de directorio, servicios de nombres, la red o servicios de comunicación.
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los servicios de soporte se definen para permitir que los equipos de TI identifiquen las interdependencias entre los componentes de TI. También mostrarán cómo se utilizan estos componentes para brindar servicios de atención al cliente internos y externos. Los servicios de soporte permiten procesos y servicios de TI, pero no son directamente visibles para el cliente. Algunos equipos de TI ven a los destinatarios de los servicios de soporte como &quot;clientes&quot;. Aunque esto promueve la buena calidad del servicio, también es engañoso. Los servicios de apoyo solo existen para combinarse con otros servicios de apoyo para producir servicios orientados al cliente. Si no pueden, no tienen ningún valor y se debe cuestionar su existencia. No puede haber acuerdos de nivel de servicio para los servicios de apoyo, ya que todos son internos del mismo departamento. En su lugar, <b>el rendimiento de los servicios de apoyo debe gestionarse mediante acuerdos de nivel operativo</b>. Cabe señalar que la Figura 3.5 solo se refiere a los servicios que se originan dentro de la organización. En algunos casos, <b>los servicios de apoyo provienen de fuera de la organización</b>. En estos casos, se gestionan de la misma manera que otros servicios de apoyo, pero utilizando <b>contratos de apoyo en lugar de acuerdos de nivel operativo.</b>
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            <b>Servicio de cara al cliente interno</b>
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Un servicio TI que soporta directamente un procesos de negocio administrado por otra unidad de negocio - ej: servicio de reporte de ventas, gestión de recursos empresariales.
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Un servicio de cara al cliente interno es identificado y definido por el negocio. Si la empresa no puede percibirlo como un servicio, entonces es probable que sea un servicio de apoyo.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los servicios orientados al cliente interno se basan en un conjunto integrado de servicios de apoyo, aunque a menudo el cliente o usuario no los ve ni los comprende.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            <b>Los servicios de cara al cliente interno se gestionan de acuerdo con los acuerdos de nivel de servicio.</b>
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            <b>Servicio de cara al cliente externo</b>
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Un servicio TI que es proveído directamente por TI a un cliente externo - ej: Acceso a Internet en un aeropuerto.
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Un servicio de cara al cliente externo está disponible para los clientes externos y se ofrece para cumplir los objetivos comerciales definidos en la estrategia de la organización.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Un servicio de TI orientado al cliente externo también es un servicio comercial por derecho propio, ya que se utiliza para llevar a cabo el negocio de la organización con clientes externos.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Dependiendo de la estrategia de la organización, el servicio se brinda sin cargo (muchas agencias gubernamentales brindan servicios al público sin cargo) o se factura directamente a la persona u organización que utiliza el servicio. En otros casos, el servicio puede ser prestado gratuitamente al cliente, pero pagado por un tercero, como un anunciante o patrocinador. <b>Estos servicios se gestionan mediante un contrato</b>; incluso un simple acuerdo en línea constituye un contrato de compraventa con términos y condiciones.
          </p>
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Nota sobre servicios de cara al cliente externo" ID="ID_127205838" CREATED="1658377557099" MODIFIED="1658378018685" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las organizaciones internas de TI no son los únicos proveedores de servicios externos a clientes. Proveedores externos, proveedores de servicio de Internet y proveedores de servicios en la nube son ejemplos de organizaciones que están en el negocio de proveer servicios externos - y los departamentos de tecnología que proveen esos servicios son unidades de negocio, soportadas por los proveedores de servicio interno de TI.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Integración de servicios" ID="ID_1674253071" CREATED="1658378061496" MODIFIED="1658378249743" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un proceso empresarial puede distribuirse entre tecnologías y aplicaciones, abarcar geografías, tener muchos usuarios y aún así residir completamente en el centro de datos. Para integrar los procesos comerciales, TI emplea con frecuencia la integración de abajo hacia arriba, uniendo un mosaico de tecnología y componentes de aplicaciones que nunca fueron diseñados para interactuar en la capa de procesos comerciales. Lo que comienza como un elegante diseño empresarial de arriba hacia abajo, con frecuencia se deteriora hasta convertirse en una solución de TI inconexa e inflexible, desconectada de los objetivos del negocio.
    </p>
    <p>
      
    </p>
    <p>
      Una mejor estrategia para respaldar estos procesos comerciales es comenzar definiendo los resultados (consulte la siguiente sección) y luego identificar los servicios de TI que los respaldan, y luego definir cómo se alinearán los servicios de respaldo para respaldar toda la cadena de dependencias.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Servicios de negocio y productos proveídos por otras unidades de negocio" ID="ID_1197161150" CREATED="1658378254549" MODIFIED="1658378483342" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un servicio de negocio se define como un servicio que las unidades del negocio brindan a los clientes externos; por ejemplo, la entrega de servicios financieros a los clientes de un banco, o bienes a los clientes de una tienda minorista. La entrega exitosa de servicios del negocio a menudo depende de uno o más servicios de TI.
    </p>
    <p>
      
    </p>
    <p>
      Aunque TI no es directamente responsable de los servicios y productos de la empresa, es responsable de proporcionar los servicios de TI que permitirán alcanzar los resultados. Por lo tanto, es importante que TI sepa cuáles son estos servicios, cómo utiliza la empresa los servicios de TI y cómo se miden estos servicios. Esto tendrá un impacto directo en la forma en que se cumple la contribución de TI a la organización.
    </p>
    <p>
      
    </p>
    <p>
      Una forma de hacerlo es definir las actividades de negocio necesarias para producir los resultados como funciones de negocio vitales. Estos se discuten más a fondo en la sección 4.4 en la gestión de la disponibilidad en el diseño del servicio.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.2.2.4 Servicios, principales, habilitadores y mejorados" ID="ID_892360384" CREATED="1658189607173" MODIFIED="1658497756753" LINK="#ID_1759042357" TEXT_SHORTENED="true">
<arrowlink DESTINATION="ID_1759042357"/>
<richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Todos los servicios, ya sean internos o externos, se pueden clasificar en términos de cómo se relacionan entre sí y con sus clientes. <b>Los servicios se pueden clasificar como básicos, habilitadores o de mejora,</b>&nbsp;&nbsp;y se definen en la sección 2.1.1. En la Tabla 3.5 se proporcionan ejemplos de estos servicios.
    </p>
    <p>
      
    </p>
    <p>
      Para ilustrar esto en otro contexto, los servicios centrales de un banco podrían ser proporcionar capital financiero a pequeñas y medianas empresas. Se crea valor para los clientes del banco solo cuando el banco puede proporcionar capital financiero de manera oportuna (después de haber evaluado todos los costos y riesgos de financiar al prestatario).
    </p>
    <p>
      
    </p>
    <p>
      Los servicios habilitadores podrían ser:
    </p>
    <ul>
      <li>
        Ayuda ofrecida por los oficiales de crédito para evaluar las necesidades de capital de trabajo y la garantía
      </li>
      <li>
        El servicio de procesamiento de solicitudes
      </li>
      <li>
        Desembolso flexible de los fondos del préstamo
      </li>
      <li>
        Una cuenta bancaria a la que el prestatario puede transferir fondos electrónicamente.
      </li>
    </ul>
    <p>
      Como factores básicos, los servicios habilitadores solo le dan al proveedor la oportunidad de servir al cliente. Los servicios habilitadores son necesarios para que los clientes utilicen los servicios básicos de manera satisfactoria. Los clientes generalmente dan por sentado tales servicios y no esperan que se les cobre adicionalmente por el valor de dichos servicios. Ejemplos de servicios de habilitación que se ofrecen comúnmente son las mesas de servicio, pago, registro y servicios de directorio.
    </p>
    <p>
      
    </p>
    <p>
      En la mayoría de los mercados, los servicios de habilitación permitirán los requisitos mínimos para la operación, aunque muchos proporcionan la base para la diferenciación, pero son los servicios de mejora los que proporcionarán la diferenciación en sí: el &quot;factor de entusiasmo&quot;.
    </p>
    <p>
      
    </p>
    <p>
      Los ejemplos de servicios de mejora son más difíciles de proporcionar, particularmente porque tienden a desviarse con el tiempo para subsumirse en servicios básicos o habilitadores. En otras palabras, lo que es emocionante para un cliente hoy se convierte en algo esperado si siempre se entrega.
    </p>
    <p>
      
    </p>
    <p>
      Un ejemplo es la prestación de un servicio de Internet de banda ancha en una habitación de hotel. Hace unos años, la prestación de un servicio de banda ancha de pago podría haber sido considerado como un diferenciador (este hotel ofrece este servicio, otros hoteles comparativos no lo hacen). A medida que más y más hoteles comenzaron a ofrecer este servicio, los clientes lo consideraron esencial, por lo que se convirtió en un servicio habilitador. Luego, los hoteles comenzaron a ofrecer servicios de Internet de banda ancha &quot;gratuitos&quot;, por lo que durante un tiempo fue un servicio mejorado, pero ahora es más común y se está convirtiendo rápidamente en un servicio necesario (y, por lo tanto, habilitador). Para algunos viajeros, este servicio se ha convertido en realidad en parte esencial, de la misma manera que, por ejemplo, un baño privado.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Tabla 3.5 Ejemplos de servicios principales, habilitadores y mejorados" ID="ID_108743924" CREATED="1658499297967" MODIFIED="1658499756921" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            
          </p>
        </th>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Servicio Principal
          </p>
        </th>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Servicio Habilitador
          </p>
        </th>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Servicio Mejorado
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Servicios TI (Automatización de oficina)
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Procesador de palabras (Word)
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Descarga e instalación de actualizaciones
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Publicación de documentos para impresoras profesionales para revistas de alta calidad.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Servicios TI (Seguimiento de beneficios)
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los empleados de una compañía pueden monitorear el estado de sus beneficios (tales como seguros de salud y cuentas de retiro)
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Un portal que provee un acceso de usuario final amigable para acceso al servicio de seguimiento de beneficios.
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los clientes pueden crear y administrar programa de fitness o pérdida de peso. Los clientes que muestran su progreso en su programa son premiados con descuentos en sus compras.
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.2.3 Valor" FOLDED="true" ID="ID_1244407609" CREATED="1658189643394" MODIFIED="1658498972126" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Se puede considerar que el valor de un servicio es el nivel en el que ese servicio cumple con las expectativas del cliente. A menudo se mide por cuánto está dispuesto a pagar el cliente por el servicio, en lugar del costo del servicio o cualquier otro atributo intrínseco del servicio en sí.
    </p>
    <p>
      
    </p>
    <p>
      A diferencia de los productos, los servicios no tienen mucho valor intrínseco. El valor de un servicio proviene de lo que le permite a alguien hacer. El valor de un servicio no lo determina el proveedor, sino la persona que lo recibe, porque deciden qué harán con el servicio y qué tipo de retorno obtendrán al utilizar el servicio.
    </p>
  </body>
</html></richcontent>
<node TEXT="Características del valor" ID="ID_197901370" CREATED="1658498405281" MODIFIED="1658498435640" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        <b>El valor lo definen los clientes</b>&nbsp;No importa cuánto anuncie el proveedor de servicios el valor de sus servicios, la decisión final sobre si ese servicio es valioso o no, recae en el cliente.
      </li>
      <li>
        <b>Combinación asequible de funciones</b>&nbsp;Es posible influir en la percepción de valor del cliente a través de la comunicación y la negociación, pero eso no cambia el hecho de que el cliente seguirá tomando la decisión final sobre lo que es valioso para él. Un buen vendedor puede convencer a un cliente de cambiar las prioridades que influyen en su compra, pero el cliente seleccionará el servicio o producto que represente la mejor combinación de características al precio que está dispuesto a pagar.
      </li>
      <li>
        <b>Logro de objetivos</b>&nbsp;Los clientes no siempre miden el valor en términos financieros, aunque pueden indicar cuánto están dispuestos a pagar por un servicio que les ayuda a obtener el resultado deseado. Muchos servicios no están diseñados para producir ingresos. sino para cumplir con algún otro objetivo organizacional, como programas de responsabilidad social o gestión de recursos humanos. Mientras que las organizaciones comerciales tienden a medir la mayoría de los servicios por los rendimientos financieros, las organizaciones gubernamentales tienden a centrarse en otros objetivos. Por ejemplo, la policía podría centrarse en la reducción de la delincuencia o la detención de los delincuentes; los departamentos de bienestar social podrían centrarse en la cantidad de fondos desembolsados a las familias necesitadas; una organización de rescate de montaña podría centrarse en la cantidad de personas advertidas o rescatadas de avalanchas.
      </li>
      <li>
        <b>El valor cambia con el tiempo y las circunstancias</b>&nbsp;Lo que es valioso para un cliente hoy puede no serlo en dos años. A medida que cada cliente cambia para enfrentar los desafíos de su entorno, también lo hacen sus necesidades y valores de servicio. Por ejemplo, los establecimientos minoristas pueden concentrarse en vender un porcentaje más alto de artículos de lujo cuando la economía es buena, pero durante una recesión cambian el enfoque a líneas de productos económicos y menos artículos de lujo.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Percepción de valor del servicio" ID="ID_1508174362" CREATED="1658498949594" MODIFIED="1658498964993" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los servicios aportan valor a una organización sólo cuando se percibe que su valor es mayor que el costo de obtener el servicio. Por lo tanto, comprender el valor de TI requiere tres piezas de información:
    </p>
    <ul>
      <li>
        <b>¿Qué servicio(s) proporcionó TI?</b>&nbsp;Si la TI solo se percibe como la gestión de un conjunto de servidores, redes y PCs, será muy difícil para el cliente comprender cómo estos contribuyeron al valor. Para que un cliente calcule el valor de un servicio, debe poder discernir un servicio específico y discreto y vincularlo directamente con actividades y resultados comerciales específicos. Por ejemplo, una organización de TI afirma que el alojamiento de aplicaciones ofrece valor a la empresa. La empresa, sin embargo, no sabe qué es el alojamiento de aplicaciones ni qué aplicaciones están alojadas. Si la organización de TI quiere comunicar su valor, debe ser capaz de identificar lo que el cliente realmente percibe, y entonces enlazar sus actividades a ese servicio. El portafolio de servicios, y el catalogo de servicios en particular, ayudará a TI a cuantificar el servicio.
      </li>
      <li>
        <b>¿Que alcanzó el servicio?</b>&nbsp;El cliente identificará que pudo hacer con el servicio, y cuan importante fue para ellos y su organización.
      </li>
      <li>
        <b>¿Cuánto costo el servicio - o cuánto es el precio del servicio?</b>&nbsp;&nbsp;Cuando un cliente compara el costo o precio de un servicio con que es lo que el servicio es capaz de realizar, ellos podrán juzgar cuan valioso fue el servicio realmente. Si TI es incapaz de determinar el costo del servicio, sera muy difícil para ellos reclamar por el valor entregado, y muy difícil para el cliente percibir a TI como &quot;valioso&quot;.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="3.2.3.1 Creacion de valor" FOLDED="true" ID="ID_498272524" CREATED="1658189655662" MODIFIED="1658499979013" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Calcular el valor de un servicio a veces puede ser sencillo en términos financieros. Por ejemplo, un cliente necesita un servicio para respaldar la venta de una nueva línea de productos. Si el servicio hace lo que se requiere, y su costo no impacta negativamente en la rentabilidad de la línea de productos, y el precio sigue siendo competitivo, lo más probable es que el cliente lo perciba como valioso.
    </p>
    <p>
      
    </p>
    <p>
      En los casos en que los resultados no son financieros, es más difícil cuantificar el valor, aunque aún es posible calificarlo. Por ejemplo, un departamento de administración de la ciudad necesita un servicio que les permita rastrear el tráfico en el centro de una ciudad para que puedan ajustar las señales de tráfico para mejorar el flujo de tráfico. Si el servicio les permite hacer esto y se ajusta al presupuesto de la ciudad, lo más probable es que se perciba como valioso.
    </p>
    <p>
      
    </p>
    <p>
      Sin embargo, hay más que valorar que solo la función del servicio y su costo. El valor debe definirse en términos de tres áreas: los resultados obtenidos para el negocio, las preferencias del cliente y la percepción del cliente de lo que fue entregado. Esto se muestra en la Figura 3.6.
    </p>
    <p>
      
    </p>
    <p>
      El valor se define no solo estrictamente en términos de los resultados comerciales del cliente; también depende en gran medida de las percepciones y preferencias del cliente. Las percepciones están influenciadas por los atributos de un servicio, las experiencias presentes o previas con atributos similares y la capacidad relativa de los competidores y otros pares. Las percepciones también se ven influidas por la imagen que el cliente tiene de sí mismo o por su posición real en el mercado, como por ejemplo, ser un innovador, un líder del mercado y un tomador de riesgos. El valor de un servicio toma muchas formas y los clientes tienen preferencias influenciadas por sus percepciones. Las preferencias y percepciones de los clientes afectarán la forma en que diferencian el valor de una oferta o proveedor de servicios sobre otro.
    </p>
    <p>
      
    </p>
    <p>
      Cuanto más intangible es el valor, más importantes se vuelven las definiciones y la diferenciación del valor. Los clientes son reacios a comprar cuando existe ambigüedad en la relación de causa y efecto entre la utilización de un servicio y la obtención de beneficios. Los proveedores de servicios deben proporcionar a los clientes información para influir en su percepción del valor, influyendo en las percepciones y respondiendo a las preferencias.
    </p>
  </body>
</html></richcontent>
<node TEXT="Entendiendo las percepciones de valor del cliente" ID="ID_1060267366" CREATED="1658499989491" MODIFIED="1658500552166" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Aunque un proveedor de servicios no puede decidir el valor de un servicio, puede influir en cómo los clientes perciben el valor del servicio. La Figura 3.7 ilustra cómo los clientes perciben el valor (según Nagle y Holden, 2002). En este diagrama, el punto de partida para la percepción del cliente es el valor de referencia. Esto podría basarse en lo que el cliente ha oído sobre el servicio, o en el hecho de que el cliente está realizando actualmente la actividad por sí mismo, o en alguna experiencia previa de ese servicio u otro similar.
    </p>
    <p>
      
    </p>
    <p>
      El valor de referencia puede estar vagamente definido o basado en hechos concretos. Un ejemplo de valor de referencia es la línea de base que los clientes mantienen sobre el costo de las funciones o servicios internos (la estrategia DIY o hágalo usted mismo). Es importante que el proveedor de servicios comprenda y tenga una idea de cuál es este valor de referencia. Esto se puede obtener a través de un diálogo extenso con el cliente, experiencia previa con el mismo o un cliente similar o investigación y análisis disponibles en el mercado.
    </p>
    <p>
      
    </p>
    <p>
      La diferencia positiva del servicio se basa en los beneficios y ganancias adicionales percibidos proporcionados por el proveedor del servicio. Estas diferencias se basan en la garantía adicional y la utilidad que el proveedor de servicios puede ofrecer. Aquí es donde el proveedor de servicios puede influir más en la percepción del cliente. Esto requiere un enfoque de marketing, que se analiza en la siguiente sección.
    </p>
    <p>
      
    </p>
    <p>
      La diferencia negativa del servicio es la percepción de lo que el cliente perdería al invertir en el servicio. Por ejemplo, pueden percibir algunos problemas de calidad o costos ocultos. Es posible que el servicio no tenga la funcionalidad completa que les gustaría al precio solicitado. El proveedor de servicios también puede influir en esta área al escuchar los requisitos del cliente y hacer coincidir las características del servicio con ellos. También pueden tratar de influir en las prioridades del cliente al enfatizar las fortalezas del servicio.
    </p>
    <p>
      
    </p>
    <p>
      La diferencia neta es la percepción real que tiene el cliente de cuánto mejor (o peor) es el servicio que el valor de referencia después de descontar la diferencia negativa. Esta es el área que impulsará la decisión del cliente de invertir o no en el servicio.
    </p>
    <p>
      
    </p>
    <p>
      El valor económico es el valor total que el cliente percibe del servicio a entregar. Incluye el valor de referencia más (o menos) la diferencia neta del servicio que reciben, y es medido por el cliente en la habilidad para cumplir con los resultados esperados.
    </p>
    <p>
      
    </p>
    <p>
      Centrarse en los resultados comerciales por encima de todo lo demás es un avance fundamental en las perspectivas para muchos proveedores de servicios. Representa un cambio de énfasis de la utilización eficiente de los recursos a la realización efectiva de los resultados. La eficiencia en las operaciones está impulsada por la necesidad de efectividad para ayudar a los clientes a obtener resultados. <b><u>Los clientes no compran servicios; compran la satisfacción de necesidades particulares</u></b>. Esta distinción explica la frecuente desconexión entre las organizaciones de TI y las empresas a las que sirven. <b><u>Lo que el cliente valora es con frecuencia diferente de lo que la organización de TI cree que proporciona</u></b>.
    </p>
    <p>
      
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Mentalidad de Mercado" ID="ID_1713296524" CREATED="1658500564271" MODIFIED="1658500998173" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Comprender y comunicar el valor requiere que el proveedor de servicios aborde la relación con una mentalidad de marketing. El marketing en este contexto se refiere no solo a los servicios publicitarios para influir en la percepción del cliente, sino también a comprender el contexto y los requisitos del cliente, y garantizar que los servicios estén orientados a lograr los resultados que son importantes para el cliente.
    </p>
    <p>
      
    </p>
    <p>
      ¿Cuáles son los resultados que importan? ¿Cómo se identifican y clasifican en términos de percepciones y preferencias de los clientes? La efectividad para responder tales preguntas requiere una mentalidad de marketing, que es bastante diferente de la mentalidad de ingeniería y operaciones. En lugar de centrarse en la producción de servicios hacia adentro, es necesario mirar desde afuera hacia adentro, desde la perspectiva del cliente. Una mentalidad de marketing comienza con preguntas simples:
    </p>
    <ul>
      <li>
        ¿Cuál es nuestro negocio?
      </li>
      <li>
        ¿Quién es nuestro cliente?
      </li>
      <li>
        ¿Qué valora el cliente?
      </li>
      <li>
        ¿Quién depende de nuestros servicios?
      </li>
      <li>
        ¿Cómo utilizan nuestros servicios?
      </li>
      <li>
        ¿Por qué son valiosos para ellos?
      </li>
    </ul>
    <p>
      El valor se puede agregar en diferentes niveles. Lo que importa es la diferencia neta (Figura 3.7). Por ejemplo, los proveedores de servicios se diferencian de los proveedores de equipos simplemente por el valor agregado, incluso cuando utilizan los equipos de esos mismos proveedores como activos. La diferenciación puede surgir al proporcionar servicios de comunicación en lugar de enrutadores y servidores. Puede ser necesaria una mayor diferenciación obtenida en la provisión de servicios en lugar de simplemente operar los servicios de correo electrónico y buzón de voz. <b><u>El cambio de enfoque desde atributos de servicio a satisfacción de necesidades.</u></b>&nbsp;Con una mentalidad de marketing es posible entender los componentes de valor desde la perspectiva del cliente.
    </p>
  </body>
</html></richcontent>
<font BOLD="true"/>
</node>
</node>
<node TEXT="Figura 3.6 Componentes de valor" ID="ID_609007146" CREATED="1658499202226" MODIFIED="1658516238110" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/png_227189247087266631.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.7 Como los clientes perciben el valor" ID="ID_422165843" CREATED="1658500220128" MODIFIED="1658516213359" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Como%20los%20clientes%20perciben%20el%20valor.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="3.2.3.2 Valor anadido y valor realizado" FOLDED="true" ID="ID_1969676731" CREATED="1658189678331" MODIFIED="1658513097012" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Para describir mejor la relación entre costos y valor, Porter (1996) introdujo el concepto de cadenas de valor. Una cadena de valor es una secuencia de procesos que crea un producto o servicio que es de valor para un cliente. Cada paso de la secuencia se basa en los pasos anteriores y contribuye al producto o servicio general. En la Figura 3.8 se representa una cadena de valor de TI simple.
    </p>
    <p>
      
    </p>
    <p>
      En la Figura 3.8, se utilizan varios componentes para prestar un servicio, y cada componente es administrado por un departamento de TI. El departamento de administración de la base de datos administra la base de datos, el departamento de administración de la aplicación administra la aplicación, el departamento de hospedaje de la aplicación se encarga del alojamiento de la aplicación, etc.
    </p>
    <p>
      
    </p>
    <p>
      Se gasta dinero en adquirir, desarrollar y mantener cada componente, y cada departamento gasta dinero en salarios, espacio de oficina, beneficios, etc. Como cada departamento administra su componente y se asegura de que esté funcionando de manera efectiva, agrega valor al servicio. Por ejemplo, una base de datos por sí sola tiene un valor relativamente pequeño, pero una aplicación combinada con una base de datos tiene un valor superior al costo de los dos componentes. Cuando se combina con el alojamiento de aplicaciones, el servicio se vuelve aún más valioso.
    </p>
    <p>
      
    </p>
    <p>
      El secreto para agregar valor es que cada vez que se agrega un paso más al servicio, el valor del servicio debe crecer a una tasa mayor que la cantidad de dinero gastado (tanto en la inversión inicial como en los costos continuos incrementales agregados en cada paso). En otras palabras, después de cada paso en la cadena de valor, el proveedor de servicios debería poder preguntarse: &quot;Si vendiéramos lo que tenemos ahora, ¿sería de mayor valor que lo que hemos gastado en él?&quot;.
    </p>
    <p>
      
    </p>
    <p>
      Una analogía podría ser útil aquí. Un restaurante compra materias primas. Podría vender esos ingredientes por una pequeña ganancia si no hay otros proveedores en el área. Si el restaurante gasta algo de dinero en combinar y cocinar las materias primas en platos sabrosos, podrá cobrar mucho más por la comida que el costo real de los ingredientes, la electricidad, etc. Servir bebidas que complementen las comidas aumentará el valor incluso más.
    </p>
    <p>
      
    </p>
    <p>
      <b><u>Una característica importante de una cadena de valor es que el verdadero valor de un servicio solo se puede calcular después de que se haya realizado el valor. </u></b>En la Figura 3.8, esto sucede cuando el cliente logra el resultado deseado. En un modelo comercial, el proveedor de servicios solo puede confirmar que se agregó valor si el cliente pagó más por el servicio que lo que costó realmente. En el sector público el cliente externo es usualmente quien paga impuestos.
    </p>
  </body>
</html></richcontent>
<node TEXT="Hay algunas reglas para añadir valor que se detalla a continuación:" ID="ID_1242446465" CREATED="1658513103111" MODIFIED="1658513295218" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        La cantidad de valor agregado solo se puede calcular una vez que se ha realizado el valor (una vez que el servicio ha logrado el resultado deseado).
      </li>
      <li>
        El valor realizado tiene que ser mayor que el dinero gastado. Esto es cierto ya sea que se mida en términos financieros (por ejemplo, cuánto dinero pagó el cliente por el servicio) o en términos no financieros (por ejemplo, ¿podría la agencia gubernamental procesar la cantidad planificada de solicitudes de licencias de conducir por día?). En estos casos, el valor no financiero del resultado se compara con los costos financieros del servicio. Luego se toma una decisión sobre si el servicio vale la pena o no.
      </li>
      <li>
        Si el valor realizado no es mayor que el dinero gastado, entonces el proveedor de servicios no ha agregado ningún valor. Más bien, simplemente han gastado dinero (y han tenido pérdidas).
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Estas reglas aplican también a organizaciones TI. Específicamente:" ID="ID_978796540" CREATED="1658513297657" MODIFIED="1658513458193" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Si TI quiere demostrar que ha agregado valor, debe vincular sus actividades a donde el negocio obtiene valor.
      </li>
      <li>
        Si Ti no puede hacer esto, será percibida como una organización que gasta dinero, no como una organización que agrega valor.
      </li>
      <li>
        La única forma en que una organización que gasta dinero puede demostrar valor es reduciendo los costos (así se supone que aumenta el margen de beneficio). Esto resulta en un círculo vicioso. TI no se percibe como un valor agregado, por lo que el negocio exige que se reduzcan los costos. Cuando recortan costos, su capacidad para agregar valor se reduce aún más, y el negocio exige aún más reducción de costos.
      </li>
    </ul>
    <p>
      Entonces, ¿cómo se asegura TI de que se perciba como un agregador de valor, en lugar de un gastador de dinero? El secreto está en poder vincular las actividades de TI a los servicios y luego vincular esos servicios a los resultados. En otras palabras, TI no debe emprender actividades a menos que pueda demostrar que cada actividad ayuda a lograr el resultado comercial. Esto es descrito en la siguiente sección.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Nota importante sobre las cadenas de valor" ID="ID_136293795" CREATED="1658513491045" MODIFIED="1658513673199" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La Figura 3.8 muestra una cadena de valor simple para demostrar los principios básicos de valor añadido y valor obtenido. En realidad la creación y obtención de valor en las organizaciones (especialmente las grandes) es más complejo, con múltiples cadenas de valor operando al mismo tiempo y a través de componentes comunes. Esa complejidad está mejor representada usando el concepto de redes de valor, las cuales son discutidas en mas detalle en la sección 3.8.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Figura 3.9 Servicios de TI y valor" ID="ID_979863099" CREATED="1658513735144" MODIFIED="1658516246503" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Servicios%20de%20TI%20y%20valor.png" SIZE="0.7662835" NAME="ExternalObject"/>
</node>
<node TEXT="3.2.3.3 Enlazando valor anadido con valor realizado" FOLDED="true" ID="ID_3324276" CREATED="1658189697265" MODIFIED="1658516801709" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Dado que la infraestructura, las aplicaciones y el personal utilizados para prestar los servicios generalmente no son visibles para el cliente, es difícil para ellos comprender cuánto valor se ha agregado a su servicio. Los intentos de describir su inversión al cliente se percibirán como defensivos y un intento de aumentar el precio del servicio. En su lugar, el proveedor de servicios debe centrarse en crear un modelo mediante el cual pueda medir la contribución de cada servicio interno y luego vincularlo con el logro del resultado comercial del cliente.
    </p>
    <p>
      
    </p>
    <p>
      La Figura 3.9 muestra los servicios internos y externos proporcionados a clientes internos y externos por un proveedor de servicios de TI en términos de su valor. Este es el mismo diagrama que la Figura 3.5, pero en lugar de mostrar el tipo de servicio, muestra cuáles de estos servicios agregan o obtienen valor, y cuáles se clasifican como &quot;dinero gastado&quot;.
    </p>
    <p>
      
    </p>
    <p>
      El valor se realiza cuando se proporciona un servicio a un cliente externo y cumple con sus expectativas. El valor se mide en términos de si se ha alcanzado un resultado comercial. En las organizaciones comerciales, esto suele medirse como beneficio o margen. En las agencias gubernamentales y sin fines de lucro, esto se mide más a menudo en términos no financieros, como se describe en la sección 3.2.3.1.
    </p>
    <p>
      
    </p>
    <p>
      En la Figura 3.9, los servicios de valor agregado solo se identificarán como tales si pueden vincularse a un servicio a un cliente externo. Si no hay vinculación con un servicio externo, se verá como un &quot;dinero gastado&quot;. Los servicios de apoyo están al menos un paso más alejados de los servicios externos. Para que se considere valioso, el proveedor de servicios debe poder vincularlos a un servicio de TI interno o externo. Esto no significa que el proveedor de servicio debería revelar todos los servicios internos al cliente. En su lugar esto significa que el proveedor de servicio podrá medir la contribución de cada servicio interno al resultado de negocio (o departamento del gobierno).
    </p>
    <p>
      
    </p>
    <p>
      Esto les permitirá comunicar efectivamente la garantía y la utilidad del servicio (sección 3.2.4), lo cual incrementará la percepción del cliente del valor de los servicios. Información adicional sobre enlazar servicios puede ser encontrada en la sección 4.2.4.3.
    </p>
  </body>
</html></richcontent>
<node TEXT="Valor y paquetes de servicio" ID="ID_1413565235" CREATED="1658516827397" MODIFIED="1658517057587" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los conceptos de valor agregado y valor realizado son dos de los principales impulsores de cómo se definen, construyen y entregan los paquetes de servicio. Las Secciones 3.4.7 y 3.4.8 describen estos conceptos en detalle, pero debe señalarse aquí que se aplica la misma dinámica de valor.
    </p>
    <p>
      
    </p>
    <p>
      Por ejemplo, un paquete de servicios para un cliente externo se puede definir en el catálogo de servicios (valor realizado). Este paquete de servicios externos puede incluir uno o más servicios internos o paquetes de servicios proporcionados por proveedores de servicios internos (valor añadido). Estos podrían nuevamente incluir paquetes de servicio que consisten de servicios de soporte proveídos por organizaciones internas de TI (dinero gastado).
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.2.3.4 Captura de valor" ID="ID_1388129276" CREATED="1658189744313" MODIFIED="1658517582827" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La captura de valor es la capacidad de un proveedor de servicios para conservar una parte del valor que ha sido creado y realizado. La capacidad de un proveedor de servicios para diferenciarse y ofrecer más valor a lo largo del tiempo depende de si puede obtener financiamiento para desarrollar y mejorar los servicios, además del costo de operar los servicios. La captura de valor es una buena forma de obtener esta financiación, ya que es más fácil vincular directamente el coste de este desarrollo con el servicio y, por lo tanto, es más fácil para el cliente evaluar el valor de las mejoras del servicio.
    </p>
    <p>
      
    </p>
    <p>
      La captura de valor es una noción importante para todos los tipos de proveedores de servicios, internos y externos. El buen sentido comercial desalienta a las partes interesadas a realizar grandes inversiones en cualquier capacidad organizativa a menos que demuestre la captura de valor. Se alienta a los proveedores internos a adoptar esta perspectiva estratégica para continuar como preocupaciones viables dentro de un negocio. La recuperación de costos es necesaria pero no suficiente. Las ganancias o excedentes permiten inversiones continuas en activos de servicio que tienen un impacto directo en las capacidades.
    </p>
    <p>
      
    </p>
    <p>
      Sin embargo, debe tenerse en cuenta que si la organización de TI captura el valor o cuáles son las reglas y los límites de la captura de valor, son decisiones estratégicas que deben tomar los ejecutivos de la organización. Algunas organizaciones, especialmente las organizaciones internas de TI, no pueden capturar valor y tienen que operar en un nivel de equilibrio. En estos casos, la financiación debe obtenerse caso por caso, normalmente de un presupuesto central.
    </p>
    <p>
      
    </p>
    <p>
      Vincular la creación de valor con la captura de valor es un esfuerzo difícil pero que vale la pena. En términos más simples, los clientes compran servicios como parte de los planes para lograr ciertos resultados comerciales. Digamos, por ejemplo, que el uso de un servicio de mensajería inalámbrica permite que el personal de ventas del cliente se conecte de forma segura al sistema de automatización de la fuerza de ventas y complete tareas críticas en el ciclo de ventas. Esto tiene un impacto positivo en los flujos de efectivo de los pagos adelantados en el tiempo. Al vincular las órdenes de compra y las facturas expedidas por el uso del servicio inalámbrico es posible detectar el impacto del servicio inalámbrico en el resultado del negocio. Este puede ser medido en términos como Días de ventas pendientes (DSO) y tiempo promedio del ciclo Pedido a efectivo. El costo total de utilizar el servicio se puede comparar con el impacto en los resultados comerciales. Es difícil establecer la relación de causa y efecto entre el uso del servicio y los cambios en los flujos de efectivo. Muy a menudo, hay varios grados de separación entre la utilización del servicio y los beneficios que finalmente obtienen los clientes. Si bien la certeza absoluta es difícil de lograr, la toma de decisiones, sin embargo, mejora.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.2.4 Utilidad y Garantia" FOLDED="true" ID="ID_35380666" CREATED="1658189762841" MODIFIED="1658519187262" TEXT_SHORTENED="true">
<arrowlink DESTINATION="ID_763896233" STARTINCLINATION="131.25 pt;0 pt;" ENDINCLINATION="131.25 pt;0 pt;"/>
<richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Desde la perspectiva del cliente, el valor consiste en alcanzar los objetivos de negocio. El valor de un servicio se crea mediante la combinación de dos elementos principales: utilidad (idoneidad para el propósito) y garantía (idoneidad para el uso). La utilidad y la garantía se definen en el apartado 2.1.6.
    </p>
    <p>
      
    </p>
    <p>
      Los clientes no pueden beneficiarse de algo que es apto para su propósito pero no apto para su uso, y viceversa. Por lo tanto, el valor de un servicio solo se entrega cuando se diseñan y entregan tanto la utilidad como la garantía. Es tentador para los clientes centrarse solo en los aspectos de utilidad de un servicio, ignorando la garantía. Por ejemplo, ¿por qué un servicio o producto es más caro que otro cuando hacen exactamente lo mismo? La respuesta suele estar en la confiabilidad o la vida útil del producto o la disponibilidad de soporte del proveedor.
    </p>
    <p>
      
    </p>
    <p>
      En muchas organizaciones de TI, el desarrollo de servicios suele estar separado de la operación de los servicios. Por ejemplo, los clientes solo tratarán con los desarrolladores de aplicaciones para asegurarse de que tengan la funcionalidad que necesitan, y asumirán que se está cuidando la capacidad de administración de la aplicación. Si los equipos operativos solo se involucran cuando se implementa la aplicación, existe una gran posibilidad de que se hayan omitido algunas consideraciones operativas clave y se requiera financiación adicional. Desde la perspectiva del cliente, ya han pagado por la funcionalidad que necesitan, y la solicitud de financiamiento de operaciones parece no agregar valor.
    </p>
    <p>
      
    </p>
    <p>
      En realidad, sin embargo, la garantía es un elemento esencial del diseño del servicio y debe diseñarse y construirse junto con la utilidad. De lo contrario, a menudo resulta en una capacidad limitada para entregar la utilidad e intentos de diseñar la garantía después que un servicio ha sido implementado podría ser costoso y disruptivo. Cada vez más, las estrategias de servicio enfatizan la importancia de diseñar y construir tanto la funcionalidad (que brinda utilidad) como la capacidad de administración (que habilita la garantía) en el servicio, y financiar ambas juntas. Esto asegura que el valor no solo se cree y agregue, sino que se realice por completo. La Figura 2.2 del Capítulo 2 ilustra la lógica de la utilidad y la garantía como componentes necesarios de un servicio.
    </p>
    <p>
      
    </p>
    <p>
      La figura 2.2 ilustra la lógica de que un servicio debe tener utilidad y garantía para crear valor. La utilidad se utiliza para mejorar el desempeño de las tareas utilizadas para lograr un resultado, o para eliminar las restricciones que impiden que la tarea se realice adecuadamente (o ambas cosas). <i>La garantía requiere que el servicio esté disponible, continuo y seguro y que tenga capacidad suficiente para que el servicio funcione al nivel requerido</i>. <b><u>Si el servicio es apto tanto para el propósito como para el uso, creará valor</u></b>.
    </p>
    <p>
      
    </p>
    <p>
      Cabe señalar que los elementos de garantía de la Figura 2.2 no son excluyentes. Es posible definir otros componentes de la garantía, como la usabilidad, que se refiere a qué tan fácil es para el usuario acceder y usar las funciones del servicio para lograr los resultados deseados.
    </p>
    <p>
      
    </p>
    <p>
      Es dudoso que se pueda obtener el valor total de los servicios cuando hay incertidumbre en el resultado del servicio. Cuando la utilidad de un servicio no está respaldada por la garantía, los clientes piensan más en las posibles pérdidas debido a la mala calidad del servicio que en las ganancias de recibir la utilidad prometida. Los clientes tienden a pensar que si un proveedor de servicios promete utilidad, automáticamente entregará garantía. Dicho de otra manera, los clientes a menudo no piensan en la garantía hasta que no está disponible, y entonces puede ser costoso adaptar los aspectos de la garantía del servicio, lo que resulta en una pérdida aún mayor del valor percibido.
    </p>
    <p>
      
    </p>
    <p>
      Para disipar tales preocupaciones e influir en las percepciones de los clientes sobre posibles ganancias y pérdidas, es importante que el valor de un servicio se describa completamente en términos de utilidad y garantía. La Tabla 3.6 proporciona ejemplos de cómo el valor de un servicio de &quot;Procesamiento de pedidos de ventas móviles (MoSOP)&quot; es descrito en términos de utilidad y garantía. En este caso el valor del servicio es dependiente de la gente de ventas para lograr los resultados del negocio de procesar ventas dentro de cinco minutos de que el cliente este de acuerdo de realizar una compra.
    </p>
  </body>
</html></richcontent>
<node TEXT="3.2.4.1 El efecto de utilidad mejorada en un servicio" ID="ID_1544199724" CREATED="1658189784414" MODIFIED="1658520030253" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Mejorar la utilidad de un servicio tiene el efecto de aumentar la funcionalidad de un servicio o lo que hace por el cliente, aumentando así el tipo y el rango de resultados que se pueden lograr.
    </p>
    <p>
      
    </p>
    <p>
      La Figura 3.10 ilustra un ejemplo de un servicio de manejo de equipaje de una aerolínea, que puede completar la carga del equipaje en un avión en 15 minutos, el 80% del tiempo. Esto se muestra mediante la curva de color claro. Con la nueva legislación de seguridad, se les exigirá que realicen controles de seguridad adicionales y que registren la ubicación de cada maleta en la bodega del avión. Estas actividades adicionales requieren cambios en la utilidad de los servicios.
    </p>
    <p>
      
    </p>
    <p>
      La aerolínea cambia el servicio para poder realizar el trabajo adicional y todavía puede cargar el equipaje en el avión en 15 minutos, el 80 % del tiempo. Este nuevo nivel de utilidad se muestra mediante la curva de color oscuro. Tenga en cuenta que la desviación estándar sigue siendo la misma ya que la garantía no ha cambiado.
    </p>
    <p>
      
    </p>
    <p>
      La figura 3.10 representa la distribución estándar de los elementos que se miden (en este caso, el eje x representa la utilidad del servicio, mientras que el eje y se refiere a la cantidad de puntos de datos utilizados para medir el servicio).
    </p>
    <p>
      
    </p>
    <p>
      Es importante tener en cuenta que la garantía no permanece automáticamente igual cuando aumenta la utilidad. De hecho, mantener niveles constantes de garantía al aumentar la utilidad generalmente requiere una buena planificación y una mayor inversión. Esta inversión es necesaria para realizar cambios en los procesos y herramientas existentes, capacitación, contratación de empleados adicionales para realizar el mayor trabajo, herramientas adicionales para realizar actividades recientemente automatizadas, etc.
    </p>
    <p>
      
    </p>
    <p>
      Por lo tanto, el efecto de utilidad significa que, aunque los activos del cliente funcionan mejor y el rango de resultados aumenta, la probabilidad de lograr esos resultados sigue siendo la misma (es decir, la forma del gráfico y el espacio debajo de la línea siguen siendo los mismos).
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Figura 3.10 La utilidad incrementa el rendimiento promedio" ID="ID_1327837442" CREATED="1658520151926" MODIFIED="1658520176689" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/La%20utilidad%20incrementa%20el%20rendimiento%20promedio.png" SIZE="0.90361446" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 3.6 Ejemplos de definiciones de utilidad y garantía" ID="ID_1955662427" CREATED="1658520259604" MODIFIED="1658520752944" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 600px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <td valign="top" style="width: 15%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Utilidad
          </p>
        </td>
        <td valign="top" style="width: 85%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <ul>
            <li>
              El personal de ventas podrá enviar pedidos para su procesamiento durante o inmediatamente después de una reunión con el cliente, utilizando teléfonos móviles habilitados para datos.
            </li>
            <li>
              El cliente y el vendedor recibirán una confirmación del pedido por correo electrónico tan pronto como el pedido haya sido aceptado.
            </li>
            <li>
              Los asistentes de ventas deben poder enviar pedidos de ventas en nombre de los vendedores si los vendedores no pueden enviarlos ellos mismos.
            </li>
          </ul>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 15%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Garantía
          </p>
        </td>
        <td valign="top" style="width: 85%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <ul>
            <li>
              A cada vendedor se le proporcionará un teléfono móvil inalámbrico habilitado para datos, con el cliente MoSOP instalado.
            </li>
            <li>
              El software operará sobre redes de telefonía móvil 2G, 3G y 4G.
            </li>
            <li>
              El servicio estará disponible de 7.00 a 19.00 horas.
            </li>
            <li>
              Se transmitirá una orden de venta dentro de los 15 segundos posteriores a la selección de &quot;Enviar&quot;.
            </li>
            <li>
              La orden de venta se procesará dentro de los 2 minutos posteriores a la recepción.
            </li>
            <li>
              La confirmación de ventas se enviará por correo electrónico dentro de los 30 segundos posteriores al procesamiento del pedido de ventas.
            </li>
            <li>
              El sistema de procesamiento de pedidos de ventas podrá procesar hasta 1,000 pedidos de ventas por hora.
            </li>
            <li>
              Si no hay señal móvil disponible, el vendedor usará un teléfono fijo para contactar a un asistente de ventas para procesar el pedido. Una vez que se haya enviado el pedido, el procesamiento y el envío por correo electrónico de la confirmación de venta tomarán la misma cantidad de tiempo.
            </li>
          </ul>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="3.2.4.2 El efecto de garantia mejorada en un servicio" ID="ID_1966317998" CREATED="1658189803925" MODIFIED="1658521730725" TEXT_SHORTENED="true">
<arrowlink DESTINATION="ID_583270109"/>
<richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El efecto de mejorar la garantía de un servicio significa que el servicio continuará haciendo las mismas cosas, pero de manera más confiable. Por lo tanto, existe una mayor probabilidad de que se logren los resultados deseados, junto con un menor riesgo de que el cliente sufra pérdidas debido a variaciones en el desempeño del servicio. Garantía mejorada también resulta en un numero incrementado de veces que una tarea puede ser realizada dentro de un nivel aceptable de costo, tiempo y actividad.
    </p>
    <p>
      
    </p>
    <p>
      La Figura 3.11 muestra como la desviación estándar del rendimiento de un servicio cambia cuando la garantía es mejorada. La línea más clara muestra que un significante porcentaje de entrega de servicio esta fuera del rango aceptable. Haciendo varias mejoras (ej: entrenamiento, mejoras de procesos y herramientas, nuevas herramientas o procesos, automatización, etc.) el proveedor de servicios puede incrementar la probabilidad de que el servicio sea realizado dentro de un rango aceptable.
    </p>
    <p>
      
    </p>
    <p>
      Usando el ejemplo del manejo de equipaje de la aerolínea, un año después de agregar la nueva utilidad, a la aerolínea le gustaría aumentar su tasa de &quot;salida a tiempo&quot;. Lograr esto significa que el manejo de equipaje necesita mejorar su desempeño. Sin agregar ninguna utilidad nueva, el servicio de manejo de equipaje encuentra una mejor manera de escanear y registrar la ubicación de las maletas. Como resultado, el servicio de manejo de equipaje puede completar la carga del equipaje en el avión en 15 minutos, el 90 % del tiempo, una mejora significativa.
    </p>
    <p>
      
    </p>
    <p>
      En la Figura 3.11 se aumenta la garantía de forma que, aunque el servicio hace exactamente lo mismo, es más fiable ya que se mejora el rendimiento de los activos del servicio. La mejora en el desempeño de los activos de servicio da como resultado la capacidad de lograr los resultados comerciales de manera más consistente. Por ejemplo, una red lenta a veces da como resultado tiempos de procesamiento deficientes para un sistema de punto de venta y elementos que no se escanean correctamente. La red se actualiza y el sistema de punto de venta da como resultado un procesamiento consistentemente más rápido y preciso. En general ha mejorado la garantía del servicio, aunque hace exactamente lo mismo que antes - en otras palabras la utilidad permanece siendo la misma.
    </p>
    <p>
      
    </p>
    <p>
      La Figura 3.11 representa la distribución estándar de los ítems que están siendo medidos (en este caso el eje de las X representa la garantía de servicio, mientras que el eje de las y se refiere al numero de puntos de datos usados para medir el servicio). El efecto de la garantía significa que el desempeño de los activos de servicio son mejorados. El efecto de utilidad significa que el rendimiento de los activos del cliente son mejorados.
    </p>
    <p>
      
    </p>
    <p>
      Para una definición de activos de cliente y servicio, referirse a la sección 2.2.1.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Figura 3.11 La garantía reduce la variación del desempeño" ID="ID_1205262029" CREATED="1658521632694" MODIFIED="1658521664253" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/La%20garantia%20reduce%20la%20variacion%20del%20desempeno.png" SIZE="0.88365245" NAME="ExternalObject"/>
</node>
<node TEXT="3.2.4.3 El efecto combinado de garantia y utilidad como un servicio" ID="ID_856362976" CREATED="1658189821567" MODIFIED="1658524379974" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Como se indicó anteriormente, tanto la garantía como la utilidad son necesarias para crear y entregar valor. Mejorar uno u otro resultará en mejoras, pero a menudo no es suficiente. Por ejemplo, aumentar la garantía de un servicio que solo cumple con el 60% de los requisitos del cliente seguirá cumpliendo con el 60% de los requisitos del cliente de manera más confiable. Es probable que aumentar la utilidad de un servicio que experimenta interrupciones frecuentes haga que el cliente se sienta aún más frustrado que antes, ya que el servicio puede hacer más, pero aún falla con frecuencia.
    </p>
    <p>
      
    </p>
    <p>
      La figura 3.12 muestra el efecto de mejorar tanto la utilidad como la garantía de un servicio. En este diagrama se muestra el servicio original en color claro, y el servicio mejorado en color oscuro. En este caso el rango y numero de resultados es incrementado (desplazamiento hacia la derecha) al mismo tiempo que se mejora la probabilidad de tener resultados positivos (La distribución estándar más delgada).
    </p>
    <p>
      
    </p>
    <p>
      La figura 3.12 muestra un servicio a su rango inicial de salidas soportadas, y su desempeño (color claro). Después de mejorar la utilidad y garantía, ahora puede soportar una mayor gama de resultados, más confiables.
    </p>
    <p>
      
    </p>
    <p>
      Los proyectos de mejora de servicios más efectivos asegurarán que hay un balance entre utilidad y garantía en los servicios. Esto se ilustra en la figura 3.13.
    </p>
    <p>
      
    </p>
    <p>
      La Figura 3.13 muestra un cuadrante donde los servicios pueden clasificarse según sus niveles de garantía y utilidad. Los servicios se colocan en el cuadrante de acuerdo con sus niveles de garantía y utilidad, así como su valor para el cliente. El gráfico muestra los servicios que tienen una mayor utilidad (los servicios tienen una funcionalidad que cumple con los requisitos del cliente), pero una menor garantía (los servicios no son confiables, no están disponibles o no son seguros). Estos servicios tienen un sesgo de utilidad.
    </p>
    <p>
      
    </p>
    <p>
      La figura 3.13 también muestra los servicios que tienen una utilidad menor que la garantía (servicios que son confiables, pero su funcionalidad no cumple con los requisitos del cliente por completo). Estos servicios tienen un sesgo de garantía. Servicios que cuentan con los niveles adecuados tanto de garantía y utilidad están equilibrados. Estos servicios tienen una funcionalidad que cumple con los requisitos del cliente y lo hacen de manera consistente.
    </p>
    <p>
      
    </p>
    <p>
      La Figura 3.13 ayuda a identificar cómo se deben realizar las inversiones para mejorar los servicios. Estos se resumen en la Tabla 3.7.
    </p>
    <p>
      
    </p>
    <p>
      En el equilibrio apropiado entre la utilidad y la garantía, los clientes verán un fuerte vínculo entre la utilización de un servicio y el efecto positivo en el desempeño de sus propios activos, permitiendo un mayor retorno en los activos (Figura 3.14).
    </p>
    <p>
      
    </p>
    <p>
      Las flechas marcadas con '+' en la Figura 3.14 indican una relación directamente proporcional - aquí la utilidad más alta, mayor es el desempeño del nivel de garantía. La flecha '-' indica una relación proporcionalmente inversa - a mayor garantía el desempeño es menor en su variación.
    </p>
    <p>
      
    </p>
    <p>
      En la Figura 3.14 los servicios en equilibrio entre utilidad y garantía incrementan el desempeño promedio de los activos del cliente (en otras palabras los resultados son de mayor valor), mientras se reduce la variación de desempeño (en otras palabras incrementa la confiabilidad del servicio). Los efectos combinados de utilidad y garantía permitirá a los activos del cliente lograr los resultados del negocio, y resultará en un retorno en sus activos. En otras palabras, entrega de valor.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Figura 3.12 Un servicio con mejora en utilidad y garantía" ID="ID_1159244865" CREATED="1658524420659" MODIFIED="1658524446456" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Un%20servicio%20con%20mejora%20en%20utilidad%20y%20garantia.png" SIZE="0.85470086" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.13 Efectos combinados de utilidad y garantía en los activos de los clientes" ID="ID_1593752037" CREATED="1658524495409" MODIFIED="1658525914554" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Efectos%20combinados%20de%20utilidad%20y%20garantia%20en%20los%20activos%20de%20los%20clientes.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 3.7 Estrategias de inversión para la utilidad y garantía" ID="ID_261050483" CREATED="1658524994393" MODIFIED="1658525903807" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 600px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Cuadrante
          </p>
        </th>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Descripción
          </p>
        </th>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Reto
          </p>
        </th>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Inversión
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Abajo izquierda
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Estos servicios tienen un bajo impacto comercial y oscilan entre un sesgo de utilidad y de garantía.
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Estos servicios son de poco valor incluso cuando brindan utilidad y garantía. No respaldan los resultados comerciales de alto valor.
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Se justifica una inversión mínima, ya que el valor para el negocio es bajo. Cuando existe una garantía de sesgo de utilidad, tiene poco valor crear un equilibrio, y podría ser preferible retirar el servicio por completo.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Arriba izquierda
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Bajo impacto en los resultados del cliente, pero con altos niveles de garantía y bajos niveles de utilidad. Estos servicios son muy confiables, pero tienen poco valor para el negocio. Además, el nivel de utilidad es bajo.
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los servicios con poca utilidad no brindan valor porque no hacen lo que el negocio necesita que hagan, independientemente de cuán confiables sean. Además, los servicios en este cuadrante no abordan los requisitos comerciales de alto valor.
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            La inversión actual en estos servicios es desproporcionada. El valor de los servicios es bajo, ya que la inversión continua para mantener el rendimiento de los activos del servicio es probablemente más alta de lo que debería ser. Cualquier inversión debe reasignarse a mejorar la utilidad de estos servicios.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Abajo derecha
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los servicios en este cuadrante tienen un alto impacto en los resultados del cliente, pero con altos niveles de utilidad y bajos niveles de garantía.
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            El valor de estos servicios es bajo ya que, si bien han sido diseñados para cumplir con los requisitos de alta prioridad de los clientes, no pueden hacerlo debido al mal desempeño de los activos del servicio.
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Se necesita inversión para mejorar el nivel de servicio proporcionado por el proveedor de servicios, y es probable que esta inversión tenga una alta prioridad.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Arriba derecha
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los servicios en este cuadrante tienen un buen equilibrio entre utilidad y garantía, y tienen un alto impacto comercial para el cliente. La percepción del cliente sobre la calidad y el valor del servicio es más alta en la zona de equilibrio.
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Las deficiencias en los servicios que están sesgados hacia un sesgo de utilidad o garantía en este cuadrante tienden a tener más visibilidad, ya que los resultados comerciales son más valiosos para el negocio.
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los niveles óptimos de inversión se encuentran en la &quot;zona de equilibrio&quot;, ya que representa servicios que cumplen con los niveles de utilidad y garantía. Es mucho más fácil justificar las inversiones necesarias para trasladar un servicio a la zona de equilibrio.
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="Figura 3.14 Valor de un servicio en términos de retorno sobre activos para el cliente" ID="ID_1811895270" CREATED="1658524901148" MODIFIED="1658525704305" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.14%20Valor%20de%20un%20servicio%20en%20terminos%20de%20retorno%20sobre%20activos%20para%20el%20cliente.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="3.2.4.4 Comunicando la utilidad" FOLDED="true" ID="ID_1802122159" CREATED="1658189846506" MODIFIED="1658526055249" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Comunicar la utilidad y la garantía es importante para que los clientes puedan calcular el valor de un servicio. Comunicar la utilidad permitirá al cliente determinar hasta qué punto la utilidad se adapta a sus requisitos de funcionalidad.
    </p>
  </body>
</html></richcontent>
<node TEXT="En términos de resultados soportados" ID="ID_930337687" CREATED="1658526057033" MODIFIED="1658526236845" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Tomemos el ejemplo de un banco que obtiene ganancias al prestar dinero a clientes solventes que pagan tarifas e intereses sobre los préstamos. Al banco le gustaría desembolsar tantos buenos préstamos como sea posible dentro de un período de tiempo (resultado deseado). El banco tiene un proceso de préstamo que incluye la actividad de determinar la calificación crediticia de los solicitantes de préstamos. El banco utiliza un servicio comercial de información crediticia, que se puede solicitar por teléfono y por Internet. El proveedor de servicios se compromete a proporcionar información precisa, completa y actual sobre los solicitantes de préstamos en menos de un minuto. El proceso de préstamo es el consumidor del informe crediticio, siendo el oficial de préstamo el usuario. La utilidad de un servicio de informes crediticios proviene de la alta calidad de la información que proporciona al proceso de préstamo (activo del cliente) para determinar la solvencia crediticia de los prestatarios, de modo que las solicitudes de préstamo puedan aprobarse de manera oportuna después de calcular todos los riesgos para el solicitante. Al reducir el tiempo que lleva obtener información de buena calidad, el banco puede tener un activo de alto rendimiento en el proceso de préstamo.
    </p>
    <p>
      
    </p>
    <p>
      La Figura 3.15 ilustra cómo la utilidad ayuda al cliente a lograr sus resultados al permitir resultados directamente, mientras que al mismo tiempo reduce el impacto de las restricciones que podrían impedir que se logren estos resultados.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="En términos de costos de propiedad y riesgos evitados" ID="ID_1838605299" CREATED="1658526244285" MODIFIED="1658527118638" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Comunicar la utilidad en términos de costos de propiedad y riesgos evitados significa que el proveedor de servicios debería conseguir:
    </p>
    <ul>
      <li>
        Que los servicios permiten al negocio alcanzar los resultados deseados más eficientemente. Esto permite al negocio reducir sus costos (y en organizaciones comerciales, incrementar sus márgenes de ganancias).
      </li>
      <li>
        Que el servicio mejora la confiabilidad de los resultados obtenidos. En otras palabras, el servicio mitiga el riesgo del negocio de no poder conseguir sus resultados.
      </li>
    </ul>
    <p>
      Utilizando el mismo ejemplo bancario de la sección anterior, el valor del servicio de informes crediticios también proviene de que la división de préstamos pueda evitar ciertos costos y riesgos en los que incurriría al operar un sistema de consulta de crédito por su cuenta en lugar de utilizar el sistema de informes de servicio. Por ejemplo, los costos de mantener las capacidades y los recursos necesarios para operar un sistema de informes crediticios serían sufragados en su totalidad por la división de préstamos. El costo por informe de crédito se volvería prohibitivo dentro del alcance del proceso de aprobación del préstamo y tendría que transferirse al costo del préstamo o absorberse en otra parte dentro del sistema bancario. Bajo las condiciones imperantes, comprar el servicio resulta ser una buena decisión para el banco. Aumenta las ganancias y reduce las pérdidas.
    </p>
    <p>
      
    </p>
    <p>
      Una estrategia alternativa es que la división de préstamos convenza a otras divisiones dentro del mismo banco, grupo de servicios financieros o industria para que utilicen su sistema de información crediticia. Esta puede ser una opción viable en la que la división de préstamos ahora ofrecería un servicio de informes crediticios a los prestamistas junto con su servicio principal a los prestatarios. Esta es una elección estratégica que deben hacer los gerentes senior de la división de préstamos y su liderazgo en el banco. Los riesgos de tal elección incluyen que la división de préstamos se desvíe de sus capacidades básicas, la incapacidad de convencer a otros de su competencia y atraiga muy poca demanda para que el servicio de informes crediticios sea económicamente viable.
    </p>
    <p>
      
    </p>
    <p>
      Al usar un servicio de informes crediticios en lugar de operar un sistema de informes crediticios, la división de préstamos evita deliberadamente riesgos y costos específicos. En efecto, la división de préstamos se libera de ciertas restricciones comerciales. Los conjuntos de restricciones a menudo se intercambian por otros, siempre que el desempeño general del negocio no disminuya.
    </p>
    <p>
      
    </p>
    <p>
      Tales compensaciones las hacen los líderes senior de los clientes que están en la mejor posición para decidir. Los líderes sénior de los proveedores de servicios se convierten en socios comerciales cuando pueden ayudar a sus contrapartes a gestionar las limitaciones de las estrategias comerciales.
    </p>
    <p>
      
    </p>
    <p>
      Desde la perspectiva comercial del ejemplo anterior, los proveedores de servicios respaldan las estrategias comerciales de sus clientes al eliminar o reducir el impacto de ciertos tipos de restricciones en los modelos y estrategias comerciales. Las restricciones son del tipo que impone costos y riesgos específicos que los clientes desean evitar, de la siguiente manera:
    </p>
    <ul>
      <li>
        <b>Mantener activos no esenciales y subutilizados</b>&nbsp;A los clientes les gustaría evitar la propiedad y el control de activos que agotan los recursos financieros de los activos principales, y los que se usan rara o esporádicamente. En tales casos, el rendimiento de los activos suele ser bajo o incierto, lo que hace que las inversiones sean riesgosas.
      </li>
      <li>
        <b>Costos de oportunidad debido a capacidad limitada y activos sobrecargados</b>&nbsp;Los activos que están sobrecargados no pueden atender unidades adicionales de demanda o acomodar aumentos inesperados en la demanda. La capacidad insuficiente también significa que no se pueden buscar nuevas oportunidades con una alta probabilidad de éxito.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Figura 3.15 La utilidad descrita en términos de resultados soportados y restricciones removidas" ID="ID_1754668371" CREATED="1658526829811" MODIFIED="1658526848275" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.15%20La%20utilidad%20descrita%20en%20terminos%20de%20resultados%20soportados%20y%20restricciones%20removidas.png" SIZE="0.7566204" NAME="ExternalObject"/>
</node>
<node TEXT="3.2.4.5 Comunicando la garantia" FOLDED="true" ID="ID_1720645881" CREATED="1658189863155" MODIFIED="1658527486059" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La garantía garantiza que la utilidad del servicio esté disponible según sea necesario con capacidad suficiente, continuidad y seguridad - al costo o precio acordado. Los clientes no pueden utilizar el valor prometido de un servicio que se ajusta a su propósito cuando no se ajusta a su uso.
    </p>
    <p>
      
    </p>
    <p>
      La garantía en general es parte de la propuesta de valor que influye en los clientes para comprar. Para que los clientes se den cuenta de los beneficios esperados de los bienes manufacturados, la utilidad es necesaria pero no suficiente. Los defectos y el mal funcionamiento hacen que un producto no esté disponible para su uso o disminuya su capacidad funcional. Las garantías aseguran que los productos conservarán su forma y función durante un período específico bajo ciertas condiciones específicas de uso y mantenimiento. Las garantías son nulas fuera de dichas condiciones. El desgaste normal no está cubierto. Lo más importante es que los clientes son propietarios y operadores de los bienes adquiridos.
    </p>
    <p>
      
    </p>
    <p>
      En el caso de los servicios, los clientes no son los propietarios ni los operadores de los activos de servicio que proporcionan utilidad. Esa responsabilidad, junto con el mantenimiento y las mejoras, corresponde al proveedor del servicio. Los clientes simplemente utilizan el servicio. No hay desgaste, mal uso, negligencia o daño de los activos de servicio que limiten la validez de la garantía. Incluso cuando un cliente subcontrata la gestión del equipo que posee, los recursos y las capacidades de la empresa de subcontratación para gestionar ese equipo no están dictados por el cliente.
    </p>
    <p>
      
    </p>
    <p>
      Los proveedores de servicios comunican el valor de la garantía en términos de niveles de certeza. Su capacidad para administrar activos de servicio infunde confianza en el cliente sobre el soporte para los resultados comerciales. <b><u>La garantía se expresa en términos de disponibilidad, capacidad, continuidad y seguridad en la utilización de los servicios</u></b>.
    </p>
    <p>
      
    </p>
    <p>
      Un ejemplo es que una organización de TI interna elige externalizar la gestión de sus servicios de alojamiento web. La organización reduce el costo de los recursos dedicados y la capacidad infrautilizada mediante el uso de un proveedor que ha logrado mejores economías de escala. El proveedor también tiene amplias capacidades de continuidad, lo que significa que es menos probable que el sitio web de la organización falle.
    </p>
  </body>
</html></richcontent>
<node TEXT="Disponibilidad" ID="ID_1977929183" CREATED="1658527493202" MODIFIED="1658537958271" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La disponibilidad es el núcleo de la satisfacción del cliente y del usuario y es el aspecto más elemental para asegurar valor a los clientes. <b><i>Asegura al cliente que los servicios estarán disponibles para su uso bajo los términos y condiciones acordados.</i></b>&nbsp;La disponibilidad de un servicio es el atributo más fácilmente percibido desde la perspectiva del usuario. <b>Un servicio está disponible solo si los usuarios pueden acceder a él de la manera acordada.</b>&nbsp;Las percepciones y preferencias varían según el cliente y el contexto empresarial. El cliente es responsable de gestionar las expectativas y necesidades de sus usuarios. Dentro de las condiciones especificadas, tales como área de cobertura, períodos y canales de entrega, se espera que los servicios estén disponibles para los usuarios que el cliente autorice.
    </p>
    <p>
      
    </p>
    <p>
      La disponibilidad de un servicio es más sutil que una evaluación binaria de disponible y no disponible. La tolerancia del cliente a la degradación gradual de la disponibilidad debe determinarse y tenerse en cuenta en el diseño del servicio. Por ejemplo, si un subconjunto de usuarios es responsable de una función comercial vital, las instancias de servicio para estos usuarios se pueden alojar en recursos dedicados con tolerancia a fallas para que el cliente conserve alguna capacidad crítica para operar.
    </p>
    <p>
      
    </p>
    <p>
      Además, las horas de servicio acordadas son una parte importante para definir la garantía del servicio. Por ejemplo, es posible que los servicios deban estar 100 % disponibles en ciertos momentos, mientras que en otros momentos es posible que no se necesiten en absoluto. El uso de métricas y estrategias de disponibilidad general no es útil para estos servicios. Por ejemplo, los informes de fin de mes solo son necesarios durante unos pocos días al mes. En otras ocasiones, los informes de disponibilidad pueden ponerse en cola y proporcionarse solo para cuando los recursos del sistema están disponibles. El uso de la disponibilidad total del servicio de todo el mes es una métrica que tiene significado - y proveer 100% (o incluso 98%) de disponibilidad todo el tiempo en el mes sería innecesariamente costoso.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Capacidad" ID="ID_1806735762" CREATED="1658527508247" MODIFIED="1658538350531" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>La capacidad es una garantía de que el servicio admitirá un nivel específico de actividad comercial o demanda en un nivel específico de calidad. </b>Los clientes impulsan la actividad empresarial con la garantía de una capacidad adecuada. Las variaciones en la demanda se acomodan dentro de un rango acordado. Los proveedores de servicios se comprometen a mantener los recursos para liberar a los clientes de la escasez de capacidad y de los activos infrautilizados. La capacidad es de particular importancia cuando la utilidad del servicio surge del acceso a recursos compartidos.
    </p>
    <p>
      
    </p>
    <p>
      <b>Los proveedores de servicios ayudan a los clientes con la escasez durante los períodos de máxima demanda.</b>&nbsp;La capacidad garantizada durante períodos particulares o en ubicaciones particulares también es valiosa para los clientes que necesitan iniciar operaciones nuevas o ampliadas con el tiempo de comercialización como un factor crítico de éxito. Dichos planes comerciales requieren bajos costos de instalación y plazos de entrega. Además, debido a los altos riesgos de operaciones nuevas o ampliadas, los clientes pueden preferir no realizar las inversiones necesarias para poseer y operar activos comerciales. Las empresas que enfrentan una demanda altamente incierta de sus propios clientes también encuentran valor en los servicios bajo demanda con poca o ninguna latencia. <b>Los costos de oportunidad son altos en términos de clientes perdidos. </b>
    </p>
    <p>
      
    </p>
    <p>
      Sin una gestión eficaz de la capacidad, los proveedores de servicios no podrán prestar la mayoría de los servicios. <b>La gestión de capacidad es un aspecto crítico de la gestión de servicios porque tiene un impacto directo en la disponibilidad de los servicios.</b>&nbsp;Es posible que un servicio que está funcionando mal no se brinde en absoluto; por ejemplo, los clientes considerarán que un cajero automático que tarda 15 minutos en entregar efectivo no está disponible. <b>La capacidad disponible para los servicios de soporte también tiene un impacto en el nivel de continuidad del servicio comprometido o entregado.</b>
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Confiabilidad" ID="ID_1741143228" CREATED="1658527517606" MODIFIED="1658538557645" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La confiabilidad asegura que tanto el proveedor del servicio como los clientes conozcan el nivel de desempeño que se puede esperar bajo las condiciones de entrega acordadas. Los clientes no pueden esperar niveles altos de rendimiento constantes si un servicio tiene un exceso de suscripciones o si se utiliza de una manera para la que no fue diseñado para ser usado. Por ejemplo, un servicio funcionará&nbsp;a un nivel consistente durante horas de servicio, teniendo en cuenta que no haya más de 500 usuarios concurrentes, y teniendo en cuenta que las ventanas de mantenimiento es usada para el mantenimiento necesario (y no ignorado en favor de de proveer niveles de disponibilidad mayores a lo acordado).
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Continuidad" ID="ID_892906057" CREATED="1658527525193" MODIFIED="1658538782420" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La continuidad asegura que el servicio continuará respaldando el negocio en el caso de fallas mayores o eventos disruptivos. El proveedor de servicios se compromete a mantener servicios activos que proporcionen un nivel suficiente de contingencia y recuperación. Los sistemas y procesos especializados se activarán para garantizar que los niveles de servicio recibidos por los activos del cliente no caigan por debajo de un nivel predefinido. La continuidad se asegura principalmente a través de la redundancia y el suministro de recursos alternativos que se dedican a brindar servicios durante contingencias y que no se ven afectados por el evento original.
    </p>
    <p>
      
    </p>
    <p>
      <b>Mensaje clave: </b>La continuidad no se trata solo de garantizar la continuidad de los servicios, sino también de garantizar la continuidad de las funciones vitales de negocio que realiza el cliente.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Seguridad" FOLDED="true" ID="ID_1543598569" CREATED="1658527534933" MODIFIED="1658538992010" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Seguridad asegura que los servicios se brinden a clientes autorizados y que la confidencialidad de la información y la propiedad intelectual estén protegidas cuando se brinden los servicios. Esto significa que los activos de los clientes estarán protegidos de ciertas amenazas. La seguridad del servicio cubre los siguientes aspectos de la reducción de riesgos:
    </p>
    <ul>
      <li>
        Uso autorizado y responsable de los servicios según lo especificado por el cliente
      </li>
      <li>
        Protección de los activos de los clientes contra el acceso no autorizado o malicioso
      </li>
      <li>
        Zonas de seguridad entre los activos del cliente y los activos del servicio
      </li>
      <li>
        Garantizar la integridad y confidencialidad de la información utilizada por la organización y sus clientes
      </li>
      <li>
        Los usuarios de los servicios tienen garantizado el acceso protegido a los servicios a través de la autenticación y el no repudio.
      </li>
    </ul>
  </body>
</html></richcontent>
<node TEXT="Características impuestas por la gestión del servicio a la seguridad" ID="ID_1030201329" CREATED="1658538995171" MODIFIED="1658539173006"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La seguridad del servicio tiene que reconocer e incorporar las propiedades generales de la seguridad de los activos físicos y humanos, e intangibles tales como datos, información, coordinación y comunicación. La seguridad de la información tiene el retos impuestos por las siguientes características de la gestión de servicio:
    </p>
    <ul>
      <li>
        Los activos de servicio están típicamente compartidos por más de una entidad del cliente.
      </li>
      <li>
        El valor es entregado justo a tiempo a través de la orquestación de varios activos de servicio.
      </li>
      <li>
        La acción o inacción del cliente es una fuente de riesgo de seguridad.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Otros elementos de la garantía" ID="ID_415412537" CREATED="1658527538757" MODIFIED="1658539251881" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los elementos de la garantía discutidos anteriormente no son una lista exhaustiva. Otros componentes que pueden considerarse como parte de la garantía son la facilidad de uso y la asequibilidad (si los clientes pueden pagar o financiar el servicio).
    </p>
    <p>
      
    </p>
    <p>
      La usabilidad se refiere a si los usuarios pueden realmente realizar las acciones requeridas y acceder a la información que necesitan para poder lograr los resultados deseados. Los factores de usabilidad incluyen la legibilidad del texto, si la entrada de datos es sencilla y lógica, etc.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.2.4.6 Comunicando el efecto combinado de utilidad y garantia" ID="ID_1247749584" CREATED="1658189877757" MODIFIED="1658539691395" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La capacidad de ofrecer un cierto nivel de garantía a los clientes por sí misma es la base de una ventaja competitiva para los proveedores de servicios . Esto es particularmente cierto cuando los servicios se mercantilizan o estandarizan. En tales casos, es difícil. diferenciar el valor en gran medida en términos de utilidad para los clientes. Cuando los clientes pueden elegir entre proveedores de servicios cuyos servicios brindan más o menos la misma utilidad pero diferentes niveles de garantía, entonces prefieren la mayor certeza en el respaldo de los resultados comerciales, la prestación ofrecida a un precio competitivo y por un proveedor de servicios con reputación. por poder cumplir lo prometido.
    </p>
    <p>
      
    </p>
    <p>
      “Menos llamadas caídas en promedio” es la propuesta de valor de uno de los principales proveedores de servicios de comunicación móvil expresada en sus anuncios. Un competidor igualmente grande contrarresta con la propuesta de valor de la mejor cobertura disponible en la mayoría de las áreas urbanas. La otra base perpetua de diferenciación es el número de llamadas realizadas por una tarifa plana dentro de las horas pico de uso. Esta es una medida indirecta de la capacidad de los activos de servicios suscritos en exceso que los proveedores de servicios están asegurando para el uso exclusivo de sus clientes. Por supuesto, cuando la acción de la competencia conduce a una diferenciación reducida basada en la garantía, los proveedores de servicios responden con paquetes de servicios que ofrecen una utilidad adicional, como navegación GPS o correo electrónico inalámbrico en teléfonos móviles.
    </p>
    <p>
      
    </p>
    <p>
      Ciertas empresas de entrega de paquetes y minoristas son líderes del mercado en negocios altamente comercializados simplemente porque ofrecen un nivel de certeza insuperable por sus pares. Sus servicios garantizan la entrega de mercancías a tiempo, independientemente de la ubicación, la zona horaria o el tamaño de los envíos. Pueden garantizar estos niveles de servicio porque han invertido en capacidades y recursos que les dan confianza en su capacidad para brindar la utilidad y la garantía acordadas.
    </p>
    <p>
      
    </p>
    <p>
      Sin embargo, los proveedores de servicios se emulan entre sí, lo que lleva a situaciones en las que los proveedores ofrecen niveles similares de utilidad o garantía. Los proveedores de servicios deben mejorar continuamente sus propuestas de valor para separarse del resto. Las mejoras pueden conducir a través de uno o más de los procesos de gestión de servicios.
    </p>
    <p>
      
    </p>
    <p>
      Todas las publicaciones principales de ITIL contienen orientación sobre cómo se diseñan, transicionan y operan la utilidad y la garantía para respaldar la ventaja competitiva de las organizaciones. Los resultados deseados se traducen en requisitos de utilidad y garantía en la cartera de servicios. El diseño del servicio se centrará en cómo proporcionar la utilidad y la garantía nuevas o mejoradas a un costo óptimo y niveles de riesgo aceptables. Estos servicios, con detalles sobre su utilidad y garantía, se documentan en el catálogo de servicios y se ponen en funcionamiento.
    </p>
    <p>
      
    </p>
    <p>
      Un punto a tener en cuenta al definir tanto la utilidad como la garantía es la asequibilidad del servicio. Un proveedor de servicios puede crear un servicio perfecto, pero la utilidad y la garantía deben equilibrarse con lo que el cliente puede pagar. La asequibilidad es una buena manera para que los clientes prioricen los elementos de garantía y utilidad, dados los resultados que desean lograr.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.2.5 Activos del cliente, activos de servicio y activos estratégicos" FOLDED="true" ID="ID_26096746" CREATED="1658189905184" MODIFIED="1658540055513" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;Una regla básica de buen comportamiento empresarial es un poco como el oxígeno: Nos interesa su presencia solo cuando esta ausente&quot;
    </p>
    <p>
      &nbsp;Amrtya Sen, Premio Novel de Economía.
    </p>
  </body>
</html></richcontent>
<node TEXT="Figura 3.16 Activos del cliente conducen a resultados del negocio." ID="ID_39042725" CREATED="1658540848674" MODIFIED="1658540867533" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.16%20Activos%20del%20cliente%20conducen%20a%20resultados%20del%20negocio.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.17 Los activos del servicio conducen a los servicios a alcanzar los resultados del negocio" ID="ID_1721553942" CREATED="1658541591047" MODIFIED="1658541635300" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.17%20Los%20activos%20del%20servicio%20coducen%20a%20los%20servicios%20a%20alcanzar%20los%20resultados%20del%20negocio.png" SIZE="0.85592014" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.18 La gestión del servicio optimiza el desempeño de los activos de servicio" ID="ID_245534273" CREATED="1658545927549" MODIFIED="1658546025833" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/La%20gestion%20del%20servicio%20optimiza%20el%20desempeno%20de%20los%20activos%20de%20servicio.png" SIZE="0.79893476" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.19 Vista simple de una organizacion TI" ID="ID_1039180216" CREATED="1658546064074" MODIFIED="1658546075429" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.19%20Vista%20simple%20de%20una%20organizacion%20TI.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.20 Procesos como medios para gestionar los silos del organigrama de la organización" ID="ID_1141159501" CREATED="1658546133331" MODIFIED="1658546153353" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.20%20Procesos%20como%20medios%20para%20gestionar%20los%20silos%20del%20organigrama%20de%20la%20organizacion.png" SIZE="0.8042895" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.21 La gestión del servicio habilita los resultados del negocio" ID="ID_1004955013" CREATED="1658546783268" MODIFIED="1658546798778" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.21%20La%20gestion%20del%20servicio%20habilita%20los%20resultados%20del%20negocio.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.22 Como un proveedor de servicio habilita los resultados de una unidad de negocio." ID="ID_1867093740" CREATED="1658548565469" MODIFIED="1658548581390" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.22%20Como%20un%20proveedor%20de%20servicio%20habilita%20los%20resultados%20de%20una%20unidad%20de%20negocio.png" SIZE="0.7682458" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.23 Crecimiento de la gestión de servicio hacia un activo estratégico confiable" ID="ID_534781824" CREATED="1658580688168" MODIFIED="1658580716282" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.23%20Crecimiento%20de%20la%20gestion%20de%20servicio%20hacia%20un%20activo%20estrategico%20confiable.png" SIZE="0.73439413" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 3.8 Ejemplos de como el potencial de servicio es incrementado" ID="ID_1407593188" CREATED="1658548596852" MODIFIED="1658549266829" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 600px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Iniciativa de gestión de servicio
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Incrementar el potencial de servicio desde las capacidades
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Incremento del potencial de servicio desde los recursos
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Racionalización del centro de datos
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Mejor control sobre las operaciones del servicio
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Menor complejidad en la infraestructura
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Desarrollo de infraestructura y activos de tecnología
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Incrementa la capacidad de los activos
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Incrementa las el alcance de las economías de escala
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Construcción de la capacidad en activos de servicio
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Entrenamiento y certificación
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Staff con conocimientos en el control del ciclo de vida del servicio
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Análisis y decisiones mejorados
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Dotando las competencias claves
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Extensión de horas de mesa de servicio
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Implementación del procesos de gestión de incidentes
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Mejor respuesta a incidentes de servicio
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Priorización de actividades de recuperación
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Reducción de pérdidas en la utilización de servicios
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Desarrollar el proceso de diseño del servicio
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Diseño sistemático de servicios
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Enriquecimiento del diseño del portafolio
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Re utilización de los componentes de servicio
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Menos fallas de servicio a través del diseño
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Computación de cliente liviano
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Incrementar la flexibilidad de la ubicaciones de trabajo
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Capacidades de continuidad del servicio mejoradas
          </p>
        </td>
        <td valign="top" style="width: 33%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Estandarización y control de configuraciones
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Centralización de las funciones de administración
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="3.2.5.1 Activos, recursos y capacidades" FOLDED="true" ID="ID_70174799" CREATED="1658189928160" MODIFIED="1658540474310" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los activos, recursos y capacidades se definen en la sección 2.2.1. En el Apéndice B (descripción de los tipos de activos) se presenta orientación complementaria sobre capacidades y recursos.
    </p>
    <p>
      
    </p>
    <p>
      Las capacidades se desarrollan con el tiempo. El desarrollo de capacidades distintivas se ve reforzado por la amplitud y profundidad de la experiencia obtenida a partir del número y variedad de clientes, espacios de mercado, contratos y servicios. La experiencia se enriquece igualmente con la resolución de problemas, el manejo de situaciones, la gestión de riesgos y el análisis de fallas. Por ejemplo, la combinación de experiencia en un espacio de mercado, reputación entre los clientes, contratos a largo plazo, expertos en la materia, procesos maduros e infraestructura en ubicaciones clave da como resultado capacidades distintivas que son difíciles de ofrecer para los competidores. Esto supone que la organización captura el conocimiento y lo retroalimenta a sus sistemas y procesos de gestión. Las inversiones en capacidades de aprendizaje son particularmente importantes para los proveedores de servicios para el desarrollo de activos estratégicos.
    </p>
  </body>
</html></richcontent>
<node TEXT="Caso de Estudio: Servicios Financieros" ID="ID_515740498" CREATED="1658540249713" MODIFIED="1658540518626" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En algún momento a fines de la década de 1990, una empresa líder en servicios financieros lanzó un servicio de banca directa. El servicio ofrecía un servicio de ahorro y préstamo basado en Internet. Después de ocho días, la empresa recibió casi 2 millones de visitas al sitio web y más de 100 000 consultas. Después de cinco semanas, la demanda era tan alta que la empresa advirtió a los clientes sobre retrasos de hasta 28 días. Solución ¡No pase por alto los activos de los clientes! Resulta que la limitación no era la capacidad o la disponibilidad de la infraestructura, sino una deficiencia de los activos del cliente en forma de 250 miembros del personal. Una vez que se resolvió este cuello de botella (250 contrataciones), la empresa ganó más de 500.000 nuevos clientes y 5.000 millones de libras esterlinas en depósitos en menos de seis meses.
    </p>
    <p>
      
    </p>
    <h4>
      Análisis Caso de Estudio
    </h4>
    <p>
      
    </p>
    <p>
      El rendimiento o crecimiento de un servicio estará limitado en última instancia por los límites de un recurso o capacidad, o por los límites de su propio potencial. Los intentos de impulsar un servicio más allá de un límite de recursos o capacidad pueden tener fuertes consecuencias, a menudo anulando los beneficios obtenidos.
    </p>
    <p>
      
    </p>
    <p>
      La restricción, en este caso, no parecía estar relacionada con la tecnología. Eran procesadores de cuentas. El CIO se lo perdió porque solo consideró los activos de servicio, pasando por alto el efecto restrictivo de los activos de los clientes en el desempeño de los servicios de su organización. El cliente del CIO, en este caso, incluye el departamento de procesamiento.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.2.5.2 Unidades de negocio y proveedores de servicio" FOLDED="true" ID="ID_1978914498" CREATED="1658189944947" MODIFIED="1658540595497" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Esta sección examina la relación entre los activos que utilizan las unidades de negocio y los que utilizan los proveedores de servicios. Específicamente muestra cómo estos activos se relacionan con los servicios y la creación de valor.
    </p>
  </body>
</html></richcontent>
<node TEXT="La unidad de negocio" ID="ID_1107064289" CREATED="1658540597349" MODIFIED="1658541168345" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una unidad de negocios es una entidad organizativa, bajo un administrador, que realiza un <b><u>conjunto definido de actividades comerciales que crean valor para los clientes en forma de bienes y servicios</u></b>. <b>Los bienes o servicios se producen y entregan utilizando un conjunto de activos, denominados activos del cliente</b>. Los clientes pagan por el valor que reciben, lo que asegura que la unidad de negocios mantenga un retorno adecuado sobre los activos. <b>La relación es buena siempre que el cliente reciba valor y la unidad de negocios recupere los costos y reciba algún tipo de compensación o ganancia. </b>
    </p>
    <p>
      
    </p>
    <p>
      La Figura 3.16 proporciona una representación simple de la dinámica del servicio dentro de una unidad de negocio. Los activos de los clientes se utilizan para impulsar el logro de los resultados del negocio. Cuanto mejor se desempeñen los activos del cliente, más resultados del negocio se pueden lograr. Sin embargo, el buen funcionamiento de los activos de los clientes se ve frenado por las limitaciones. Estas pueden ser restricciones internas, como la falta de conocimiento o financiamiento, o restricciones externas, como una economía o regulaciones débiles.
    </p>
    <p>
      
    </p>
    <p>
      Las capacidades de la unidad de negocio coordinan, controla e implementan sus recursos para crear valor. El valor es siempre definido en el contexto de los clientes y los activos del cliente. Algunos servicios simplemente incrementan los recursos disponibles para el cliente. Por ejemplo, un servicio de almacenamiento puede garantizar que los sistemas comerciales de un cliente puedan lograr un nivel particular de rendimiento en el procesamiento de transacciones con la disponibilidad de un almacenamiento de datos de transacciones adecuado, seguro y sin errores. El servicio de almacenamiento simplemente aumenta la capacidad del sistema, aunque se podría argumentar que en realidad permite la capacidad de procesamiento de transacciones de gran volumen. Otros servicios aumentan el rendimiento de la gestión, la organización, las personas y los procesos de un cliente. Por ejemplo, un servicio de suministro de noticias proporciona datos de mercado en tiempo real para que los comerciantes los utilicen para tomar mejores y más rápidas decisiones sobre las transacciones.
    </p>
    <p>
      
    </p>
    <p>
      La relación con los clientes se fortalece cuando existe un equilibrio entre el valor creado y los retornos generados. Esta relación es un ciclo creciente. Cuanto más utiliza un cliente un servicio o producto en particular, más se enfoca la unidad de negocios en lo que el cliente está comprando. Cuanto más se centre la unidad de negocio en los bienes y servicios, más fuertes serán sus capacidades y recursos. Cuanto mejores sean los servicios, más dispuestos estarán los clientes a comprarlos. Cuanto mejores sean los rendimientos o la recuperación de costos, más aumentará la unidad de negocios su inversión en capacidades y recursos.
    </p>
    <p>
      
    </p>
    <p>
      La unidad de negocios podría ser parte de una organización en los sectores público o privado. En lugar de ingresos por ventas, podría haber ingresos por impuestos recaudados o por el logro de otros objetivos gubernamentales. En lugar de ganancias podría haber excedentes. Los clientes de la unidad de negocio pueden ser internos o externos a la organización.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="El proveedor de servicio" ID="ID_714013483" CREATED="1658541171913" MODIFIED="1658541812891" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Mientras que algunas unidades de una organización son unidades de negocio, otras son claramente proveedores de servicios internos. Estos departamentos son entidades organizacionales, bajo un gerente, que realizan un conjunto definido de actividades para crear y brindar servicios que respaldan las actividades de las unidades de negocios. Los servicios definen la relación entre las unidades de negocio y sus contrapartes proveedoras de servicios.
    </p>
    <p>
      
    </p>
    <p>
      Los proveedores de servicios y su relación con las unidades de negocio se ilustran en la Figura 3.17. Los proveedores de servicios utilizan activos de servicios para prestar servicios (en este caso, servicios de TI) a la unidad de negocio. Estos servicios están diseñados para mejorar el rendimiento de los activos del cliente y/o para reducir el efecto de las restricciones.
    </p>
    <p>
      
    </p>
    <p>
      Así como los activos del cliente están sujetos a restricciones, también lo están los activos del servicio. Estas restricciones pueden ser similares, ej. financiación o capacidad limitada. El proveedor de servicios puede invertir en servicios de un proveedor para ayudar a reducir estas limitaciones o para mejorar el rendimiento de los activos del servicio. En este caso, el proveedor vería los activos del servicio como activos del cliente y los servicios de TI como resultados de negocio. El proveedor tendría que invertir en los recursos y capacidades apropiados para apoyar a su cliente. De esta manera, la cadena de clientes y proveedores de servicios podría extenderse hacia arriba y hacia abajo a través de varias iteraciones.
    </p>
    <p>
      
    </p>
    <p>
      Sin embargo, en la estrategia del servicio, el proveedor de servicios generalmente solo se preocupa por el resultado comercial de su cliente, suponiendo que estos clientes estén haciendo lo mismo por sus clientes.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="La gestión de servicio TI" ID="ID_1354223827" CREATED="1658541195526" MODIFIED="1658546421160" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En el contexto de los activos del cliente y del servicio, <b><u>la gestión de servicios de TI es la gestión de los activos del servicio (recursos y capacidades) utilizados para prestar servicios que respaldan el logro de los resultados de negocio del cliente</u></b>. Los clientes pueden ser externos o internos.
    </p>
    <p>
      
    </p>
    <p>
      La figura 3.18 ilustra cómo la gestión de servicios permite que los activos del servicio funcionen de acuerdo con los requisitos del cliente, mientras identifica y reduce el impacto de las restricciones en los activos del servicio. La gestión de servicios de TI hace esto mediante la gestión de las capacidades y los recursos de TI. Esto es. ya sea internamente o mediante el apoyo de proveedores de servicios externos y proveedores de tecnología. Lograr esto no es sencillo y, a menudo, lleva varios años de arduo trabajo y cambio cultural. El enfoque básico para lograr esto se describe a continuación.
    </p>
    <p>
      
    </p>
    <p>
      La mayoría de los proveedores de servicios de TI comienzan organizando sus departamentos según la especialización técnica. Este es un principio importante ya que cada tipo de tecnología es muy especializada y requiere personas con habilidades especializadas para manejarla. En la Figura 3.19 se muestra una organización de TI muy simple. Este organigrama, si bien indica las líneas de autoridad y la especialización técnica de cada departamento, no indica cómo se relaciona con sus clientes. Además, no muestra cómo el servicio es proveído. Un simple cuadro de organización no muestra lo que se dedica la organización, ni como lo hace ni para quien lo hace.
    </p>
    <p>
      
    </p>
    <p>
      Otro inconveniente de este tipo de organización es que el establecimiento de objetivos y la presentación de informes se realizan en silos. Los empleados se contratan en función de la experiencia para una tecnología o función específica, en lugar de las competencias en planificación estratégica, experiencia empresarial, previsión o gestión de métricas. Cada gerente funcional o de tecnología percibe al otro como un competidor más que como un socio; posicionándose para la prioridad de recursos, presupuesto y avance.
    </p>
    <p>
      
    </p>
    <p>
      Este tipo de organización evita que los problemas entre silos se resuelvan en niveles bajos. En su lugar, los problemas deben escalarse a los gerentes funcionales, quienes luego abordan los problemas con otros gerentes funcionales, quienes a veces no desean cooperar con sus rivales. En otras palabras, los gerentes se ven obligados continuamente a resolver problemas de bajo nivel, tomándose su tiempo. lejos de los problemas de los clientes de alto nivel. Los colaboradores de bajo nivel, en lugar de resolver estos problemas, se ven a sí mismos como implementadores pasivos, simplemente tomando órdenes y brindando información técnica. Los problemas interfuncionales con frecuencia no se abordan y, a menudo, pasan desapercibidos en la organización.
    </p>
    <p>
      
    </p>
    <p>
      La oportunidad de mejorar una organización a menudo se encuentra en estas grietas: el espacio en blanco del organigrama (el &quot;espacio en blanco&quot; del organigrama se examina en Rummler, 1995). Son los puntos en los que las cajas interactúan y pasan información.
    </p>
    <p>
      
    </p>
    <p>
      La gestión de servicios de TI debe poder garantizar que la experiencia técnica se centre en sus competencias, pero también debe alinear los departamentos para brindar y respaldar mejor los servicios de TI. Los procesos mapean las actividades, el conocimiento, la información y los datos que deben cruzar el &quot;espacio en blanco&quot; para cumplir con los objetivos de la organización.
    </p>
    <p>
      
    </p>
    <p>
      La Figura 3.20 muestra cómo los procesos vinculan los silos organizacionales y los alinean con los requisitos del cliente.
    </p>
    <p>
      
    </p>
    <p>
      Algunos procesos pueden ser autónomos dentro de un área funcional, mientras que otros son multifuncionales. Algunos procesos gestionan y producen un producto o servicio recibido por un cliente externo a TI. El desempeño organizacional mejora a medida que estos procesos lo permiten. La disciplina de estos procesos se conoce comúnmente como gestión de servicios de TI (ITSM). <b>ITSM significa pensar en TI como un conjunto cohesivo de recursos y capacidades de negocio</b>. Estos recursos y capacidades se gestionan a través de procesos y, en última instancia, se representan como servicios. Sin embargo, es importante no solo implementar procesos y asumir que se entregará el servicio. Centrarse solo en los resultados significará que la organización de TI está brindando servicios medibles, pero no garantiza que estos servicios cumplan con los resultados de negocio de los clientes.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="La gestión de servicio TI como un proveedor de servicio para clientes internos y externos" ID="ID_241540042" CREATED="1658541242495" MODIFIED="1658547390259" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Desde un punto de vista del negocio, la gestión de servicios de TI permite la entrega de servicios los cuales serán usados para conseguir los resultados del negocio. (Figura 3.21)
    </p>
    <p>
      
    </p>
    <p>
      La Sección 3.2.2.3 muestra cómo TI proporciona servicios orientados al cliente y apoya los servicios del negocio. Esto significa que TI debe mirar más allá de simplemente entregar un resultado al negocio. Más bien, las prioridades de TI deben estar claramente alineadas con otros impulsores del valor empresarial. Para que TI organice sus actividades en torno a los objetivos de negocio, la organización debe vincularse a los procesos y servicios de negocio, no solo observarlos. El liderazgo de TI debe entablar un diálogo significativo con los propietarios de la línea de negocio y comunicarse en términos de los resultados deseados.
    </p>
    <p>
      
    </p>
    <p>
      Las organizaciones se están enfocando menos en la infraestructura de TI y más en cómo automatizar los procesos comerciales de extremo a extremo y brindar servicios comerciales. El desafío es comprender los objetivos operativos del proceso de negocio y traducirlos en actividades que la infraestructura de TI pueda proporcionar.
    </p>
    <p>
      
    </p>
    <p>
      Superar este problema es el objetivo de los procesos descritos en el Capítulo 4. La Figura 3.22 ilustra la relación entre el proveedor de servicios, la unidad de negocios y los resultados comerciales del cliente.
    </p>
    <p>
      
    </p>
    <p>
      En la Figura 3.22, un proveedor de servicios de TI brinda servicios a una unidad de negocio interna, lo que le permite lograr los resultados de negocio deseados. En este diagrama, la naturaleza de los resultados comerciales determina qué activos de clientes necesitará la unidad comercial. El proveedor de servicios utiliza sus activos de servicio para brindar un servicio que satisfaga las necesidades de la unidad de negocios. La dinámica de esta relación de servicio se ilustra en la Figura 3.22 y es la siguiente (una flecha con un '+' representa una relación directamente proporcional; y una flecha con un '~' representa una relación inversamente proporcional)
    </p>
    <ul>
      <li>
        Para lograr los resultados , la unidad de negocio necesita un nivel mínimo de servicio. El potencial de rendimiento del servicio indica qué utilidad y garantía tendrá el servicio. Esto indicará, en términos de unidad de negocio, el rendimiento del que será capaz el servicio. La unidad de negocios puede entonces determinar si eso será adecuado para permitir que los activos de sus clientes produzcan el nivel deseado de resultados. Cuanta más utilidad y garantía, mayor será el potencial de rendimiento.
      </li>
      <li>
        Cuanto más altos sean los niveles de utilidad y garantía, menor será el riesgo de que el servicio no cumpla con los requisitos del cliente; y que los resultados de negocio no se cumplirán.
      </li>
      <li>
        Los requisitos de utilidad y garantía se trasladan a TI (según el nivel de riesgo aceptable para la empresa y el potencial de servicio que requieren). TI usa esta información para decidir cómo utilizará sus activos de servicio para entregar el servicio. Cuantas más capacidades y recursos tenga (o construya o adquiera) el proveedor de servicios, mayor será el nivel de servicio que podrá ofrecer. Esto se llama el potencial de servicio.
      </li>
      <li>
        Cuanto mayor sea el potencial del servicio y menor el riesgo, mayor será el costo del servicio. Este costo debe ser cubierto o justificado por los resultados que se logren.
      </li>
      <li>
        Cuanto más exitoso sea el desempeño del servicio, mayor será la probabilidad de que sea utilizado por la unidad de negocios, lo que aumentará la demanda del servicio.
      </li>
      <li>
        Cuanto mayor sea la demanda del servicio, mayor será la demanda de los activos del servicio, lo que reducirá la cantidad de capacidad ociosa.
      </li>
      <li>
        Si la utilización aumenta demasiado y supera el potencial del servicio, la empresa deberá decidir si está preparada para financiar un aumento del potencial de rendimiento; de lo contrario, correrá un mayor riesgo de fallas en el servicio.
      </li>
    </ul>
    <p>
      La Figura 3.22 ilustra y refuerza la necesidad de que el proveedor de servicios de TI que mire más allá del servicio para comprender cómo los resultados del negocio afectarán la estrategia de un servicio. La capacidad de aumentar el potencial de servicio y el potencial de rendimiento se analiza a continuación.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Incrementando el potencial de servicio" ID="ID_910906794" CREATED="1658541305001" MODIFIED="1658547605909" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En la figura 3.22, las capacidades y los recursos (activos del servicio) de un proveedor de servicios representan el potencial del servicio o la capacidad productiva disponible para los clientes a través de un conjunto de servicios. Los proyectos que desarrollan o mejoran capacidades y recursos aumentan el potencial del servicio. Existe una mayor eficiencia en la utilización de esos activos y, por lo tanto, potencial de servicio debido a las mejoras de capacidad en la gestión de la configuración. Ejemplos similares se dan en la Tabla 3.8. Uno de los objetivos clave de la gestión de servicios es mejorar el potencial de servicio de sus capacidades y recursos.
    </p>
    <p>
      
    </p>
    <p>
      <b>A través de la gestión de la configuración, todos los activos de servicio deben etiquetarse con el nombre de los servicios a los que agregan potencial de servicio.</b>&nbsp;Esto ayuda a las decisiones relacionadas con la mejora del servicio y la gestión de activos. Las relaciones claras hacen que sea más fácil determinar el impacto de los cambios, hacer casos de negocios para inversiones en activos de servicio e identificar oportunidades para economías de escala y alcance. Identifica activos de servicio críticos en toda la cartera de servicios para un cliente o espacio de mercado determinado.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Incremento en el potencial de desempeño" ID="ID_1158343853" CREATED="1658547606955" MODIFIED="1658548192877" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La Figura 3.22 muestra que los servicios ofrecidos por un proveedor de servicios representan el potencial para aumentar el rendimiento de los activos de los clientes. Sin este potencial, no hay justificación para que los clientes adquieran los servicios. Visualice y defina el potencial de rendimiento de los servicios para que todas las decisiones que tomen los gerentes se basen en la creación de valor para los clientes. Este enfoque evita muchos de los problemas de las empresas de servicios donde el valor para los clientes se crea en formas intangibles y, por lo tanto, más difíciles de definir y controlar. Trabajar hacia atrás desde el potencial de rendimiento de los clientes garantiza que los proveedores de servicios siempre estén alineados con las necesidades comerciales, independientemente de la frecuencia con la que cambien esas necesidades.
    </p>
    <p>
      
    </p>
    <p>
      El potencial de rendimiento de los servicios aumenta principalmente al tener la combinación adecuada de servicios para ofrecer a los clientes y al diseñar esos servicios para tener un impacto en el negocio de los clientes. Las preguntas clave a ser consultadas son:
    </p>
    <ul>
      <li>
        ¿Quiénes son nuestros clientes?
      </li>
      <li>
        ¿Qué quieren esos clientes?
      </li>
      <li>
        ¿Podemos ofrecer algo único a esos clientes?
      </li>
      <li>
        ¿Están las oportunidades ya saturadas con buenas soluciones?
      </li>
      <li>
        ¿Tenemos el portafolio correcto de servicios desarrollados para las oportunidades dadas?
      </li>
      <li>
        ¿Tenemos el catálogo de servicios ofrecido a un cliente específico?
      </li>
      <li>
        ¿Está cada servicio diseñado para soportar los resultados requeridos?
      </li>
      <li>
        ¿Cada servicio está siendo operado para soportar los resultados requeridos?
      </li>
      <li>
        ¿Tenemos los modelos y estructuras correctos para ser un proveedor de servicio?
      </li>
    </ul>
    <p>
      La capacidad productiva de los activos de servicio se transforma en la capacidad productiva de los activos de los clientes. Un aspecto importante de la entrega de valor para los clientes a través de los servicios es la reducción de los riesgos para los clientes. Al decidir utilizar un servicio, los clientes a menudo buscan evitar asumir ciertos riesgos y costos. Por lo tanto, el potencial de desempeño de los servicios también surge de la eliminación de costos y riesgos de los negocios del cliente.
    </p>
    <p>
      
    </p>
    <p>
      Por ejemplo, un servicio que procesa pagos o transferencias de fondos de manera segura para el cliente reduce los riesgos de pérdidas financieras por error y fraude y, al mismo tiempo, reduce el costo por transacción al aprovechar economías de escala y alcance en nombre del cliente. El proveedor de servicios puede implementar el mismo conjunto de activos de servicio para procesar un gran volumen de transacciones y liberar al cliente de tener que poseer y operar dichos activos. Para ciertas funciones comerciales, como nómina, finanzas y administración, el cliente puede enfrentar el riesgo financiero. de activos infrautilizados o sobreutilizados y, por lo tanto, puede preferir un servicio ofrecido por un proveedor de servicios independiente.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Demanda, capacidad y costo" ID="ID_544620752" CREATED="1658548195201" MODIFIED="1658548411602" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Cuando los servicios son efectivos para aumentar el rendimiento potencial de los activos del cliente, hay un aumento en la demanda de los servicios ( Figura 3.22), La demanda de servicios va acompañada de una compensación de los clientes por los niveles de servicio recibidos. La forma de pago recibida depende del tipo de acuerdo entre el proveedor de servicios y la unidad de negocio. Cuanto más altos sean los niveles de servicio, mayor será la compensación que los proveedores esperan lograr.
    </p>
    <p>
      
    </p>
    <p>
      A medida que aumenta la madurez de la gestión de servicios, es posible ofrecer mayores niveles de utilidad y garantía sin un aumento proporcional de los costos. Debido al efecto de los costos fijos y los gastos generales, los costos de proporcionar unidades adicionales de producción de servicios pueden disminuir con un aumento en la demanda de servicios. Los activos de servicio están en un estado productivo cuando se dedican a respaldar los activos de los clientes. En cada ciclo de demanda del cliente, el valor es creado por un ciclo de entrega correspondiente. La creación de valor para el cliente coincide con la captura de valor para el proveedor de servicio.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.2.5.3 Activos estrategicos" FOLDED="true" ID="ID_1390062731" CREATED="1658189976914" MODIFIED="1658580587254" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un activo estratégico es cualquier activo (cliente o servicio) que proporciona la base para una competencia central, un desempeño distintivo o una ventaja competitiva sostenible, o que califica a una unidad de negocio para participar en oportunidades comerciales. Los activos estratégicos son de naturaleza dinámica. Se espera que continúen desempeñándose bien bajo las cambiantes condiciones comerciales y los objetivos de su organización. Eso requiere activos estratégicos para tener capacidades de aprendizaje. El desempeño en el futuro inmediato debe beneficiarse del conocimiento y la experiencia adquiridos en el pasado.
    </p>
    <p>
      
    </p>
    <p>
      Parte de la estrategia de servicio es identificar cómo la TI puede verse como un activo estratégico en lugar de una función administrativa interna. Como se discutió en la sección 3.2.3, es importante que TI pueda vincular sus servicios con los resultados el negocio, lo que a su vez contribuirá a la ventaja competitiva de la organización y la diferenciación en el mercado.
    </p>
    <p>
      
    </p>
    <p>
      Percibir la TI como un habilitador comercial estratégico valioso y confiable no sucede de la noche a la mañana. Se necesita un esfuerzo concertado y formal en el que TI demuestre su contribución en un área a la vez. Cada nuevo desafío que TI le permite al negocio. superar, y cada nuevo espacio de mercado que TI permite que la empresa domine, proporciona credibilidad adicional para TI como proveedor de servicios estratégicos. Esto se ilustra en la Figura 3.23.
    </p>
    <p>
      
    </p>
    <p>
      En la Figura 3.23, el proveedor de servicios se mueve a través de tres ciclos de construcción de confianza y percepción de valor. El primer ciclo está indicado por las formas blancas, el segundo por las formas claras y el tercero por las formas oscuras. Para la mayoría de los proveedores de servicios de TI, este ciclo debe iniciarse mientras se entregan los servicios existentes.
    </p>
    <p>
      
    </p>
    <p>
      El ciclo funciona de la siguiente manera (el número de cada punto se menciona en la Figura 3.23):
    </p>
    <ol>
      <li>
        Dentro de un proveedor de servicios de TI existente, el ciclo comienza cuando el proveedor de servicios y el negocio han seleccionado una oportunidad, que podría ser un servicio existente o una iniciativa que ya ha sido definida. El valor de esta oportunidad está definido en términos de resultados que se necesitan ser alcanzados y la inversión requerida para cumplirlos. La oportunidad también especifica cuales clientes y espacios de mercado la oportunidad direcciona.
      </li>
      <li>
        El proveedor de servicio asegura que las capacidades y recursos están en su lugar para la entrega del o los servicios.
      </li>
      <li>
        Cuando son entregados a los niveles de servicio acordados, esos servicios permiten al negocio alcanzar sus objetivos, o superar el reto definido. Es importante que esos logros sean documentados y reportados a los interesados.
      </li>
      <li>
        El cliente percibe que el proveedor de servicio TI ha entregado valor.
      </li>
      <li>
        Como resultado, el cliente está dispuesto a confiar aún más oportunidades al proveedor de servicio.
      </li>
    </ol>
    <p>
      Solo tres iteraciones de el ciclo se muestran aquí, pero en realidad el ciclo se repite muchas más veces sobre un tiempo significante (usualmente hasta 3 años) antes que el proveedor de servicio TI interno sea visto como completamente confiable y como una unidad estratégica de negocio.
    </p>
  </body>
</html></richcontent>
<node TEXT="Gestión de servicio como activo estratégico" ID="ID_286988342" CREATED="1658541348857" MODIFIED="1658581000991" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La gestión de servicio es vista como un activo estratégico (en lugar de un conjunto de procesos operacionales puramente) cuando este puede demostrar como permite la provisión de servicio para competir y diferenciarse de si mismo efectivamente. La gestión de servicio lo hace por medio de:
    </p>
    <ul>
      <li>
        Establecer un catálogo de servicios que contribuye a los objetivos estratégicos&nbsp;y resultados del negocio.
      </li>
      <li>
        Identifica en cuales espacios del mercado TI permite al negocio competir.
      </li>
      <li>
        Define como esos servicios cumplen los retos de negocio y cuando medirlos para asegurar que son alcanzados.
      </li>
      <li>
        Construir capacidades y recursos para entregar esos servicios y superar los retos identificados.
      </li>
      <li>
        Comunicarse con el negocio sobre los entregables obtenidos.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
<node TEXT="3.3 Proveedores de Servicio" FOLDED="true" ID="ID_4739159" CREATED="1658189995085" MODIFIED="1658581117571" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;No existe tal cosa como una industria de servicios. Solo hay industrias cuyos componentes de servicio son mayores o menores que los de otras industrias. Todo el mundo está en el servicio.&quot;
    </p>
    <p>
      Profesor Emérito Theodore Levitt, Escuela de Negocios de Harvard
    </p>
  </body>
</html></richcontent>
<node TEXT="Figura 3.24 Proveedores tipo I" ID="ID_1288034976" CREATED="1658582377849" MODIFIED="1658582390853" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.24%20Proveedores%20tipo%20I.png" SIZE="0.8583691" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.25 Proveedores tipo II comunes" ID="ID_434356853" CREATED="1658582438281" MODIFIED="1658582448750" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.25%20Proveedores%20tipo%20II%20comunes.png" SIZE="0.77021825" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.26 Proveedores tipo III" ID="ID_249055131" CREATED="1658584310469" MODIFIED="1658584317697" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.26%20Proveedores%20tipo%20III.png" SIZE="0.7692308" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 3.9 Decisiones del cliente sobre tipos de proveedor de servicio" ID="ID_833945206" CREATED="1658622331000" MODIFIED="1658622577925" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 100%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Desde/hacia
          </p>
        </th>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Tipo I
          </p>
        </th>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Tipo II
          </p>
        </th>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Tipo III
          </p>
        </th>
      </tr>
      <tr>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Tipo I
          </p>
        </th>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Reorganización funcional
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Agregación
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Tercerización
          </p>
        </td>
      </tr>
      <tr>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Tipo II
          </p>
        </th>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Desagregación
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Reorganización Corporativa
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Tercerización
          </p>
        </td>
      </tr>
      <tr>
        <th valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Tipo III
          </p>
        </th>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Internalización
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Internalización
          </p>
        </td>
        <td valign="top" style="width: 25%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Valor neto de reorganización
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="Tipos de proveedores de servicio" ID="ID_1138767487" CREATED="1658581130416" MODIFIED="1658581275928" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Es necesario distinguir entre diferentes tipos de proveedores de servicios. Si bien la mayoría de los aspectos de la gestión de servicios se aplican por igual a todos los tipos de proveedores de servicios, otros aspectos como los clientes, los contratos, la competencia, los espacios de mercado, los ingresos y la estrategia adquieren diferentes significados según el tipo específico. Hay tres tipos principales de proveedores de servicios:
    </p>
    <ul>
      <li>
        Tipo | - proveedor de servicios interno
      </li>
      <li>
        Tipo II - unidad de servicios compartidos
      </li>
      <li>
        Tipo III: proveedor de servicios externo.
      </li>
    </ul>
    <p>
      En aras de la simplicidad, cada una se define a continuación como si fuera la única opción utilizada en una sola organización. En realidad, la mayoría de las organizaciones tienen una combinación de proveedores de servicios de TI. En una sola organización, es posible que algunas unidades de TI estén dedicadas a una sola unidad de negocios, otras brinden servicios compartidos y otras se hayan subcontratado.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.3.1 Tipo I (Proveedor de servicios interno)" ID="ID_218799910" CREATED="1658190004696" MODIFIED="1658582206480" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los proveedores Tipo I son proveedores de servicio dedicados, y usualmente dentro de una unidad individual de negocio. Las unidades de negocio por si mismas podrían ser parte de una empresa más grande o un organización padre. Las funciones de negocio tales como financiero, administrativo, logística, recursos humanso y TI proveen servicios requeridos por varias partes del negocio. Se financian con gastos generales y deben operar estrictamente dentro de los mandatos del negocio. Los proveedores tipo I tienen el beneficio de un estrecho acoplamiento con sus propietarios-clientes, evitando ciertos costos y riesgos asociados con la realización de negocios con partes externas.
    </p>
    <p>
      
    </p>
    <p>
      Ya que los proveedores de servicios tipo I están dedicados a unidades de negocio específicas, se les exige un conocimiento profundo del negocio y sus objetivos, planes y operaciones. Por lo general, son altamente especializados y, a menudo, se centran en el diseño, la personalización y el soporte de aplicaciones específicas, o en el soporte de un tipo específico de proceso comercial.
    </p>
    <p>
      
    </p>
    <p>
      Los principales objetivos de los proveedores tipo I deben lograr la excelencia funcional y la rentabilidad de sus unidades de negocio (Goold y Campbell, 2002). Se especializan en atender un conjunto relativamente limitado de necesidades comerciales. Los servicios se pueden personalizar en gran medida y los recursos se dedican a proporcionar niveles de servicio relativamente altos. El gobierno y la administración de las funciones comerciales son relativamente sencillos. Los derechos de decisión están restringidos en términos de las estrategias y modelos operativos de la unidad de negocio. Los gerentes generales de las unidades de negocios toman todas las decisiones clave, como la cartera de servicios a ofrecer, las inversiones en capacidades y recursos y las métricas para medir el desempeño y los resultados.
    </p>
    <p>
      
    </p>
    <p>
      Los proveedores tipo I operan dentro de los espacios del mercado interior. Su crecimiento está limitado por el crecimiento de la unidad de negocio a la que pertenecen. Cada unidad de negocio (BU) puede tener su propio proveedor tipo I. El éxito de los proveedores tipo I no se mide en términos de ingresos o ganancias porque tienden a operar sobre una base de recuperación de costos con financiamiento interno. Todos los costos corren a cargo de la unidad comercial o empresa propietaria.
    </p>
    <p>
      
    </p>
    <p>
      La Figura 3.24 muestra tres unidades de negocio (BU) con proveedores de servicio tipo I. Cada unidad de TI está dedicada a una sola unidad de negocios y brinda servicios especializados solo a esa unidad de negocios.
    </p>
    <p>
      
    </p>
    <p>
      La competencia para los proveedores tipo I viene desde fuera de la unidad de negocio, tal como funciones de negocio corporativas, quienes ejercen ventajas tales como escala, alcance y autonomía. En general, los proveedores de servicio que sirven a mas de un cliente encaran mucho menor riesgo de una falla de mercado. Con múltiples fuentes de demanda, la demanda máxima de una fuente puede compensarse con una demanda baja de otra. Hay duplicación y desperdicio cuando los proveedores tipo I se replican dentro de la empresa. Esto es especialmente cierto cuando varias unidades de negocios necesitan usar datos que mantienen otras unidades de negocios. Esto da como resultado la duplicación (en la que no hay dos conjuntos de datos &quot;iguales&quot;) o la integración de sistemas complejos, lo que a menudo genera problemas de rendimiento y dificultades para mantener la vigencia de los sistemas.
    </p>
    <p>
      
    </p>
    <p>
      Para aprovechar las economías de escala y alcance, Los proveedores tipo I a menudo se consolidan en una función comercial corporativa cuando existe un alto grado de similitud en sus capacidades y recursos. En este nivel de agregación de los proveedores tipo I equilibran las necesidades de la empresa con las de la unidad de negocio. Las compensaciones pueden ser complejas y requieren una cantidad significativa de atención y control por parte de los altos ejecutivos. Como tal, los proveedores tipo I consolidados son más apropiados cuando las clases de activos como TI, I+D, marketing o fabricación son el núcleo de la ventaja competitiva de la organización y, por lo tanto, necesitan un control cuidadoso.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.3.2 Tipo II (Unidad de servicios compartidos)" FOLDED="true" ID="ID_1456985376" CREATED="1658190025759" MODIFIED="1658582831888" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Funciones como finanzas, TI, recursos humanos y logística no siempre son el núcleo de la ventaja competitiva de una organización. Por lo tanto, no es necesario que se mantengan a nivel corporativo donde exigen la atención del equipo del director ejecutivo (Goold y Campbell, 2002). En cambio, los servicios de tales funciones compartidas se consolidan en una unidad especial autónoma llamada unidad de servicios compartidos (SSU) (que se muestra en la Figura 3.25). En este diagrama, TI se muestra como un solo departamento con un catálogo de servicio que está disponible a múltiples unidades de negocio.
    </p>
    <p>
      
    </p>
    <p>
      El modelo de la Figura 3.25 permite una estructura de gobierno más delegada bajo la cual las SSUs pueden concentrarse en servir a las unidades de negocios como clientes directos. Las SSU pueden crear, hacer crecer y mantener un mercado interno para sus servicios y modelarse a sí mismas siguiendo las líneas de los proveedores de servicios en el mercado abierto. Al igual que las funciones comerciales corporativas, pueden aprovechar las oportunidades en toda la empresa y distribuir sus costos y riesgos entre una base más amplia. A diferencia de las funciones comerciales corporativas, tienen menos protecciones bajo la bandera de valor estratégico y competencia central. Están sujetos a comparaciones con proveedores de servicios externos cuyas prácticas comerciales, modelos operativos y estrategias deben emular y cuyo rendimiento deben aproximarse, si no superar.
    </p>
    <p>
      
    </p>
    <p>
      Aunque la figura 3.25 muestra diferentes tipos de servicios compartidos en la SSU, muchas organizaciones de TI son unidades de tipo II separadas y no se combinan con otros servicios corporativos. Cuando se usa el término Tipo I, esta publicación se refiere principalmente al proveedor de servicios de TI (ya sea que sea parte de una SSU o de un departamento separado).
    </p>
    <p>
      
    </p>
    <p>
      Los clientes del Tipo II son unidades de negocios bajo una matriz corporativa, partes interesadas comunes y una estrategia de nivel empresarial. Lo que puede ser subóptimo para una unidad comercial en particular puede estar justificado por las ventajas obtenidas a nivel corporativo por las cuales la unidad comercial puede ser compensada. El tipo II puede ofrecer precios más bajos en comparación con los proveedores de servicios externos aprovechando las ventajas corporativas, los acuerdos internos y las políticas contables. Con la autonomía para funcionar como una unidad de negocios, los proveedores de Tipo II pueden tomar decisiones fuera de las restricciones de las políticas a nivel de unidad de negocios. Pueden estandarizar sus ofertas de servicios en todas las unidades comerciales y utilizar precios basados en el mercado para influir en los patrones de demanda.
    </p>
    <p>
      
    </p>
    <p>
      Un proveedor de servicios Tipo II exitoso puede encontrarse en una posición en la que puede brindar sus servicios tanto externa como internamente. En estos casos, son proveedores de servicios tanto de Tipo II como de Tipo III. En estos casos, es importante tomar una decisión estratégica para proporcionar servicios tanto externa como internamente, y establecer las estructuras de gobierno y gestión adecuadas. Este no es solo un caso de prestación de servicios existentes externamente.
    </p>
  </body>
</html></richcontent>
<node TEXT="Fijación de precio basado en el mercado" ID="ID_1876882890" CREATED="1658582836418" MODIFIED="1658583089923" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La fijación de precios basada en el mercado se refiere al enfoque de definir el costo de TI para el negocio en términos de lo que las organizaciones externas cobrarían por un servicio similar. Con la fijación de precios basada en el mercado, hay una necesidad mínima de discusiones y negociaciones complejas sobre requisitos, tecnologías, asignaciones de recursos, arquitecturas y diseños (que serían necesarios con los arreglos Tipo |) porque los precios impulsarían ajustes, autocorrecciones y optimización en ambos lados de la ecuación de valor.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Provisión de servicios internos y externos" ID="ID_1453981341" CREATED="1658583091366" MODIFIED="1658583413031" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las decisiones estratégicas y de gobierno incluyen cómo la organización contabilizará los ingresos, si la organización de TI se dividirá en dos unidades (una interna y otra externa), cómo venderá TI sus servicios, asegurando que los objetivos originales de servicio todavía puedan ser cumplidos, mientras al mismo tiempo expanden su uso a clientes externos (ej: ¿quienes obtienen prioridad cuando hay problemas de capacidad?).
    </p>
    <p>
      
    </p>
    <p>
      Algunas unidades de negocios pueden no estar satisfechas con el proveedor Tipo II, ya sea porque sus expectativas no han sido priorizadas por el negocio en general, o porque están insatisfechas con algún aspecto de la calidad del servicio. Si estas unidades de negocios tienen financiamiento, pueden intentar competir directamente con el proveedor de Tipo II mediante la creación de organizaciones de TI &quot;falsas&quot; o &quot;en la sombra&quot; dentro de la unidad de negocios. Este es un problema de gobierno y, a menudo, los ejecutivos de la organización no lo hacen cumplir, ya que la estructura de servicio de la organización no se comprende bien. Esto no debe confundirse con un proveedor de servicios híbrido formal, en el que proveedores de servicio del tipo I de tipo II coexisten dentro de la misma organización, uno centrado en servicios compartidos y el otro centrado en aplicaciones y servicios específicos de BU.
    </p>
    <p>
      
    </p>
    <p>
      En el contexto de una híbrida tipo I y tipo II, una competencia clave requerida es poder cumplir con las prioridades en competencia y mantenerse cerca de aquellos departamentos que no tienen prioridad y trabajar con ellos para establecer expectativas y proporcionar los servicios posibles dentro de sus presupuestos.
    </p>
    <p>
      
    </p>
    <p>
      Las unidades de servicios compartidos líderes en la industria han sido escindidas con éxito por sus padres como negocios independientes que compiten en el mercado externo. Se convierten en una fuente de ingresos a partir del estatuto inicial de simplemente proporcionar una ventaja de costos.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.3.3 Tipo III (proveedor de servicios externo)" FOLDED="true" ID="ID_9417727" CREATED="1658190044578" MODIFIED="1658584128247" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un proveedor de servicios de Tipo III es un proveedor de servicios que brinda servicios de TI a clientes externos.
    </p>
    <p>
      
    </p>
    <p>
      Las estrategias comerciales de los clientes a veces requieren capacidades fácilmente disponibles de un proveedor de Tipo III. Los riesgos adicionales que asumen los proveedores de Tipo III sobre los de Tipo I y el tipo II se justifican por una mayor flexibilidad y libertad para buscar oportunidades. Los proveedores del tipo III pueden ofrecer precios competitivos y reducir los costos unitarios al consolidar la demanda.
    </p>
    <p>
      
    </p>
    <p>
      Ciertas estrategias comerciales no son atendidas adecuadamente por proveedores de servicios internos como Tipo I y Tipo II. Los clientes pueden seguir estrategias de abastecimiento que requieran servicios de proveedores externos. La motivación puede ser el acceso a conocimiento, experiencia, escala, alcance, capacidades y recursos que están más allá del alcance de la organización o fuera del alcance de una cartera de inversiones cuidadosamente considerada. Las estrategias comerciales a menudo requieren reducciones en la base de activos, los costos fijos y los riesgos operativos, o la redistribución de los activos financieros.
    </p>
    <p>
      
    </p>
    <p>
      Los entornos comerciales competitivos a menudo requieren que los clientes tengan estructuras flexibles y eficientes. En tales casos, es mejor comprar servicios en lugar de poseer y operar los activos necesarios para ejecutar ciertas funciones y procesos comerciales. Para tales clientes, el Tipo II es la mejor opción para un conjunto dado de servicios.
    </p>
    <p>
      
    </p>
    <p>
      La figura 3.26 ilustra una organización que ha subcontratado varios servicios y componentes de TI a proveedores del tipo III, cada uno con un catálogo de servicios que pueden seleccionar las unidades de negocio.
    </p>
    <p>
      
    </p>
    <p>
      Aunque esto no se muestra en la Figura 3.26, se debe tener en cuenta que las organizaciones que utilizan proveedores de servicios Tipo II aún necesitarán una función o funciones internas de TI para administrar la especificación de servicios, coordinar los contratos y garantizar que se cumplan los resultados comerciales.
    </p>
    <p>
      
    </p>
    <p>
      La experiencia de los proveedores del tipo III a menudo no se limita a ninguna empresa o mercado. La amplitud y profundidad de tal experiencia es a menudo la fuente de valor más distintiva para los clientes. La amplitud proviene de servir a múltiples tipos de clientes o mercados. La profundidad proviene de servir múltiples del mismo tipo.
    </p>
    <p>
      
    </p>
    <p>
      Desde cierta perspectiva, los proveedores del Tipo III están operando bajo un modelo extendido de servicios compartidos a gran escala. Asumen un mayor nivel de riesgo de sus clientes en comparación con Tipo I y Tipo II. Pero sus capacidades y recursos son compartidos por sus clientes, algunos de los cuales pueden ser rivales. Esto significa que los clientes rivales tienen acceso al mismo paquete de activos, lo que disminuye cualquier ventaja competitiva que esos activos otorgan.
    </p>
    <p>
      
    </p>
    <p>
      Al mismo tiempo, los proveedores de servicios de tipo III tienen mayor libertad para seleccionar el negocio en el que quieren estar. Pueden definir su cartera de servicios de la manera más estrecha o amplia que deseen y decidir no ofrecer cierto tipo de servicio. o comprometerse con ciertos tipos de clientes. Esto les permite ser más ágiles y les permite descartar negocios que podrían ser riesgosos.
    </p>
    <p>
      
    </p>
    <p>
      Un aspecto adicional de los proveedores de servicio tipo III es que muchos de ellos proveen capacidades y actividades que son usados por un proveedor de servicio tipo I o II para soportar sus servicios. Por ejemplo, una organización TI podría contratar la administración de un servidor, para enfocarse en la administración de las aplicaciones.
    </p>
  </body>
</html></richcontent>
<node TEXT="Nota importante acerca de los proveedores de servicio tipo III" ID="ID_1195248869" CREATED="1658584131843" MODIFIED="1658584267027" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Es muy importante no externalizar ningún aspecto de los servicios de TI y la gestión de servicios que sean fundamentales para gestionar proveedores o las relaciones entre TI y sus clientes. Las organizaciones que externalizan la gestión de proveedores, por ejemplo, pierden la capacidad de negociar con los proveedores, especificar lo que necesitan y gestionar el rendimiento de los proveedores. Más allá de esto, no es ético que un proveedor de servicios gestione el contrato en virtud del cual está contratado por el cliente.
    </p>
    <p>
      
    </p>
    <p>
      Una empresa de subcontratación tampoco está bien posicionada para gestionar las relaciones con los clientes de sus clientes. Esta relación es clave para crear y entregar valor y, por lo tanto, solo la empresa, no el subcontratista, es responsable de administrar esa relación. <b><u>Esto significa que ni la gestión de las relaciones comerciales ni la gestión del nivel de servicio deben subcontratarse nunca.</u></b>
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Seguridad" ID="ID_702793130" CREATED="1658584340966" MODIFIED="1658584442761" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La seguridad es siempre un problema en los entornos de servicios compartidos. Pero cuando el entorno se comparte con los competidores, la seguridad se convierte en una preocupación mayor. Este es un factor de costos adicionales para los proveedores de Tipo III. Como contrapartida, los proveedores de Tipo III mitigan un tipo de riesgo inherente a los Tipos I y II: las funciones comerciales y los proveedores de servicios compartidos están sujetos al mismo sistema de riesgos que su unidad comercial o empresa matriz. Esto establece un círculo vicioso, mediante el cual los riesgos que enfrentan las unidades de negocios o la empresa se transfieren al proveedor de servicios y luego se retroalimentan con amplificación a través de los servicios utilizados. Los clientes pueden reducir los riesgos sistémicos transfiriéndolos a proveedores de servicios externos que distribuyen esos riesgos en una red de mayor valor.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.3.4 Como los clientes eligen entre tipos?" FOLDED="true" ID="ID_1959757445" CREATED="1658190063284" MODIFIED="1658621879622" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Desde la perspectiva del cliente, existen ventajas y desventajas con cada tipo de proveedor. Los servicios, la infraestructura, las aplicaciones, etc. pueden obtenerse de cada tipo de proveedor de servicios con decisiones basadas en los costos de transacción, los factores estratégicos de la industria, la competencia central y las capacidades de gestión de riesgos del cliente.
    </p>
  </body>
</html></richcontent>
<node TEXT="Caso de estudio: Externalización" ID="ID_707453457" CREATED="1658621751095" MODIFIED="1658621870651" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El proveedor interno de servicios de TI de un conglomerado global decidió subcontratar todas las operaciones del centro de datos a proveedores de servicios externos. El principal impulsor fueron los costos más bajos. Cinco años y varias fusiones y adquisiciones después, y a pesar de haber logrado sus reducciones de costos, el proveedor interno está considerando internalizar todas las operaciones del centro de datos. La razón de esto fue la siguiente.
    </p>
    <p>
      
    </p>
    <p>
      El proveedor de Tipo II para el conglomerado había logrado sus reducciones de costos a través de una relación con un proveedor de servicios de Tipo III. Sin embargo, como resultado de la actividad de fusiones y adquisiciones, la compañía creció para incluir proveedores tipo I. Cuando la empresa volvió a examinar su estrategia de servicio, se dio cuenta de que podía incorporar y consolidar a todos los proveedores de servicios en un solo Tipo II, a un costo más bajo y con una diferenciación tecnológica mejorada que no está disponible en ningún Tipo III.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Principio de costos de transacción" ID="ID_1242380656" CREATED="1658621872239" MODIFIED="1658622295527" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El principio de los costos de transacción es útil para explicar por qué los clientes pueden preferir un tipo de proveedor a otro. Los costos de transacción son los costos generales de realizar un negocio con un proveedor de servicios. Más allá del costo de compra de los servicios vendidos, incluyen, entre otros, el costo de encontrar y seleccionar proveedores calificados, definir requisitos, negociar acuerdos, medir el desempeño, gestionar la relación con los proveedores, el costo de resolver disputas y realizar cambios o enmiendas a los acuerdos. Cuanto más difícil es (y más ineficiente es el proceso), menos probable es que el cliente elija ese proveedor de servicios.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Criterios para externalización" ID="ID_1409363438" CREATED="1658622299033" MODIFIED="1658622322885" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Además, si los clientes mantienen una actividad empresarial internamente (agregada), la separan para una gestión dedicada (desagregada) o la obtienen de fuera (tercerización) depende de las respuestas a las siguientes preguntas (Milgrom y Roberts, 1992):
    </p>
    <ul>
      <li>
        ¿La actividad requieren activos altamente especializados? ¿Estarán inactivos u obsoletos esos activos si esa actividad ya no se realiza? (En caso afirmativo, desagregar y/o subcontratar)
      </li>
      <li>
        ¿Con qué frecuencia se realiza la actividad dentro de un período o ciclo comercial? ¿Es poco frecuente o esporádico? (En caso afirmativo, subcontratar)
      </li>
      <li>
        ¿Requiere la actividad conocimientos específicos para una unidad de negocios en particular, incluso si la actividad es poco frecuente y/o especializada? (En caso afirmativo, desagregar o internalizar)
      </li>
      <li>
        ¿Qué tan compleja es la actividad? ¿Es simple y rutinario? ¿Es estable en el tiempo con pocos cambios? (En caso afirmativo, subcontratar). Si es complejo y volátil, es posible que deba desagregarse.
      </li>
      <li>
        ¿Es difícil definir un buen desempeño? (En caso afirmativo, agregue)
      </li>
      <li>
        ¿Es difícil medir un buen desempeño? (En caso afirmativo, agregue)
      </li>
      <li>
        ¿Está estrechamente relacionado con otras actividades o activos en el negocio? ¿Separarlo aumentaría la complejidad y causaría problemas de coordinación? (En caso afirmativo, agregue)
      </li>
    </ul>
    <p>
      Basado en las respuestas a estas preguntas, los clientes podrían decidir cambiarse entre los tipos de proveedor de servicio (Tabla 3.9). Las respuestas a estas preguntas por si mismas podrían cambiar sobre el tiempo dependiendo de nuevas condiciones económicas, regulaciones o innovación tecnológica.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Análisis tabla 3.9" ID="ID_786775382" CREATED="1658622599880" MODIFIED="1658623013486" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En la Tabla 3.9 se indican los siguientes tipos de decisión
    </p>
    <ul>
      <li>
        <b>Reorganización funcional</b>&nbsp;En esta estrategia, la empresa ha pasado por una reorganización funcional, que requiere una nueva estructura de proveedores de servicios tipo I, por lo que el proveedor de servicios actual se reorganiza para poder brindar servicios mejores o más rentables.
      </li>
      <li>
        <b>Reorganización corporativa</b>&nbsp;En esta estrategia, se mantiene un modelo de proveedor de servicios Tipo II, pero las unidades de servicios compartidos se reorganizan para brindar mejores o servicios más rentables.
      </li>
      <li>
        <b>Reconfiguración neta de valor</b>&nbsp;Esta estrategia requiere una reorganización de la forma actual en que los proveedores de servicios externos entregan y crean valor. Algunos proveedores nuevos se integran en la red de valor, otros se mudan y otros desempeñarán un papel diferente. Un ejemplo de esto es la aparición de los modelos de computación en la nube.
      </li>
      <li>
        <b>Agregación</b>&nbsp;En esta estrategia, los servicios están centralizados (combinados) bajo un solo proveedor Tipo II.
      </li>
      <li>
        <b>Desagregación</b>&nbsp;Esta estrategia implica la descentralización (desagregación) de la unidad de servicios compartidos para que el servicio sea proporcionado por un proveedor de servicios dedicado para cada BU, servicio o actividad.
      </li>
      <li>
        <b>Subcontratación</b>&nbsp;Esta estrategia consiste en utilizar un proveedor de servicios de Tipo III para proporcionar un servicio que antes brindaba un proveedor de servicios interno. Tenga en cuenta que primero puede ser necesario desagregar, ya que no siempre es posible subcontratar todas las actividades o funciones juntas.
      </li>
      <li>
        <b>Internalización</b>&nbsp;Esta estrategia requiere que un proveedor de servicios interno comience a brindar un servicio que anteriormente brindaba un proveedor de servicios externo.
      </li>
    </ul>
    <p>
      Los clientes pueden adoptar una estrategia de abastecimiento que combine las ventajas y mitigue los riesgos de los tres tipos de proveedores de servicios. En tales casos, la red de valor que respalda a un cliente atraviesa los límites de más de una organización. Como parte de una estrategia de abastecimiento cuidadosamente considerada, los clientes pueden asignar sus necesidades entre los diferentes tipos de proveedores de servicios en función del tipo que mejor proporcione los resultados comerciales que desean. Los servicios, la infraestructura, las aplicaciones, etc. básicos (a menudo especializados) se buscan en de proveedores de&nbsp;&nbsp;Tipo I o Tipo II, mientras que los servicios complementarios que mejoran los servicios básicos se buscan de proveedores de Tipo II o Tipo III.
    </p>
    <p>
      
    </p>
    <p>
      En un entorno de fuentes múltiples, el centro de gravedad de una red de valor se basa en el tipo de proveedor de servicios que domina la cartera de abastecimiento. Las decisiones de subcontratación o desagregación alejan el centro de gravedad del núcleo corporativo. Las decisiones de agregación o de internalización acercan el centro de gravedad al núcleo corporativo y están impulsadas por la necesidad de mantener las ventajas específicas de la empresa que no están disponibles para los competidores. Ciertas decisiones no cambian el centro de gravedad, sino que reasignan servicios entre proveedores de servicios del mismo tipo.
    </p>
    <p>
      
    </p>
    <p>
      La estructura de abastecimiento puede verse alterada debido a cambios en los fundamentos comerciales del cliente, lo que hace que un tipo de proveedor de servicios sea más deseable que otro. Por ejemplo, la fusión o adquisición de un cliente puede alterar drásticamente la economía que sustenta una estrategia de abastecimiento hasta ahora sólida.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.3.5 La ventaja relativa de la incumbencia" ID="ID_12905119" CREATED="1658190127240" MODIFIED="1658623207502" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las relaciones duraderas con los clientes permiten a las organizaciones aprender y mejorar. Se cometen menos errores, se recuperan las inversiones y la ventaja de costos resultante se puede aprovechar para aumentar la brecha con los competidores.
    </p>
    <p>
      
    </p>
    <p>
      A los clientes les resulta menos atractivo alejarse de los titulares de buen desempeño debido a los costos de cambio. La experiencia se puede utilizar para mejorar activos como los procesos, el conocimiento y las competencias que son de naturaleza estratégica.
    </p>
    <p>
      
    </p>
    <p>
      Por lo tanto, los proveedores de servicios deben centrarse en proporcionar la base para una relación duradera con los clientes. Requiere que ejerzan la planificación y el control estratégicos para garantizar que los objetivos comunes impulsen todo, el conocimiento se comparta de manera efectiva entre las unidades y la experiencia se retroalimente en planes y acciones futuras para una curva de aprendizaje menos pronunciada.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.4 Como definir servicios" FOLDED="true" ID="ID_1347178337" CREATED="1658190184796" MODIFIED="1658623421509" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las secciones anteriores de este capítulo describieron los servicios, los clientes, el valor y los proveedores de servicios. Comprender estos conceptos y cómo la organización se relaciona con ellos ayuda a definir qué servicios se entregarán y a qué clientes.
    </p>
    <p>
      
    </p>
    <p>
      Esta sección analiza cómo identificar a los clientes y sus requisitos, y si existe una oportunidad que el proveedor de servicios pueda satisfacer. Luego discutirá la definición real de servicios que abordará cada oportunidad.
    </p>
    <p>
      
    </p>
    <p>
      Estos pasos normalmente ocurrirán como parte de la gestión de la cartera de servicios (discutidos en la sección 4.2), pero se analizan aquí para ilustrar la aplicación de los principios contenidos en la sección anterior. Los pasos se enumeran aquí y se analizan a continuación:
    </p>
    <ul>
      <li>
        Paso 1 - Definir el mercado e identificar a los clientes
      </li>
      <li>
        Paso 2 - Comprender al cliente
      </li>
      <li>
        Paso 3 - Cuantificar los resultados
      </li>
      <li>
        Paso 4 - Clasificar y visualizar el servicio
      </li>
      <li>
        Paso 5 - Comprender las oportunidades (espacios de mercado)
      </li>
      <li>
        Paso 6 - Definir servicios basados en resultados
      </li>
      <li>
        Paso 7 - Modelos de servicios
      </li>
      <li>
        Paso 8 - Definir unidades y paquetes de servicio.
      </li>
    </ul>
  </body>
</html></richcontent>
<node TEXT="Figura 3.27 Analizar como un servicio impactará un resultado" ID="ID_1568987919" CREATED="1658753154649" MODIFIED="1658753182632" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.27%20Analizar%20como%20un%20servicio%20impactara%20un%20resultado.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.28 Clasificando servicios usando arquetipos de servicio y activos del cliente" ID="ID_1956510356" CREATED="1658758883345" MODIFIED="1658758896887" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.28%20Clasificando%20servicios%20usando%20arquetipos%20de%20servicio%20y%20activos%20del%20cliente.png" SIZE="0.7643312" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.29 Estrategias basadas en activos y basadas en la utilidad" ID="ID_263469236" CREATED="1658758938916" MODIFIED="1658758961335" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.29%20Estrategias%20basadas%20en%20activos%20y%20basadas%20en%20la%20utilidad.png" SIZE="0.877193" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.30 Visualización de servicios como patrones de creacion de valor" ID="ID_720451707" CREATED="1658758999543" MODIFIED="1658759013813" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.30%20Visualizacion%20de%20servicios%20como%20patrones%20de%20creacion%20de%20valor.png" SIZE="0.955414" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.31 Definiendo servicios con componentes de utilidad" ID="ID_1436563586" CREATED="1658760926199" MODIFIED="1658760945435" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.31%20Definiendo%20servicios%20con%20componentes%20de%20utilidad.png" SIZE="0.7566204" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.32 Definiendo servicios con componentes de garantía" ID="ID_500346270" CREATED="1658760976191" MODIFIED="1658760985365" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.32%20Definiendo%20servicios%20con%20componentes%20de%20garantia.png" SIZE="0.7662835" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.33 Dinamica de un modelo de servicio" ID="ID_176463129" CREATED="1658764482789" MODIFIED="1658764497276" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.33%20Dinamica%20de%20un%20modelo%20de%20servicio.png" SIZE="0.7692308" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.34 Un paquete de servicio" ID="ID_1795563731" CREATED="1658764647652" MODIFIED="1658764657469" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.34%20Un%20paquete%20de%20servicio.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.35 Los Paquetes de servicio pueden consistir de múltiples servicios individuales de cualquier tipo" ID="ID_225917561" CREATED="1658765292278" MODIFIED="1658765307624" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.35%20Los%20Paquetes%20de%20servicio%20pueden%20consistir%20de%20multiples%20servicios%20individuales%20de%20cualquier%20tipo.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.36 Los paquetes de servicio pueden contener otros paquetes de servicio" ID="ID_1527006883" CREATED="1658765352859" MODIFIED="1658765363704" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.36%20Los%20paquetes%20de%20servicio%20pueden%20contener%20otros%20paquetes%20de%20servicio.png" SIZE="0.86580086" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 3.10 Definiendo los componentes de servicio accionables" ID="ID_595372310" CREATED="1658761538301" MODIFIED="1658761941258" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 100%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            <b>Tipo de servicio </b>
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            <b>Utilidad (Parte A y B)</b>
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ¿Qué&nbsp;servicios proveemos?
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ¿Quiénes son nuestros clientes?
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ¿Qué resultados soportamos?
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ¿Como ellos crean valor para sus clientes?
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ¿Que restricciones dan la cara nuestros clientes?
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            <b>Activos del cliente </b>
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            <b>Activos del servicio</b>
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ¿Cuáles activos del cliente soportamos?
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ¿Quiénes son los usuarios de nuestros servicios?
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ¿Qué activos implementamos para proveer valor?
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ¿Cómo implementamos nuestros activos?
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            <b>Actividad o tarea </b>
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            <b>Garantía</b>
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ¿Qué tipo de actividad soportamos?
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ¿Cómo damos seguimiento al desempeño?
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ¿Cómo creamos valor para ellos?
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            ¿Qué aseguramiento proveemos?
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="Tabla 3.11 Segmentación de usuario y paquetes de usuario" ID="ID_409979384" CREATED="1658775135509" MODIFIED="1658776063330" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 400px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Tipos de usuario
          </p>
        </th>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Paquetes
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Trabajador de oficina estándar (2,500 staff)
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Paquete estándar (Configuración de hardware de escritorio básico estándar más paquete de servicio de escritorio)
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Especialista basado en la seguridad de la oficina (50 staff)
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Paquete estándar (Configuración de hardware de escritorio básico estándar más paquete de servicio de escritorio) más paquete de servicio de seguridad
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Especialista en seguridad industrial y salud de la oficina (50 staff)
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Paquete estándar (Configuración de hardware de escritorio básico estándar más paquete de servicio de escritorio) más paquete de servicio de salud y seguridad industrial
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Trabajador estándar de oficina y ocasional en la casa (300 staff)
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Paquete estándar&nbsp;(Configuración de hardware de escritorio básico estándar más paquete de servicio) más paquete de servicio VPN
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Trabajador desde la casa (200 staff)
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Paquete del hogar (Configuración de hardware de escritorio de la casa más paquete de servicio básico más paquete de servicio para el hogar)
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Trabajador móvil
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Paquete móvil (configuración portátil estándar más paquete de servicio básico) más paquete de servicio móvil
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            VIP
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Paquete VIP (Configuración de portátil VIP más paquete de servicio básico) más paquete de servicio VIP
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="3.4.1 Paso 1 - Definir le mercado e identificar los clientes" FOLDED="true" ID="ID_288747672" CREATED="1658190198832" MODIFIED="1658753170613" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En este contexto, un mercado puede definirse como el grupo de clientes que están interesados y pueden permitirse comprar el servicio que ofrece un proveedor de servicios, y a quienes el proveedor de servicios puede legalmente y logísticamente suministrar esos servicios.
    </p>
    <p>
      
    </p>
    <p>
      El primer paso para definir los servicios es comprender el mercado en el que opera el proveedor de servicios. Esto ayudará a reducir las opciones abiertas para el proveedor de servicios. Por ejemplo:
    </p>
    <p>
      
    </p>
    <ul>
      <li>
        Un proveedor de servicios Tipo I normalmente solo atenderá a una unidad de negocio. Todo su mercado consiste en un solo cliente interno.
      </li>
      <li>
        Un mercado de proveedores de servicios de Tipo II constará de varias unidades de negocio. Por lo general, no definirán ni proporcionarán servicios de TI fuera de la organización. Si lo hacen, generalmente son servicios comerciales automatizados.
      </li>
      <li>
        Un proveedor de servicios del Tipo III no puede proporcionar un número ilimitado de servicios a todos los mercados. Por lo general, brindan un servicio genérico a varios mercados (p. ej., servicios de Internet para usuarios residenciales y pequeñas empresas), o brindan servicios especializados a un solo mercado (p. ej., el monitoreo y el control de manufactura).
      </li>
    </ul>
  </body>
</html></richcontent>
<node TEXT="La definición de los mercados" ID="ID_1308058812" CREATED="1658623739727" MODIFIED="1658624190800" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los Mercados pueden definirse mediante uno o más criterios tales como como sigue:
    </p>
    <ul>
      <li>
        <b>Industria</b>&nbsp;(p. ej., fabricación, comercio minorista, servicios financieros, atención de la salud, transporte, etc.)
      </li>
      <li>
        <b>Geográfico</b>&nbsp;(p. ej., un país o una región específicos) Un proveedor de servicios puede proporcionar el mejor servicio de su clase, pero decide limitar su disponibilidad a una sola región geográfica. Un proveedor de servicios diferente puede proporcionar un estándar de servicio más bajo, pero lo brinda de manera consistente en varias regiones, lo que facilita que los clientes se estandaricen en un solo proveedor para todas las regiones.
      </li>
      <li>
        <b>Datos demográficos</b>&nbsp;El proveedor de servicios puede brindar un servicio orientado hacia un grupo cultural específico (por ejemplo, programación de televisión para un cliente de habla hispana en los EE. UU.) o un grupo de clientes con ingresos similares (p. ej. servicios de lujo o económicos).
      </li>
      <li>
        <b>Relaciones societarias</b>&nbsp;Algunos proveedores de servicios se han constituido específicamente para prestar servicios a un grupo de empresas con una participación accionarial común, y no pueden comercializar dichos servicios a competidores de dichas empresas.
      </li>
    </ul>
    <p>
      En todos los casos, identificar los mercados en los que el proveedor de servicios está activo es una parte importante de identificar qué servicios entregará el proveedor de servicios ya qué clientes.
    </p>
    <p>
      
    </p>
    <p>
      En algunos casos identificar al cliente sera lo mismo que identificar el mercado. Por ejemplo un proveedor de tipo I, o un proveedor que entrega soporte para un sistema especializado en una ubicación específica. Si solo hay un proveedor de servicio de ese tipo en esa ubicación, entonces todas las organizaciones que usan el sistema son potenciales clientes.
    </p>
    <p>
      
    </p>
    <p>
      En otros casos el mercado es más grande y requerirá mayor investigación y análisis para identificar clientes potenciales, o tipos de clientes (ej: servicios de telefonía móvil pensados para familias que necesitan múltiples líneas). Identificar cada cliente sería imposible, pero entender el 60% de esas personas en una región particular que potencialmente podría utilizar el servicio determinará que el mercado es viable, y también ayudará a moldear los planes de marketing.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.4.2 Paso 2 - Entendiendo al cliente" FOLDED="true" ID="ID_139062331" CREATED="1658190236525" MODIFIED="1658751843082" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Para los proveedores de servicios internos, comprender al cliente significa comprender las estrategias comerciales generales y los objetivos de la organización, y cómo cada unidad comercial los cumple. También significa comprender los resultados comerciales que cada unidad comercial debe lograr.
    </p>
    <p>
      
    </p>
    <p>
      Para un proveedor de servicios externo, comprender al cliente significa comprender por qué necesita el servicio que está comprando. El proveedor de servicios no tiene que comprender la estrategia, las tácticas y las operaciones detalladas del cliente, pero sí debe comprender las razones por las que el cliente necesita el servicio y qué características son importantes.
    </p>
    <p>
      
    </p>
    <p>
      Comprender al cliente implica comprender:
    </p>
    <ul>
      <li>
        <b>Resultados comerciales deseados</b>&nbsp;Los clientes utilizan sus activos para lograr resultados específicos. Comprender cuáles son estos resultados ayudará al proveedor de servicios a definir la garantía y la utilidad de los servicios, y a priorizar las necesidades del servicio.
      </li>
      <li>
        <b>Activos del cliente</b>&nbsp;Los servicios permiten y respaldan el rendimiento de los activos que utiliza el cliente para lograr sus resultados comerciales. Por lo tanto, es necesario al definir los servicios a. comprender el vínculo entre el servicio y los activos del cliente.
      </li>
      <li>
        <b>Restricciones</b>&nbsp;Todos los activos del cliente estarán limitados por algún tipo de restricción: falta de fondos, falta de conocimiento, regulaciones, legislación, etc. Comprender esas restricciones permitirá al proveedor de servicios definir los límites del servicio y también ayudará al cliente a superar, o trabajar dentro, muchos de ellos.
      </li>
      <li>
        <b>Cómo se percibirá y medirá el valor</b>&nbsp;Los clientes siempre miden el rendimiento, la calidad y el valor. Muchos proveedores de servicios establecen complejos sistemas de seguimiento y medición sin comprender cómo los clientes miden el servicio. Esto da como resultado una desalineación entre las expectativas del cliente y la entrega real del proveedor de servicios. Por lo tanto, es vital que el proveedor de servicios comprenda cómo el cliente mide el servicio, incluso si el proveedor de servicios no puede medir el servicio de la misma manera.
      </li>
    </ul>
    <p>
      A los gerentes comerciales se les otorga la responsabilidad, la autoridad y los recursos necesarios para lograr ciertos resultados utilizando los mejores medios posibles. Los servicios son un medio para que los gerentes habiliten o mejoren el rendimiento de los activos comerciales que conducen a mejores resultados. El valor de un servicio se mide mejor en términos de la mejora en los resultados que se pueden atribuir al impacto del servicio en el desempeño de los activos comerciales. Algunos servicios aumentan el rendimiento de los activos del cliente, algunos servicios mantienen el rendimiento y otros restauran el rendimiento después de eventos adversos. Un aspecto importante de proporcionar valor es prevenir o reducir la variación en el desempeño de los activos del cliente.
    </p>
    <p>
      
    </p>
    <p>
      En un sistema de comercio, por ejemplo, no es suficiente que el servicio alimente el sistema de comercio con datos de mercado en tiempo real. Para minimizar las pérdidas comerciales, la fuente de datos debe estar disponible sin interrupción durante el horario comercial y en tantas mesas comerciales como sea necesario con un sistema de contingencia implementado. Por lo tanto, un banco de inversión está dispuesto a pagar una prima por un servicio de transmisión de noticias que proporcione un mayor nivel de garantía que un servicio utilizado por un competidor. La diferencia se traduce en mayores ganancias comerciales.
    </p>
  </body>
</html></richcontent>
<node TEXT="Enfocarse en los activos del cliente" ID="ID_73551138" CREATED="1658751846409" MODIFIED="1658751958237" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El desempeño de los activos del cliente debería ser la principal preocupación de los profesionales de la gestión de servicios porque sin los activos del cliente no existe una base para definir el valor de un servicio.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.4.3 Paso 3 - Cuantificando los resultados" ID="ID_1071071947" CREATED="1658190252140" MODIFIED="1658753055199" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En este paso el proveedor de servicio trabajará con el cliente para identificar sus resultados deseados. Estas definiciones necesitan estar claras y medibles y necesitan ser algo que pueda ser enlazado al servicio.
    </p>
    <p>
      
    </p>
    <p>
      En el siguiente ejemplo, un banco prestamista crea valor al lograr el resultado de procesar una solicitud de préstamo a tiempo. Los clientes que reciben el préstamo tendrán acceso al capital financiero requerido y el prestamista se beneficia desde el inicio y el devengo de intereses. El proceso de préstamo es, por lo tanto, un activo comercial cuyo desempeño conduce a resultados comerciales específicos. Procesar la solicitud de préstamo a tiempo facilita el proceso para el cliente y crea una ventaja competitiva.
    </p>
    <p>
      
    </p>
    <p>
      Esto se ilustra en la Figura 3.27 (según Ulwick, 2005), en la que el resultado deseado por el banco es procesar la solicitud de préstamo a tiempo. La medida que se utilizará es el número de solicitudes de préstamo que se pudieron procesar a tiempo. El objetivo del servicio no es solo permitir que se logren estos resultados, sino también aumentar el número que se logrará. En este caso, se ha lanzado una campaña de marketing y es necesario actualizar el servicio actual para hacer frente al aumento previsto de solicitudes de préstamo.
    </p>
    <p>
      
    </p>
    <p>
      Definir los resultados es una parte importante de la definición de los servicios, pero los clientes a menudo dan por sentado que todos entienden sus resultados particulares porque trabajan en ellos como una cuestión de rutina. Por lo tanto, es importante que el proveedor de servicios trabaje con el cliente para cuantificar cada resultado y documentarlo como parte de la descripción del servicio que se ingresará en la canalización del servicio (consulte la sección 4.2).
    </p>
    <p>
      
    </p>
    <p>
      Comprender cómo los servicios afectan los resultados y, por lo tanto, qué tipo y nivel de servicio se necesita, requerirá que el proveedor de servicios mapee los servicios y los resultados. Aunque esto puede ser más difícil para los proveedores de servicios externos, un buen proceso de gestión de relaciones comerciales ayudará al proveedor de servicios a definir y documentar los resultados en términos que el proveedor de servicios pueda medir. La valoración de servicios y activos de servicios se vuelve más fácil cuando es posible visualizar el tipo de salida que ellos facilitan. Mapear los resultados con los servicios y los activos de los servicios puede ser realizado como parte del sistema gestión&nbsp;de la configuración y el portafolio del servicio (ver la sección 4.2).
    </p>
    <p>
      
    </p>
    <p>
      Cualquier resultado que no esté bien respaldado representa una oportunidad para el proveedor de servicios. Por lo tanto, es importante revisar el logro de los resultados con regularidad, tanto para garantizar que el proveedor de servicios no pierda una oportunidad como para asegurarse de que se están entregando los resultados actuales, para no abrir las puertas a un competidor. Los resultados para revisión se encontrarán en las siguientes ubicaciones:
    </p>
    <ul>
      <li>
        <b>El catálogo de servicios</b>&nbsp;Cada servicio en el catálogo de servicios debe estar claramente vinculado a resultados definidos y cuantificados. La revisión del nivel de servicio o las encuestas de gestión de relaciones comerciales deben verificarse periódicamente para garantizar que estos resultados sigan siendo válidos y que los resultados adicionales se identifiquen y documenten a través de la gestión de la cartera de servicios.
      </li>
      <li>
        <b>La canalización del servicio</b>&nbsp;Estos son los resultados que un nuevo servicio, o un cambio importante en un servicio existente, debe poder respaldar. Estos se utilizarán como criterio para el éxito del diseño y la validación del servicio.
      </li>
      <li>
        <b>Acuerdo de nivel de servicio</b>&nbsp;Muchos acuerdos de nivel de servicio especificarán el servicio, así como el resultado para el que ha sido diseñado. Esto asegurará que el vínculo entre los dos se mantenga y se mida conscientemente.
      </li>
    </ul>
    <p>
      Obtener información sobre el negocio del cliente y tener un buen conocimiento de los resultados del cliente es esencial para desarrollar una relación comercial sólida con los clientes. Esta es una actividad clave dentro de la gestión de relaciones comerciales (ver sección 4.5).
    </p>
    <p>
      
    </p>
    <p>
      Una definición de servicios basada en resultados garantiza que los gerentes planifiquen y ejecuten todos los aspectos de la gestión de servicios desde la perspectiva de lo que es valioso para el cliente. Este enfoque garantiza que los servicios no solo creen valor para los clientes, sino que también capturen valor para el proveedor del servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.4.4 Paso 4 - Clasificando y visualizando el servicio" FOLDED="true" ID="ID_549258717" CREATED="1658190274050" MODIFIED="1658758375452" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Cada servicio es único, pero muchos tienen características similares. Si un nuevo servicio comparte características en común con un servicio existente, será más fácil determinar que se necesitará para entregar el servicio. Si no tiene características en común con servicios existentes, esto significa que el servicio necesitará ser evaluado y diseñado desde el inicio.
    </p>
    <p>
      
    </p>
    <p>
      Crear una forma de clasificar los servicios y representarlos visualmente ayudará a identificar si un nuevo requisito de servicio se ajusta a la estrategia actual o si representará una expansión de esa estrategia. También podría ayudar al proveedor de servicios a decidir no realizar una inversión en un servicio que lo aleje de su estrategia.
    </p>
    <p>
      
    </p>
    <p>
      Una forma de clasificar los servicios es utilizar arquetipos de servicios o bloques de construcción básicos para los servicios. En la Figura 3.28, se identifican varios arquetipos de servicio en los recuadros a la izquierda del diagrama, etiquetados como U1-9. La 'U' se usa para representar la utilidad del servicio, y los arquetipos se basan en el conjunto actual de recursos y capacidades del proveedor de servicios. El lado derecho del diagrama representa los activos del cliente que son compatibles con cada servicio (ya sea mejorando el rendimiento del activo o la reducción del impacto de las restricciones), etiquetado como A1-9. Los servicios se crean cuando se utiliza un arquetipo de servicio para respaldar un activo de cliente.
    </p>
    <p>
      
    </p>
    <p>
      Por ejemplo, U3xA3 indicaría un servicio que se usa para definir, consolidar y limpiar los datos que se usan para dar soporte a uno o más procesos. Esto significa que el proveedor de servicios tiene las habilidades genéricas para administrar las tareas de integración de datos y, en este caso, ha aplicado esas habilidades para respaldar un proceso o procesos en particular. Por ejemplo, TI garantiza que los datos requeridos de varias fuentes diferentes puedan consolidarse en una sola interfaz y ser utilizados por la empresa para verificar los niveles de existencias, realizar pedidos y entregar productos a los clientes.
    </p>
  </body>
</html></richcontent>
<node TEXT="Mapeo de arquetipos" ID="ID_592534870" CREATED="1658758343381" MODIFIED="1658758374304" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El mapeo de arquetipos de servicios y activos de clientes también puede ser útil para definir estrategias o revelar patrones de demanda o competencia que se han construido a lo largo del tiempo. Esto es especialmente útil para aquellos proveedores de servicios a los que se les pidió que entregaran cualquier servicio que la empresa demandara en el pasado, sin tener una estrategia clara. Este tipo de mapeo proporcionará una línea de base a partir de la cual podrán identificar futuras oportunidades y servicios.
    </p>
    <p>
      
    </p>
    <p>
      Por ejemplo, el proveedor de servicios puede aprender que muchos servicios se basan en el mismo arquetipo de servicio. Pueden aprender que sus recursos y capacidades están destinados principalmente a respaldar los procesos comerciales. Esta sería una estrategia de servicio basada en activos - representada por la flecha vertical en la figura 3.29.
    </p>
    <p>
      
    </p>
    <p>
      Alternativamente, ellos podrían aprender que los&nbsp;diferencia es la capacidad de proporcionar servicios administrativos que respaldan una amplia gama de activos de los clientes. Esto se muestra con la flecha horizontal en la Figura 3.29, que representa una estrategia basada en la utilidad.
    </p>
    <p>
      
    </p>
    <p>
      La mayoría de las organizaciones tienen una combinación de patrones de servicio basados en utilidades y activos, pero la visualización de los servicios les ayuda a comprender dónde son más fuertes en cada uno y dónde necesitan fortalecer su cartera, recursos o capacidades.
    </p>
    <p>
      
    </p>
    <p>
      Esta combinación de arquetipos de servicio y activos del cliente a menudo da como resultado una serie de patrones que indican las posiciones donde el proveedor de servicios es fuerte. Los servicios con patrones muy parecidos indican una oportunidad de consolidación o empaquetado como servicios compartidos. Si el tipo de activo de las aplicaciones aparece en muchos patrones, los proveedores de servicios pueden invertir más en capacidades y recursos que respaldan los servicios relacionados con las aplicaciones. De manera similar, si muchos patrones incluyen el arquetipo de apoyo, es una indicación de que el apoyo se ha convertido en una capacidad central. Estos son solo ejemplos simples de cómo se puede visualizar el catálogo de servicios como una colección de patrones útiles. La estrategia de servicio puede resultar en una colección particular de patrones (estrategia prevista) o una colección de patrones puede hacer que una estrategia de servicio particular sea atractiva (estrategia emergente).
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Patrones de servicio basado en arquetipos" ID="ID_386634082" CREATED="1658758387341" MODIFIED="1658758827150" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La Figura 3.30 muestra ejemplos de patrones de servicios como se describe arriba. En este diagrama, un solo servicio puede construirse a partir de uno o más arquetipos de servicio y puede admitir uno o más activos de cliente, como se indica a continuación:
    </p>
    <ul>
      <li>
        El Servicio A es un servicio de comunicación basado en utilidades, que admite tres tipos de activos del cliente: organización, procesos y conocimiento.
      </li>
      <li>
        El Servicio B es en gran medida un servicio de procesos basado en activos, en el que los procesos están respaldados por arquetipos de servicios de informes, control y soporte. En este servicio también se brindan servicios de soporte para respaldar el activo de la organización.
      </li>
      <li>
        El Servicio C es un arquetipo de servicio de soporte que respalda activos financieros, información y aplicaciones.
      </li>
      <li>
        El Servicio D es una variedad de arquetipos de servicios (administrativos, de informes, de control y de soporte) que respaldan los activos de las aplicaciones. También incluye soporte de informes para activos de infraestructura.
      </li>
    </ul>
    <p>
      Este método visual puede ser útil en la comunicación y coordinación entre funciones y procesos de gestión de servicios. Estas visualizaciones son la base de definiciones más formales de los servicios. La combinación adecuada del contexto de creación de valor (activos del cliente) con el concepto de creación de valor (arquetipo de servicio) puede evitar deficiencias en el rendimiento. Por ejemplo, el negocio del cliente puede implicar la revisión y el procesamiento de formularios de solicitud, solicitudes y registros de cuentas. Las preguntas del siguiente tipo pueden ser útiles:
    </p>
    <ul>
      <li>
        ¿Tenemos las capacidades para admitir aplicaciones de flujo de trabajo?
      </li>
      <li>
        ¿Cuáles son los patrones recurrentes en el procesamiento de solicitudes y formularios de solicitud?
      </li>
      <li>
        ¿Varían los patrones según la época del año, el tipo de solicitantes o en torno a eventos específicos?
      </li>
      <li>
        ¿Tenemos los recursos adecuados para soportar los patrones de actividad empresarial?
      </li>
      <li>
        ¿Existen conflictos potenciales en el cumplimiento de los compromisos de nivel de servicio? ¿Existen oportunidades de consolidación o recursos compartidos?
      </li>
      <li>
        ¿Las aplicaciones y solicitudes están sujetas al cumplimiento normativo? ¿Tenemos conocimiento y experiencia en cumplimiento normativo?
      </li>
      <li>
        ¿Estamos en contacto directo con los clientes del negocio? Se ser afirmativo, existen controles adecuados para gestionar las interacciones con el usuario y la información?
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.4.5 Paso 5 - Entendiendo las oportunidades (oportunidades de mercado)" ID="ID_25803674" CREATED="1658190295069" MODIFIED="1658759880016" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El proveedor de servicios ahora tiene una buena comprensión del cliente y sus activos, y puede asignar capacidades y recursos existentes a los activos del cliente existentes para comprender qué servicio pueden proporcionar.
    </p>
    <p>
      
    </p>
    <p>
      Cada cliente tiene una serie de requisitos, y cada proveedor de servicios tiene una serie de competencias. ¿Cómo entiende el proveedor de servicios dónde sus competencias podrán cumplir con los requisitos del cliente? Estas intersecciones entre las competencias del proveedor de servicios y los requisitos del cliente se denominan espacios de mercado.
    </p>
    <p>
      
    </p>
    <p>
      Más formalmente, <b>los espacios de mercado son las oportunidades que un proveedor de servicios de TI podría aprovechar para satisfacer las necesidades comerciales de los clientes. </b>Los espacios de mercado identifican los posibles servicios de TI que un proveedor de servicios de TI puede desear considerar entregar.
    </p>
    <p>
      
    </p>
    <p>
      <b>Un espacio de mercado se define por un conjunto de resultados comerciales, que pueden ser facilitados por un servicio. La oportunidad de facilitar esos resultados define un espacio de mercado.</b>&nbsp;Los siguientes son ejemplos de resultados comerciales que pueden ser la base de uno o más espacios de mercado:
    </p>
    <ul>
      <li>
        Los equipos de ventas son productivos con el sistema de gestión de ventas en computadoras inalámbricas.
      </li>
      <li>
        El sitio web de comercio electrónico está vinculado al sistema de gestión de almacenes.
      </li>
      <li>
        Las aplicaciones comerciales clave están monitoreadas y seguras,
      </li>
      <li>
        Los oficiales de crédito tienen un acceso más rápido a la información requerida sobre los solicitantes de préstamos.
      </li>
      <li>
        El servicio de pago de facturas en línea ofrece más opciones para que los compradores paguen.
      </li>
      <li>
        La continuidad del negocio está asegurada.
      </li>
    </ul>
    <p>
      Cada uno de los resultados está relacionado con una o más categorías de activos del cliente, como personas, infraestructura, información, cuentas por cobrar y órdenes de compra, y luego puede vincularse a los servicios que los hacen posibles. Cada resultado se puede lograr de múltiples maneras, aunque los clientes normalmente prefieren aquellos con menor costo y riesgo. Los proveedores de servicio crean las condiciones bajo las cuales los resultados serán conseguidos mediante los servicios que ellos entregan.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.4.6 Paso 6 - Definir servicios basados en los resultados" FOLDED="true" ID="ID_548808608" CREATED="1658190317654" MODIFIED="1658760284567" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una definición de servicios basada en resultados garantiza que los gerentes planifiquen y ejecuten todos los aspectos de la gestión de servicios desde la perspectiva de lo que es valioso para el cliente. Este enfoque garantiza que los servicios no solo creen valor para los clientes, sino que también capturen valor para el proveedor del servicio.
    </p>
    <p>
      
    </p>
    <p>
      Las soluciones que permiten o mejoran el rendimiento de los activos del cliente respaldan indirectamente el logro de los resultados generados por esos activos. Tales soluciones y propuestas tienen utilidad para el negocio. Cuando esa utilidad está respaldada por una garantía adecuada, los clientes están listos para comprar.
    </p>
    <p>
      
    </p>
    <p>
      Los clientes pueden expresar su insatisfacción con un proveedor de servicios incluso cuando se cumplen los términos y condiciones de los acuerdos de nivel de servicio (SLA). A menudo esto se debe a que no está claro cómo los servicios crean valor para los clientes. En estos casos, los servicios se definen a menudo en términos de recursos puestos a disposición de los clientes. Las definiciones de servicio carecen de claridad sobre el contexto en el que dichos recursos son útiles y los resultados comerciales que justifican el gasto de un servicio desde la perspectiva del cliente. Este problema conduce a diseños deficientes, operación ineficaz y desempeño mediocre en los contratos de servicios. Las mejoras del servicio son difíciles cuando no está claro dónde se requieren realmente las mejoras. Los clientes pueden comprender y apreciar las mejoras solo dentro del contexto de sus propios activos comerciales, desempeños y resultados. Una definición adecuada de los servicios tiene en cuenta el contexto en el que los clientes perciben el valor de los servicios.
    </p>
  </body>
</html></richcontent>
<node TEXT="Ejemplos definiciones de servicio bien formadas" ID="ID_1344895271" CREATED="1658760287160" MODIFIED="1658760880289" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las definiciones de servicio bien formadas conducen a procesos de gestión de servicios eficaces y eficientes. A continuación se dan ejemplos genéricos:
    </p>
    <ul>
      <li>
        <b>Ejemplo 1</b>&nbsp;Los servicios de colaboración brindan valor al cliente cuando las comunicaciones comerciales cooperativas se llevan a cabo sin restricciones de ubicación o dispositivo. Se crea valor cuando el proveedor opera para el cliente métodos de mensajería electrónica de almacenamiento y reenvío y en tiempo real, de modo que los empleados del cliente puedan componer, enviar, almacenar y recibir comunicaciones en una manera conveniente, confiable y segura, para una comunidad específica de usuarios.
      </li>
      <li>
        <b>Ejemplo 2</b>&nbsp;Los servicios de hospedaje de aplicaciones brindan valor al negocio cuando los servicios y procesos de funciones comerciales continúan operando sin la necesidad de invertir capital en una capacidad comercial no central. El valor se crea cuando el proveedor mantiene para el negocio un sistema de plataforma de software de aplicación y asegura que los empleados y los sistemas comerciales puedan trabajar continuamente de manera conveniente, segura y confiable, para una cartera específica de servicios.
      </li>
      <li>
        <b>Ejemplo 3</b>&nbsp;Los servicios móviles en el lugar de trabajo brindan valor al cliente cuando la actividad comercial se lleva a cabo sin las limitaciones de una ubicación fija. El valor se crea cuando el proveedor opera para el cliente un sistema de mensajería inalámbrica y asegura que los empleados (del cliente) y los sistemas comerciales pueden intercambiar mensajes de voz y datos de manera conveniente, confiable y segura, dentro de un área específica de cobertura.
      </li>
      <li>
        <b>Ejemplo 4</b>&nbsp;Los servicios Order-to-Cash brindan valor al negocio cuando las órdenes de compra se convierten en flujos de efectivo sin la necesidad de invertir capital en una capacidad comercial no central. El valor se crea cuando el proveedor otorga la licencia a la empresa de un sistema de cumplimiento de pedidos y asegura que los equipos de ventas y compradores en línea puedan ingresar o modificar órdenes de compra de manera conveniente, rápida y segura dentro de un horario especificado.
      </li>
    </ul>
    <p>
      En las Figuras 3.31 y 3.32 se dan dos ejemplos de definiciones de servicio basadas en resultados. Observe cómo se utilizan en estas definiciones los arquetipos de servicio y los activos de clientes específicos del paso 4.
    </p>
    <p>
      
    </p>
    <p>
      El primer ejemplo (Figura 3.31) muestra resultados basados en la utilidad de tres líneas de servicio diferentes. En este caso, los resultados se expresan en términos de los resultados logrados y las restricciones eliminadas (tenga en cuenta que la utilidad se puede lograr sin que se logren los resultados y se eliminen las restricciones).
    </p>
    <p>
      
    </p>
    <p>
      El segundo ejemplo (Figura 3.32) muestra los mismos servicios expresados en términos de garantía.
    </p>
    <p>
      
    </p>
    <p>
      Las definiciones bien construidas facilitan la visualización de patrones en catálogos de servicios y carteras que antes estaban ocultos debido a definiciones no estructuradas. Los patrones aportan claridad a las decisiones a lo largo del ciclo de vida del servicio.
    </p>
    <p>
      
    </p>
    <p>
      Las definiciones de servicios procesables son útiles cuando se dividen en elementos discretos que luego se pueden asignar a diferentes grupos, quienes los administrarán de manera coordinada para controlar el efecto general de entregar valor a los clientes.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Definir los servicios" ID="ID_1278996073" CREATED="1658761162327" MODIFIED="1658761525708" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Ser capaz de definir los servicios de una manera factible tiene sus ventajas desde una perspectiva estratégica. Elimina la ambigüedad de la toma de decisiones y evita la desalineación entre lo que quieren los clientes y los proveedores de servicios que están organizados y son lo suficientemente capaces de ofrecer. Sin el contexto en el que los clientes usan los servicios, es difícil definir completamente el valor. Sin una definición completa de valor, no puede haber una producción completa de valor. Como resultado, los resultados no se cumplen a satisfacción del cliente.
    </p>
    <p>
      
    </p>
    <p>
      Sin embargo, no quiere decir que no se pueda desarrollar un servicio sin un cliente en la mano. Simplemente significa que la historia de un servicio comienza con las necesidades de un cliente específico o una categoría de clientes (es decir, espacio de mercado). Las necesidades de los clientes existen y se satisfacen independientemente de los proveedores de servicios o sus servicios. Sin embargo, el valor para un cliente se basa no sólo en el cumplimiento de estas necesidades, sino también cómo se cumplen y, a menudo, a qué riesgos y costos. Ciertos servicios crean valor al prevenir o recuperarse de condiciones o estados indeseables. En tales casos, los clientes pueden desear un cambio en los riesgos a los que pueden estar expuestos sus activos. En cualquier caso, el efecto de segundo orden de los servicios es que los cambios que producen o evitan tienen un efecto positivo y generalmente medible en el desempeño y los resultados del negocio del cliente.
    </p>
    <p>
      
    </p>
    <p>
      Preguntas que pueden ser útiles para definir los servicios de una manera práctica. se enumeran en la Tabla 3.10.
    </p>
    <p>
      
    </p>
    <p>
      Este tipo de preguntas son cruciales para que una organización las considere en la implementación de un enfoque estratégico para la gestión de servicios. Son aplicados por todo tipo de proveedores de servicios, internos y externos. Lo que cambia es el contexto y significado de ciertas ideas como clientes, contratos, competencia, espacios de mercado, ingresos y estrategia. De hecho, estas preguntas clarificantes son particularmente importantes para los proveedores de servicios internos quienes típicamente operan dentro del dominio de una empresa o agencia de gobierno, tienen clientes que son también dueños y cuyos objetivos estratégicos podrían no siempre ser claros.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.4.7 Paso 7 - Modelos de servicio" FOLDED="true" ID="ID_1877743732" CREATED="1658190346201" MODIFIED="1658764432185" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La definición de un <b>modelo de servicio</b>&nbsp;es &quot;un modelo que muestra cómo los <b>activos del servicio interactúan con los activos del cliente para crear valor</b>. Los <b>modelos de servicio describen la estructura de un servicio</b>&nbsp;(cómo encajan los elementos de configuración) <b>y la dinámica del servicio</b>&nbsp;(actividades, flujo de recursos e interacciones). Un modelo de servicio se puede <b>utilizar como plantilla o modelo para múltiples servicios</b>.&quot;&nbsp;
    </p>
    <p>
      
    </p>
    <p>
      Los modelos de servicio pueden tomar muchas formas, desde un gráfico lógico simple que muestra los diferentes componentes y sus dependencias, hasta un modelo analítico complejo que analiza la dinámica de un servicio bajo diferentes configuraciones. y patrones de demanda.
    </p>
    <p>
      
    </p>
    <p>
      Los modelos de servicio <b>son los planos de los procesos y funciones de gestión de servicios para comunicarse y colaborar en la creación de valor</b>. Los modelos de servicio describen cómo los activos del servicio interactúan con los activos del cliente y crean valor para una cartera de contratos determinada. La interacción significa que la demanda se conecta con la capacidad de servir. <b>Los acuerdos de nivel de servicio especifican los términos y condiciones en los que se produce dicha interacción con los compromisos y expectativas de cada parte.</b>&nbsp;Los resultados definen el valor que se creará para el cliente, que a su vez se basa en la utilidad proporcionada a los clientes y la garantía.
    </p>
    <p>
      
    </p>
    <p>
      Los modelos de servicio también representan la estructura y la dinámica de los servicios, que a su vez están influenciados por los requisitos de utilidad y garantía del cliente. La estructura y la dinámica están influenciadas por factores de utilidad y garantía para ser entregados a los clientes. La estructura se define en términos de activos de servicio particulares necesarios y los patrones en los que se configuran. Las dinámicas se definen en términos de actividades, flujo de recursos, coordinación e interacciones. Esto incluye la cooperación y comunicación entre usuarios y proveedores de servicios. La dinámica de un servicio incluye patrones de actividad de negocio, patrones de demanda, excepciones y variaciones.
    </p>
    <p>
      
    </p>
    <p>
      La figura 3.33 proporciona un ejemplo de cómo se puede representar la dinámica de un servicio en un modelo de servicio. En este caso se ilustra un servicio minorista. En este ejemplo, cada componente del servicio se enumera en un tramo (o parte) separado, y cada actividad se numera en la secuencia en la que normalmente se entrega el servicio. Cada componente del servicio se enumera en relación con los otros componentes, las dependencias identificadas y los flujos de comunicación y datos indicados.
    </p>
  </body>
</html></richcontent>
<node TEXT="Usos de los modelos de servicio" ID="ID_1877566149" CREATED="1658764404964" MODIFIED="1658764415062" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los modelos de servicio tienen una serie de usos, especialmente en la gestión de carteras de servicios (consulte la sección 4.2), que incluyen:
    </p>
    <ul>
      <li>
        Comprender lo que se necesita para brindar un nuevo servicio
      </li>
      <li>
        Identificar los componentes críticos del servicio, los activos del cliente o los activos del servicio, y luego asegurar que estos están diseñados para cubrir los requerimientos de la demanda.
      </li>
      <li>
        Ilustrar como el valor es creado.
      </li>
      <li>
        Mapear los equipos y activos que están involucrados en la prestación de un servicio y garantizar que comprendan su impacto en la capacidad del cliente para lograr sus resultados comerciales
      </li>
      <li>
        Como punto de partida para diseñar nuevos servicios
      </li>
      <li>
        Como una herramienta de evaluación para comprender el impacto de los cambios en los servicios
      </li>
      <li>
        Como un medio para identificar si se pueden prestar nuevos servicios utilizando los activos existentes
      </li>
      <li>
        Si no, entonces evaluar qué tipo de inversión sería necesaria para prestar el servicio
      </li>
      <li>
        Identificar la interfaz entre la tecnología, las personas y los procesos necesarios para desarrollar y prestar el Servicio.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Mensaje clave" ID="ID_800478274" CREATED="1658763896000" MODIFIED="1658763909923" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un modelo de servicio no es un diseño. Un modelo de servicio es una lista o diagrama de elementos que se necesitarán para poder prestar el servicio. El modelo de servicio muestra cómo se relacionan estos elementos y cómo los utiliza el servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Utilidad de los modelos de servicio" ID="ID_1501838842" CREATED="1658764319795" MODIFIED="1658764330688" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los modelos de servicio son muy útiles durante la transición y la operación del servicio para ayudar a comunicar cómo interactúan los componentes para crear el servicio. Esto facilitará la realización de pruebas, la construcción y la comunicación de cambios más exhaustivas a las partes interesadas. Durante la operación del servicio, ayuda a los equipos operativos a realizar mejor la gestión de incidentes y la gestión de problemas, ya que las dependencias y los impactos están claramente indicados. También ayudan a los equipos operativos a administrar el desempeño del servicio a través de una mejor comprensión de todos los componentes que podrían afectar el desempeño.
    </p>
    <p>
      
    </p>
    <p>
      Los modelos de servicio son útiles para la eficacia en la mejora continua del servicio (CSI). Se pueden realizar mejoras en la estructura o la dinámica de un modelo. La transición del servicio evalúa las opciones o rutas de mejora y recomienda soluciones rentables y de bajo riesgo. Los modelos de servicio evolucionan continuamente, en función de los comentarios externos recibidos de los clientes y los comentarios internos de los procesos de gestión de servicios. Las actividades de CSI aseguran que la retroalimentación se transmita a todas las etapas del ciclo de vida del servicio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Herramientas para desarrollar modelos de servicio" ID="ID_18045176" CREATED="1658764354888" MODIFIED="1658764368535" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los métodos y herramientas de la ingeniería de sistemas y la gestión del flujo de trabajo son útiles para desarrollar mapas de procesos, diagramas de flujo de trabajo, modelos de colas y patrones de actividad necesarios para la integridad de los modelos de servicio. La transición del servicio evalúa los modelos de servicio detallados para garantizar que sean aptos para su propósito y uso antes de ingresar a la operación del servicio a través del catálogo de servicios. Es necesario que los modelos de servicio estén bajo control de cambios porque la utilidad y la garantía de un servicio pueden tener una variación no deseada si hay cambios en los activos del servicio o en su configuración.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.4.8 Paso 8 - Definir las unidades y paquetes de servicio" FOLDED="true" ID="ID_1739761113" CREATED="1658190362513" MODIFIED="1658764932612" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los servicios pueden ser tan simples como permitir que un usuario complete una sola transacción, pero la mayoría de los servicios son complejos. Consisten en una gama de entregables y funcionalidades. Si cada aspecto individual de estos servicios complejos se definiera de forma independiente, pronto sería imposible para el proveedor de servicios rastrear y registrar todos los servicios.
    </p>
    <p>
      
    </p>
    <p>
      Algunos servicios se pueden entregar solos, pero otros requieren servicios adicionales para que funcionen. Por ejemplo, comprar un libro es sencillo, pero comprar un libro electrónico requiere que el cliente tenga un dispositivo de lectura, un medio para conectarse con el proveedor de servicios y un medio para rastrear la licencia de la propiedad intelectual.
    </p>
    <p>
      
    </p>
    <p>
      Cuando se entrega un solo servicio a un cliente, el proveedor del servicio lo ve como un servicio. Cuando dos o más servicios se agrupan y venden o entregan juntos, el proveedor de servicios los considera como un paquete de servicios. Los paquetes de servicios se crean por dos razones principales:
    </p>
    <ul>
      <li>
        Si el servicio principal no se puede entregar sin que estén presentes los servicios habilitadores
      </li>
      <li>
        Para crear valor adicional para el cliente, lo que se traduce en ingresos adicionales y lealtad del cliente para el proveedor de servicios.
      </li>
    </ul>
    <p>
      El empaquetado de servicios requieren una comprensión de los diferentes tipos de servicios y cómo se pueden comercializar y vender. Los tres tipos básicos de servicios que se pueden prestar son: los servicios principales, los servicios de habilitación y los servicios de mejora (estos se describen en la sección 2.1.1). Estos servicios pueden, en algunos casos, ser entregados por sí solos, y en otros casos empaquetados junto con otros servicios.
    </p>
  </body>
</html></richcontent>
<node TEXT="3.4.8.1 Paquetes de servicio" FOLDED="true" ID="ID_1575025052" CREATED="1658190393294" MODIFIED="1658765145596" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una estrategia es que un proveedor de servicios tenga una pequeña cantidad de clientes y diseñe servicios únicos para cada uno. Dado que estos servicios serán costosos (debido a los altos costos de desarrollo y los recursos dedicados), hay un número muy pequeño de proveedores de servicios que podrán hacer que esta estrategia sea un éxito comercial (dado que hay un número limitado de clientes dispuestos a pagar la prima por el servicio personalizado).
    </p>
    <p>
      
    </p>
    <p>
      La mayoría de los proveedores de servicios seguirán una estrategia en la que puedan ofrecer un conjunto de servicios más genéricos a una amplia gama de clientes, logrando así economías de escala y competir sobre la base del precio y cierta flexibilidad. Una forma de lograr esto es mediante el uso de paquetes de servicios. Un paquete de servicios es una colección de dos o más servicios que se han combinado para ofrecer una solución a un tipo específico de necesidad del cliente o para respaldar resultados comerciales específicos. Los paquetes de servicios pueden consistir en una combinación de servicios principales, de habilitación y de mejora, como se ilustra en la Figura 3.34. Tenga en cuenta que un paquete de servicios no necesita tener los tres tipos de servicio, solo necesita tener más de un servicio de cualquier tipo para ser un paquete de servicios. Cabe señalar que los paquetes de servicios pueden constar de múltiples servicios de cada tipo, como se ilustra en la Figura 3.35. También los paquetes de servicios pueden incluir uno o más paquetes de servicios como en la Figura 3.36. Es importante asegurarse de que los servicios y paquetes de servicios estén agrupados para que los clientes puedan relacionarse mejor con ellos, lo que facilita su compra y uso.
    </p>
  </body>
</html></richcontent>
<node TEXT="Mensaje clave" ID="ID_1315833825" CREATED="1658765150053" MODIFIED="1658765208769" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los clientes no siempre ven las capas de paquetes de servicios - en su lugar ellos se enfocan en los servicios que ellos usan sin necesidad de entender como estos están construidos.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Construcción de paquetes de servicios" ID="ID_544632013" CREATED="1658765216024" MODIFIED="1658765589196" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los paquetes se construyen de modo que, en lugar de ofrecer una cantidad desconcertante de servicios individuales a los clientes, los servicios se pueden combinar en agrupaciones lógicas para producir productos que luego se pueden comercializar, vender y consumir para satisfacer mejor las necesidades de los clientes.
    </p>
    <p>
      
    </p>
    <p>
      Las empresas de medios proporcionan buenos ejemplos de paquetes de servicios. Un ejemplo podría ser una empresa que brinda los siguientes servicios residenciales:
    </p>
    <ul>
      <li>
        Canales de televisión (cable o satélite)
      </li>
      <li>
        Internet de banda ancha
      </li>
      <li>
        Teléfono digital
      </li>
      <li>
        Almacenamiento en línea
      </li>
      <li>
        Mensajería.
      </li>
    </ul>
    <p>
      Las compañías de medios (y sus clientes) tendrían muchas dificultades si cada canal de televisión tuviera que pedirse por separado, o si los clientes tuvieran que pedir y facturar cada tipo de servicio por separado. Imagine la frustración si un cliente tuviera que contactar a la empresa cinco veces, abrir cinco cuentas y recibir cinco facturas debido a que ellos quieren el uso de todo el rango de servicios ofrecidos.
    </p>
    <p>
      
    </p>
    <p>
      En cambio, la empresa de medios identifica grupos de clientes típicos (p. ej., familias jóvenes, fanáticos de los deportes, parejas de jubilados, lugares de entretenimiento, etc.) y luego empaqueta opciones de servicio atractivas para satisfacer mejor los requisitos de cada grupo. De esta forma, el proveedor puede satisfacer las necesidades de la mayoría de los clientes de forma comercialmente eficaz. Esta identificación de grupos de clientes a veces se denomina &quot;segmentación&quot; (consulte la sección 3.4.8.4).
    </p>
    <p>
      
    </p>
    <p>
      En el mundo de TI, muchas organizaciones han adoptado un enfoque similar empaquetando servicios para satisfacer las demandas de los clientes pero de una manera que permite un alto grado de estandarización con una reducción en la complejidad y los costos de mantenimiento. Un paquete de servicios podría adoptar la forma de una imagen de escritorio estándar, que incluye todos los componentes de servicio que necesitan los usuarios y, al mismo tiempo, proporciona un entorno estándar que es mucho más fácil de soportar y mantener.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.4.8.2 Los Servicios y paquetes de servicio pueden incluir opciones" ID="ID_529024282" CREATED="1658190410749" MODIFIED="1658773868940" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En muchos casos, especialmente cuando se prestan servicios a un gran número y diversidad de clientes, es posible que sea necesario diferenciar los paquetes de servicios del mismo tipo. Por ejemplo, una empresa de telecomunicaciones móviles proporcionará un tipo de servicio (comunicación por telefonía móvil), pero puede adaptar los tipos de teléfono, la cantidad de líneas, los límites de datos, etc. a diferentes tipos de consumidores, como usuarios comerciales, familias y comunidades rurales. Aunque el servicio es esencialmente el mismo, cada tipo de consumidor requerirá un nivel diferente de garantía y utilidad.
    </p>
    <p>
      
    </p>
    <p>
      Cuando es necesario diferenciar un servicio o un paquete de servicios para diferentes tipos de clientes, se pueden cambiar uno o más componentes del paquete, u ofrecerse con diferentes niveles de utilidad y garantía, para crear opciones de servicio. Estas diferentes opciones de servicio se pueden ofrecer a los clientes y, a veces, se denominan paquetes de nivel de servicio.
    </p>
    <p>
      
    </p>
    <p>
      Usando la compañía de medios como ejemplo, cada servicio se puede dividir en varias opciones, cada una de las cuales ofrece niveles de servicio más altos o más bajos para diferentes tipos de clientes. Por ejemplo, al considerar un servicio básico de canales de televisión (cable o satélite):
    </p>
    <ul>
      <li>
        La opción A incluye 100 canales
      </li>
      <li>
        La opción B incluye 200 canales
      </li>
      <li>
        La opción C incluye solo canales apropiados para familias con niños pequeños
      </li>
      <li>
        La opción D incluye 300 canales y 5 eventos deportivos de pago por mes.
      </li>
    </ul>
    <p>
      Cada uno de los servicios anteriores tiene un nivel definido de utilidad y garantía. Estos niveles de utilidad y garantía quedarán documentados en el catálogo de servicios, permitiendo así al cliente elegir la opción que mejor se adapte a sus necesidades.
    </p>
    <p>
      
    </p>
    <p>
      Se pueden crear paquetes de servicios para ofrecer varias opciones diferentes al cliente; por ejemplo, un proveedor de red puede ofrecer una variedad de paquetes de servicios, cada uno con una cantidad diferente de almacenamiento disponible (por ejemplo, 5 GB, 10 GB, 20 GB), junto con un servicio de correo electrónico estándar. Esto permitirá al cliente elegir el paquete de servicios para su opción preferida.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.4.8.3 Computacion en la nube" FOLDED="true" ID="ID_363384309" CREATED="1658190441348" MODIFIED="1658774738010" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En el caso de la computación en la nube, el cliente puede seleccionar y elegir sus propias combinaciones de servicios y niveles de servicio, por lo que, en efecto, elige sus propios paquetes y opciones. Esto podría ser el resultado de que los clientes se vean impulsados a servicios estandarizados y de menor costo que están ampliamente disponibles en Internet, en lugar de invertir en contratos de servicios personalizados.
    </p>
  </body>
</html></richcontent>
<node TEXT="Caso de Estudio de una gran empresa de TI" ID="ID_557518823" CREATED="1658773983651" MODIFIED="1658774267210" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Durante 10 años, los servicios de TI proporcionados por una gran empresa de TI habían aumentado en número a más de 2000. Muchos de estos servicios hicieron exactamente lo mismo, pero en diferentes modelos de hardware. Algunos de los servicios tenían nombres que incluían jerga técnica, que se usaban para comunicar a los ingenieros de implementación&nbsp;&nbsp;qué era lo que tenían que hacer, aunque el cliente no tenía idea de lo que querían decir.
    </p>
    <p>
      
    </p>
    <p>
      Al vender hardware, el personal de ventas tendía a evitar vender servicios relacionados porque la lista de servicios en el catálogo era demasiado larga y compleja para encontrar el servicio exacto que necesitaban. Como resultado, los clientes generalmente buscarían un tercero para brindar estos servicios de mayor margen.
    </p>
    <p>
      
    </p>
    <p>
      El administrador de esta cartera necesitaba simplificar el catálogo de servicios sin sacrificar la capacidad de rastrear qué servicios se entregaron en qué plataforma de hardware.
    </p>
    <p>
      
    </p>
    <p>
      El resultado fue un nuevo catálogo de servicios. El gestor de cartera evaluó qué servicios se vendían y en qué combinación. Las combinaciones más populares se agruparon y vendieron como paquetes: los miles de servicios individuales se consolidaron en menos de 200 paquetes de servicios. Se capacitó a los vendedores sobre cómo vender los nuevos envases. Todo lo que tenían que hacer era seleccionar el paquete de servicios, especificar con qué hardware se vendía y el equipo de entrega entregaría los servicios individuales que componían el paquete, sin que el cliente tuviera que preocuparse por comprender los 20 o 30 elementos de línea. en la factura con nombres que solo el vendedor podría entender.
    </p>
    <p>
      
    </p>
    <p>
      El resultado fue un conjunto más simple de servicios, que fue más simple de vender, y los clientes tuvieron un entendimiento claro de que estaban comprando.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Ejemplo TI de paquetes de servicios" ID="ID_1986455467" CREATED="1658774273786" MODIFIED="1658774580738" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Para ayudar a resumir y aclarar el uso del empaque, aquí hay un ejemplo basado en la entrega de un servicio de correo electrónico.
    </p>
    <p>
      
    </p>
    <p>
      Una organización bancaria crea un servicio de correo electrónico que proporciona una utilidad básica de correo electrónico y garantía para todos los usuarios del banco. También tiene un servicio de red y un servicio de resolución de problemas. La organización encapsuló estos servicios en un producto de servicio de correo electrónico básico (paquete de servicios) listo para el consumo de los usuarios del banco.
    </p>
    <p>
      
    </p>
    <p>
      La opción básica de este servicio consiste en los niveles básicos de utilidad y garantía del servicio de correo electrónico que cualquier persona del banco puede utilizar. El paquete de servicios básicos se puede ofrecer a cualquier usuario del banco.
    </p>
    <p>
      
    </p>
    <p>
      Una opción diferente del paquete de servicios de correo electrónico (que consta de los mismos servicios básicos, pero con niveles más altos de garantía y utilidad) está diseñada para comerciantes de productos básicos en el banco (en los EE. UU., el correo electrónico para este segmento de clientes debe cumplir con las reglas de la SEC, garantizar registro seguro de correos electrónicos y mensajes de texto instantáneos, solicitudes de auditoría de soporte, etc.).
    </p>
    <p>
      
    </p>
    <p>
      El proveedor de servicios crea otra opción más de este paquete de servicios que representa niveles avanzados de utilidad y garantía para los altos ejecutivos del banco (es decir, prioridades de respuesta más altas, diferentes canales para recibir correos electrónicos, etc.).
    </p>
    <p>
      
    </p>
    <p>
      Nota: los paquetes de servicios no deben confundirse con los paquetes de diseño de servicios y los paquetes de lanzamiento; estos se utilizan para propósitos muy diferentes. La forma en que se empaquetan los servicios para la comercialización, las ventas y el consumo puede ser completamente diferente de la forma en que se empaquetan para el diseño, la construcción, la distribución y el lanzamiento.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Ejemplo en servicios de nube" ID="ID_659597471" CREATED="1658774600685" MODIFIED="1658774735292" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un ejemplo de esto se ilustra a continuación. Un proveedor de servicios en la nube ofrece una variedad de paquetes de servicios. Cada paquete de servicios se ofrece en tres niveles de servicio, a tres niveles de precios diferentes. Los clientes elegirán qué servicios utilizarán y en qué nivel. De esta forma elegirán múltiples servicios y paquetes de servicios para crear su propio paquete de servicios, compuesto por tres opciones:
    </p>
    <ul>
      <li>
        Respaldo de PC en línea (100GB, 200GB, 500GB)
      </li>
      <li>
        Email: (50 mensajes por día, 250 mensajes por día, mensajes ilimitados)
      </li>
      <li>
        Alojamiento de páginas web: (5 páginas, 10 páginas, 50 páginas)
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.4.8.4 Segmentacion" ID="ID_126094595" CREATED="1658190457711" MODIFIED="1658774950925" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Como se mencionó brevemente anteriormente, una organización puede elegir categorizar a sus clientes para poder empaquetar servicios dirigidos a las necesidades de cada categoría. Esto a veces se denomina segmentación.
    </p>
    <p>
      
    </p>
    <p>
      En su forma más simple, esto podría ser, por ejemplo, un club de golf o un centro de ocio que ofrezca diferentes paquetes de membresía (por ejemplo, individual, familiar, junior, senior, etc.), pero puede ser más sofisticado, como el ejemplo de la empresa de medios que ofrece entretenimiento y paquetes de comunicación, mencionados anteriormente.
    </p>
    <p>
      
    </p>
    <p>
      La segmentación puede ser extremadamente valiosa para orientar campañas de marketing, ventas, etc., así como para definir paquetes de servicios dirigidos a diferentes segmentos. Los proveedores de TI pueden utilizar la misma técnica para segmentar a los usuarios de TI. Esto permitirá desarrollar paquetes de servicios para satisfacer las necesidades de cada tipo de usuario, al mismo tiempo que brinda las siguientes ventajas:
    </p>
    <ul>
      <li>
        Maximización de la estandarización
      </li>
      <li>
        Minimización de servicios diferentes y diversos
      </li>
      <li>
        Distribución más fácil
      </li>
      <li>
        Simplificación del tema de soporte.
      </li>
    </ul>
    <p>
      La tabla 3.11 brinda un ejemplo simplificado de cómo una organización de TI podría segmentar a sus usuarios y los paquetes de servicios que podrían brindar.
    </p>
    <p>
      
    </p>
    <p>
      Los procesos de segmentación requerirán una estrecha consulta con los clientes para comprender completamente sus requisitos, y con los grupos técnicos para diseñar las soluciones empaquetadas exactas.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.4.8.5 Disenando y transicionando paquetes de servicio" ID="ID_1312895162" CREATED="1658190467215" MODIFIED="1658775123628" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El diseño y la transición de los paquetes de servicios siguen exactamente el mismo proceso que cualquier otro servicio. Una sugerencia o solicitud de un nuevo paquete de servicios (o cambio de un paquete de servicios existente) se envía a través de la gestión de la cartera de servicios. El paquete de servicios se modela y evalúa, y se presenta una propuesta de cambio. Una vez autorizados, los niveles requeridos de utilidad y garantía para el paquete de servicios y sus opciones se documentará en la carta de servicios y se enviará a la coordinación de diseño (consulte la sección 4.2 para obtener una descripción detallada de este proceso).
    </p>
    <p>
      
    </p>
    <p>
      Se creará un paquete de diseño de servicios (SDP) para respaldar el diseño, la transición y la operación del paquete de servicios a lo largo del ciclo de vida del servicio. Los procesos de transición del servicio crearán, probarán e implementarán los paquetes de servicios, tal como lo harían con cualquier servicio individual, utilizando la carta de servicios y SDP como base para esta actividad.
    </p>
    <p>
      
    </p>
    <p>
      El paquete de servicios y sus opciones se documentarán en el catálogo de servicios.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="3.5 Estrategias para la satisfaccion del cliente" FOLDED="true" ID="ID_148820319" CREATED="1658190493827" MODIFIED="1658785674277" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      A menudo, no es suficiente que un servicio satisfaga los resultados comerciales de un cliente. Especialmente en un entorno competitivo, es necesario que los clientes se sientan satisfechos con el nivel de servicio que han recibido. También deben tener confianza en la capacidad del proveedor de servicios para continuar brindando ese nivel de servicio, o incluso mejorarlo con el tiempo.
    </p>
    <p>
      
    </p>
    <p>
      La única dificultad es que las expectativas de los clientes siguen cambiando, y un proveedor de servicios que no haga un seguimiento de esto pronto perderá negocios. La siguiente discusión sobre los atributos del servicio y el modelo de Kano es útil para comprender cómo esto sucede, y cómo un proveedor de servicios puede adaptar sus servicios para cumplir con el entorno cambiante del cliente.
    </p>
    <p>
      
    </p>
    <p>
      Los atributos de un servicio son las características que proporcionan forma y función al servicio desde una perspectiva de utilización. Los atributos se rastrean a partir de los resultados comerciales para que sean compatibles con el servicio. Ciertos atributos deben estar presentes para que comience la creación de valor. Otros agregan valor en una escala móvil determinada por cómo los clientes evalúan los incrementos en la utilidad y la garantía. Los acuerdos de nivel de servicio comúnmente prevén niveles diferenciados de calidad de servicio para diferentes conjuntos de usuarios.
    </p>
    <p>
      
    </p>
    <p>
      Algunos atributos son más importantes para los clientes que otros. Tienen un impacto directo en el desempeño de los activos del cliente y, por lo tanto, en la realización de los resultados básicos. Dichos atributos son atributos &quot;imprescindibles&quot; (Kano et al., 1984). La Tabla 3.12 describe el tipo de atributos que influyen en la percepción del cliente sobre la utilidad de un servicio. Un factor que linear significa que el nivel de satisfacción del cliente está directamente relacionado con el grado en que se satisfacen las necesidades del cliente. Los factores no lineales son aquellos en los que, aunque se satisface una fracción de las necesidades del cliente, dan como resultado altos niveles de satisfacción del cliente.
    </p>
  </body>
</html></richcontent>
<node TEXT="Figura 3.12 Percepciones de utilidad y satisfacción del cliente" ID="ID_1647477486" CREATED="1658785845936" MODIFIED="1658785898235" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.12%20El%20modelo%20Kano%20y%20los%20atributos%20de%20servicio.png" SIZE="0.80862534" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 3.12 El modelo Kano y los atributos de servicio" ID="ID_1588728238" CREATED="1658785860119" MODIFIED="1658836707493" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 600px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Tipo de atributo
          </p>
        </th>
        <th valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Cumplimiento y percepciones de utilidad (pérdida/ganancia)
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Factores básicos (B) (debe tener, no lineal)
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Atributos de servicio esperados o tomados para aceptación. El no cumplimiento de estas provocarían percepción de pérdida de utilidad. Su cumplimiento resulta en una ganancia de utilidad pero solo hasta una zona neutral después de la cual no hay ganancia.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Factores de entusiasmo (E) (utilidad atrayente, no lineal)
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Atributos del servicio que impulsan las percepciones de ganancia de utilidad pero que cuando no se cumplen no provocan percepciones de pérdida de utilidad
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Factores de desempeño (P) (utilidad atrayente, lineal)
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Atributos del servicio que resultan en percepciones de ganancia de utilidad cuando se cumplen y pérdidas de utilidad cuando no se cumplen en un patrón unidimensional casi lineal
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Atributos indiferentes (I)
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            No causan ganancias ni pérdidas en las percepciones de utilidad independientemente de que se cumplan o no.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Atributos reversos (R)
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Provocan ganancias en las percepciones de utilidad cuando no se cumplen y pérdidas cuando se cumplen. Los supuestos deben revertirse.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Respuesta Cuestionable (Q)
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Las respuestas son cuestionables, posiblemente porque las preguntas no fueron claras o se malinterpretaron.
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="Tabla 3.13 La tabla de evaluación Kano" ID="ID_818326100" CREATED="1658836180884" MODIFIED="1658836797489" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Tabla%203.13%20La%20tabla%20de%20evaluación%20Kano.png" SIZE="0.7712082" NAME="ExternalObject"/>
</node>
<node TEXT="3.5.1 El modelo Kano" FOLDED="true" ID="ID_1434863438" CREATED="1658190510484" MODIFIED="1658794955811" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La figura 3.37 muestra el modelo Kano de la percepciones de utilidad del cliente. Esta figura muestra una relación entre el nivel de cumplimiento de las necesidades del cliente que ofrece servicios, y el nivel de satisfacción que sienten los clientes.
    </p>
    <p>
      
    </p>
    <p>
      El modelo de Kano muestra que hay tres tipos diferentes de factores involucrados en la satisfacción al cliente:
    </p>
    <ul>
      <li>
        <b>Factores básicos</b>&nbsp;Estos son aspectos del servicio que deben existir para que el cliente reciba el servicio. En muchos casos, estos factores básicos se dan por sentados. A veces no ofrecen ningún valor manifiesto, pero si fallan o no se entregan, los clientes quedarán muy insatisfechos. Por ejemplo, nadie come en un restaurante porque tiene luces, pero si hay un corte de luz y los clientes no pueden ver lo que están comiendo, probablemente se irán. De la misma manera, un usuario no viene a trabajar porque tiene una computadora personal conectada a la red; pero si la red o la PC no funcionan, no podrán acceder al correo electrónico y otros servicios clave. Estos factores a veces se denominan &quot;utilidad imprescindible&quot;, o lo mínimo que el proveedor de servicios puede ofrecer para un servicio en particular. Como se muestra en la figura 3.37, los factores básicos no son responsables de los altos niveles de satisfacción, a menos que por sí solos cumplan completamente con los requisitos del cliente.
      </li>
      <li>
        <b>Factores de desempeño</b>&nbsp;Estos son factores que permiten a un cliente obtener más de lo que necesita, o un mayor nivel de calidad de servicio. Cuanto más quiera un cliente de cada factor de rendimiento, más esperará pagar, por lo tanto, mayor valor debe aportar. También se denominan utilidades unidimensionales, ya que cada factor aumenta o disminuye a lo largo de una línea predecible basada en la demanda del cliente. Cuanto más alto sea el nivel de desempeño que cumpla con un mayor porcentaje de los requisitos del cliente, más satisfecho estará el cliente, aunque los niveles extremadamente altos de desempeño pueden percibirse como “agradables de tener”. Un ejemplo de factores de rendimiento es cuando un restaurante ofrece un descuento a los comensales frecuentes o a las mesas que piden dos o más del especial del día. Los ejemplos de TI pueden incluir proporcionar almacenamiento adicional, ancho de banda u otra asignación de recursos para servicios de alta prioridad.
      </li>
      <li>
        <b>Factores de entusiasmo</b>&nbsp;Estos son atributos de un servicio que son generosos y que los clientes no esperan. Cuando se ofrecen (dentro de lo razonable) ellos provocan altos niveles de satisfacción. Se considera que el proveedor de servicios está &quot;haciendo un esfuerzo adicional&quot;. Por ejemplo, el restaurante ofrece una bebida gratis con cada plato principal. Si bien los factores de emoción dan como resultado los niveles más altos y rápidos de satisfacción del cliente, su mantenimiento es costoso, ya que el proveedor de servicios ofrece más de lo necesario al mismo precio. Estos factores pueden ser emocionantes, pero no son esenciales para el servicio y, por lo tanto, también se los conoce como &quot;utilidad agradable de tener&quot;. <u>Los proveedores de servicios internos deben tener cuidado de introducir factores de entusiasmo en interés de la &quot;satisfacción del cliente&quot; sin hacer un caso comercial adecuado para justificar los costos adicionales.</u>
      </li>
    </ul>
    <p>
      Los factores de entusiasmo y los factores de rendimiento son la base para la segmentación del mercado y los niveles de servicio diferenciados. Se utilizan para satisfacer las necesidades de determinados tipos de clientes. Dichos atributos son necesarios para cualquier estrategia que involucre la segmentación de clientes en grupos y brindarles un paquete de servicios adecuado. Los factores básicos son el costo de entrada en el mercado. Sin factores básicos, el proveedor de servicios no puede entrar en el mercado. A medida que pasa el tiempo, los factores de excitación se vuelven comúnmente disponibles y pierden su capacidad de diferenciación. La competencia, los cambios en las percepciones de los clientes y las nuevas innovaciones pueden hacer que los factores de entusiasmo se desvíen hacia factores de rendimiento o básicos.
    </p>
  </body>
</html></richcontent>
<node TEXT="A la deriva" ID="ID_803861454" CREATED="1658794906590" MODIFIED="1658794931188" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Muchos proveedores de servicios caen en la trampa de creer que deben brindar el nivel de servicio más alto posible, independientemente del nivel de cumplimiento que se requiera. Si bien esto puede resultar en niveles más altos de satisfacción del cliente a corto plazo (consulte la Figura 3.37), no dura mucho, por dos razones:
    </p>
    <ul>
      <li>
        En primer lugar, es costoso de mantener y, después de un tiempo, el proveedor del servicio debe aumentar el precio del servicio, o dejar de ofrecer los factores de excitación.
      </li>
      <li>
        En segundo lugar, niveles generosos de servicio se convierten rápidamente en esperados. Por tanto lo que desearía tener viene a ser un factor de desempeño, y en un debe tener.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Factores de entusiasmo" ID="ID_1268783293" CREATED="1658794936878" MODIFIED="1658836022573" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los factores de entusiasmo solo deben ofrecerse en tres circunstancias:
    </p>
    <ul>
      <li>
        Si el proveedor del servicio tiene una estrategia para atraer clientes y tiene la intención de mantener estos factores como una característica permanente del servicio (y tiene los fondos para continuar haciéndolo).
      </li>
      <li>
        Si el proveedor de servicios los utiliza como parte de una campaña de marketing y solo los ofrecerá por un tiempo limitado. La fecha de finalización está claramente identificada y los clientes saben que estos atributos no estarán disponibles después de esa fecha.
      </li>
      <li>
        Si el proveedor del servicio ha decepcionado al cliente por una falla en el servicio o por no cumplir con las expectativas. Lo bueno de tener puede ofrecerse una vez para compensar el error, pero tanto el proveedor del servicio como el cliente entienden claramente que se trata de una excepción especial.
      </li>
    </ul>
    <p>
      Un ejemplo de TI puede ayudar a aclarar la naturaleza de los factores básicos, de rendimiento y entusiasmo y la satisfacción del cliente.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Ejemplo almacenamiento en la nube" ID="ID_1186579103" CREATED="1658836023703" MODIFIED="1658836036917" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Tome el ejemplo de un servicio de almacenamiento en línea con capacidades de copia de seguridad y restauración sincronizadas. Debe proporcionar acceso las 24 horas del día, con altas velocidades de carga y descarga. Debe proteger contra la corrupción, el acceso no autorizado y la divulgación accidental. Al mismo tiempo, debe ser muy accesible para los usuarios autorizados. Existe una ganancia de utilidad al tener acceso al servicio de almacenamiento en una red pública a través de un navegador seguro. El servicio es un sustituto de un dispositivo de almacenamiento portátil, que necesita un manejo y transporte cuidadoso por parte de los usuarios para mantener el acceso a los datos almacenados. Hasta cierto punto, la seguridad y la accesibilidad son factores básicos. Su provisión no resulta en ganancias de utilidad para el cliente. Aunque no proporcionan ningún nivel de entusiasmo, no proporcionarlos provocaría una caída drástica en la satisfacción del cliente.
    </p>
    <p>
      
    </p>
    <p>
      Algunos usuarios necesitan una mayor cantidad de almacenamiento que otros. Dentro de un cierto rango, valoran una cantidad cada vez mayor de almacenamiento y están dispuestos a pagar un precio proporcionalmente más alto. El tamaño del almacenamiento es un factor de rendimiento con utilidad unidimensional, junto con el cual es significativo ofrecer opciones. Dentro del rango, la relación entre la utilidad y el espacio de almacenamiento es aproximadamente lineal. Fuera de este rango, los clientes tienen una utilidad decreciente en el almacenamiento adicional o la falta de este. Otro tipo de utilidad unidimensional podría ser la cantidad de &quot;subcuentas&quot; para que los clientes puedan asignar diferentes cajas de almacenamiento para diferentes propósitos, como proyectos, tipo de medios e información personal. Más subcuentas significan una mayor utilidad con una utilidad decreciente después de un número particular de subcuentas.
    </p>
    <p>
      
    </p>
    <p>
      Los servicios pueden tener atributos de entusiasmo, que los clientes no esperan pero que están contentos de tener, dada una oferta razonable. El servicio de almacenamiento puede ofrecer atributos como copias de seguridad y notificaciones programadas, privilegios de estilo administrador, múltiples subcuentas, medición, control de acceso, administración de cuentas y protocolos seguros de transferencia de archivos. Algunos clientes pueden verlos como factores de rendimiento con una utilidad unidimensional. Para otros, estos son factores de excitación. Su ausencia no causa insatisfacción. Su presencia provoca un aumento espectacular de la satisfacción a un precio razonable.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Tabla de evaluación Kano" ID="ID_901473655" CREATED="1658836837457" MODIFIED="1658836989567" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Se requiere un amplio diálogo con los clientes objetivo o segmentos de espacios de mercado para determinar los atributos que un servicio debe tener, debería tener y podría tener en términos de utilidad imprescindible. Los cuestionarios se utilizan para obtener respuestas de los clientes a partir de las cuales es posible un análisis más detallado. La tabla de evaluación de Kano es un método útil (Tabla 3.13).
    </p>
    <p>
      
    </p>
    <p>
      Un servicio bien diseñado proporciona una combinación de atributos básicos, de rendimiento y de emoción para ofrecer un nivel adecuado de utilidad para el cliente. Diferentes clientes le darán diferente peso o importancia a la misma combinación de atributos. Además, incluso si un tipo particular de cliente valora una combinación particular, es posible que no encuentre justificación para pagar cargos adicionales.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="3.6 Economia del servicio" FOLDED="true" ID="ID_1669504694" CREATED="1658190532748" MODIFIED="1658837766641" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La economía de los servicios se relaciona con el equilibrio entre el costo de la prestación de los servicios, el valor de los resultados logrados y los rendimientos que los servicios permiten lograr al proveedor del servicio.
    </p>
    <p>
      
    </p>
    <p>
      La dinámica de la economía del servicio para los proveedores de servicios externos es diferente de la de los proveedores de servicios internos. Esto se debe a que los rendimientos de los proveedores de servicios internos se miden principalmente por sus clientes internos y no corresponden directamente al proveedor del servicio. Esto se ilustra en las Figuras 3.38 y 3.39.
    </p>
    <p>
      
    </p>
    <p>
      En la Figura 3.38, el proveedor de servicios de Tipo III entrega un servicio a un cliente externo para el pago. El proveedor de servicios calcula la inversión total requerida para brindar ese servicio y lo mide con los ingresos totales obtenidos al brindar el servicio. El éxito del proveedor de servicios se mide por el retorno de su inversión (ROI).
    </p>
    <p>
      
    </p>
    <p>
      Por el contrario, el retorno de la inversión no puede ser medido por un proveedor de servicios interno de forma aislada de sus clientes internos. En la figura 3.39, un proveedor de servicios de TI brinda un servicio a otra unidad comercial, que cubre los costos del servicio de TI. Por lo tanto, la inversión en el servicio de TI la realiza la unidad de negocio y no el proveedor del servicio. La financiación proporcionada al proveedor de servicios de TI no puede verse como un retorno de la inversión, ya que la inversión la realiza la unidad de negocio. En cambio, el retorno es en forma de rentabilidad generada por la unidad de negocios, y el cálculo del ROI lo realiza la unidad de negocios.
    </p>
    <p>
      
    </p>
    <p>
      <u>Si el proveedor de servicios de TI interno intenta demostrar su ROI sin hacer referencia a los ingresos de la unidad de negocios, la única vez que podrá demostrar un rendimiento real es cuando reduzca sus costos.</u>&nbsp;&nbsp;Como resultado, muchas organizaciones de TI encuentran que la empresa les pide que reduzcan los costos, incluso cuando sus servicios son críticos para que la empresa logre sus resultados. Esto se cubre con más detalle en la sección 3.2.3, donde se analiza la importancia de vincular las inversiones en TI con los resultados comerciales.
    </p>
    <p>
      
    </p>
    <p>
      La economía de los servicios se basa en cuatro áreas principales:
    </p>
    <ul>
      <li>
        <b>Gestión del portafolio de servicios</b>&nbsp;El proceso que define los resultados que la empresa desea lograr y los servicios que se utilizarán para lograrlos. Esto se cubre en detalle en la sección 4.2.
      </li>
      <li>
        <b>Gestión financiera para servicios de TI</b>&nbsp;El proceso mediante el cual los proveedores de servicios (y otras unidades de negocio) calculan, pronostican y rastrean los costos e ingresos relacionados con los servicios. Esto se trata en detalle en la sección 4.3.
      </li>
      <li>
        <b>Retorno de la inversión (ROI)</b>&nbsp;Una medida del beneficio esperado o real de una inversión. La Sección 3.6.1 se centrará en cómo ROI se utiliza, junto con la gestión de la cartera de servicios y la gestión financiera de los servicios de TI, para crear una economía de servicios saludable para la organización del proveedor de servicios.
      </li>
      <li>
        <b>Análisis de impacto en el negocio (BIA)</b>&nbsp;Esto permite que una organización establezca las prioridades relativas de los servicios en función de su efecto en el negocio si no están disponibles durante un período de tiempo. Este es un método clave utilizado en la gestión de la continuidad del servicio de TI y se analiza en detalle en el Diseño del servicio ITIL. Aquí se proporciona una descripción general para explicar el papel de BIA en la estrategia de servicio.
      </li>
    </ul>
    <p>
      Además de estas cuatro áreas, se debe hacer una nota especial sobre la gestión de la demanda (descrita en la sección 4.4). Este es el proceso mediante el cual se identifican las funciones comerciales vitales y se implementan los recursos para garantizar que se puedan alcanzar los niveles apropiados de desempeño comercial. La gestión de la demanda funciona para orientar los recursos de TI para satisfacer las demandas cambiantes de la empresa a medida que responde a su entorno. Los cambios en la demanda reflejan un cambio potencial en el valor de los servicios y su capacidad continua para permitir que la empresa alcance sus objetivos. Por esta razón, la gestión de la demanda juega un papel importante en la evaluación continua de la economía de los servicios.
    </p>
  </body>
</html></richcontent>
<node TEXT="Figura 3.38 Dinámica de economía del servicio para proveedores de servicio externos" ID="ID_1744107831" CREATED="1658837872033" MODIFIED="1658837900172" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.38%20Dinamica%20de%20economia%20del%20servicio%20para%20proveedores%20de%20servicio%20externos.png" SIZE="0.75376886" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.39 Dinámica de economía de servicio para proveedores de servicio internos" ID="ID_1784457755" CREATED="1658837941631" MODIFIED="1658837958228" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.39%20Dinamica%20de%20economia%20de%20servicio%20para%20proveedores%20de%20servicio%20internos.png" SIZE="0.7672634" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.40 Un solo impacto al negocio puede afectar múltiples objetivos del negocio" ID="ID_851620572" CREATED="1658843524771" MODIFIED="1658843537311" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.40%20Un%20solo%20impacto%20al%20negocio%20puede%20afectar%20multiples%20objetivos%20del%20negocio.png" SIZE="0.80862534" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.41 Múltiples impactos al negocio pueden afectar un solo objetivo de negocio" ID="ID_504233034" CREATED="1658844186239" MODIFIED="1658844193907" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.41%20Múltiples%20impactos%20al%20negocio%20pueden%20afectar%20un%20solo%20objetivo%20de%20negocio.png" SIZE="0.8264463" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.42 Enfoque ROI post programa" ID="ID_899912216" CREATED="1658850273847" MODIFIED="1658850278664" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.42%20Enfoque%20ROI%20post%20programa.png" SIZE="0.77619666" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.43 Análisis de pronóstico" ID="ID_1588543312" CREATED="1658851267893" MODIFIED="1658851294729" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Analisis%20de%20pronostico.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 3.14 Ejemplo de estructura de un caso de negocio" ID="ID_1784045834" CREATED="1658843067980" MODIFIED="1658843117644" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Tabla%203.14%20Ejemplo%20de%20estructura%20de%20un%20caso%20de%20negocio.png" SIZE="0.7653061" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 3.15 Objetivos de negocio comunes" ID="ID_163789016" CREATED="1658843112360" MODIFIED="1658843127340" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Tabla%203.15%20Objetivos%20de%20negocio%20comunes.png" SIZE="0.7712082" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 3.16 Desiciones de valor presente neto VPN" ID="ID_1386300868" CREATED="1658845082428" MODIFIED="1658845266210" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Tabla%203.16%20Desiciones%20de%20valor%20presente%20neto%20VAN.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 3.17 Ejemplo de VPN de un programa de gestión de servicio propuesto" ID="ID_337346444" CREATED="1658845886673" MODIFIED="1658845897916" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Tabla%203.17%20Ejemplo%20de%20VPN%20de%20un%20programa%20de%20gestion%20de%20servicio%20propuesto.png" SIZE="0.76045626" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 3.18 Tipos de flujo de efectivo" ID="ID_440299212" CREATED="1658845929373" MODIFIED="1658845934552" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Tabla%203.18%20Tipos%20de%20flujo%20de%20efectivo.png" SIZE="0.7712082" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 3.19 Ejemplo de TIR de un programa de gestión de servicio propuesto" ID="ID_1639912834" CREATED="1658850218132" MODIFIED="1658850227540" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Tabla%203.19%20Ejemplo%20de%20TIR%20de%20un%20programa%20de%20gestión%20de%20servicio%20propuesto.png" SIZE="0.7653061" NAME="ExternalObject"/>
</node>
<node TEXT="3.6.1 Retorno de la inversion" FOLDED="true" ID="ID_1628427516" CREATED="1658190545994" MODIFIED="1658842931703" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El Retorno de la Inversión (ROI) es un concepto para cuantificar el valor de una inversión, y el cálculo es normalmente realizado por la gestión financiera. El término no siempre es utilizado de forma consistente. A veces es usado por oficiales financieros para indicar el ROIC (Retorno a un capital invertido), una medida de rendimiento del negocio. En la gestión de servicio, ROI es usado como una medida de la habilidad para usar activos para generar valor adicional. En palabras simples, es el beneficio neto de una inversión dividido por el valor neto de los activos invertidos. El porcentaje resultante se aplica a los ingresos brutos adicionales o a la eliminación del costo neto. En términos económicos, una buena inversión es la que excede la tasa de retorno del mercado de capitales.
    </p>
    <p>
      
    </p>
    <p>
      Una fórmula simple para calcular ROI sería:
    </p>
  </body>
</html></richcontent>
<node TEXT="\latex $ROI=\frac{\text{Incremento en beneficio resultante del servicio}}{\text{Inversion total en el servicio}}$" ID="ID_1823063372" CREATED="1658841463132" MODIFIED="1658842011246">
<font SIZE="14"/>
</node>
<node TEXT="Factores a tener en cuenta para el cálculo de ROI" ID="ID_184630988" CREATED="1658842876470" MODIFIED="1658842927707" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Aunque este cálculo parece sencillo, es algo simplista ya que la percepción del cliente es subjetiva y hay muchos factores intangibles involucrados en la prestación de los servicios. Es aún más difícil cuantificar el valor de la gestión de servicios, ya que no se entrega directamente a un cliente y no ofrece resultados comerciales directamente para el cliente. Si bien los cálculos de ROI pueden ser útiles para indicar el éxito de un servicio o una implementación de gestión de servicios, hay una serie de factores que deben tenerse en cuenta, entre ellos:
    </p>
    <ul>
      <li>
        Los ejercicios de ROI que se centran exclusivamente en métricas financieras no indican el rendimiento potencial completo . Por ejemplo, algunos servicios tienen poco rendimiento directo, pero proporcionan la base sobre la cual se pueden prestar otros servicios.
      </li>
      <li>
        El cálculo del ROI debe incluir alguna medida de cuánto el servicio, o el proyecto de gestión del servicio movió a la organización más cerca de alcanzar su estrategia. Estos usualmente serán sentencias cualitativas sobre el servicio o proyecto - por ejemplo, niveles incrementados de fidelidad del cliente.
      </li>
      <li>
        Para el ROI que está basado únicamente en reducción de costos, el proveedor de servicios no será percibido por el negocio como un retorno de la inversión si no hay un impacto en el costo por unidad del servicio o producto del negocio.
      </li>
      <li>
        Los cálculos de ROI que solo se enfocan en los resultados a corto plazo a menudo arrojan cifras negativas. Por ejemplo, muchos procesos de gestión de servicios se centran en mejorar la capacidad y los recursos del proveedor de servicios. Estos pueden tomar algún tiempo para diseñar y construir (y una inversión significativa) antes de que produzcan algún retorno.
      </li>
    </ul>
    <p>
      Los límites de muchos cálculos de ROI tradicionales han llevado a la aparición de cálculos destinados a incluir más resultados intangibles que se esperan de un servicio. Estos cálculos a menudo se denominan cálculos de valor de la inversión y se tratan con más detalle en la Mejora continua del servicio ITIL. Sin embargo, el ROI sigue siendo la principal herramienta comercial para evaluar el valor de un servicio y, por lo tanto, se tratará en detalle a continuación.
    </p>
    <p>
      
    </p>
    <p>
      Si bien un servicio puede estar directamente vinculado y justificado a través de imperativos comerciales específicos, pocas empresas pueden identificar fácilmente el rendimiento financiero de los aspectos específicos de la gestión del servicio. A menudo es una inversión que las empresas deben hacer antes de cualquier retorno. La gestión de servicios por sí sola no proporciona ninguno de los beneficios tácticos que los gerentes comerciales suelen presupuestar. Uno de los mayores desafíos para quienes buscan financiamiento para proyectos ITIL es identificar un imperativo de negocio específico que dependa de la gestión de servicio. Por estas razones, esta discusión de ROI se enfoca en tres áreas:
    </p>
    <ul>
      <li>
        <b>Caso de negocio</b>&nbsp;Un medio para identificar los imperativos del negocio que dependan de la gestión del servicio.
      </li>
      <li>
        <b>ROI Pre-programa</b>&nbsp;Técnicas para cuantitativamente analizar una inversión en gestión de servicio.
      </li>
      <li>
        <b>ROI Post-programa</b>&nbsp;Técnicas para retroactivamente analizar una inversión en gestión de servicio.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="3.6.1.1 Caso de negocio" FOLDED="true" ID="ID_265516730" CREATED="1658190561276" MODIFIED="1658843305836" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un caso de negocio es una herramienta de planificación y apoyo a la toma de decisiones que proyecta las posibles consecuencias de una acción empresarial. Las consecuencias pueden tener dimensiones cualitativas y cuantitativas. Un análisis financiero, por ejemplo, suele ser central para un buen caso de negocios. En la Tabla 3.14 se proporciona un ejemplo de una estructura de caso de negocio.
    </p>
  </body>
</html></richcontent>
<node TEXT="Objetivos del negocio" ID="ID_445801167" CREATED="1658842944839" MODIFIED="1658843548967" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estructura de un caso de negocio varía de una organización a otra. Lo que todos tienen en común es un análisis detallado del impacto comercial o los beneficios. El impacto comercial está a su vez vinculado a los objetivos comerciales. Un objetivo comercial es la razón para considerar una iniciativa de gestión de servicios en primer lugar. Los objetivos deben comenzar de manera amplia. Por ejemplo:
    </p>
    <ul>
      <li>
        Los objetivos comerciales para las organizaciones de proveedores comerciales suelen ser los objetivos del negocio mismo, incluido el desempeño financiero y organizacional.
      </li>
      <li>
        Los objetivos comerciales de un proveedor de servicios interno deben estar vinculados a los objetivos comerciales de la unidad comercial a la que se presta el servicio y los objetivos corporativos generales.
      </li>
      <li>
        Los objetivos comerciales de las organizaciones sin fines de lucro suelen ser los objetivos para los integrantes, la población o los miembros atendidos, así como el desempeño financiero y organizacional.
      </li>
    </ul>
    <p>
      La Tabla 3.15 muestra posibles objetivos de negocio.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Impacto al negocio" ID="ID_1515347275" CREATED="1658843551893" MODIFIED="1658844021192" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Si bien la mayor parte del argumento de un caso de negocios se basa en el análisis de costos, hay mucho más en una iniciativa de gestión de servicios que las finanzas. Un impacto comercial no financiero se puede identificar por cómo se ve afectado el logro de uno o más objetivos comerciales. Por ejemplo, una organización cambia su servicio de pedidos de ventas para rastrear transacciones de clientes individuales e informar sobre las tendencias de compra para cada cliente. El impacto financiero para el negocio no es inmediatamente obvio, pero se vuelve más claro una vez que se definen los impactos no financieros. Estos incluyen la capacidad de participar en marketing dirigido (y más efectivo), una mejor anticipación de los niveles de existencias (lo que resulta en un menor costo de adquisición y almacenamiento) y una mayor lealtad del cliente. En las Figuras 3.40 y 3.41 se dan más ejemplos.
    </p>
    <p>
      
    </p>
    <p>
      En la Figura 3.40, el impacto comercial de la capacidad de mantenimiento mejorada podría resultar en costos reducidos (ya que el servicio requiere menos tiempo del personal para resolver incidentes), lo que a su vez resulta en una menor inversión en el servicio por parte del negocio y una mayor rentabilidad. Al mismo tiempo, aumenta la satisfacción del cliente porque la empresa puede mejorar su calidad de entrega, lo que a su vez da como resultado un aumento en el porcentaje de clientes que regresan a la organización para repetir negocios. Se mejora la imagen de mercado de la organización, ya que la calidad del servicio da como resultado una mayor satisfacción del cliente y un mayor desempeño de la empresa, lo que a su vez da como resultado una mejor clasificación en los resultados de las encuestas de la industria.
    </p>
    <p>
      
    </p>
    <p>
      Cuando se usa el término “caso de negocios”, a menudo crea la impresión de que es apropiado incluir solo los aspectos financieros del servicio o proyecto. Esto no es cierto, las empresas exitosas entienden que a sus clientes no solo les interesa pagar dinero; necesitan sentirse cómodos con el proveedor y su capacidad para cumplir lo que prometieron. Esto significa que todas las empresas deben centrarse tanto en los impactos financieros como no financieros del proyecto o servicio propuesto.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.6.1.2 ROI previo programa" FOLDED="true" ID="ID_209591361" CREATED="1658190574173" MODIFIED="1658844554043" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El término presupuesto de capital se usa para describir cómo los gerentes planifican desembolsos significativos en proyectos que tienen implicaciones a largo plazo. Una iniciativa de gestión de servicios a veces puede requerir un presupuesto de capital para financiar el proyecto o servicio.
    </p>
    <p>
      
    </p>
    <p>
      Un factor adicional a recordar al realizar el ROI previo al programa es el valor relativo de la inversión a lo largo del tiempo. Por lo general, una inversión ocurre temprano, mientras que los retornos no ocurren hasta algún tiempo después. El valor del dinero gastado hoy probablemente cambiará con el tiempo debido a la inflación, las fluctuaciones monetarias, etc.
    </p>
    <p>
      
    </p>
    <p>
      Por lo tanto, no es seguro suponer que cualquier ingreso futuro pueda calcularse con el mismo valor. Por ejemplo, si hoy se realiza una inversión de 1.000£, y la tasa de inflación y los tipos de cambio de divisas devalúan la moneda un 25% en un año, recuperar la inversión supondrá una renta de más de 1.250£ al final del año (asumiendo que el dinero no fue prestado, ya que eso también requeriría tener en cuenta un cálculo de interés). Esta fluctuación en el valor de los ingresos y gastos durante un período de tiempo se denomina <i>flujo de caja descontado</i>.
    </p>
  </body>
</html></richcontent>
<node TEXT="Definición: Presupuesto de capital" ID="ID_1541244086" CREATED="1658844247121" MODIFIED="1658844401600" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El presupuesto de capital es el compromiso actual de fondos para recibir un rendimiento en el futuro en forma de entradas de efectivo adicionales o salidas de efectivo reducidas.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Flujo de caja descontado" ID="ID_1898159361" CREATED="1658844555771" MODIFIED="1658844720871" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los cálculos de ROI deben tener en cuenta estos flujos de efectivo descontados al calcular el rendimiento durante un período de tiempo. Afortunadamente, la ciencia de la contabilidad tiene métodos bien establecidos para hacer esto, y las siguientes secciones se proporcionan como referencia para los especialistas de TI para que puedan comunicarse de manera efectiva con los expertos financieros y el negocio.
    </p>
    <p>
      
    </p>
    <p>
      Las decisiones de presupuesto de capital se dividen en dos grandes categorías. Estas son decisiones de selección y preferencia:
    </p>
    <ul>
      <li>
        Las decisiones de selección se relacionan con si una iniciativa de administración de servicios propuesta supera un obstáculo predeterminado, por ejemplo, un rendimiento mínimo. Las decisiones de selección generalmente se toman utilizando un método de flujo de efectivo descontado del valor presente neto (NPV).
      </li>
      <li>
        Las decisiones de preferencia se relacionan con elegir entre varias alternativas que compiten, por ejemplo, elegir entre un plan de mejora del servicio interno (SIP) y un programa de abastecimiento de servicios. Las decisiones de preferencia generalmente se toman utilizando un método de flujo de caja descontado de tasa interna de retorno (TIR).
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Decisiones de selección (utilizando el valor presente neto)" ID="ID_174311851" CREATED="1658844784229" MODIFIED="1658845260343" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Bajo el método NPV, las entradas de efectivo del programa se comparan con las salidas de efectivo a lo largo del tiempo (flujos de efectivo descontados - consulte la sección anterior). La diferencia, denominada valor actual neto, determina si la inversión es adecuada o no (Cuadro 3.16). Siempre que el valor actual neto sea negativo, es poco probable que la inversión sea adecuada.
    </p>
    <p>
      
    </p>
    <p>
      Tenga en cuenta, sin embargo, que la organización no tiene que utilizar la tasa de descuento como mínimo. Estas son dos cosas separadas, aunque por varias razones pueden optar por usar la misma figura. Cuando se habla de VPN, la tasa de descuento utilizada debe ser una estimación razonable de la inflación/tasas de interés de otras fuentes, etc., de modo que refleje con cierta precisión el valor del dinero ahora que se gastará teniendo en cuenta lo que podría haber valido. en el futuro. Pero la tasa que una empresa usa como mínimo antes de gastar capital podría establecerse en una tasa más alta, solo para incluir varios factores de riesgo, o porque no están interesados en empresas que no les den un alto rendimiento. Aunque puede haber razones por las que los dos podrían ser similares, no son lo mismo.
    </p>
    <p>
      
    </p>
    <p>
      La Tabla 3.17 proporciona una expresión simple pero efectiva de un análisis de detección del VPN para el ejemplo de estudio de caso en esta sección:
    </p>
    <ul>
      <li>
        El ahorro de costes previsto es de 16.500 libras esterlinas. Esta entrada se multiplica por 2,991 (el valor actual de una serie de 5 pagos de 1 libra esterlina a intervalos anuales. Este factor se puede encontrar en la Tabla A.1 del Apéndice A).
      </li>
      <li>
        A la inversión inicial se le resta el ahorro, proporcionando el valor actual neto.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Caso de estudio: Valor Presente Neto" ID="ID_1456281129" CREATED="1658845219622" MODIFIED="1658845468460" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un proveedor Tipo I de una pequeña empresa en América del Sur considera invertir en un programa de gestión de servicios. Se estima que el programa costará £ 50,000. Se espera que el programa reduzca los costos laborales en £16,500 por año. La empresa exige una rentabilidad mínima antes de impuestos del 20 % en todos los programas de inversión. Una ventana de 5 años se usa para el retorno de la inversión. Para simplificar, ignore la inflación y los impuestos.
    </p>
    <p>
      
    </p>
    <p>
      ¿Se debe hacer la inversión?
    </p>
    <p>
      Respuesta: no
    </p>
    <p>
      
    </p>
    <p>
      A primera vista la respuesta parece positiva ya que el ahorro (82.500£ = 5 años x 16.500£) supera la inversión (50.000£). Sin embargo, no es suficiente que las reducciones de costos cubran la inversión. También debe producir un rendimiento de al menos el 20%.
    </p>
    <p>
      
    </p>
    <p>
      Para determinar la idoneidad de la inversión, se debe descontar el ahorro anual de £16.500, a su valor presente. Dado que la empresa utiliza una barrera mínima del 20 %, esta tasa se utiliza en el proceso de descuento y se denomina tasa de descuento (consulte la tabla 3.17). Si se deduce el valor presente de la inversión requerida del valor presente de los ahorros en costos, se obtiene un valor presente neto de -£648. Según el análisis, la empresa no debe proceder.
    </p>
    <p>
      
    </p>
    <p>
      Tenga en cuenta que la empresa también podría haber utilizado una tarifa mínima diferente (más alta) de la tarifa con descuento, siguiendo la explicación dada anteriormente. En cualquier caso, la respuesta seguiría siendo no”.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Tasa de retorno de la organización" ID="ID_1636599537" CREATED="1658845515442" MODIFIED="1658845839771" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      ¿Qué es la tasa de descuento de una organización? El costo de capital generalmente se considera la tasa de rendimiento mínima requerida para una organización. Esta es &quot;la tasa de rendimiento promedio que la empresa debe pagar a sus accionistas o acreedores a largo plazo por el uso de sus fondos&quot;. Por lo tanto, el costo de capital sirve como dispositivo mínimo de detección.
    </p>
    <p>
      
    </p>
    <p>
      Para los programas de gestión de servicios, el método NPV tiene varias ventajas sobre el método TIR:
    </p>
    <ul>
      <li>
        VPN es generalmente más fácil de usar.
      </li>
      <li>
        TIR puede requerir la búsqueda de una tasa de descuento que resulte en un VPN de cero.
      </li>
      <li>
        TIR supone que la tasa de rendimiento es la tasa de rendimiento del programa, una suposición que puede no ser cierta para entornos con experiencia mínima en programas de gestión de servicios.
      </li>
      <li>
        Cuando el VAN y la TIR no están de acuerdo sobre el atractivo del proyecto, es mejor optar por el VAN. Hace la suposición más realista sobre la tasa de rendimiento.
      </li>
    </ul>
    <p>
      Existen otros métodos que se utilizan para tomar decisiones de presupuesto de capital, como el reembolso y la tasa de rendimiento simple. Ninguno de los métodos está cubierto, ya que el reembolso no es una medida real de la rentabilidad de una inversión, mientras que la tasa de rendimiento simple no considera el valor del dinero en el tiempo.
    </p>
    <p>
      
    </p>
    <p>
      En el VAN de la gestión de servicios, el enfoque permanece en los flujos de efectivo y no en la utilidad neta contable. Los gerentes deben buscar los tipos de flujos de efectivo que se muestran en la tabla 3.18.
    </p>
    <p>
      
    </p>
    <p>
      Aunque tiene efecto sobre los impuestos, no se descuenta la depreciación. Los métodos de flujo de efectivo descontado proporcionan automáticamente el retorno de la inversión original, lo que hace innecesaria una deducción por depreciación.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Beneficios intangibles" ID="ID_1830321250" CREATED="1658846102376" MODIFIED="1658848847445" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Hay una serie de técnicas disponibles cuando los flujos de efectivo de la gestión de servicios son inciertos. Algunos son muy técnicos, ya que involucran simulaciones por computadora y habilidades matemáticas avanzadas.
    </p>
    <p>
      
    </p>
    <p>
      La mejora y la automatización de procesos son ejemplos comunes de flujos de efectivo difíciles de estimar. Los costos iniciales y tangibles son fáciles de estimar. Los beneficios intangibles, como menor riesgo, mayor confiabilidad, calidad y velocidad, son mucho más difíciles de estimar. Tienen un impacto muy real pero, sin embargo, son un desafío para estimar los flujos de efectivo. Afortunadamente, hay un procedimiento simple disponible.
    </p>
    <p>
      
    </p>
    <p>
      Tomemos, por ejemplo, la organización que busca comprar software de automatización de procesos de gestión de servicios. La organización tiene una tasa de descuento del 8%. La vida útil del software está establecida en 5 años. Un análisis previo del VAN de los costos y beneficios tangibles muestra un VAN de -£139,755. Si los beneficios intangibles son lo suficientemente grandes, el VAN podría pasar de negativo a positivo. Para calcular el beneficio requerido (entrada), primero encuentre el factor de valor presente en la Tabla A.1, Apéndice A. Una mirada en la columna 8%, fila de un período de 5 años, revela un factor de 3,993 (redondeado a tres posiciones decimales). Ahora realizando los cálculos:
    </p>
    <p>
      
    </p>
    <p>
      El resultado sirve como guía subjetiva para la estimación. Si los beneficios intangibles son por lo menos £35,000, entonces el VAN es aceptable. Se debe realizar la automatización del proceso. Si a juicio de los altos directivos, los beneficios intangibles no valen 35.000 libras esterlinas, entonces no se debe realizar la automatización del proceso.
    </p>
  </body>
</html></richcontent>
<node TEXT="Calculo VAN" ID="ID_1248928304" CREATED="1658847749735" MODIFIED="1658847760035" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Calculo%20VAN.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="Decisiones preferentes (usando la tasa interna de retorno)" ID="ID_1209950547" CREATED="1658848857310" MODIFIED="1658850099531" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Si bien muchas oportunidades pasan el proceso de decisión de selección, no se puede actuar en todas. Las limitaciones financieras o de recursos pueden impedir invertir en todas las oportunidades. Deben tomarse decisiones de preferencia, a veces llamadas decisiones de racionamiento o clasificación. Las alternativas en competencia están clasificadas.
    </p>
    <p>
      
    </p>
    <p>
      Calcular y comparar simplemente el VAN de un proyecto con el de otro no compara el tamaño real de la inversión y los rendimientos. Como resultado, la tasa interna de retorno (TIR) se usa ampliamente para decisiones de preferencia. Cuanto mayor sea la tasa interna de retorno, más deseable será la iniciativa.
    </p>
    <p>
      
    </p>
    <p>
      La TIR, a veces llamada rendimiento, es la tasa de rendimiento durante la vida de una iniciativa. La TIR se calcula encontrando la tasa de descuento que iguala el valor actual de las salidas de efectivo de un proyecto con el valor actual de sus entradas. Es decir, la TIR es la tasa de descuento que resulta en un VAN de cero (o el momento exacto en que el proyecto alcanzará el punto de equilibrio y comenzará a producir una contribución positiva).
    </p>
    <p>
      
    </p>
    <p>
      En el ejemplo de estudio de caso anterior, se tomarían los siguientes pasos para calcular la TIR: Primero, encuentre la tasa de descuento que resultará en un valor actual neto de cero. El enfoque más simple es dividir la inversión en el proyecto por el flujo de caja anual neto esperado. Esto producirá un factor a partir del cual se puede calcular la TIR.
    </p>
    <p>
      
    </p>
    <p>
      En segundo lugar, ubique el factor TIR (en este caso 3.03) en la Tabla A.1, Apéndice A, para determinar la tasa de rendimiento que representa. Use la línea del período de 5 años ya que el programa tiene una ventana de 5 años. Un escaneo en la línea del período de 5 años revela que un factor TIR de 3.03 representa una tasa de rendimiento entre 19% y 20%.
    </p>
    <p>
      
    </p>
    <p>
      En tercer lugar, una vez calculada la TIR, compárela con la tasa de rendimiento requerida. En este caso, la tasa de rendimiento requerida es del 20%. Dado que la TIR es un poco menor, es probable que se rechace durante una decisión de selección. En la Tabla 3.19 se da un resumen.
    </p>
    <p>
      
    </p>
    <p>
      La TIR de los candidatos seleccionados se puede comparar directamente con la de otros candidatos seleccionados. Luego, los proyectos viables pueden clasificarse según su TIR respectiva. Los proyectos con la clasificación más alta son aquellos con los porcentajes de TIR más altos.
    </p>
  </body>
</html></richcontent>
<node TEXT="Cálculo TIR" ID="ID_1389967283" CREATED="1658850130120" MODIFIED="1658850144347" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Calculo%20TIR.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="3.6.1.3 ROI post programa" FOLDED="true" ID="ID_1185736229" CREATED="1658191164441" MODIFIED="1658850452669" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Muchas empresas justifican con éxito las implementaciones de gestión de servicios a través de argumentos cualitativos, sin un caso o plan comercial, y a menudo clasifican el ahorro de costos como un impulsor comercial bajo. Pero sin objetivos financieros claramente definidos, las empresas no pueden medir el valor agregado generado por la gestión del servicio, lo que genera un riesgo futuro en forma de una fuerte oposición de los líderes empresariales.
    </p>
    <p>
      
    </p>
    <p>
      Habiendo experimentado un historial de deficiencias en marcos anteriores, las partes interesadas pueden cuestionar el valor resultante de un programa de gestión de servicios. Sin prueba de valor, los ejecutivos pueden dejar de realizar más inversiones. Por lo tanto, todos los proyectos significativos deben someterse a un análisis de ROI posterior al programa. Sin embargo, si se inicia una iniciativa de gestión de servicios sin un análisis previo del ROI, es incluso más importante que se realice un análisis posterior en el momento apropiado (cuando los retornos esperados puedan medirse razonablemente). El cálculo de un ROI de gestión de servicios se ilustra en el modelo básico que se muestra en la Figura 3.42 y se explica a continuación.
    </p>
  </body>
</html></richcontent>
<node TEXT="Objetivos de programa" ID="ID_1055123530" CREATED="1658850454508" MODIFIED="1658850648517" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los objetivos deben ser claros y medibles, ya que sirven para guiar la profundidad y el alcance del análisis del ROI. Los objetivos pueden variar desde terminología simple hasta la adopción de prácticas de la industria:
    </p>
    <ul>
      <li>
        Brindar un servicio consistente y repetible
      </li>
      <li>
        Reducir el costo total de propiedad
      </li>
      <li>
        Mejorar la calidad del servicio
      </li>
      <li>
        Implementar las mejores prácticas en toda la industria
      </li>
      <li>
        Proporcionar una estructura y un proceso generales
      </li>
      <li>
        Facilitar el uso de conceptos y terminología comunes.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Recolección de datos" ID="ID_639987082" CREATED="1658850462926" MODIFIED="1658850828172" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La recopilación de datos apropiados es vital para un resultado de ROI válido y cuantificable. El alcance y los objetivos de la iniciativa deben indicar qué datos son apropiados. Datos recopilados antes de la implementación deberían ser comparados con aquellos recolectados después de la implementación para permitir una comparación de dos líneas base. Ejemplos de datos recolectados incluye:
    </p>
    <ul>
      <li>
        Métricas para calidad de servicio
      </li>
      <li>
        Costos para transacciones de servicio
      </li>
      <li>
        Cuestionarios de satisfacción del cliente.
      </li>
    </ul>
    <p>
      Note que la recolección de datos para procesar transacciones será diferente de la recolección de datos para una función.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Aislar los efectos del programa" ID="ID_1538161817" CREATED="1658850473402" MODIFIED="1658851005593" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En esta etapa, los resultados del programa de gestión de servicios se hacen evidentes. Al aislar los efectos, no debe haber duda de que los resultados deben atribuirse al programa. Hay muchas técnicas disponibles:
    </p>
    <ul>
      <li>
        <b>Análisis de pronóstico</b>&nbsp;Se utiliza un análisis de línea de tendencia u otro modelo de pronóstico para proyectar puntos de datos si el programa no se hubiera llevado a cabo. En la Figura 3.43 se da un ejemplo.
      </li>
      <li>
        <b>Estimaciones de impacto</b>&nbsp;Cuando un enfoque de pronóstico no es factible, ya sea por falta de datos o inconsistencias en las mediciones, se realiza un enfoque alternativo en forma de estimaciones. En pocas palabras, los clientes y las partes interesadas estiman el nivel de mejoras. Se buscan aportes de gerentes organizacionales, expertos independientes y evaluaciones externas.
      </li>
      <li>
        <b>Grupo de control</b>&nbsp;En esta técnica, se lleva a cabo una implementación piloto en un subconjunto de la empresa. Ese subconjunto puede basarse en la geografía, el centro de entrega o la rama organizativa. El rendimiento resultante se compara con un subconjunto similar pero no afectado.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Conversión datos a dinero" ID="ID_3627113" CREATED="1658850489045" MODIFIED="1658851098018" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Para calcular el ROI, es esencial convertir los datos de impacto a valores monetarios. Solo entonces se pueden comparar esos valores con los costos del programa. El reto está en asignar un valor a cada unidad de datos. La técnica aplicada variará y, a menudo, dependerá de la naturaleza de los datos:
    </p>
    <ul>
      <li>
        Se asigna o calcula una medida de calidad, como una queja o una infracción, y se notifica como un valor estándar.
      </li>
      <li>
        Las reducciones de personal o las mejoras en la eficiencia, en forma de costos cargados, se notifican como un valor estándar,
      </li>
      <li>
        Las mejoras en el rendimiento empresarial, en forma de impactos reducidos, se notifican como un valor estándar,
      </li>
      <li>
        Se recurre a expertos internos o externos para establecer el valor de una medida.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Determinar los costos del programa" ID="ID_728310188" CREATED="1658850505877" MODIFIED="1658851149100" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Esto requiere hacer un seguimiento de todos los costos relacionados con el programa de gestión de servicios. Puede incluir:
    </p>
    <ul>
      <li>
        Los costos de planificación, diseño e implementación. Estos son prorrateados durante la vida esperada del programa
      </li>
      <li>
        Los costos de adquisición de tecnología
      </li>
      <li>
        Los gastos de educación
      </li>
      <li>
        Costos de oportunidad (o los beneficios potenciales perdidos al elegir una opción sobre otra).
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Calcular el ROI" ID="ID_1615052258" CREATED="1658850517108" MODIFIED="1658851207493" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una vez que se han determinado los beneficios y costos del programa, se puede calcular el ROI real y compararlo con el ROI proyectado en el caso de negocios original.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Identificar los beneficios cualitativos" ID="ID_1131841167" CREATED="1658850523110" MODIFIED="1658851234176" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los beneficios cualitativos comienzan con los detallados en el caso de negocio, como se describe en una sección anterior. Una segunda mirada a los beneficios cualitativos de la gestión de servicios se encuentra en la Mejora Continua del Servicio ITIL.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="3.6.2 Analisis de impacto en el negocio (BIA)" FOLDED="true" ID="ID_1532851528" CREATED="1658191225305" MODIFIED="1658851582955" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El análisis de impacto empresarial (BIA) es un método utilizado para evaluar el valor relativo de los servicios y, por lo general, se realiza como parte de la gestión de la cartera de servicios. En lugar de analizar los retornos positivos de los servicios, BIA examina qué sucedería si el servicio no estuviera disponible, o solo parcialmente disponible, durante diferentes períodos de tiempo. El valor de este método es que es fácil para el cliente expresar el valor del servicio en términos significativos para él, tanto financieros como no financieros.
    </p>
    <p>
      
    </p>
    <p>
      Una ventaja de este enfoque para los proveedores de servicios internos es que es una excelente herramienta de comunicación, ayudándoles a compartir con la organización en general cómo han entendido sus prioridades y, por lo tanto, cómo han asignado sus recursos.
    </p>
    <p>
      
    </p>
    <p>
      Este enfoque en la evaluación de las interrupciones de los servicios, combinado con la evaluación de la gravedad de la interrupción, lo convierte en un método útil para la gestión de la continuidad del servicio de TI y otros procesos y funciones de gestión de servicios relacionados, por ejemplo:
    </p>
    <ul>
      <li>
        Puede ayudar a identificar riesgos y priorizar contramedidas, lo que da como resultado niveles óptimos de disponibilidad y garantía.
      </li>
      <li>
        Ayuda al proveedor de servicios a tomar mejores decisiones sobre cómo priorizar los incidentes.
      </li>
      <li>
        Ayuda a la gestión de problemas a reducir su enfoque a aquellas áreas que brindan el mejor impacto por la cantidad de esfuerzo requerido.
      </li>
      <li>
        Los proyectos se pueden priorizar mejor en función de lo que tendrá un impacto más positivo en el negocio.
      </li>
      <li>
        El desempeño operativo se puede mejorar enfocando los recursos y las capacidades donde tendrán el efecto más positivo (sin perder el foco en las áreas importantes, resultando en un impacto negativo.
      </li>
      <li>
        Sirve como una entrada excelente para el proceso de gestión del servicio de continuidad del negocio.
      </li>
    </ul>
  </body>
</html></richcontent>
<node TEXT="Prioridades del servicio" ID="ID_1003917346" CREATED="1658851585454" MODIFIED="1658851872494" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Sin embargo, lo más importante es que permite que la empresa decida las prioridades de la prestación del servicio de manera significativa, comparando la inversión en el servicio con el costo de no tenerlo (o tener acceso a un nivel reducido).
    </p>
    <p>
      
    </p>
    <p>
      BIA identifica el costo de la interrupción del servicio para una empresa y el valor relativo de un servicio. Estos dos conceptos no son idénticos:
    </p>
    <ul>
      <li>
        El costo de la interrupción del servicio es un valor financiero que se asigna a un servicio específico y está destinado a reflejar el valor de la productividad y los ingresos perdidos durante un período de tiempo específico.
      </li>
      <li>
        El valor de un servicio en relación con otros servicios en una cartera puede no resultar exclusivamente de las características financieras. El valor del servicio, como se discutió anteriormente, se deriva de características que pueden ir más allá de las consideraciones de gestión financiera y representan aspectos tales como la capacidad de completar trabajar o comunicarse con clientes que pueden no estar directamente relacionados con la generación de ingresos.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Pasos para generar un BIA" ID="ID_1130451948" CREATED="1658851874358" MODIFIED="1658852117169" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Se deben completar una serie de pasos al generar un BIA. Aunque hay una serie de enfoques y métodos disponibles en la industria, todos implican las siguientes actividades:
    </p>
    <ul>
      <li>
        Organice los recursos del negocio y de TI que trabajarán juntos en el análisis.
      </li>
      <li>
        Identifique todos los servicios candidatos principales para la designación como críticos, secundarios y terciarios (no es necesario que los designe en este momento).
      </li>
      <li>
        Identifique los puntos centrales del análisis para su uso en la evaluación del riesgo y el impacto, tales como:

        <ul>
          <li>
            Pérdida de ingresos en ventas
          </li>
          <li>
            Multas
          </li>
          <li>
            Riesgo de falla
          </li>
          <li>
            Pérdida de productividad
          </li>
          <li>
            Pérdida de oportunidad
          </li>
          <li>
            Número de usuarios afectados
          </li>
          <li>
            Visibilidad para los accionistas, la gerencia, etc.
          </li>
          <li>
            Riesgo de obsolescencia del servicio
          </li>
          <li>
            Daño a la reputación entre los clientes, interesados y autoridades regulatorias.
          </li>
          <li>
            Penalidades incurridas como resultado de no conocer las obligaciones contractuales
          </li>
        </ul>
      </li>
      <li>
        Con el negocio, sopesar y estimar la probabilidad de los elementos de riesgo e impacto identificados.
      </li>
      <li>
        Califique los servicios candidatos frente a los elementos ponderados de riesgo e impacto, y sume sus puntajes de riesgo individuales (Análisis de modos y efectos de fallas - FMEA, se puede usar aquí para obtener información adicional).
      </li>
      <li>
        Generar una lista de servicios por orden de perfil de riesgo.
      </li>
      <li>
        Decida un período de tiempo universal con el que estandarizar la traducción de la interrupción del servicio al costo (1 minuto, 1 hora, 1 día, etc.).
      </li>
      <li>
        Calcular el impacto de cada servicio que se analiza dentro del BIA utilizando métodos, fórmulas y supuestos acordados. Generalmente, el impacto será financiero, pero también podría haber otros impactos. Por ejemplo, un departamento gubernamental relacionado con la atención médica podría estimar el impacto en la salud de una comunidad en particular.
      </li>
      <li>
        Genere una lista de servicios en orden de impacto.
      </li>
      <li>
        Utilice los datos de riesgo e impacto generados para crear gráficos que ilustren las aplicaciones de mayor riesgo de la empresa que también tienen el mayor impacto. Estos datos también se pueden utilizar para justificar la inversión en la gestión de servicios, ya que esto garantizará una mayor disponibilidad y confiabilidad de los servicios clave.
      </li>
    </ul>
    <p>
      Los desafíos de realizar BIA implican obtener la aceptación de una amplia gama de personas, cada una con su propia perspectiva diferente de la naturaleza de los riesgos y el impacto. Cada grupo también podría tener una idea diferente de qué riesgos son más probables, y el resultado podría ser ignorar una amenaza en particular simplemente porque no se acordó que fuera &quot;probable&quot;.
    </p>
    <p>
      
    </p>
    <p>
      Es posible que algunos impactos comerciales y medidas de mitigación no se basen en el riesgo real, sino en lo que está actualmente en los titulares, o es algo &quot;de moda&quot;.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="3.7 Estrategia de abastecimiento" FOLDED="true" ID="ID_861579587" CREATED="1658191249416" MODIFIED="1659487749355" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;Las siguientes capas de la creación de valor - ya sea en tecnología, marketing, biomedicina o fabricación - se están volviendo&nbsp;tan complejas que una sola marca o departamento podrán dominarlo por sí&nbsp;solos&quot;
    </p>
    <p>
      -- Tomas L. Friedman
    </p>
  </body>
</html></richcontent>
<node TEXT="Figura 3.45 Usando interfaces de proveedor de servicios.png" ID="ID_264229842" CREATED="1659476794716" MODIFIED="1659476797818" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.45%20Usando%20interfaces%20de%20proveedor%20de%20servicios.png" SIZE="0.9063444" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.44 La escalera de la provisión de servicios.png" ID="ID_197543251" CREATED="1659476598739" MODIFIED="1659476604987" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.44%20La%20escalera%20de%20la%20provisión%20de%20servicios.png" SIZE="0.8253095" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 3.20 Principales estructuras de externalización (estrategias de entrega)" ID="ID_556580023" CREATED="1659469899569" MODIFIED="1659470833904" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 600px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 20%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Estructura de Externalización
          </p>
        </th>
        <th valign="top" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Descripción
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 20%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Internalización (Insourcing)
          </p>
        </td>
        <td valign="top" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Este enfoque se basa en la utilización de recursos organizacionales internos en el diseño, desarrollo, transición, mantenimiento, operación y/o soporte de servicios nuevos, modificados o revisados.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 20%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Externalización (Outsourcing)
          </p>
        </td>
        <td valign="top" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Este enfoque utiliza los recursos de una organización u organizaciones externas en un acuerdo formal para proporcionar una parte bien definida del diseño, desarrollo, mantenimiento, operaciones y/o soporte de un servicio. Esto incluye el consumo de servicios de proveedores de servicios de aplicaciones (ASP) que se describen a continuación.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 20%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Co-sourcing o multi-sourcing
          </p>
        </td>
        <td valign="top" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            A menudo, una combinación de contratación interna y externa, utilizando varias organizaciones que trabajan juntas para obtener elementos clave dentro del ciclo de vida. Esto generalmente implica el uso de varias organizaciones externas que trabajan juntas para diseñar, desarrollar, hacer la transición, mantener, operar y/o respaldar. una parte de un servicio.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 20%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Partnership
          </p>
        </td>
        <td valign="top" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Acuerdos formales entre dos o más organizaciones para trabajar juntas en el diseño, desarrollo, transición, mantenimiento, operación y/o soporte de servicios de TI. El enfoque aquí tiende a estar en las asociaciones estratégicas que aprovechan la experiencia crítica o las oportunidades de mercado.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 20%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Externalización del proceso del negocio (BPO)
          </p>
        </td>
        <td valign="top" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            La tendencia creciente de reubicar funciones comerciales completas utilizando acuerdos formales entre organizaciones donde una organización proporciona y administra los procesos o funciones comerciales completos de la otra organización. en un lugar de bajo costo. Ejemplos comunes son las operaciones de contabilidad, nómina y centro de llamadas.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 20%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Provisión de servicio de aplicaciones (ASP)
          </p>
        </td>
        <td valign="top" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Implica acuerdos formales con una organización proveedora de servicios de aplicaciones (ASP) que proporcionará servicios informáticos compartidos a las organizaciones de los clientes a través de una red desde las instalaciones del proveedor de servicios. A través de los ASP, las complejidades y los costos de dicho software compartido pueden reducirse y proporcionarse a organizaciones que, de otro modo, no podrían justificar la inversión.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 20%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Externalización del proceso de conocimiento (KPO)
          </p>
        </td>
        <td valign="top" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            KPO está un paso por delante de BPO en un aspecto. Las organizaciones de KPO proporcionan procesos basados en dominios y experiencia comercial en lugar de solo experiencia en procesos. En otras palabras, no solo se requiere que la organización ejecute un proceso, sino también que tome ciertas decisiones de bajo nivel basadas en el conocimiento de las condiciones locales o información específica de la industria. Un ejemplo es la subcontratación de la evaluación del riesgo crediticio, donde la organización subcontratada tiene información histórica que ha analizado para crear conocimiento que a su vez les permite brindar un servicio. Para cada compañía de tarjetas de crédito, recopilar y analizar estos datos por sí mismos no sería tan rentable como usar KPO.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 20%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            &quot;Cloud&quot;
          </p>
        </td>
        <td valign="top" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Los proveedores de servicios en la nube ofrecen servicios predefinidos específicos, generalmente a pedido. Los servicios generalmente son estándar, pero se pueden personalizar para un específico organización si hay suficiente demanda del servicio. Los servicios en la nube se pueden ofrecer internamente, pero generalmente se refieren a la prestación de servicios subcontratados.
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 20%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Multi-vendor sourcing
          </p>
        </td>
        <td valign="top" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Este tipo de abastecimiento implica obtener diferentes fuentes de diferentes proveedores, lo que a menudo representa opciones de abastecimiento diferentes a las anteriores.
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
<node TEXT="Definición" ID="ID_276368340" CREATED="1659459649118" MODIFIED="1659459896924" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El Abastecimiento se trata de analizar cómo obtener e implementar de la manera más efectiva los recursos y capacidades necesarios para entregar resultados a los clientes. Se trata de decidir sobre la mejor combinación de tipos de proveedores para apoyar los objetivos de la organización y la prestación eficaz y eficiente de los servicios.
    </p>
    <p>
      
    </p>
    <p>
      Una estrategia de servicio debe mejorar las fortalezas especiales y las competencias básicas de una organización. Cada componente debe reforzar al otro. Cualquier cambio y todo el modelo cambia. A medida que las organizaciones buscan mejorar su desempeño, deben considerar qué competencias son esenciales y saber cuándo ampliar sus capacidades asociándose en áreas tanto dentro como fuera de su empresa.
    </p>
    <p>
      
    </p>
    <p>
      La subcontratación mueve una actividad de creación de valor que se realizó dentro de la organización hacia fuera de la organización, donde la realiza otra empresa. Lo que impulsa a una organización a subcontratar una actividad es la misma lógica que determina si una organización fabrica o compra insumos. Es decir, ¿el valor adicional generado por realizar una actividad dentro de la organización supera los costos de administrarla internamente? Esta decisión puede cambiar con el tiempo.
    </p>
    <p>
      
    </p>
    <p>
      Los servicios de TI son proporcionados cada vez más por proveedores de servicios fuera de la empresa. Tomar una decisión informada de abastecimiento de servicios requiere encontrar un equilibrio entre consideraciones cualitativas y cuantitativas exhaustivas.
    </p>
    <p>
      
    </p>
    <p>
      Históricamente, el caso comercial financiero es la base principal para la mayoría de las decisiones de abastecimiento. Estos análisis incluyen ahorros de costos puros, menores inversiones de capital, redirecciones de inversiones y contención de costos a largo plazo. Desafortunadamente, la mayoría de los análisis financieros no incluyen todos los costos relacionados con las opciones de abastecimiento, lo que genera relaciones difíciles con los proveedores de servicios, lo que implica costos inesperados y problemas de servicio. Si los costos son un factor principal para una decisión de abastecimiento, incluya las finanzas para la transición del servicio, la gestión de relaciones, el apoyo legal, los incentivos, la capacitación, las implicaciones de la licencia de herramientas y la racionalización de procesos, entre otros.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.7.1 Decidiendo que abastecer" FOLDED="true" ID="ID_1971976988" CREATED="1658191288731" MODIFIED="1659469177073" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Decidir que subcontratar es encontrar las vías para mejorar la diferenciación competitiva reimplementando recursos y capacidades. Cualquier capacidad o recursos que son solo externamente relacionados a la estrategia central del negocio y a la diferenciación deben ser considerados para subcontratación. Una vez que los candidatos para subcontratación son identificados, las siguientes preguntas pueden servir para clarificar su importancia:
    </p>
    <ul>
      <li>
        ¿Los servicios candidatos mejoran las capacidades y recursos del negocio?
      </li>
      <li>
        ¿Qué tanto están los servicios conectados a la ventaja competitiva del negocio y a los recursos y capacidades estratégicas?
      </li>
      <li>
        ¿Los servicios candidatos requieren interacciones extensivas entre los proveedores de servicio y la competitividad del negocio y las capacidades y recursos estratégicos?
      </li>
    </ul>
  </body>
</html></richcontent>
<node TEXT="Caso de estudio: Estrategia de subcontratación" ID="ID_774659334" CREATED="1659460283982" MODIFIED="1659460497944" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      A principios de la década de 2000, las empresas se apresuraron a implementar una estrategia de servicios basada en el arbitraje laboral: los proveedores de servicios reducen los costos laborales al hacer uso de recursos extraterritoriales menos costosos. La intención estratégica es hacer que la propuesta de valor de un proveedor sea más convincente a través de estructuras de menor costo.
    </p>
    <p>
      
    </p>
    <p>
      Si bien los costos de hecho disminuyeron para los clientes, los proveedores no pudieron obtener ganancias a largo plazo en sus resultados financieros.
    </p>
    <p>
      
    </p>
    <p>
      ¿Por qué?
    </p>
    <p>
      
    </p>
    <p>
      Respuesta: La incapacidad de capturar valor
    </p>
    <p>
      
    </p>
    <p>
      Los primeros en adoptar una estrategia de arbitraje laboral obtuvieron grandes ganancias porque, por un tiempo, los costos de los servicios que ofrecían eran más bajos que cualquier alternativa de la competencia. Pero a medida que más y más proveedores de servicios hacían uso de recursos extraterritoriales, el costo de los servicios se redujo para todos. Esto fue excelente para los clientes pero malo para los proveedores ~ esta distinción finalmente se eliminó. Se creó valor para los clientes, pero los proveedores de servicios no pudieron mantener nada de eso. Esta capacidad de un proveedor de servicios para quedarse con una parte de cualquier valor creado se conoce como &quot;captura de valor&quot;.
    </p>
    <p>
      
    </p>
    <p>
      La estrategia de abastecimiento fue de vital importancia para defenderse de las alternativas competidoras. Sin embargo, los proveedores de servicios que se centraron únicamente en esta estrategia, a expensas de otras capacidades distintivas, pronto encontraron fallas estratégicas en forma de &quot;rendimiento mediocre frente a alternativas competidoras&quot;.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Extructuras de externalización" ID="ID_1668538871" CREATED="1659469035181" MODIFIED="1659469204696" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Si las respuestas descubren dependencias mínimas e interacciones poco frecuentes entre los servicios adquiridos y el posicionamiento competitivo y estratégico de la empresa, entonces los candidatos son fuertes contendientes. Si los candidatos para el abastecimiento están estrechamente relacionados con el posicionamiento competitivo o estratégico de la empresa, entonces se debe tener cuidado. Tales estructuras de abastecimiento son particularmente vulnerables a:
    </p>
    <ul>
      <li>
        <b>Sustitución</b>&nbsp;'¿Por qué necesito el proveedor de servicios cuando su proveedor puede ofrecer los mismos servicios? El proveedor externo desarrolla capacidades competitivas y reemplaza a la organización de abastecimiento.
      </li>
      <li>
        <b>Interrupción</b>&nbsp;El proveedor externo tiene un impacto directo en la calidad o la reputación de la organización de abastecimiento.
      </li>
      <li>
        <b>Carácter distintivo</b>&nbsp;El proveedor externo es la fuente de carácter distintivo para la organización contratante. La organización contratante se vuelve particularmente dependiente del desarrollo continuo y el éxito de la segunda organización.
      </li>
    </ul>
    <p>
      Se debe tener cuidado para distinguir entre actividades distintivas y actividades críticas. Las actividades críticas no necesariamente se refieren a actividades que pueden ser distintivas del proveedor de servicios. Por ejemplo, aunque el servicio al cliente es muy probablemente fundamental, si no diferencia al proveedor de las alternativas de la competencia, entonces no es distintivo, lo que aquí se denomina &quot;contexto&quot;.
    </p>
    <p>
      
    </p>
    <p>
      Esto no significa que las actividades críticas sean menos importantes que las actividades distintivas. Significa que no aportan el beneficio diferenciador que genera valor. El contexto de un proveedor de servicios puede ser el carácter distintivo de otro. Lo que es distintivo hoy puede convertirse con el tiempo en contexto. Los procesos contextuales pueden recombinarse en procesos distintivos. Aquí hay una prueba básica:
    </p>
    <ul>
      <li>
        ¿El cliente o el mercado espera que el proveedor de servicios realice esta actividad? (contexto)
      </li>
      <li>
        ¿El cliente o el espacio de mercado le da crédito al proveedor de servicios por realizar esta actividad excepcionalmente bien? (diferencia)
      </li>
    </ul>
    <p>
      Los primeros usuarios de quioscos de aerolíneas, por ejemplo, se diferenciaron a través de la tecnología de autoservicio. Si bien los quioscos eran una actividad distintiva central para la estrategia de servicio, apenas eran críticos. Años más tarde, los clientes esperan quioscos en todos los aeropuertos para todas las aerolíneas. Todas las principales aerolíneas consideran esto una actividad crítica pero no distintiva - esto ya no es diferenciador. Por eso las aerolíneas consolidan o externalizan su actividad crítica. Ellos colaboran con otras aerolíneas para proveer quioscos que cualquier miembro de la aerolínea pueda usar. Ellos externalizan los quioscos a proveedores externos quienes los ubican en otros lugares, tales como lobbies en hoteles u oficinas corporativas.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.7.2 Estructuras de abastecimiento" ID="ID_1533773811" CREATED="1658191365401" MODIFIED="1659469875475" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Independientemente de dónde se obtengan los servicios, las capacidades o los recursos, la organización conserva la responsabilidad de su adecuación. La subcontratación no significa que un servicio o su desempeño dejen de ser importantes. En la mayoría de los casos, a menudo significa que el servicio es tan importante que debe ser proporcionado por un proveedor de servicios que pueda hacer un mejor trabajo (o más rentable). En otras palabras, subcontratar una capacidad, un recurso o un servicio no significa subcontratar la gobernanza de lo que hace ese elemento.
    </p>
    <p>
      
    </p>
    <p>
      La organización debe adoptar un enfoque de gobierno formal para gestionar sus servicios subcontratados y garantizar la entrega de valor. Esto incluye la planificación del cambio organizativo iniciado por la estrategia de abastecimiento y una descripción formal y verificable de cómo se toman las decisiones sobre los servicios. En la Tabla 3.20 se proporciona una descripción general de las estructuras de abastecimiento predominantes. Estas estructuras de abastecimiento también representan la estrategia que utiliza la organización para prestar sus servicios.
    </p>
    <p>
      
    </p>
    <p>
      La selección de una estructura de abastecimiento debe equilibrarse con riesgos y niveles de control aceptables. El método que utiliza una organización para administrar una relación de abastecimiento depende en gran medida de las características de la organización de abastecimiento, como los grados de centralización, los estándares y la madurez del proceso. Esto requerirá una función dedicada para gestionar la relación. En general, la organización de abastecimiento debe sobresalir en el establecimiento de un conjunto de estándares y procesos de relación. Otras responsabilidades clave son:
    </p>
    <ul>
      <li>
        Supervisar el desempeño de los acuerdos y la relación general con los proveedores
      </li>
      <li>
        Administrar los acuerdos de abastecimiento
      </li>
      <li>
        Proporcionar un nivel de escalamiento para asuntos y problemas
      </li>
      <li>
        Asegurar que los proveedores comprendan las prioridades de los servicios de la organización
      </li>
    </ul>
    <p>
      Cuando se externaliza los servicios, las organizaciones se deben enfocar en definir claramente que se espera que haga el proveedor de servicio. Demasiado a menudo el foco principal es en las estructuras de reportería y la alineación de los recursos a esas estructuras. La alineación de recursos y las estructuras organizacionales deben ser analizadas y ajustadas solo después de comprender la dinámica de los servicios nuevos o mejorados.
    </p>
    <p>
      
    </p>
    <p>
      Una vez que los recursos y la discusión organizacional inicia, la organización debe asegurarse de la introducción de las nuevas habilidades críticas. Estas competencias generalmente caen en tres categorías: negocios, técnica y comportamiento. Por ejemplo, a mayor externalización es mayor la necesidad del negocio y las habilidades de comportamiento. A mayor nivel de internalización, es mayor las necesidad de necesidades técnicas.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="3.7.3 Abastecimiento multi-proveedor" FOLDED="true" ID="ID_788808421" CREATED="1658191421722" MODIFIED="1659477395977" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La contratación de servicios de múltiples proveedores se ha convertido en la norma y no en la excepción. Este enfoque ha estado brindando beneficios y ganando cada vez más apoyo. La organización mantiene una fuerte relación con cada proveedor, distribuyendo el riesgo y reduciendo costos. También se debe tener en cuenta que cada proveedor puede representar un tipo diferente de opción de abastecimiento de la Tabla 3.20.
    </p>
    <p>
      
    </p>
    <p>
      La gobernanza y la gestión de múltiples proveedores, que a menudo tienen poco que ver entre sí fuera del cliente común, pueden ser un desafío. Al buscar múltiples proveedores, se deben evaluar cuidadosamente los siguientes aspectos:
    </p>
    <ul>
      <li>
        <b>Complejidad técnica</b>&nbsp;La contratación de servicios de proveedores de servicios externos es útil para los procesos de servicio estandarizados (aunque como incrementa la personalización es más difícil alcanzar la eficiencia deseada).
      </li>
      <li>
        <b>Interdependencias Organizacionales</b>&nbsp;Los vehículos contractuales deben estructurarse cuidadosamente para abordar la dinámica de múltiples organizaciones, asegurando que todos los proveedores reciban una capacitación constante en los procesos de la organización del cliente, etc. Además, los contratos deben incluir incentivos diseñados para fomentar la coherencia en el desempeño contractual entre los proveedores.
      </li>
      <li>
        <b>Planificación de la integración</b>&nbsp;Los procesos, datos y herramientas de cada organización pueden ser diferentes o pueden estar duplicados. Cualquiera de los casos requerirá la integración de ciertos procesos, datos y herramientas. Además, es fundamental que los informes de cada organización estén integrados para que la gobernanza se pueda realizar de forma coherente en cada organización.
      </li>
      <li>
        <b>Abastecimiento administrado</b>&nbsp;Esto se refiere a la necesidad de una interfaz única para los múltiples proveedores cuando corresponda. Una receta segura para el fracaso en un entorno de múltiples fuentes que requiere la colaboración entre proveedores de servicios es que cada contrato sea negociado y administrado por diferentes grupos dentro de la organización.
      </li>
    </ul>
    <p>
      Existen múltiples enfoques y diversos grados en el abastecimiento. Hasta dónde está dispuesta a llegar una organización con el abastecimiento depende de los objetivos comerciales que se deben lograr y las limitaciones que se deben superar. La figura 3.44 ilustra las limitaciones experimentadas en cada uno de los tres niveles de abastecimiento y luego indica los beneficios o características adicionales que se pueden obtener al pasar al siguiente nivel. Tenga en cuenta que esta cifra no implica que una sola forma de abastecimiento sea mejor que otra, simplemente que existen diferentes beneficios potenciales a medida que la organización sube la escalera. Será necesario evaluar si cada organización puede lograr o no estos beneficios para cada servicio.
    </p>
  </body>
</html></richcontent>
<node TEXT="Elegir los proveedores" ID="ID_1419255812" CREATED="1659477398507" MODIFIED="1659477419045" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      En la Figura 3.44, una organización con limitaciones en la capacidad interna podría pasar a un modelo de subcontratación en el que se utiliza una sola organización de subcontratación para aumentar las capacidades actuales a través de las economías de escala (por ejemplo, las capacidades especializadas y costosas se pueden compartir entre más de un cliente, lo que las hace disponibles a un costo general más bajo para todos los clientes). Un contrato de subcontratación de un solo proveedor eventualmente se verá limitado en lo que puede proporcionar a una base de clientes grande y compleja, lo que requerirá que los clientes obtengan servicios de múltiples proveedores.
    </p>
    <p>
      
    </p>
    <p>
      Independientemente del enfoque de abastecimiento, los altos ejecutivos deben evaluar cuidadosamente los atributos del proveedor. La siguiente es una lista de verificación útil:
    </p>
    <ul>
      <li>
        <b>Competencias demostradas</b>&nbsp;En términos de personal, uso de tecnologías, innovación, experiencia en la industria y certificaciones (por ejemplo, ISO/IEC 20000)
      </li>
      <li>
        <b>Logros pasados</b>&nbsp;En términos de calidad de servicio lograda, valor financiero creado y compromiso demostrado con la mejora continua
      </li>
      <li>
        <b>Dinámica de relaciones</b>&nbsp;En términos de visión y estrategia, el ajuste cultural, el tamaño relativo del contrato en su cartera y la calidad de la gestión de relaciones.
      </li>
      <li>
        <b>Calidad de las soluciones</b>&nbsp;Relevancia de los servicios para sus requisitos, gestión de riesgos y puntos de referencia de rendimiento
      </li>
      <li>
        <b>Capacidades generales</b>&nbsp;En términos de solidez financiera, recursos, sistemas de gestión y alcance y gama de servicios
      </li>
      <li>
        <b>Compromiso con el personal transferido</b>&nbsp;En términos de su retención a largo plazo, desarrollo personal y oportunidades de carrera.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.7.4 Interfaces de proveedor de servicio (SPI)" FOLDED="true" ID="ID_1792655389" CREATED="1658191442787" MODIFIED="1659477734954" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Para apoyar el desarrollo de relaciones de abastecimiento en un entorno de múltiples proveedores, se necesitan directrices y puntos de referencia (técnicos, de procedimiento, organizativos) entre los distintos proveedores de servicios. Estos puntos de referencia pueden referirse a tecnología, procedimientos o estructuras de organización. Un método para formalizar estos puntos de referencia es el uso de interfaces de proveedores de servicios (SPI).
    </p>
    <p>
      
    </p>
    <p>
      Una interfaz de proveedor de servicios es un punto de referencia formalmente definido que identifica alguna interacción entre un proveedor de servicios y un usuario, cliente, proceso o uno o más proveedores. Los SPI generalmente se utilizan para garantizar que varias partes en una relación comercial tengan los mismos puntos de referencia para definir, entregar y reportar servicios.
    </p>
    <p>
      
    </p>
    <p>
      Los SPl ayudan a coordinar la gestión integral de los servicios críticos. El catálogo de servicios impulsa las especificaciones del servicio, que forman parte de las definiciones de procesos estándar. Las responsabilidades y los niveles de servicio se negocian en el momento en que se establece la relación de abastecimiento e incluyen:
    </p>
    <ul>
      <li>
        Identificación de puntos de integración entre varios procesos de gestión del cliente y el proveedor de servicios
      </li>
      <li>
        Identificación de funciones y responsabilidades específicas para gestionar la relación de gestión de sistemas en curso con ambos partes
      </li>
      <li>
        Identificación de la información de gestión de sistemas relevante que debe comunicarse al cliente de forma continua.
      </li>
    </ul>
    <p>
      Las SPIs también deben considerar más de un servicio activo. Hay una necesidad para actividades de gestión, estrategia y transición así como servicios en vivo. Esto es especialmente verdad si el proveedor de servicio externo es visto como un socio estratégico en la transformación del negocio del cliente o el modelo de negocio.
    </p>
  </body>
</html></richcontent>
<node TEXT="¿Cómo los SPIs deberían ser usados?" ID="ID_1084407325" CREATED="1659477736578" MODIFIED="1659486174509"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La Figura 3.45 ilustra cómo se podrían utilizar los SPI. A la izquierda de este diagrama hay una serie de actividades o procesos que realiza una organización. A la derecha del diagrama hay una serie de contratos de abastecimiento. Cada proveedor debe poder interactuar con cada una de las tres actividades en el lado izquierdo del diagrama, pero no es necesario que participen en el proceso de principio a fin. proceso de cada uno. Para resolver esta situación, se define un SPI para cada actividad, identificando lo que el proveedor necesita saber sobre cada actividad y cómo interactúa con ella. Estos SPIs se utilizan para identificar lo que se incluirá en el contrato.
    </p>
    <p>
      
    </p>
    <p>
      Las definiciones de SPI de procesos constan de:
    </p>
    <ul>
      <li>
        Prerrequisitos tecnológicos (p. ej., estándares de herramientas de gestión o protocolos prescritos)
      </li>
      <li>
        Requisitos de datos (p. ej., eventos o registros específicos), formatos (p. ej., diseños de datos), interfaces (p. ej., API, puertos de cortafuegos) y protocolos (p. ej., SNMP , XML)
      </li>
      <li>
        Requisitos no negociables (por ejemplo, prácticas, actividades, procedimientos operativos)
      </li>
      <li>
        Roles/responsabilidades requeridas dentro del proveedor de servicios y las organizaciones del cliente
      </li>
      <li>
        Tiempos de respuesta y escalamientos.
      </li>
    </ul>
    <p>
      Los SPI se definen, mantienen y son propiedad de los propietarios del proceso. Otros involucrados en la definición incluyen:
    </p>
    <ul>
      <li>
        Representantes comerciales, que negocian los requisitos del SPI y son responsables de gestionar las relaciones estratégicas con y entre los proveedores de servicios.
      </li>
      <li>
        Coordinador(es) del proceso del proveedor de servicios, quien asumir la responsabilidad operativa de garantizar que los procesos operativos estén sincronizados.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.7.5 Gobernanza del abastecimiento" FOLDED="true" ID="ID_1051503670" CREATED="1658191468884" MODIFIED="1659486958917" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La gobernabilidad de abastecimiento es un área compleja. Además, crear una estrategia de abastecimiento es una tarea importante y será más exigente de lo que esta sección puede articular. Esta sección destaca la necesidad de emprender estas actividades y proporciona una descripción general de alto nivel del área.
    </p>
    <p>
      
    </p>
    <p>
      Hay un malentendido frecuente de la definición de gobernanza, particularmente en un contexto de abastecimiento. Las empresas han usado la palabra indistintamente con &quot;gestión de proveedores&quot;, &quot;personal retenido&quot;, &quot;gestión de proveedores&quot; y &quot;organización de gestión de abastecimiento&quot;. La gobernanza no es nada de esto.
    </p>
    <p>
      
    </p>
    <p>
      La gobernanza se cubre en detalle en la sección 5.1, pero para facilitar la referencia, la gobernanza se refiere a las reglas, políticas, procesos (y en algunos casos, leyes) mediante los cuales se operan, regulan y controlan las empresas. accionistas, o la constitución de la organización; pero también pueden estar definidos por la legislación, la regulación o los grupos de consumidores.
    </p>
    <p>
      
    </p>
    <p>
      La gestión y la gobernanza son disciplinas diferentes. La gestión se ocupa de la toma de decisiones y la ejecución de procesos. La gobernanza es el marco de los derechos de decisión que fomentan los comportamientos deseados en el abastecimiento y la organización de origen. Cuando las empresas confunden gestión y gobierno, inevitablemente se centran en la ejecución a expensas de la toma de decisiones estratégicas. Ambos son de vital importancia.
    </p>
    <p>
      
    </p>
    <p>
      La gobernanza es invariablemente el eslabón más débil en una estrategia de abastecimiento de servicios. Se ha demostrado que las siguientes áreas son efectivas para mejorar esa debilidad:
    </p>
    <ul>
      <li>
        <b>Un órgano de gobierno</b>&nbsp;Al formar un órgano de gobierno de tamaño manejable con una comprensión clara de la estrategia de abastecimiento de servicios, se pueden tomar decisiones sin escalar a los niveles más altos de la alta dirección. Al incluir la representación de cada proveedor de servicios, se pueden tomar decisiones más sólidas.
      </li>
      <li>
        <b>Dominios de gobierno</b>&nbsp;Los dominios pueden cubrir la toma de decisiones para un área específica de la estrategia de abastecimiento de servicios. Los dominios pueden cubrir, por ejemplo, la prestación de servicios, la comunicación, la estrategia de abastecimiento o la gestión de contratos. Un dominio de gobernanza no incluye la responsabilidad de su ejecución, solo su toma de decisiones estratégicas
      </li>
      <li>
        <b>Creación de una matriz de derechos de decisión</b>&nbsp;Esto une todas las recomendaciones. Los gráficos RACI son formas comunes de una matriz de derechos de decisión (consulte la sección 6.9).
      </li>
      <li>
        <b>Gestión de proveedores</b>&nbsp;(consulte ITIL Diseño de servicios) Esto garantiza que los contratos y los proveedores de servicios externos se gestionen de acuerdo con las políticas, estándares y controles de gobierno de la organización.
      </li>
    </ul>
  </body>
</html></richcontent>
<node TEXT="Asociación con proveedores de servicio que cumplen estándares" ID="ID_511094592" CREATED="1659486963858" MODIFIED="1659487169827" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Asociarse con proveedores que cumplan con la norma ISO/IEC 20000 podría ser un elemento importante en la selección del subcontratista adecuado. Las organizaciones que han logrado esta certificación han demostrado el cumplimiento de un estándar de gestión de servicio&nbsp;consistente. Esta credencial es particularmente importante en entornos de múltiples fuentes donde un marco común promueve una mejor integración. Los entornos de fuentes múltiples requieren un lenguaje común, procesos integrados y una estructura de gestión entre proveedores internos y externos. Aunque ITIL proporciona terminología común y definiciones de procesos, no hay garantía de que una organización los utilice como parte de sus operaciones estándar; incluso si sus empleados están certificados por ITIL, ISO/IEC 20000 proporciona un medio para determinar si un proveedor de servicios realmente ha adoptado el marco estándar.
    </p>
    <p>
      
    </p>
    <p>
      ISO/IEC 20000 es el primer estándar internacional formal específico para la gestión de servicios de TI. Una organización que se sienta cómoda con ITIL no tendrá dificultades para interpretar ISO/IEC 20000. Se pueden encontrar más detalles sobre ISO/IEC 20000 en el Apéndice D, sección D.6.
    </p>
    <p>
      
    </p>
    <p>
      Además, el marco de los objetivos de control definidos en COBIT proporciona una buena fuente para definir las prácticas de gobierno. Puede encontrar más información sobre COBIT en el Apéndice D, sección D.65.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.7.6 Factores criticos de exito" ID="ID_1433735961" CREATED="1658191486152" MODIFIED="1659487742592" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Es importante comprender los costos y riesgos asociados con el cambio de un modelo de abastecimiento a otro. Los costos del cambio a veces pueden eclipsar cualquier beneficio, y la administración adicional de un contrato subcontratado puede reducir los ahorros esperados. Además, el riesgo de trasladar actividades operativas críticas de una entidad a otra puede ser disruptivo y puede ser irrevocablemente dañino si esta interrupción se siente durante un tiempo prolongado. Por lo tanto, es importante tener en cuenta los siguientes factores críticos de éxito al tomar la decisión de garantizar una estrategia de abastecimiento exitosa. Los factores para una estrategia de abastecimiento de servicios dependen con frecuencia de:
    </p>
    <ul>
      <li>
        Los resultados deseados, como la reducción de costos, la mejora de la calidad del servicio o la disminución del riesgo comercial
      </li>
      <li>
        El modelo óptimo para brindar el servicio
      </li>
      <li>
        La mejor ubicación para entregar el servicio, tal como local, en tierra, cerca de la costa o en alta mar.
      </li>
    </ul>
    <p>
      El enfoque recomendado para decidir una estrategia incluye:
    </p>
    <ul>
      <li>
        Análisis de las competencias en gestión del servicio internas de la organización.
      </li>
      <li>
        Comparar esos resultados con las referencias de la industria.
      </li>
      <li>
        Analizar la habilidad de la organización para entregar valor estratégico.
      </li>
    </ul>
    <p>
      El enfoque probablemente lleve a los siguientes escenarios:
    </p>
    <ul>
      <li>
        Si la competencia de gestión de servicios internos de la organización es alta y también proporciona valor estratégico, entonces una estrategia de servicios internos o compartidos es la opción más probable. La organización debe continuar invirtiendo internamente, aprovechando proveedores expertos de alto valor para refinar y mejorar las competencias de gestión de servicios.
      </li>
      <li>
        Si la competencia interna de gestión de servicios de la organización es baja pero proporciona un valor estratégico, entonces la subcontratación es una opción siempre que los servicios se puedan mantener o mejorar mediante el uso de proveedores de alto valor.
      </li>
      <li>
        Si la competencia de gestión de servicios internos de la organización es alta pero no proporciona valor estratégico, existen múltiples opciones. La empresa puede querer invertir en sus capacidades de servicio para que proporcionen un valor estratégico o puede vender esta capacidad de servicio, porque puede ser de mayor valor para un tercero.
      </li>
      <li>
        Si la competencia de gestión de servicios internos y el valor estratégico de la organización son bajos, entonces deben considerarse candidatos para la subcontratación.
      </li>
    </ul>
    <p>
      Antes de cualquier implementación, una organización debe establecer y mantener una línea de base de sus métricas de desempeño. Sin tales métricas, será difícil evaluar el verdadero impacto y las tendencias de una implementación de abastecimiento de servicios. La medición puede adoptar dos formas:
    </p>
    <ul>
      <li>
        <b>Métricas comerciales</b>: ahorros financieros, mejoras en el nivel de servicio, eficiencia de los procesos comerciales
      </li>
      <li>
        <b>Métricas del cliente</b>: disponibilidad y consistencia de los servicios, mayor oferta, calidad del servicio.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.8 Estructuras de servicio en la red de valor" FOLDED="true" ID="ID_1891526047" CREATED="1658191516287" MODIFIED="1659489294496" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;Todos los modelos están mal, pero algunos de ellos son útiles.&quot;
    </p>
    <p>
      -- George Box, estatista
    </p>
  </body>
</html></richcontent>
<node TEXT="3.8.1 Desde la cadena de valor a la red de valor" FOLDED="true" ID="ID_370329193" CREATED="1658191534959" MODIFIED="1659489289124" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los ejecutivos de negocios han descrito durante mucho tiempo el proceso de creación de valor como eslabones de una cadena de valor. Este modelo se basa en la línea de producción de la era industrial: una serie de actividades de valor agregado que conectan el lado de la oferta de una organización con su lado de la demanda. Cada servicio proporciona valor a través de una secuencia de eventos que conducen a la entrega, consumo y mantenimiento de ese servicio en particular. Al analizar cada etapa de la cadena, los altos ejecutivos presumiblemente encuentran oportunidades de mejora.
    </p>
    <p>
      
    </p>
    <p>
      Gran parte del valor de la gestión de servicios, sin embargo, es intangible y complejo. Incluye conocimientos y beneficios como experiencia técnica, información estratégica, conocimiento de procesos y diseño colaborativo. A menudo, el valor radica en cómo se combinan, empaquetan e intercambian estos intangibles. Los modelos lineales han demostrado ser inadecuados para describir y comprender las complejidades del valor para la gestión de servicios, a menudo tratando la información como un elemento de apoyo en lugar de una fuente de valor. La información se usa para monitorear y controlar en lugar de crear valor nuevo.
    </p>
    <p>
      
    </p>
    <p>
      Las cadenas de valor siguen siendo una herramienta importante. Proporcionan una estrategia para integrar y coordinar verticalmente los activos dedicados necesarios para el desarrollo de productos. Sin embargo, no reflejan la situación dinámica de los servicios.
    </p>
    <p>
      
    </p>
    <p>
      Es importante comprender la fuerza más poderosa para interrumpir las cadenas de valor convencionales: el bajo costo de la información. La información fue el pegamento que mantuvo unida la integración vertical. Históricamente, obtener la información necesaria para los proveedores y proveedores de servicios ha sido costoso, requiriendo activos dedicados y sistemas patentados. Estas barreras de entrada dieron a las cadenas de valor su ventaja competitiva. Sin embargo, a través del intercambio de información abierta y económica, las empresas ahora pueden hacer uso de los recursos y capacidades sin poseerlos.
    </p>
    <p>
      
    </p>
    <p>
      Los costos de transacción más bajos permiten a las organizaciones controlar y rastrear información que habría sido demasiado costosa de capturar y procesar en solo unos años atrás. Los costos de transacción todavía existen, pero son cada vez más onerosos dentro de la organización que fuera de ella. Esto, a su vez, ha creado nuevas oportunidades de colaboración entre proveedores de servicios y proveedores. El resultado final es una mezcla flexible de mecanismos que socavan la rígida integración vertical.
    </p>
  </body>
</html></richcontent>
<node TEXT="Figura 3.46 Ejemplo de una red de valor.png" ID="ID_597817697" CREATED="1659489873698" MODIFIED="1659489876740" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.46%20Ejemplo%20de%20una%20red%20de%20valor.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Caso de estudio: Servicios de comercio" ID="ID_750294878" CREATED="1659489301399" MODIFIED="1659489468982" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una empresa de comercio web prospera a pesar de una grave desaceleración económica. El modelo de negocio, basado en subastas online, es rentable. Sin embargo, el modelo de negocio no explica por qué sus servicios tienen éxito en la creación de valor sostenible mientras que otros sitios fracasan.
    </p>
    <p>
      
    </p>
    <p>
      Los flujos de proceso no proporcionan información. Sin embargo, un análisis de valor neto revela la distinción entre el subastador y sus competidores.
    </p>
    <p>
      
    </p>
    <p>
      ¿Qué reveló el valor neto sobre los servicios que un flujo de proceso no pudo revelar?
    </p>
    <p>
      
    </p>
    <p>
      Respuesta
    </p>
    <p>
      La mayoría de los servicios se enfocan en obtener ganancias o realizar beneficios sociales. Un análisis de valor neto reveló que el subastador en línea hizo ambas cosas.
    </p>
    <p>
      
    </p>
    <p>
      La red de valor reveló un participante oculto y sus intercambios intangibles: los aficionados. Los aficionados descubrieron que podían participar en la microeconomía del subastador. Se convirtieron en participantes profesionales con su propia captura de valor. Crearon un sentido de comunidad, lealtad, mecanismos de retroalimentación y referencias. Al crear indirectamente prosperidad para los aficionados, el subastador creó prosperidad para sí mismo. El subastador utilizó esta idea para crear una nueva clase de servicios dirigidos a los aficionados.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Nuevas estrategias ahora son disponibles para los proveedores de servicio:" ID="ID_363607113" CREATED="1659489476248" MODIFIED="1659489805384" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        <b>Mariscal de talento externo</b>&nbsp;Ninguna organización por sí sola puede producir orgánicamente todos los recursos y capacidades requeridos dentro de una industria. La mayor parte de la innovación ocurre fuera de la organización.
      </li>
      <li>
        <b>Reducir costos</b>&nbsp;Producir servicios más robustos en menos tiempo y por menos gastos que los posibles a través de enfoques de cadena de valor convencionales. Si es menos costoso realizar una transacción dentro de la organización, manténgala ahí. Si es más barato obtener una fuente externa, eche un segundo vistazo. Una organización debe contratar hasta que el costo de una transacción interna ya no exceda el costo de realizar la transacción externamente. Este es un corolario de la 'l<i>ey de Coase</i>': una empresa tiende a expandirse hasta que los costos de organizar una transacción adicional dentro de la empresa sean iguales a los costos de llevar a cabo la misma transacción en el mercado abierto. El concepto de ley de Coase fue desarrollado por primera vez por Tapscott et al. (2000).
      </li>
      <li>
        <b>Cambie el punto focal de distinción</b>&nbsp;Al aprovechar el talento externo, una organización puede redistribuir sus propios recursos y mejorar la capacidad de los servicios que se adaptan mejor a su cliente o espacio de mercado. Tomemos el caso de una liga deportiva popular de América del Norte y su proveedor de servicio Tipo I. Al aprovechar las capacidades de los proveedores de servicios de infraestructura Tipo III, el Tipo I es libre de redistribuir sus capacidades para mejorar sus nuevos servicios de medios, a saber, servicios basados en la web con transmisión de video de última generación, venta de boletos, estadísticas, ligas de fantasía y promociones.
      </li>
      <li>
        <b>Aumentar la demanda de servicios complementarios</b>&nbsp;Una organización, particularmente una Tipo I, puede carecer de la amplitud de servicios que ofrecen los proveedores de servicios Tipo II y 'Tipo III'. Al actuar como un integrador de servicios, estas organizaciones no solo subsanan la brecha, sino que también aumentan la demanda a través de ofertas complementarias.
      </li>
      <li>
        <b>Colaborar</b>&nbsp;A medida que bajan los costos de transacción, la colaboración es menos opcional. Siempre hay más personas inteligentes fuera de una organización que dentro.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
<node TEXT="Proveedor efectivo de servicio" ID="ID_899593741" CREATED="1659489915691" MODIFIED="1659490107781" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un proveedor de servicios eficaz verá la gestión de servicios como patrones de intercambios colaborativos, en lugar de una línea de montaje. Desde la perspectiva del pensamiento sistémico, es más útil pensar en la gestión de servicios como una red o red de valor.
    </p>
    <p>
      
    </p>
    <p>
      Una red de valor es una red de relaciones que genera valor tangible e intangible a través de intercambios dinámicos complejos a través de dos o más organizaciones. Un ejemplo simple de esto se muestra en la Figura 3.46.
    </p>
    <p>
      
    </p>
    <p>
      En un diagrama de valor neto, una flecha designa una transacción. La dirección de la flecha indica la dirección de la transacción o el impacto en un participante: proveedor de servicios o cliente. Las transacciones pueden ser temporales, pueden incluir entregables, tangibles o intangibles. Las flechas punteadas se pueden utilizar para distinguir las transacciones intangibles. La Figura 3.46 muestra que el modelo tradicional de proveedor-proveedor de servicios-unidad de negocio no es adecuado para mostrar la complejidad de las transacciones reales en una situación de gestión de servicios.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="3.8.2 Uso de las redes de valor" FOLDED="true" ID="ID_385978425" CREATED="1658191554647" MODIFIED="1659491045253" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los servicios a menudo se caracterizan por redes complejas de flujos de valor y formas de valor, que a menudo involucran a muchas partes que se influyen mutuamente de muchas maneras. Las redes de valor sirven para comunicar el modelo de forma clara y sencilla.
    </p>
    <p>
      
    </p>
    <p>
      Están diseñados para aprovechar las capacidades externas. Por lo tanto, dibujar una red de valor identificará a los jugadores clave y cómo contribuyen al valor dentro de la empresa central. A pesar de muchos actores, las redes de valor pueden ayudar a que los servicios operen con la eficiencia de una empresa independiente, operando sobre una base de proceso en lugar de una organización. La empresa central es el punto central de ejecución, en lugar de un actor en una cadena, y es responsable de toda la red de valor. Esto incluye la infraestructura mediante la cual otros socios comerciales pueden colaborar para entregar bienes y servicios.
    </p>
    <p>
      
    </p>
    <p>
      En primer lugar, considere las expectativas del cliente. Solo entonces considere los recursos y las capacidades necesarias para prestar los servicios. Este modelo requiere flujos de información de alto rendimiento, no cadenas de suministro rígidas. No hace mucho tiempo, los empleados de la empresa eran los únicos consumidores de sus servicios de TI. Los ejemplos omnipresentes de banca ATM, quioscos de aeropuerto y sistemas de reserva en línea ilustran que ya no es el caso. Servicios colaborativos como Wikipedia, Youtube, y Second life sugieren incremento en los niveles de sofisticación en la interacciones con los clientes. Como los clientes y proveedores llegan a ser usuarios directos de los servicios TI, las espectativas y requerimientos son más demandantes - requiriendo un enfoque de red de valor.
    </p>
  </body>
</html></richcontent>
<node TEXT="Figura 3.47 diagrama de flujo existente de como la mesa de servicio se supone que trabaja.png" ID="ID_249988939" CREATED="1659490928952" MODIFIED="1659490932081" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.47%20diagrama%20de%20flujo%20existente%20de%20como%20la%20mesa%20de%20servicio%20se%20supone%20que%20trabaja.png" SIZE="0.7653061" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 3.48 Intercambio de valor neto muestra como las cosas realmente trabajan.png" ID="ID_258419077" CREATED="1659490986250" MODIFIED="1659490989182" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%203.48%20Intercambio%20de%20valor%20neto%20muestra%20como%20las%20cosas%20realmente%20trabajan.png" SIZE="0.8344923" NAME="ExternalObject"/>
</node>
<node TEXT="Caso de estudio: Mesa de servicio" ID="ID_703320953" CREATED="1659490522116" MODIFIED="1659490737402" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un proveedor de servicios interno de una unidad comercial de atención médica realizó una evaluación de su mesa de servicio. Se desarrolló un mapa del proceso de la mesa de servicio, como se muestra en la Figura 3.47. Este diagrama de flujo describía cómo funcionaba la función de la mesa de servicio. Si bien el diagrama de flujo parecía ordenado, la experiencia del personal no coincidía con el flujo documentado. Posteriormente se realizó un análisis de valor neto.
    </p>
    <p>
      
    </p>
    <p>
      El personal describió procesos informales utilizados para maniobrar alrededor de las restricciones del modelo de proceso. Los procesos informales eran necesarios para ser efectivos. Como era de esperar, los recién llegados al personal tardaron más en ser efectivos a medida que aprendían estas formas no documentadas de hacer las cosas.
    </p>
    <p>
      
    </p>
    <p>
      El análisis alejó el enfoque de la representación lineal del proceso. Más bien, se centró en las personas que cumplían diferentes roles. Se hizo evidente que los pasos simples en el diagrama de flujo eran en realidad complejos. Involucraron a varios miembros del personal y requirieron actividades continuas a lo largo de todo el proceso (Figura 3.48).
    </p>
    <p>
      
    </p>
    <p>
      El valor neto parecía desordenado. Pero el personal estuvo de acuerdo en que describía con precisión cómo funcionaba realmente la mesa de servicio. El análisis capturó los intangibles por los cuales el personal era responsable pero no se reflejaron en el diagrama de flujo.
    </p>
    <p>
      
    </p>
    <p>
      El objetivo no era reemplazar el modelado de procesos o mapear toda la organización. El método se utilizó para describir un proceso no lineal complejo que había sido forzado artificialmente en el diagrama de flujo lineal.
    </p>
    <p>
      
    </p>
    <p>
      Como demuestran las Figuras 3.47 y 3.48 (ambas adaptadas de Allee, 2003), lo que parece un proceso simple en realidad es a menudo un conjunto de interacciones mucho más complejo.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Análisis de la dinámica del modelo de servicio en una red de valor" ID="ID_390653793" CREATED="1659491053774" MODIFIED="1659491377178" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las siguientes preguntas son útiles en la construcción y análisis de las dinámicas de un modelo de servicio en una red de valor:
    </p>
    <ul>
      <li>
        ¿Quiénes son todos los participantes en el servicio?
      </li>
      <li>
        ¿Cuáles son los patrones globales de intercambio o transacciones?
      </li>
      <li>
        ¿Cuáles son los impactos o entregables de cada transacción o cada participante?
      </li>
      <li>
        ¿Cuál es la mejor forma de generar valor?
      </li>
    </ul>
    <p>
      Los diagramas de valor neto son herramientas para el análisis de servicio, en lugar de los diagramas de flujo para instrucciones de trabajo. Ellos muestran que hace la organización, como lo hace y quien lo hace. Ellos pueden ayudar a simplificar la manera que la organización trabaja, haciéndola más eficiente. No deben ser demasiados complejos para ser útiles.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="3.9 Entradas y salidas de la estrategia de servicio" FOLDED="true" ID="ID_716956936" CREATED="1658191574227" MODIFIED="1659491530113" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las principales salidas desde la estrategia de servicios son la visión y la misión, estrategias y planes estratégicos, el portafolio de servicio, las propuestas de cambio y la información financiera. La tabla 3.21 muestra las principales entradas y salidas de la estrategia de servicio. El apéndice F provee un resumen de las principales entradas y salidas entre cada fase y cada fase del ciclo de vida del servicio.
    </p>
  </body>
</html></richcontent>
<node TEXT="Tabla 3.21 Entradas y salidas de la Estrategia del Servicio durante cada fase del ciclo de vida del servicio." ID="ID_620550208" CREATED="1659491532014" MODIFIED="1659492595849" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 600px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <th valign="top" style="width: 10%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Fase del ciclo de vida
          </p>
        </th>
        <th valign="top" style="width: 45%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Entradas en la Estrategia de Servicio (desde la fase del ciclo de vida indicado en la primera columna)
          </p>
        </th>
        <th valign="top" style="width: 45%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Salidas de la Estrategia de Servicio (hacia la fase del ciclo de vida indicado en la primera columna)
          </p>
        </th>
      </tr>
      <tr>
        <td valign="top" style="width: 10%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Diseño del servicio
          </p>
        </td>
        <td valign="top" style="width: 45%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Datos de entrada para los casos de negocio y la cartera de servicios
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Paquetes de diseño de servicios
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Modelos de servicios actualizados
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Actualizaciones de la cartera de servicios, incluido el catálogo de servicios
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Estimaciones e informes financieros
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Conocimiento e información relacionados con el diseño en el sistema de gestión del conocimiento de servicios (SKM)
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Diseños para procesos y procedimientos de estrategia de servicios
          </p>
        </td>
        <td valign="top" style="width: 45%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Visión y misión
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Portafolio de servicios.
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Políticas Estrategias y planes estratégicos Prioridades
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Cartas de servicio, incluidos paquetes de servicios y detalles de utilidad y garantía
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Información financiera y presupuestos
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Patrones documentados de actividad comercial y perfiles de usuario
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Modelos de servicio
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 10%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Transición del servicio
          </p>
        </td>
        <td valign="top" style="width: 45%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Servicios en transición
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Información y retroalimentación para casos de negocios y cartera de servicios
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Respuesta a propuestas de cambio
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Actualizaciones de la cartera de servicios
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Calendario de cambios
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Retroalimentación sobre estrategias y políticas
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Información financiera para entrada a presupuestos Informes financieros
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Conocimiento e información en el SKMS
          </p>
        </td>
        <td valign="top" style="width: 45%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Visión y misión
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Portafolio de servicios
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Políticas
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Estrategias y planes estratégicos
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Prioridades
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Propuestas de cambio, incluidos requisitos de utilidad y garantía y escalas de tiempo esperadas
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Información financiera y presupuestos
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Información para la evaluación del cambio y reuniones de la junta asesora del cambio (CAB)
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 10%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Operación del servicio
          </p>
        </td>
        <td valign="top" style="width: 45%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Riesgos de operación
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Información de costos operativos para el cálculo del costo total de propiedad (TCO)
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Datos de rendimiento actual
          </p>
        </td>
        <td valign="top" style="width: 45%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Visión y misión
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Cartera de servicios
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Políticas Estrategias y planes estratégicos
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Prioridades
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Información financiera y presupuestos
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Pronósticos y estrategias de demanda
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Riesgos estratégicos
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 10%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Mejora continua del servicio
          </p>
        </td>
        <td valign="top" style="width: 45%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Resultados de las encuestas de satisfacción de clientes y usuarios
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Información para los casos comerciales y la cartera de servicios
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Comentarios sobre estrategias y políticas
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Información financiera sobre iniciativas de mejora para información sobre presupuestos
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Datos necesarios para métricas, clave indicadores de desempeño (KPls) y factores críticos de éxito (CSFs)
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Reportes de servicio
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Solicitudes de cambio (RFCs) para implementar mejoras
          </p>
        </td>
        <td valign="top" style="width: 45%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Visión y misión
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Cartera de servicios
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Políticas
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Estrategias y planes estratégicos
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Prioridades
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Información financiera y presupuestos
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Patrones de actividad comercial
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Logros contra métricas, KPIs y CSFs
          </p>
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            Oportunidades de mejora registradas en el registro CSI
          </p>
        </td>
      </tr>
    </table>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="4. Proceso de estrategia de servicio" ID="ID_1195792350" CREATED="1658191628603" MODIFIED="1659984515053" TEXT_SHORTENED="true">
<arrowlink DESTINATION="ID_1164680760"/>
<richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Este capítulo establece los procesos y actividades de los que depende una estrategia de servicio eficaz. Estos comprenden tanto los procesos del ciclo de vida como los contenidos casi en su totalidad dentro de la estrategia de servicio. Cada uno se describe en detalle, estableciendo los elementos clave de ese proceso o actividad.
    </p>
    <p>
      
    </p>
    <p>
      Los procesos y actividades específicamente tratados en este capítulo son:
    </p>
    <ul>
      <li>
        Gestión de estrategia para servicios de TI
      </li>
      <li>
        Gestión de cartera de servicios
      </li>
      <li>
        Gestión financiera para servicios de TI
      </li>
      <li>
        Gestión de la demanda
      </li>
      <li>
        Gestión de relaciones del negocio.
      </li>
    </ul>
    <p>
      La mayoría de estos procesos se utilizan a lo largo del ciclo de vida del servicio, pero se abordan en la estrategia de servicio ITIL, ya que son fundamentales para una estrategia de servicio eficaz.
    </p>
    <p>
      
    </p>
    <p>
      El objeto y alcance de la estrategia de servicio en su conjunto se recogen en el apartado 1.1.
    </p>
  </body>
</html></richcontent>
<node TEXT="Figura 4.1 Estrategia general del negocio y las estrategias de las unidades de negocio.png" ID="ID_1871583475" CREATED="1660079279865" MODIFIED="1660079290238" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%204.1%20Estrategia%20general%20del%20negocio%20y%20las%20estrategias%20de%20las%20unidades%20de%20negocio.png" SIZE="0.8152174" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 4.2 El alcance de la gestión estratégica.png" ID="ID_224429163" CREATED="1660153029006" MODIFIED="1660153043563" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%204.2%20El%20alcance%20de%20la%20gestión%20estratégica.png" SIZE="0.84033614" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 4.3 Proceso de gestión estratégica.png" ID="ID_1504045539" CREATED="1660163302126" MODIFIED="1660163305739" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%204.3%20Proceso%20de%20gestión%20estratégica.png" SIZE="0.7585335" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 4.4 Factores estratégicos de la industria y posiciones competitivas en los campos de juego.png" ID="ID_1045578398" CREATED="1660179106231" MODIFIED="1660179110112" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%204.4%20Factores%20estratégicos%20de%20la%20industria%20y%20posiciones%20competitivas%20en%20los%20campos%20de%20juego.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 4.5 Análisis estratégico de la cartera de clientes.png" ID="ID_498645641" CREATED="1660179146217" MODIFIED="1660179150961" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%204.5%20Análisis%20estratégico%20de%20la%20cartera%20de%20clientes.png" SIZE="0.927357" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 4.6 Opciones estratégicas para el proveedor de servicio.png" ID="ID_66888803" CREATED="1660179415950" MODIFIED="1660179418940" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%204.6%20Opciones%20estratégicas%20para%20el%20proveedor%20de%20servicio.png" SIZE="0.8797654" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 4.7 Posicionamiento basado en variedad (izq) y basado en las necesidades (der).png" ID="ID_274117834" CREATED="1660224725479" MODIFIED="1660224728973" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%204.7%20Posicionamiento%20basado%20en%20variedad%20(izq)%20y%20basado%20en%20las%20necesidades%20(der).png" SIZE="0.8298755" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 4.8 Posicionamiento basado en el acceso.png" ID="ID_1860531262" CREATED="1660224760666" MODIFIED="1660224763750" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%204.8%20Posicionamiento%20basado%20en%20el%20acceso.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 4.9 Posicionamiento combinado basado en la variedad, necesidades y acceso.png" ID="ID_1964648216" CREATED="1660224837591" MODIFIED="1660224845569" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%204.9%20Posicionamiento%20combinado%20basado%20en%20la%20variedad,%20necesidades%20y%20acceso.png" SIZE="0.933126" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 4.10 Factores críticos del éxito apalancados a traves de los espacios de mercado.png" ID="ID_1186269155" CREATED="1660227056969" MODIFIED="1660227071623" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%204.10%20Factores%20críticos%20del%20éxito%20apalancados%20a%20traves%20de%20los%20espacios%20de%20mercado.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 4.11 Priorización de inversiones estratégicas basado en las necesidades del cliente.png" ID="ID_1483946072" CREATED="1660791174471" MODIFIED="1660791179496" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%204.11%20Priorización%20de%20inversiones%20estratégicas%20basado%20en%20las%20necesidades%20del%20cliente.png" SIZE="0.8902077" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 4.12 Expansión hacia espacios de mercado adyacentes.png" ID="ID_1105236452" CREATED="1660792799783" MODIFIED="1660792810781" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%204.12%20Expansión%20hacia%20espacios%20de%20mercado%20adyacentes.png" SIZE="0.7692308" NAME="ExternalObject"/>
</node>
<node TEXT="Figura 4.13 Expansión dentro de un solo cliente y espacios de mercado.png" ID="ID_1780505322" CREATED="1660792890607" MODIFIED="1660792897612" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Figura%204.13%20Expansión%20dentro%20de%20un%20solo%20cliente%20y%20espacios%20de%20mercado.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 4.1 Preguntas para analizar los servicios existentes como diferenciadores.png" ID="ID_1898767658" CREATED="1660166641998" MODIFIED="1660166645773" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Tabla%204.1%20Preguntas%20para%20analizar%20los%20servicios%20existentes%20como%20diferenciadores.png" SIZE="0.7653061" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 4.2 Entrada de datos de cliente para la creación de objetivos.png" ID="ID_422956472" CREATED="1660180201864" MODIFIED="1660228308640" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Tabla%204.2%20Entrada%20de%20datos%20de%20cliente%20para%20la%20creación%20de%20objetivos.png" SIZE="0.7633588" NAME="ExternalObject"/>
</node>
<node TEXT="Tabla 4.3 Ejemplos de patrones de gestión de servicio.png" ID="ID_1760486573" CREATED="1660228303227" MODIFIED="1660228309740" TEXT_SHORTENED="true">
<hook URI="ITIL2011_files/Tabla%204.3%20Ejemplos%20de%20patrones%20de%20gestión%20de%20servicio.png" SIZE="0.7633588" NAME="ExternalObject"/>
</node>
<node TEXT="4.1 Gestión estratégica para servicios TI" FOLDED="true" ID="ID_1342850842" CREATED="1659984533675" MODIFIED="1659984760316" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Esta sección describe un proceso para gestión de la estrategia para una empresa y muestra como es aplicada para gestionar una estrategia para servicios TI. Aunque los altos ejecutivos participan a este nivel de gestión estratégica, la mayoría de organizaciones TI usan este proceso para gestionar una estrategia de servicio la cual forma parte de la estrategia de la empresa general. La sección 4.1.5 se enfocará en describir el proceso genérico de gestión estratégica, y la sección 4.1.5.20 explicará como esta es aplicada dentro de los proveedores de servicios internos.
    </p>
  </body>
</html></richcontent>
<node TEXT="4.1.1 Propósito y objetivos" ID="ID_154346803" CREATED="1659984763048" MODIFIED="1660077205796" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La gestión de la estrategia para los servicios de TI es el proceso de definir y mantener la perspectiva, posición, planes y patrones de una organización con respecto a sus servicios y la gestión de esos servicios. El propósito de una estrategia de servicio es articular cómo un proveedor de servicios permitirá que una organización logre sus resultados comerciales; establece los criterios y mecanismos para decidir qué servicios serán los más adecuados para cumplir con los resultados del negocio y los más efectivos y la manera más eficiente para gestionar esos servicios. La gestión estratégica para los servicios TI es el procesos que asegura que la estrategia está definida, mantenida y alcanza su propósito.
    </p>
  </body>
</html></richcontent>
<node TEXT="Objetivos de la Gestión estratégica de TI" ID="ID_803189485" CREATED="1660077176166" MODIFIED="1660077203784" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los objetivos de la gestión estratégica para los servicios de TI son:
    </p>
    <ul>
      <li>
        Analizar los entornos internos y externos en los que se encuentra el proveedor de servicios, para identificar oportunidades que beneficiarán a la organización.
      </li>
      <li>
        Identificar las restricciones que podrían impedir el logro de los resultados comerciales, la prestación de servicios o la gestión de servicios; y definir cómo podrían eliminarse esas restricciones o reducirse sus efectos.
      </li>
      <li>
        Acordar la perspectiva del proveedor de servicios y revisar periódicamente para garantizar la pertinencia continua. Esto dará como resultado una declaración clara de la visión y misión del proveedor de servicios.
      </li>
      <li>
        Establecer la posición del proveedor de servicios en relación con sus clientes y otros proveedores de servicios. Esto incluye definir qué servicios se entregarán a qué espacios de mercado y cómo mantener una ventaja competitiva.
      </li>
      <li>
        Producir y mantener documentos de planificación de la estrategia y asegurarse de que todas las partes interesadas relevantes tengan copias actualizadas de los documentos apropiados. Esto incluirá la estrategia de TI, la estrategia de gestión de servicios y los planes de estrategia para cada servicio cuando corresponda.
      </li>
      <li>
        Asegúrese de que los planes estratégicos se hayan traducido en planes tácticos y operativos para cada unidad organizacional que se espera que cumpla con la estrategia.
      </li>
      <li>
        Administre los cambios en las estrategias y documentos relacionados, asegurándose de que las estrategias sigan el ritmo de los cambios en los entornos internos y externos.
      </li>
    </ul>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="4.1.2 Alcance" FOLDED="true" ID="ID_1186203572" CREATED="1660077211775" MODIFIED="1660079211398" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La gestión de la estrategia es responsabilidad de los ejecutivos de una organización. Les permite establecer los objetivos de la organización, especificar cómo la organización cumplirá esos objetivos y priorizar las inversiones necesarias para alcanzarlos. Sin embargo, en organizaciones medianas y grandes es poco probable que los propios ejecutivos realicen las evaluaciones, redacten los documentos de estrategia y gestionen la ejecución. Esto normalmente lo realiza un gerente de estrategia y planificación dedicado que reporta directamente a la junta directiva.
    </p>
    <p>
      
    </p>
    <p>
      La estrategia de una organización no se limita a un solo documento o departamento. La estrategia general de una organización se dividirá en una estrategia para cada unidad de negocio. La Figura 4.1 da un ejemplo de cómo una estrategia de negocios puede dividirse en estrategias para TI y para manufactura. Es probable que haya varias estrategias dentro de cada organización. La gestión de la estrategia para la empresa debe garantizar que todos estos estén vinculados y sean coherentes entre sí. La gestión de la estrategia para los servicios de TI tiene que garantizar que los servicios y la forma en que se gestionan respalden la estrategia general de la empresa.
    </p>
    <p>
      
    </p>
    <p>
      La gestión de la estrategia se describe anteriormente como un proceso genérico que podría aplicarse al negocio entero, o a cualquier unidad de negocio. Sin embargo, esta publicación se ocupa específicamente de cómo se aplica este proceso a TI como proveedor de servicios. Tenga en cuenta que en un proveedor de servicios externo, la estrategia empresarial puede estar relacionada con los servicios de TI prestados a un cliente externo, y la estrategia de TI estaría relacionada con la forma en que esos servicios se prestarán y recibirán soporte. Al mismo tiempo, los proveedores de servicios externos no solo brindan servicios de TI a los clientes. También son consumidores de sus propios servicios de TI (y potencialmente de otros terceros). Los proveedores de servicios externos también tienen requisitos de servicios de TI internos que deben cumplirse para permitirles sobrevivir.
    </p>
  </body>
</html></richcontent>
<node TEXT="Mensaje clave" ID="ID_831379290" CREATED="1660079155813" MODIFIED="1660079184935" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una estrategia de servicio es un subconjunto de la estrategia general de la organización. En el caso de una organización de TI, la estrategia de TI abarcará la estrategia de servicio de TI.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Descripción" FOLDED="true" ID="ID_42345438" CREATED="1660152497345" MODIFIED="1660152959012" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El alcance de la gestión de la estrategia en ITIL Service Strategy se ilustra en la Figura 4.2. Este diagrama muestra cómo se utiliza una estrategia empresarial para desarrollar un conjunto de tácticas (enfoques, procesos y técnicas detallados que se utilizarán para alcanzar los objetivos estratégicos) y operaciones (procedimientos, tecnologías y actividades específicos que ejecutarán personas y equipos). La estrategia de TI (y por lo tanto también la estrategia para los servicios de TI) se deriva de la estrategia comercial, pero también proporciona la validación de la estrategia comercial. La estrategia de TI puede determinar si un objetivo estratégico es tecnológicamente posible y qué nivel de inversión sería necesario para alcanzar ese objetivo. Entonces, la empresa puede decidir sobre si el objetivo debe incluirse y con qué prioridad.
    </p>
    <p>
      
    </p>
    <p>
      Las tácticas de TI están determinadas en parte por la estrategia de TI, pero también por las tácticas comerciales. Por ejemplo, si una táctica comercial requiere el cumplimiento de una regulación o estándar, TI deberá asegurarse de que sus tácticas lo respalden. Si no lo hacen, la empresa y TI pueden decidir qué nivel de inversión se requiere para abordar la situación. Las tácticas de TI también pueden ayudar a la empresa a determinar si sus tácticas son adecuadas. Por ejemplo, definir cómo funcionará un equipo de ventas dependerá en parte del tipo de servicios de automatización de ventas que proporcione TI. Nuevamente, si los límites impuestos por TI son demasiado rígidos, entonces el negocio y TI pueden investigar qué nivel de inversión resolvería la situación, y si esto es apropiado.
    </p>
    <p>
      
    </p>
    <p>
      Las operaciones de TI se derivan de las tácticas de TI, pero también de los requisitos de las operaciones comerciales. La forma en que se coordinan los diferentes entornos operativos y cómo interactúan es muy importante para la gestión de la estrategia de los servicios de TI. Solo una vez que se ha ejecutado una estrategia, se puede validar. La evaluación del desempeño real de las actividades y los servicios puede indicar si los parámetros utilizados para establecer la estrategia fueron precisos y también puede validar cualquier suposición realizada.
    </p>
    <p>
      
    </p>
    <p>
      También en la Figura 4.2, la estrategia de TI está relacionada (usando una línea punteada) con las tácticas comerciales. Dado que tanto la estrategia de TI como las tácticas comerciales se derivan de la estrategia comercial, deben verificarse para garantizar la coherencia. TI no debe definir una estrategia que choque con las tácticas comerciales. Además, las tácticas comerciales no deben tomar una decisión táctica sobre cómo se utilizarán los servicios de TI si la estrategia de TI no permite ese tipo de uso.
    </p>
    <p>
      
    </p>
    <p>
      Existe una relación similar entre la operación comercial y las tácticas de TI. Dado que TI existe para respaldar el negocio, es importante que cualquier táctica que implementen sea válida para la operación comercial.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Diferencias entre estrategia de servicio y estrategia ITSM" ID="ID_1927898148" CREATED="1660152924815" MODIFIED="1660152955357" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Es importante señalar, además, que una estrategia de servicio no es lo mismo que una estrategia de ITSM, que en realidad es un plan táctico. La diferencia se puede resumir de la siguiente manera:
    </p>
    <ul>
      <li>
        <b>Estrategia de servicio</b>&nbsp;La estrategia que seguirá un proveedor de servicios para definir y ejecutar servicios que satisfagan los objetivos comerciales de un cliente. Para un proveedor de servicios de TI, la estrategia de servicio es un subconjunto de la estrategia de TI.
      </li>
      <li>
        <b>Estrategia de gestión de servicios (ITSM)</b>&nbsp;El plan para identificar, implementar y ejecutar los procesos utilizados para gestionar los servicios identificados en una estrategia de servicios. En un proveedor de servicios de TI, la estrategia de ITSM será un subconjunto de la estrategia de servicio.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="4.1.3 Valor al negocio" ID="ID_968250052" CREATED="1660152970917" MODIFIED="1660162082711" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estrategia de una organización articula sus objetivos y define cómo cumplirá esos objetivos y cómo sabrá que los ha cumplido. Sin una estrategia, la organización solo podrá reaccionar a las demandas de los diversos interesados, con poca capacidad para evaluar cada demanda y cómo afectará a la organización. En estos casos, las acciones de las organizaciones tienden a ser lideradas por quien más exige, y no por lo que es mejor para la organización. La estrategia se convierte en una función de la política organizacional y el interés propio, en lugar del logro general de sus objetivos.
    </p>
    <p>
      
    </p>
    <p>
      Una estrategia bien definida y administrada garantiza que los recursos y las capacidades de la organización estén alineados para lograr sus resultados comerciales y que las inversiones coincidan con el desarrollo y el crecimiento previstos por la organización.
    </p>
    <p>
      
    </p>
    <p>
      La gestión de la estrategia asegura que todas las partes interesadas estén representadas al decidir la dirección apropiada para la organización y que todos estén de acuerdo en sus objetivos y los medios por los cuales se priorizan los recursos, las capacidades y la inversión. La gestión de la estrategia también garantiza que los recursos, las capacidades y las inversiones se gestionen adecuadamente para lograr la estrategia.
    </p>
    <p>
      
    </p>
    <p>
      Para un proveedor de servicios, la gestión de la estrategia para los servicios de TI garantiza que tenga el conjunto adecuado de servicios en su cartera de servicios, que todos sus servicios tengan un propósito claro y que todos en la organización del proveedor de servicios conozcan su papel logrando ese propósito. La gestión de estrategias para los servicios de TI fomenta aún más los niveles apropiados de inversión, lo que resultará en uno o más de los siguientes:
    </p>
    <ul>
      <li>
        Ahorro de costes, ya que las inversiones y los gastos se ajustan a la consecución de objetivos empresariales validados, en lugar de demandas sin fundamento
      </li>
      <li>
        Mayores niveles de inversión para proyectos clave o mejoras de servicios
      </li>
      <li>
        Cambios en las prioridades de inversión. El proveedor de servicios podrá descentrar la atención de un servicio y volver a enfocarse en otro, asegurándose de que sus esfuerzos y presupuesto se gasten en las áreas con el mayor nivel de impacto comercial.
      </li>
    </ul>
    <p>
      Para el cliente del proveedor de servicios, la gestión de la estrategia para los servicios de TI les permite articular claramente sus prioridades comerciales de una manera comprensible para el proveedor de servicios. El proveedor de servicios puede entonces tomar una decisión sobre cómo responder al cliente. En algunos casos, la demanda del cliente representa una desviación de la estrategia del proveedor de servicios. El proveedor de servicios utilizará la gestión de la estrategia para los servicios de TI para tomar una decisión sobre si cambiar su estrategia o si rechazar el negocio. Cuando el proveedor de servicios es una organización de TI interna, la segunda opción no siempre es posible y, en estos casos, utilizará la gestión de estrategias para que los servicios de TI trabajen con las unidades de negocio para que sean conscientes del impacto de su demanda en la estrategia actual. Los ejecutivos de negocios podrán trabajar con TI para cambiar la estrategia existente o rechazar la oportunidad.
    </p>
    <p>
      
    </p>
    <p>
      En otros casos, las demandas de los clientes no cambian la estrategia del proveedor de servicios, pero requerirán que cambie sus prioridades. La gestión de estrategias para servicios de TI permite al proveedor de servicios determinar la mejor manera de cambiar sus prioridades y equilibrar sus recursos, capacidades e inversiones.
    </p>
    <p>
      
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Nota en estrategia de servicios" ID="ID_269838513" CREATED="1660162091232" MODIFIED="1660162218250" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La gestión de estrategia para servicios de TI está destinada a gestionar la estrategia de un proveedor de servicios. Incluirá una especificación del tipo de servicios que brindará, los clientes de esos servicios y los resultados comerciales generales que se lograrán cuando el proveedor de servicios ejecute la estrategia. La estrategia de servicios de TI es un subconjunto de la estrategia de TI que, además de la estrategia de servicios de TI, incluye estrategias para la arquitectura de TI, la gestión de carteras (que no sean servicios), la gestión de aplicaciones, la gestión de infraestructuras, la gestión de proyectos, la dirección tecnológica, etc.
    </p>
    <p>
      
    </p>
    <p>
      La estrategia de un servicio individual se define durante el proceso de gestión de la cartera de servicios y se documenta en la cartera de servicios. Esto incluirá una descripción de los resultados comerciales específicos que respaldará el servicio y también definirá cómo se entregará el servicio. La información de la cartera de servicios (ver sección 4.2) se utilizará como insumo en el proceso de definición de la estrategia del proveedor de servicios.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.4 Políticas, principios y conceptos básicos" FOLDED="true" ID="ID_483535448" CREATED="1660162221941" MODIFIED="1660162789980" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La estrategia ha sido definida y explicada en detalle en el capítulo 3. Esta sección define un modelo genérico para definir, ejecutar y medir la estrategia de servicio. Esto puede ser aplicado desde el nivel más ejecutivo de la organización, o como una estrategia para una parte de la organización.
    </p>
    <p>
      
    </p>
    <p>
      Como se ha dicho con frecuencia, la estrategia de una organización es definida por sus ejecutivos. Si la organización es un proveedor de servicios externo con un negocio principal de prestación de servicios, la estrategia de servicio será el componente central de la estrategia de la organización.
    </p>
    <p>
      
    </p>
    <p>
      En los proveedores de servicios internos, la estrategia de servicios respaldará la estrategia empresarial general y proporcionará un plan táctico para el proveedor de servicios internos. En otras palabras, si bien la estrategia de TI es estratégica para la organización de TI, la junta directiva consideraría que es más táctico desde su punto de vista. Por lo tanto, es vital que la estrategia de TI se defina en términos de la estrategia general de la organización.
    </p>
    <p>
      
    </p>
    <p>
      Si la estrategia de TI no puede respaldar la estrategia general de la organización (por ejemplo, debido a la falta de fondos), esto debe comunicarse al liderazgo senior de la organización, que deberá cambiar la estrategia de la organización o hacer que los fondos necesarios estén disponibles.
    </p>
    <p>
      
    </p>
    <p>
      El capítulo 5 contiene una discusión sobre la gobernanza y el rol de los altos ejecutivos en la definición y ejecución de la estrategia.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Caso de estudio: Gestión estratégica en acción" ID="ID_1364955203" CREATED="1660162795141" MODIFIED="1660162962321" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un departamento gubernamental constaba de cinco agencias, cada una con una misión, servicios, aplicaciones e infraestructura diferentes. Se esperaba que la oficina central proporcionara servicios compartidos para todas las agencias, pero las inversiones generalmente se priorizaban para la agencia que tenía un exceso de presupuesto o aquellas que estaban experimentando las interrupciones más graves. Ninguna de las agencias quedó satisfecha con los servicios de la oficina central, ya que las agencias que recibieron inversiones pensaron que eran reactivas, mientras que las otras agencias pensaron que no respondieron. El personal de la oficina central siempre estaba severamente sobrecargado de trabajo mientras trataban de mantenerse al día con las prioridades siempre cambiantes.
    </p>
    <p>
      
    </p>
    <p>
      El CIO creó un grupo de dirección con el gerente de TI de cada agencia, que se reunió semanalmente para definir el rol y los objetivos de la oficina central y acordar prioridades para las inversiones. Las agencias pudieron establecer sus objetivos y requisitos, y priorizar sus demandas entre sí, y la oficina central pudo definir una estrategia clara y mantenerla actualizada con los cambios en las agencias.
    </p>
    <p>
      
    </p>
    <p>
      El resultado final fue un aumento significativo en la satisfacción del cliente, junto con ahorros sustanciales en los costos y menos interrupciones en los sistemas críticos de las agencias.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="4.1.4.1 Nota de advertencia" ID="ID_671244780" CREATED="1660162972532" MODIFIED="1660163173055" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La gestión de la estrategia no debe tomarse a la ligera. El simple hecho de lanzarse a un ejercicio de estrategia para toda la organización, donde antes se ha implementado poco, dará como resultado rápidamente que el equipo se vea inundado de datos e información, antes de que puedan producir algo que pueda ser aprobado por la alta dirección.
    </p>
    <p>
      
    </p>
    <p>
      Los equipos que serán esenciales para generar y ejecutar la estrategia pueden dudar en apoyar la iniciativa, especialmente si perciben que los afectará negativamente.
    </p>
    <p>
      
    </p>
    <p>
      Es crucial que la estrategia tenga el alcance correcto la primera vez que se utiliza este proceso, y es aún más importante que se vea que los altos ejecutivos están detrás de la iniciativa. Existe la necesidad de un progreso incremental para que una organización llegue al punto de abordar la estrategia de servicio de la manera integral descrita en esta sección.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5 Actividades, métodos y técnicas de Procesos" FOLDED="true" ID="ID_377179759" CREATED="1660163181548" MODIFIED="1660163245758" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El proceso para la gestión estratégica es mostrado en la figura 4.3
    </p>
  </body>
</html>
</richcontent>
<node TEXT="4.1.5.1 Análisis estratégico" ID="ID_451321636" CREATED="1660163252884" MODIFIED="1660165824716" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Ninguna organización existe de forma aislada, y cada organización se define por la forma en que interactúa con su entorno en constante cambio. El propósito de la evaluación estratégica es determinar la situación actual del proveedor de servicios y qué cambios pueden afectarlo en el futuro previsible. La evaluación también destacará las restricciones que limitarán o impedirán que el proveedor de servicios pueda avanzar en sus objetivos actuales o adaptarse al cambio.
    </p>
    <p>
      
    </p>
    <p>
      La evaluación estratégica analiza tanto el entorno interno (la propia organización del proveedor de servicios) como el entorno externo (el mundo con el que interactúa la organización del proveedor de servicios), y luego llega a un conjunto de objetivos que se utilizarán para definir la estrategia real.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="4.1.5.2 Análisis estratégico: analizar el ambiente interno" FOLDED="true" ID="ID_1841862410" CREATED="1660165839705" MODIFIED="1660167033643" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Al diseñar una estrategia de servicio, un proveedor primero debe observar detenidamente lo que ya hace. El punto de partida es identificar las fortalezas y debilidades del proveedor de servicios a través de un análisis interno. Esta información ayudará a definir la estrategia al identificar qué fortalezas se pueden aprovechar y qué debilidades se deben fortalecer. Aunque el análisis interno a veces se ha reducido a una sesión de lluvia de ideas entre los altos directivos, debe ser una actividad consciente basada en una evaluación cuidadosa de la organización durante un período de tiempo.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Las categorías típicas de análisis de fortalezas y debilidades incluyen:" ID="ID_1591335653" CREATED="1660167035242" MODIFIED="1660167055565" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        <b>Servicios existentes</b>&nbsp;Es probable que el proveedor de servicios ya tenga una base para diferenciarse, aunque los proveedores de servicios establecidos a menudo no reconocen sus propios diferenciadores únicos. Las preguntas en la Tabla 4.1 pueden ayudar a identificar las capacidades distintivas y competencias básicas de un proveedor de servicios.
      </li>
      <li>
        <b>Análisis financiero</b>&nbsp;Esto indicará el costo de los servicios y el retorno de la inversión en el proveedor del servicio. La evaluación financiera deberá tener en cuenta cómo el proveedor de servicios contribuye al logro de los resultados comerciales. Se debe tener cuidado aquí porque algunas grandes inversiones tardarán en generar un rendimiento. Esto no es necesariamente una debilidad, ya que la inversión en realidad puede haber resultado en un diferenciador significativo para el proveedor de servicios y generará altos rendimientos en el futuro (consulte la sección 3.6.1).
      </li>
      <li>
        <b>Recursos humanos</b>&nbsp;Es importante saber qué habilidades y capacidades tiene el proveedor de servicios y de dónde provienen. Las habilidades que se contratan, pero que son críticas para el negocio, representan un riesgo significativo. Esta parte de la evaluación también se centrará en la calidad de la formación, la contratación, la gestión, la compensación, la planificación de la sucesión, las relaciones laborales (por ejemplo, el papel de los sindicatos).
      </li>
      <li>
        <b>Operaciones</b>&nbsp;Esta parte del análisis se enfoca en qué tan eficiente y efectiva es la organización para apoyar y brindar servicios, y cómo gestiona la tecnología en la que se basan esos servicios. Buscará cosas como la duplicación de esfuerzos o tecnología, el nivel de control, el impacto de los incidentes, la capacidad de gestionar cambios, etc.
      </li>
      <li>
        <b>Relación con las unidades de negocio</b>&nbsp;(para proveedores de servicios internos) Una buena estrategia requiere una buena comprensión de la estrategia general y requisitos del cliente (en este caso, clientes internos), y cómo el proveedor de servicios cumple actualmente con esos requisitos y permite esa estrategia.
      </li>
      <li>
        <b>Recursos y capacidades</b>&nbsp;que existen actualmente para proporcionar servicios, así como la forma en que se utilizan actualmente,
      </li>
      <li>
        <b>Proyectos existentes</b>&nbsp;Proyectos que cambiarán cualquiera de los aspectos anteriores de la organización.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5.3 Análisis estratégico: analizar el ambiente externo" FOLDED="true" ID="ID_1400434030" CREATED="1660167060504" MODIFIED="1660167836865" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Hay varias publicaciones que brindan orientación detallada sobre el análisis del entorno externo. Algunos de estos se mencionan al final de la sección titulada &quot;Referencias y lecturas adicionales&quot;. Esta sección actual resume los factores externos más importantes que debe considerar un proveedor de servicios. Mientras que el análisis interno se enfoca en analizar las fortalezas y debilidades, el análisis externo se enfoca en las oportunidades y amenazas, y especialmente en cómo se desarrollarán en el futuro. El objetivo de definir una estrategia será identificar qué oportunidades explotar y contra qué amenazas defenderse.
    </p>
    <p>
      
    </p>
    <p>
      La técnica utilizada aquí que analiza las relaciones de la organización para el caso de factores internos y externos es denominada FODA (Fortalezas, Oportunidades, Debilidades y Amenazas). Esto será discutido en detalle en la Mejora Continua del servicio ITIL.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Los factores externos incluyen:" ID="ID_457998694" CREATED="1660167171170" MODIFIED="1660167704330" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        <b>Análisis de la industria y el mercado</b>&nbsp;Esto se enfoca en las tendencias y prácticas en la industria del proveedor de servicios. Por ejemplo, ¿las organizaciones gastan más en un tipo particular de servicio o invierten en un nuevo tipo de tecnología? ¿Qué tipo de modelos de abastecimiento están utilizando (p. ej., 9. hay una tendencia en la subcontratación de tipos específicos de servicios?). ¿Qué tipo de métodos de gestión están utilizando?
      </li>
      <li>
        <b>Clientes</b>&nbsp;¿Quiénes son los clientes? ¿A qué desafíos y oportunidades se enfrentan? ¿Cuáles son sus estrategias? ¿Qué tan buena es la relación del proveedor de servicios con ellos? ¿Qué servicios están utilizando y por qué, y esto cambiará?
      </li>
      <li>
        <b>Proveedores</b>&nbsp;¿Quiénes son los proveedores? ¿Qué cambios prevén para sus productos y servicios? Cómo estos impactan las arquitecturas y servicios actuales del proveedor de servicios?
      </li>
      <li>
        <b>Socios</b>&nbsp;¿La organización está asociada con otras organizaciones? ¿Qué oportunidades y fortalezas ofrecen? ¿Siguen siendo relevantes? ¿Cuáles son las obligaciones y responsabilidades de la organización con respecto a estas asociaciones?
      </li>
      <li>
        <b>Competidores</b>&nbsp;¿Cómo se han diferenciado los competidores? ¿Han encontrado una forma más rentable de hacer negocios? ¿Ofrecen servicios de mayor calidad o de menor costo que nosotros? ¿Están ganando o perdiendo terreno en el mercado?
      </li>
      <li>
        <b>Legislación y regulación</b>&nbsp;¿Qué legislación o estándar afectará la forma en que trabajamos (por ejemplo, Sarbanes-Oxley (SOX), ISO/IEC 27001)? ¿Los competidores se enfrentan a las mismas limitaciones? ¿Podríamos usar nuestro cumplimiento con estos como diferenciadores?
      </li>
      <li>
        <b>Política</b>&nbsp;¿Cómo se ven afectadas nuestras prácticas y estrategias actuales por los cambios políticos? ¿Los cambios en la política fiscal mejorarán o limitarán nuestra capacidad para brindar servicios? ¿Cómo continuaremos (o continuaremos) ofreciendo servicios en áreas políticamente volátiles?
      </li>
      <li>
        <b>Socioeconómico</b>&nbsp;¿Cuál es el pronóstico económico y tendrá un impacto en nuestra situación actual? Por ejemplo, durante la recesión de 2009, las empresas se vieron sometidas a una gran presión para reducir el empleo, lo que obligó a los proveedores de servicios a seguir prestando servicios con menos personas. Esto dio como resultado una reducción en la cantidad de proyectos, mayores telecomunicaciones debido a las restricciones de viaje y una mayor automatización y dependencia de los proveedores debido a los recortes de personal. Además, ¿cuál es nuestra política y acciones con respecto a la responsabilidad social y cómo nos impactan los grupos de responsabilidad social?
      </li>
      <li>
        <b>Tecnología</b>&nbsp;¿Cómo cambiará la nueva tecnología los servicios prestados y cómo cambiará la forma en que se prestan los servicios? Es importante tener en cuenta que los cambios tecnológicos no solo afectan el entorno de TI. Muchos cambios de TI han llevado a cambios radicales y directos en la forma en que opera el negocio en sí. Por ejemplo, el uso del correo electrónico e Internet no son solo servicios proporcionados por un proveedor de servicios de TI, son un conjunto fundamental de herramientas comerciales. La empresa define cómo se utilizarán, a menudo sin una entrada por parte del proveedor de TI.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5.4 Análisis estratégico: definir los espacios de mercado" FOLDED="true" ID="ID_1095433981" CREATED="1660167839608" MODIFIED="1660169411975" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El concepto de espacios de mercado se describe en la sección 3.4. En resumen, los espacios de mercado definen oportunidades en las que un proveedor de servicios puede ofrecer valor a sus clientes. Identifican oportunidades haciendo coincidir los arquetipos de servicio con los activos del cliente.
    </p>
    <p>
      
    </p>
    <p>
      En la gestión de la estrategia, este paso da como resultado la documentación de todos los espacios de mercado actuales y cualquier espacio de mercado nuevo potencial que se identificó a partir del análisis interno y externo.
    </p>
    <p>
      
    </p>
    <p>
      Cuando se genera la estrategia, el proveedor de servicios usará esta información para decidir si continúa brindando servicios a los espacios de mercado existentes y, de ser así, si se necesitan cambios para garantizar la retención exitosa del espacio de mercado. Esto podría deberse a que los competidores pueden brindar los mismos servicios a un costo más bajo o con mayor calidad, o si están utilizando una nueva tecnología que cambia efectivamente la forma en que se atiende el espacio del mercado, lo que hace que el competidor sea más atractivo que el actual proveedor de servicio. Por ejemplo, la mayoría de las cadenas hoteleras han pasado del acceso a Internet por cable al inalámbrico, lo que permite a los huéspedes acceder al correo electrónico y las aplicaciones comerciales desde cualquier lugar del hotel. Dado que todas las computadoras portátiles están equipadas con tarjetas inalámbricas, esto significa que estas cadenas hoteleras serán más atractivas para los viajeros que necesitan acceso a Internet durante su estadía.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Exploración del potencial negocio" ID="ID_1337606081" CREATED="1660169414040" MODIFIED="1660169788268" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los proveedores de servicios pueden estar presentes en más de un espacio de mercado. Como parte de la planificación estratégica, los proveedores de servicios tanto internos como externos deben analizar su presencia en varios espacios del mercado. Las revisiones estratégicas incluyen el análisis de fortalezas, debilidades, oportunidades y amenazas en cada espacio de mercado. Los proveedores de servicios también analizan su potencial comercial en función de los espacios de mercado desatendidos o desatendidos.
    </p>
    <p>
      
    </p>
    <p>
      Este análisis identifica oportunidades con clientes actuales y potenciales. También prioriza las inversiones en activos de servicio basado en su potencial para servir a los espacios de mercado de interés. Por ejemplo, si un proveedor de servicio tiene fuertes capacidades y recursos en recuperación de servicios, este explorará todos esos espacios de mercado donde tales activos pueden entregar valor para los clientes.
    </p>
    <p>
      
    </p>
    <p>
      Comience con un amplio conjunto de resultados, como la productividad de los activos comerciales. Esto define un amplio espacio de mercado. Las necesidades de los clientes desatendidos y subatendidos se identifican dentro de este contexto y se aplica el enfoque en función de las fortalezas y oportunidades existentes. Esto define espacios de mercado más estrechos con especialización basada en las categorías de activos comerciales y la forma en que son respaldados por los servicios.
    </p>
    <p>
      
    </p>
    <p>
      Los proveedores deciden qué necesidades de los clientes se atienden de manera efectiva y eficiente a través de los servicios, mientras eligen atender ciertos espacios del mercado y evitar otros. Este aspecto esencial de la estrategia de servicio se desglosa en las siguientes decisiones. En primer lugar, identifique:
    </p>
    <ul>
      <li>
        Espacios de mercado que se atienden mejor con los activos de servicio existentes
      </li>
      <li>
        Espacios de mercado que se deben evitar con los activos de servicio existentes.
      </li>
    </ul>
    <p>
      Luego, para cada espacio de mercado que se atenderá, se toman decisiones con respecto a:
    </p>
    <ul>
      <li>
        Servicios a ofrecer (cartera de servicios — ver sección 4.2.4.1)
      </li>
      <li>
        Clientes a atender (cartera de clientes - ver sección 4.2.4.7)
      </li>
      <li>
        Factores críticos de éxito (CSFs)
      </li>
      <li>
        Modelos de servicio y activos de servicio
      </li>
      <li>
        Cadena de servicios y catálogo de servicios.
      </li>
    </ul>
    <p>
      El análisis del espacio de mercado para los proveedores de servicios internos sigue principios similares a los de los proveedores de servicios externos. Las diferencias están en términos de la medida en que las decisiones están influenciadas por:
    </p>
    <ul>
      <li>
        Prioridad y valor estratégico
      </li>
      <li>
        Inversiones requeridas
      </li>
      <li>
        Objetivos financieros (incluido el afán de lucro)
      </li>
      <li>
        Riesgos involucrados
      </li>
      <li>
        Restricciones políticas.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5.5 Análisis estratégico: Identificar los factores estratégicos de la industria" FOLDED="true" ID="ID_704097408" CREATED="1660169791161" MODIFIED="1660176852673" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Para cada espacio de mercado existen factores críticos que determinan el éxito o falla de una estrategia de servicio. En la literatura de negocio esos factores son llamados factores estratégicos de la industria (Amit and Schoemaker, 1993). Estos están influenciados por las necesidades de los clientes, las tendencias de negocio, competencia, ambiente regulatorio, proveedores, estándares, mejores prácticas de la industria y tecnologías.
    </p>
    <p>
      
    </p>
    <p>
      Por ejemplo, un proveedor de servicio quien desea proveer servicios TI a la industria de la salud en USA necesita cumplir con las complejas leyes y regulaciones de privacidad, como HIPAA (Health Insurance Portability and Accountability Act) y las regulaciones de seguridad.
    </p>
    <p>
      
    </p>
    <p>
      Los factores estratégicos de la industria se traducen en un conjunto de factores críticos de éxito ejecutables para cada espacio de mercado. Estos factores críticos de éxito requieren una combinación de varios activos de servicio, como activos financieros, experiencia, competencias, propiedad intelectual, procesos, infraestructura y escala de operaciones. Por ejemplo, en el espacio de mercado para el procesamiento de datos en tiempo real de gran volumen, como los que requiere la industria de servicios financieros, los proveedores de servicios deben tener sistemas informáticos a gran escala, infraestructura de red altamente confiable, instalaciones seguras, conocimiento de las regulaciones de la industria y un nivel muy alto de contingencia. Sin estos activos, los proveedores de servicios no podrían brindar la utilidad y la garantía que exigen los clientes en ese espacio de mercado.
    </p>
    <p>
      
    </p>
    <p>
      Los factores críticos de éxito determinan los activos de servicio necesarios para implementar con éxito una estrategia de servicio. Por ejemplo, si una estrategia requiere que los servicios estén disponibles en una gran red de ubicaciones o en una amplia área de cobertura, el proveedor de servicios no solo debe desarrollar capacidad en ubicaciones clave, sino que también debe operar la red como un sistema de nodos para que el costo de atender a todos los clientes es aproximadamente idéntico y dentro de un punto de precio consistente con una posición estratégica en un espacio de mercado.
    </p>
    <p>
      
    </p>
    <p>
      No todos los factores críticos de éxito deben favorecer a las grandes organizaciones o a la economía de escala en las operaciones. Algunas estrategias favorecen a las organizaciones de tamaño pequeño pero altamente competitivas debido al conocimiento que tienen de los clientes y los espacios de mercado relacionados. Por lo tanto, los gerentes deben realizar ejercicios de evaluación para determinar los factores críticos de éxito vigentes.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Los factores estratégicos de la industria tienen las siguientes características generales:" ID="ID_887382719" CREATED="1660176856980" MODIFIED="1660177001208" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Se definen en términos de capacidades y recursos.
      </li>
      <li>
        Los líderes de la industria han demostrado que son determinantes clave del éxito.
      </li>
      <li>
        Están definidos por niveles de espacio de mercado, no peculiares a ninguna firma.
      </li>
      <li>
        Son la base para la competencia entre rivales.
      </li>
      <li>
        Cambian con el tiempo, por lo que son dinámicos, no estáticos.
      </li>
      <li>
        Suelen requerir importantes inversiones y tiempo para su desarrollo.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Los factores críticos del éxito por si mismos son alterados o influenciados por uno o más de los siguientes factores:" ID="ID_304376285" CREATED="1660177002145" MODIFIED="1660177261570" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Clientes
      </li>
      <li>
        Competidores
      </li>
      <li>
        Socios
      </li>
      <li>
        Proveedores
      </li>
      <li>
        Reguladores.
      </li>
    </ul>
    <p>
      La naturaleza dinámica de los mercados, las estrategias comerciales y las organizaciones requiere que los factores estratégicos de la industria se revisen periódicamente o en eventos significativos, como cambios en las carteras de clientes, expansión a nuevos espacios de mercado, cambios en el entorno regulatorio y tecnologías disruptivas. Por ejemplo, la nueva legislación para la industria de la salud sobre la portabilidad y privacidad de los datos de los pacientes alteraría el conjunto de factores críticos de éxito para todos los proveedores de servicios que operan en espacios de mercado relacionados con la atención médica.
    </p>
    <p>
      
    </p>
    <p>
      Dado que son determinantes del éxito en un espacio de mercado, los factores estratégicos de la industria también son útiles para evaluar la posición estratégica de un proveedor de servicios en un espacio de mercado e impulsar cambios en las posiciones existentes. Por ejemplo, ser competitivo en un espacio de mercado puede requerir niveles muy altos de disponibilidad, funcionamiento a prueba de fallas de la infraestructura de TI y capacidad adecuada para respaldar la continuidad comercial de los servicios. En muchos espacios de mercado, la rentabilidad es un factor de éxito crítico común, mientras que en otros pueden ser conocimiento de dominio especializado o confiabilidad de la infraestructura. La satisfacción del cliente, la riqueza de las ofertas de servicios, el cumplimiento de los estándares y la presencia global también son factores críticos de éxito comunes. Los proveedores de servicio interno tienden a calificar bien en familiaridad con el negocio del cliente.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Proceso de gestión estratégica" ID="ID_1204889534" CREATED="1660177279581" MODIFIED="1660177531242" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Esta parte del proceso de gestión de la estrategia implica realizar un análisis de cada espacio de mercado, cliente importante y la cartera de servicios existente para determinar las posiciones estratégicas actuales y las posiciones estratégicas deseadas para el éxito. Este análisis requiere que los proveedores de servicios recopilen datos de encuestas de clientes, revisiones del nivel de servicio, puntos de referencia de la industria y análisis competitivo realizados por terceros o equipos de investigación internos. Cada factor estratégico de la industria se mide en un índice o escala significativa. Lo mejor es adoptar índices y escalas que se utilicen comúnmente dentro de un espacio de mercado o industria para facilitar la evaluación comparativa y el análisis comparativo.
    </p>
    <p>
      
    </p>
    <p>
      Los factores estratégicos de la industria se utilizan para definir campos de juego, que sirven como marcos de referencia para la evaluación de posiciones estratégicas y escenarios competitivos (Figura 4.4).
    </p>
    <p>
      
    </p>
    <p>
      Un campo de juego tendrá los siguientes puntos de referencia, que determinarán las diversas zonas en las que un proveedor de servicios está actualmente posicionado o planea estar:
    </p>
    <ul>
      <li>
        <b>Nivel de entrada</b>&nbsp;El rendimiento por debajo de este nivel no es aceptable para los clientes (gris en la Figura 4.4).
      </li>
      <li>
        <b>Promedio de la industria</b>&nbsp;El desempeño en este nivel es aceptable para los clientes, pero no diferenciará al proveedor de servicios (blanco en la Figura 4.4).
      </li>
      <li>
        <b>El mejor rendimiento de la industria</b>&nbsp;por encima de este nivel significa liderazgo (coloreado en la Figura 4.4).
      </li>
    </ul>
    <p>
      Estos puntos de referencia no son absolutos y sus valores en un índice pueden variar con el tiempo. Por ejemplo, en un nuevo espacio de mercado con menos competidores, podría ser bastante fácil cruzar el punto de referencia de costo inicial de nivel de entrada. Con el tiempo, los costos necesarios para ingresar a ese espacio de mercado pueden aumentar o disminuir. Por ejemplo, si el primer proveedor de servicios en ese espacio de mercado desarrolla una nueva innovación tecnológica como diferenciador, será más costoso para los competidores ingresar a ese espacio de mercado. Si, por otro lado, el punto de referencia de entrada se establece teniendo habilidades especializadas, y hay un exceso de oferta de personas con esa habilidad, podría ser menos costoso ingresar al mercado. El análisis estratégico debe tener en cuenta no solo los puntos de referencia actuales para un campo de juego, sino también la dirección en la que se espera que se muevan (alto o bajo), la magnitud de cambio y las probabilidades relacionadas.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Análisis necesario para los proveedores de servicio" ID="ID_829475417" CREATED="1660177536777" MODIFIED="1660179022418" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Este análisis es necesario para que los proveedores de servicios eviten ser sorprendidos por cambios en el espacio del mercado que pueden destruir por completo su propuesta de valor. Los proveedores de servicios internos pueden ser particularmente vulnerables a tales puntos ciegos si no están acostumbrados al análisis comercial que se encuentra en los proveedores de servicios externos. Los proveedores de servicios internos también enfrentan competencia incluso si tienen clientes cautivos dentro de su empresa. El campo de juego se utiliza para realizar análisis estratégicos de espacios de mercado, carteras de clientes (Figura 4.5), carteras de servicios y carteras de acuerdos con clientes. Los gerentes deciden los escenarios requeridos para construir utilizando factores, escalas e índices estratégicos de la industria aplicables.
    </p>
    <p>
      
    </p>
    <p>
      En la Figura 4.5, cuatro clientes están posicionados en relación con dos factores estratégicos de la industria. Este proveedor de servicios se centra actualmente en proporcionar servicios promedio de la industria a estos clientes. Sin embargo, los servicios prestados al cliente con mayor valor de contrato se acercan al liderazgo del mercado. ¿Qué estrategia seguirá el proveedor de servicios? Algunas opciones se enumeran a continuación y se ilustran en la Figura 4.6:
    </p>
    <ul>
      <li>
        Para el cliente 1, es importante que el proveedor de servicios invierta en activos que solidificarán su posición y permitirán un mayor crecimiento hacia la posición de liderazgo en el mercado.
      </li>
      <li>
        Después del análisis, se supo que el cliente 2 estaba especialmente enfocado en el factor estratégico de la industria Y. En el futuro, el proveedor de servicios necesitaría invertir en activos que lo hicieran más competitivo en el factor estratégico de la industria Y.
      </li>
      <li>
        A medida que el proveedor de servicios invierte en activos para cumplir con los factores estratégicos de la industria X e Y, sus capacidades generales crecerán. Esto significa que el proveedor de servicios podrá ofrecer al cliente 3 una gama y calidad de servicios más amplia.
      </li>
      <li>
        El cliente 4 está utilizando servicios similares al cliente 1, por lo que el proveedor de servicios podría aprovechar los mismos o similares activos para reposicionar al cliente 4 y asegurar la retención. Cualquier inversión para el cliente 1 debe evaluarse y aprovecharse para que la use el cliente 4.
      </li>
      <li>
        <i>Precaución</i>: El hecho de que sea posible reposicionar o ampliar un servicio no implica que todos los clientes deseen hacerlo. Se debe tener cuidado de no alienar a estos clientes obligándolos a aceptar niveles y tipos de servicios que no necesitan, o aumentando los precios a niveles que podrían considerar irrazonables.
      </li>
      <li>
        Si un servicio ha sido reposicionado, existe una posibilidad significativa de que los clientes previamente seleccionados para este servicio ya no sean el mercado objetivo. Por lo tanto, también es importante que el proveedor de servicios tome decisiones claras sobre qué clientes quiere retener. cliente no quiere tomar el servicio reposicionado, entonces puede que no sea un cliente que el proveedor de servicios de TI desee retener.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5.6 Análisis estratégico: Establecer objetivos" FOLDED="true" ID="ID_1791403974" CREATED="1660169836367" MODIFIED="1660179368428" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los objetivos definidos como una salida del análisis estratégico son el resultado de lo que el proveedor de servicio espera alcanzar siguiendo una estrategia. Una vez que los objetivos han sido definidos, el proveedor de servicios necesitará definir como alcanzará los resultados de forma anticipada. Esta es la estrategia (o estrategias).
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Objetivos claros" ID="ID_323301548" CREATED="1660179457865" MODIFIED="1660179811108" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los objetivos claros facilitan la toma de decisiones coherentes, minimizando los conflictos posteriores. Establecen prioridades y sirven como estándares.
    </p>
    <p>
      
    </p>
    <p>
      Cuando una organización evita el análisis riguroso y el establecimiento de objetivos, o cuando no tiene una buena comprensión de sus servicios y clientes, tiende a utilizar las siguientes estrategias (o formas de 'no administrar por objetivos'). Estos son sorprendentemente comunes, pero las organizaciones no deben confundirlos con estrategias válidas:
    </p>
    <ul>
      <li>
        <b>Gestión por crisis</b>&nbsp;La creencia de que la capacidad de resolver problemas de manera efectiva es una buena estrategia. Este enfoque permite que los eventos dicten las decisiones de gestión y es muy arriesgado porque asume que siempre podrán resolver problemas y que podrán hacerlo sin un impacto significativo en el negocio. Más aún, cada vez que el proveedor de servicios reacciona a un estrategia, potencialmente los lleva en una dirección diferente. Cuando hay algún tipo de crisis, la organización reacciona a la crisis y luego, en lugar de aprovechar la oportunidad para reevaluar la estrategia, espera a la próxima crisis (que es más probable que ocurra ya que la situación no ha sido rectificada).
      </li>
      <li>
        <b>Administrar según la demanda del cliente</b>&nbsp;Este tipo de organización no busca comprender a sus clientes o los resultados de su negocio. Hará que los servicios básicos estén disponibles, como el almacenamiento o el alojamiento de aplicaciones, y seguirá gastando dinero para actualizar la capacidad cada vez que los usuarios se quejen de un rendimiento deficiente. Nunca cuestiona la validez de la demanda y nunca puede cuantificar el valor de su inversión.
      </li>
      <li>
        <b>Administrar por extrapolación</b>&nbsp;Continuar las mismas actividades de la misma manera porque las cosas están yendo bien. Este tipo de organización se verá sorprendida por los cambios en sus clientes o en la industria, muchos de los cuales han ocurrido lentamente durante varios años, y luego se dará cuenta de repente de que no puede continuar brindando servicios relevantes, a menos que haga grandes inversiones y cambios drásticos. Muchos proveedores de servicios no sobrevivirán a este nivel de cambio.
      </li>
      <li>
        <b>Administrar con esperanza</b>&nbsp;Tomar decisiones con la creencia de que finalmente funcionarán. La mayoría de las empresas están impulsadas a lograr resultados tangibles y medibles. Si el proveedor de servicios no se vincula a estos, se encontrará cada vez menos relevante.
      </li>
      <li>
        <b>Administrar con el mejor esfuerzo</b>&nbsp;Hacer lo mejor posible para lograr lo que se debe hacer. No hay un plan general, tampoco hay una comprensión clara de la inversión real requerida y, por lo tanto, no hay capacidad para demostrar el valor del trabajo duro. Además, las expectativas de los clientes para los servicios de &quot;mejor esfuerzo&quot; son generalmente más altas que cuando se cuantifican los servicios y se explican los costos.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Objetivos significativos" ID="ID_794987535" CREATED="1660179814522" MODIFIED="1660180112573" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los objetivos significativos se basan en los resultados que los clientes desean lograr. Los objetivos deben ser capaces de determinar la mejor manera de satisfacer estos resultados, especialmente aquellos que actualmente están desatendidos. Así es como se determinan las métricas para medir el rendimiento de un servicio. Por lo tanto, es importante que los objetivos no solo se deriven de las evaluaciones estratégicas generales, sino que también deben tener en cuenta las aportaciones específicas de los clientes. La entrada del cliente para crear objetivos consta de tres tipos distintos de datos y ayudará al proveedor de servicios a identificar exactamente cómo crean valor (consulte la Tabla 4.2).
    </p>
    <p>
      
    </p>
    <p>
      Sin embargo, a pesar de lo valiosos que son estos datos, pueden llegar a ser muy detallados. Es importante distinguir entre el nivel de detalle requerido para los objetivos estratégicos y el requerido para definir la estrategia o el propósito de un servicio individual. Para los objetivos estratégicos, los datos sobre tareas, resultados y restricciones deben estar en un nivel estratégico, y el proveedor del servicio no se debe dejar abrumar por requisitos específicos para servicios individuales. Este tipo de requerimiento se define durante el proceso de gestión de la cartera de servicios.
    </p>
    <p>
      
    </p>
    <p>
      Aunque esto puede parecer trillado, el acrónimo 'SMART' contiene un conjunto viable de pautas que se usa con frecuencia para garantizar la definición de objetivos significativos. Esto significa:
    </p>
    <ul>
      <li>
        Los objetivos <b>específicos</b>&nbsp;deben indicar claramente lo que la estrategia va a lograr o no (por ejemplo, afirmar que una estrategia dará como resultado servicios mejorados no establece qué servicios o qué se entiende por mejora, por ejemplo, costo, tiempos de respuesta, disponibilidad).
      </li>
      <li>
        <b>Mensurable</b>&nbsp;Los gerentes deben poder evaluar si se ha cumplido el objetivo. Idealmente, también deberían poder medir el progreso hacia el objetivo (por ejemplo, ¿qué porcentaje de este objetivo se ha alcanzado?).
      </li>
      <li>
        <b>Alcanzable</b>&nbsp;Debe ser posible cumplir el objetivo (por ejemplo, la automatización total de toda la gestión de servicios no es factible).
      </li>
      <li>
        <b>Relevante</b>&nbsp;Esto verifica que el objetivo sea consistente con la cultura, estructura y dirección de la organización; y que se desprende de los resultados de las evaluaciones.
      </li>
      <li>
        <b>Limitado en el tiempo</b>&nbsp;El tiempo para la estrategia como un todo debe estar contenido en la declaración de la visión, pero cada objetivo puede tener un tiempo diferente. Esto debe quedar claramente establecido.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Líneas guía" ID="ID_105277309" CREATED="1660180114972" MODIFIED="1660187352247" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Si bien estas son buenas guías, la experiencia ha demostrado que pueden no ser suficientes. Las siguientes son algunas pautas adicionales a tener en cuenta al establecer objetivos:
    </p>
    <ul>
      <li>
        <b>No tenga demasiados</b>&nbsp;El número ideal de objetivos parece estar en el rango de 5 a 7. Después de la redacción inicial, es común tener entre 20 y 40 objetivos. La mayoría de estos se pueden agrupar en un objetivo de nivel superior, aunque la persona que planteó cada objetivo debe estar satisfecha de que su objetivo se puede cumplir (o si no se incluye, debe estar satisfecha de que no lo es).
      </li>
      <li>
        <b>Use objetivos primarios y secundarios</b>&nbsp;Cuando algunas personas no están preparadas para dejar su objetivo, es posible crear una jerarquía de objetivos con cada uno de los 5 a 7 objetivos de alto nivel ampliados para incorporar aclaraciones o descripciones más detalladas del objetivo principal. Aun así, no debe haber más de 3 objetivos secundarios por objetivo principal.
      </li>
      <li>
        <b>Manténgalos simples</b>&nbsp;Cada objetivo debe ser fácil de leer y comprender. Esto ayudará a mantener el enfoque del proveedor de servicios y también facilitará la venta de la estrategia a otras partes interesadas.
      </li>
      <li>
        <b>Evite la ambigüedad</b>&nbsp;Al simplificar los objetivos, es importante no crear declaraciones ambiguas. Al decidir entre brevedad y claridad, siempre se debe favorecer la claridad.
      </li>
      <li>
        <b>Sea positivo, pero declare lo negativo</b>&nbsp;Los objetivos indicarán lo que la estrategia va a lograr, pero a veces es más claro y menos ambiguo declarar lo que la organización no va a lograr. Esto ayudará a establecer expectativas.
      </li>
    </ul>
    <p>
      
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Razones por las que los objetivos no son alcanzados" ID="ID_349990586" CREATED="1660187359807" MODIFIED="1660187752378" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Incluso con objetivos bien establecidos y claros, muchos no se logran. Esto se debe a varias razones, entre ellas:
    </p>
    <ul>
      <li>
        <b>El objetivo no estaba bien diseñado.</b>&nbsp;Esto significa que no tuvo en cuenta las pautas descritas anteriormente. Si un objetivo no es medible o alcanzable, no se puede cumplir.
      </li>
      <li>
        <b>Expectativas diferentes</b>&nbsp;Cuando un objetivo es entendido de manera diferente por diferentes grupos, uno puede creer que han alcanzado el objetivo, mientras que otro cree que aún queda trabajo por hacer. Esto sucede incluso cuando el objetivo se establece claramente y, por lo general, es el resultado de diferentes contextos grupales o diferentes ideas sobre por qué se definió la estrategia en primer lugar. Por ejemplo, TI diseñó una mayor disponibilidad del servicio de pedidos de ventas al reducir las tasas de fallas, pero cuando los usuarios solicitaron una mayor disponibilidad, significaron horas de servicio extendidas, no menos tiempo de inactividad.
      </li>
      <li>
        <b>Cambios organizacionales</b>&nbsp;Cuanto más compleja sea una estrategia y cuanto más tarde en ejecutarse, más probable es que se vea afectada por un cambio en las partes interesadas. Con cada cambio organizacional viene un cambio en las prioridades y requisitos. Lo que acordaron las partes interesadas cuando se definió la estrategia puede no ser tan relevante para las nuevas partes interesadas.
      </li>
      <li>
        <b>Falta de apropiación del objetivo</b>&nbsp;A veces ocurre que el equipo define un objetivo, pero no hay apropiación por parte de las partes interesadas. Esto podría ser el resultado de una falta de comunicación o la suposición de que alguien más fue el responsable. Es importante que cada objetivo tenga una propiedad clara. Si la propiedad es compartida, esto debe indicarse claramente y las respectivas implicaciones de la propiedad deben ser aceptadas por cada parte interesada.
      </li>
      <li>
        <b>Política</b>&nbsp;Nunca es prudente suponer que debido a que una parte interesada aprobó un objetivo, él o ella realmente estuvo de acuerdo con él o lo apoyó. Muchas decisiones que se toman en los negocios no son lógicas en absoluto, sino que están determinadas por las complejas interrelaciones entre personas, grupos y conjuntos de objetivos en conflicto. Estas políticas pueden cambiar los objetivos o incluso hacer que la estrategia se descarrile y se nombre un nuevo equipo de liderazgo. Una estrategia exitosa requiere que estas relaciones sean monitoreadas cuidadosamente y que la gerencia siga informando sobre los objetivos acordados a una audiencia tan amplia como sea necesaria para asegurar que las partes interesadas tengan expectativas más realistas.
      </li>
      <li>
        <b>Cambios ambientales </b>Algunos objetivos no pueden ser cumplidos simplemente por un cambio de algo en que el equipo no puede controlar. Ejemplo, una estrategia incluye moverse a una arquitectura de software en particular, pero la compañía de software es comprada y el software reemplazado por la suite del comprador. Esto requerirá una re-evaluación de la estrategia y redefinición de objetivos con todos los interesados.
      </li>
      <li>
        <b>Otros factores </b>La organización ha tenido que cambiar la estrategia debido a factores externos o internos, y la estrategia existente ya no es válida.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Implicaciones de que los objetivos se cumplen cuando todos están de acuerdo" ID="ID_245108194" CREATED="1660187764255" MODIFIED="1660187918136" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El punto más importante a recordar acerca de los objetivos de la reunión es que solo se cumplen cuando todas las partes están de acuerdo en que se han cumplido. Esto tiene algunas implicaciones claras:
    </p>
    <ul>
      <li>
        Al establecer un objetivo, asegúrese de que todas las partes entiendan cómo se medirá y cuáles son los términos de aceptación.
      </li>
      <li>
        Cualquier logro o resultado identificado en la estrategia debe estar vinculado a un objetivo. Si la gerencia puede probar que se entregó el entregable, entonces es más fácil probar que se cumplió el objetivo.
      </li>
      <li>
        Si se expresa un requisito que no puede vincularse a un objetivo, la gerencia tiene dos opciones ~ rechazar el requisito o agregar un nuevo objetivo, cambiando efectivamente la estrategia.
      </li>
      <li>
        Si un requerimiento es rechazado, debe hacerse por escrito bajo la firma del ejecutivo de más alto rango. Esto reducirá la cantidad de cambios en la estrategia y también asegurará que cualquier política se maneje a nivel ejecutivo, dejando así que la organización siga adelante con la ejecución de la estrategia con la menor mala voluntad posible.
      </li>
      <li>
        Si se acepta, el nuevo requisito y objetivo debe manejarse como un cambio en la estrategia y debe ser aprobado por todas las partes interesadas.
      </li>
      <li>
        Cuando se ejecuta la estrategia, los gerentes no solo deben enfocarse en qué hitos se han alcanzado, sino que también deben incluir información sobre cómo se están cumpliendo los objetivos. Cualquier actividad que contribuya o reste valor a un objetivo debe informarse específicamente.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5.7 Selección, evaluación y generación de la Estrategia" ID="ID_562403951" CREATED="1660169880435" MODIFIED="1660188158873" TEXT_SHORTENED="true">
<arrowlink DESTINATION="ID_943462275" STARTINCLINATION="109.5 pt;0 pt;" ENDINCLINATION="99.75 pt;0 pt;"/>
<richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una vez completado el análisis y el proveedor de servicio ha definido los objetivos de la estrategia, es posible generar la estrategia actual en términos de las &quot;4 Ps&quot; descritas en la sección 3.1.2.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="4.1.5.8 Generación de la estrategia: Determinar la perspectiva" FOLDED="true" ID="ID_61184044" CREATED="1660169907079" MODIFIED="1660188575020" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La perspectiva de un proveedor de servicios define su dirección general, valores, creencias y propósito; y, a un alto nivel, cómo se propone lograrlos. Las formas más comunes de declaraciones de perspectiva son las declaraciones de <b>visión y misión</b>.
    </p>
    <p>
      
    </p>
    <p>
      <u>La declaración de visión articula lo que el proveedor de servicios pretende lograr. Las declaraciones de visión analizan un estado deseado que se logrará en algún momento en el futuro</u>. <u>Las declaraciones de misión articulan el propósito y los valores básicos de la organización y su funcionamiento. Las declaraciones de misión se refieren más a cómo la organización hará realidad su visión.</u>&nbsp;
    </p>
    <p>
      
    </p>
    <p>
      Una perspectiva clara permite que el proveedor de servicios y sus clientes entiendan su dirección y valor. Una buena perspectiva tiene cuatro propósitos principales:
    </p>
    <ul>
      <li>
        Aclarar la dirección del proveedor de servicios
      </li>
      <li>
        Motivar a las personas a tomar medidas que muevan a la organización a hacer realidad la visión
      </li>
      <li>
        Coordinar las acciones de diferentes personas o grupos
      </li>
      <li>
        Representar la visión de la gestión superior como dirigen a la organización hacia sus objetivos generales.
      </li>
    </ul>
    <p>
      A partir de estos puntos, queda claro que la perspectiva no solo articula los objetivos y el negocio principal de la organización, sino que también constituye la base de su cultura. La perspectiva proporciona una base para establecer objetivos comunes en toda la organización y garantizar que se fomenten las acciones y el comportamiento adecuados en todos los niveles.
    </p>
    <p>
      
    </p>
    <p>
      La implicación es que una perspectiva exitosa está en la raíz de la cultura de una organización. Por lo tanto, un cambio de perspectiva no es solo un caso de redefinir la visión y la misión, y luego hacer cambios estructurales para implementarlos. La simple creación y comunicación de estas declaraciones no es suficiente para garantizar la aceptación. Un cambio de perspectiva requerirá un esfuerzo significativo para cambiar la cultura actual y la forma de pensar de la organización. ITIL Service Transition analiza el cambio organizativo y de las partes interesadas con cierto detalle.
    </p>
    <p>
      
    </p>
    <p>
      El proceso real de definir la perspectiva es valioso para lograr que las personas en el equipo de gestión alineen sus agendas y pensamientos. No es raro que el equipo gaste proporcionalmente más&nbsp;en definir la visión que en definir el resto de la estrategia.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Definir o actualizar la perspectiva" ID="ID_300251825" CREATED="1660188587957" MODIFIED="1660188988009" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Por lo general, es útil definir o actualizar la perspectiva en una reunión cara a cara de los altos directivos, donde cualquier desacuerdo o aclaración puede tratarse de inmediato. Esto también ayuda a lidiar directamente con cualquier agenda o política interpersonal que pueda afectar el resultado.
    </p>
    <p>
      
    </p>
    <p>
      Las perspectivas bien definidas sirven de referencia para posiciones, planes o pautas de actuación posteriores. Las afirmaciones públicas realizadas por un proveedor de servicios generalmente se basan en la estrategia como perspectiva y se reflejan en su propuesta de valor para los clientes. La propuesta de valor puede estar implícita en los clientes a los que sirve, los servicios que ofrece y la perspectiva particular de la calidad del servicio que adopta. Una perspectiva clara ayuda a hacer explícita esta propuesta de valor. La perspectiva se define al más alto nivel de abstracción y mantiene el horizonte de planificación más lejano de la organización. Impulsa otras vistas de control de la estrategia (las otras 'P') y se modifica en función de los comentarios de esas vistas.
    </p>
    <p>
      
    </p>
    <p>
      A pesar de su abstracción de alto nivel, la perspectiva no debe ignorarse ni trivializarse. A diferencia de los planes o patrones, las perspectivas no se cambian fácilmente. Tome la perspectiva de los relojeros suizos, por ejemplo, cuando se enfrentan al surgimiento de la tecnología de cuarzo, una invención suiza. Descartando la tecnología como una novedad incompatible con la perspectiva de la artesanía intensiva en habilidades, la industria relojera suiza casi fue diezmada por las empresas japonesas. Es decir, hasta que adoptó la tecnología para los principales nichos de mercado y recuperó participación de mercado a través de una perspectiva centrada en la moda en lugar de la mano de obra.
    </p>
    <p>
      
    </p>
    <p>
      Una vez que se ha logrado una perspectiva, se puede probar haciendo las siguientes preguntas:
    </p>
    <ul>
      <li>
        ¿Capta lo que el proveedor de servicios pretende hacer solo durante los próximos tres a cinco años, o captura una esencia más atemporal de la distinción de la organización?
      </li>
      <li>
        ¿Es claro y memorable?
      </li>
      <li>
        ¿Tiene la capacidad de promover y orientar la acción?
      </li>
      <li>
        ¿Establece límites dentro de los cuales las personas son libres de experimentar?
      </li>
    </ul>
    <p>
      <b>La destilación de la estrategia de una organización en una frase prescriptiva y memorable es importante.</b>
    </p>
    <p>
      
    </p>
    <p>
      Una buena estrategia es de poca utilidad a menos que la gente la entienda lo suficientemente bien como para aplicarla durante oportunidades imprevistas o ambiguas.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Ejemplo de una perspectiva" ID="ID_1840930044" CREATED="1660189003517" MODIFIED="1660189162427" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      <i>Visión </i>
    </p>
    <p>
      La organización de TI contribuye a la ventaja competitiva de la Compañía X al brindar servicios de TI que cumplen con los resultados comerciales definidos de otras unidades comerciales.
    </p>
    <p>
      
    </p>
    <p>
      <i>Misión </i>
    </p>
    <p>
      La organización de TI comprende los resultados comerciales que permiten que la empresa X tenga éxito. Construye, obtiene y entrega servicios de TI a otras unidades de negocios para lograr estos resultados. La organización de TI permite que la empresa X opere de manera eficiente y con una interrupción mínima, y también ayuda a cada unidad de negocios a innovar nuevas formas de crear y entregar valor a los clientes externos.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5.9 Generación de la estrategia: Formar una posición" FOLDED="true" ID="ID_1937147355" CREATED="1660169932307" MODIFIED="1660225116238" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La posición estratégica define cómo se diferenciará el proveedor de servicios de otros proveedores de servicios en la industria. Por ejemplo, el posicionamiento podría basarse en el tipo o la gama de servicios ofrecidos, o en la prestación de servicios al menor costo. El posicionamiento se basa en el resultado de la evaluación estratégica, especialmente el análisis de los espacios de mercado y los factores estratégicos de la industria.
    </p>
    <p>
      
    </p>
    <p>
      La posición de un proveedor de servicios se expresa con frecuencia a través de políticas sobre qué servicios se prestarán, a qué nivel ya qué clientes. Estas políticas también definirán cualquier estándar o criterio que se utilice para garantizar que la diferenciación se diseñe e incorpore adecuadamente en los servicios, las operaciones y la organización. Las políticas definidas durante esta etapa del proceso se utilizarán como entrada en las otras etapas del ciclo de vida del servicio.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Tipos de proveedores de servicios" FOLDED="true" ID="ID_1238245826" CREATED="1660189282084" MODIFIED="1660224201294" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Además del costo y la calidad de los servicios, existen cuatro tipos generales de puestos para los proveedores de servicios. Estos se describen a continuación:
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Posicionamiento basado en la variedad" ID="ID_1466499616" CREATED="1660223492086" MODIFIED="1660223646883" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El posicionamiento basado en la variedad se enfoca en entregar un catálogo limitado de servicios a una variedad de clientes y tipos de clientes. Aunque el catálogo de servicios es estrecho, suele haber más profundidad en cuanto a opciones de servicio, niveles y paquetes. Los activos de servicio están altamente especializados para ofrecer este catálogo limitado. Los proveedores de servicios no intentan satisfacer todas las necesidades de un segmento de clientes determinado. El éxito se mide en términos de un desempeño excepcionalmente bueno para satisfacer un subconjunto de necesidades (Figura 4.7). Es posible aprovechar las economías de escala, ya que el proveedor de servicios gestiona tipos de demanda similares de diferentes clientes y satisface la demanda con un catálogo de servicios pequeño y estable. El crecimiento se basa predominantemente en nuevas oportunidades para un mismo catálogo de servicios. Por ejemplo, un proveedor de servicios puede especializarse en servicios de nómina para varios grupos dentro de una unidad comercial, varias unidades comerciales dentro de una empresa o varias empresas dentro de una región.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Posicionamiento basado en las necesidades" ID="ID_1650961241" CREATED="1660223648339" MODIFIED="1660223853665" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Esto a veces se denomina un enfoque de &quot;intimidad del cliente&quot; porque se centra en un solo cliente o tipo de cliente y proporciona una gama de servicios para satisfacer una gran cantidad de necesidades del cliente (Figura 4.7). El catálogo de servicios es relativamente amplio. ya que el proveedor de servicios está abordando una gama más amplia de servicios. El posicionamiento basado en las necesidades refleja el enfoque tradicional de agrupar a los clientes en segmentos y luego tratar de satisfacer mejor las necesidades de uno o más segmentos objetivo. Los proveedores de servicios no pretenden ganar cuota de mercado ampliando sus servicios a un número ilimitado de clientes potenciales. Más bien, se diferencian por desempeñarse excepcionalmente bien al satisfacer la mayoría de las necesidades de un cliente o segmento en particular. Sus capacidades se basan en poder aprovechar economías de alcance, gestionar diferentes demandas de los mismos clientes, y cumpliéndolas con un catálogo flexible de servicios. El crecimiento se basa predominantemente en nuevos servicios en el catálogo de la misma fuente de demanda.
    </p>
    <p>
      
    </p>
    <p>
      Por ejemplo, un proveedor de servicios puede especializarse en respaldar la mayoría o todas las necesidades comerciales de un grupo de hospitales. Podrá ofrecer un catálogo de servicios que abarque servicios de infraestructura, mantenimiento de aplicaciones, seguridad de la información, gestión documental y servicios de recuperación ante desastres especializados para la industria de la salud. Mantiene experiencia en registros médicos electrónicos, cuestiones de privacidad, equipos médicos y procesamiento de reclamaciones. De manera similar, un proveedor que se enfoca en la industria de servicios financieros tiene una visión profunda de los desafíos y oportunidades peculiares que enfrentan los bancos de inversión, las aseguradoras y las casas de bolsa.
    </p>
    <p>
      
    </p>
    <p>
      Los proveedores de servicios internos a menudo están posicionados para servir a un segmento de clientes de uno. Solo tienen un cliente a nivel de empresa, incluso si hay varios a nivel de unidad de negocio. Se espera que muchas organizaciones internas de TI satisfagan todas las necesidades de TI de la empresa que las posee. No se preocupan por satisfacer las necesidades de otras empresas y, por lo tanto, pueden organizar sus activos de servicio para servir mejor a un cliente empresarial.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Posicionamiento basado en el acceso" ID="ID_1801070974" CREATED="1660223855119" MODIFIED="1660224156060" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El posicionamiento basado en el acceso no tiene que ver (como podría sugerir su nombre) con la forma en que los clientes solicitan o reciben un servicio. Se refiere a brindar un servicio, o una variedad de servicios, a varios clientes que tienen algo en común, generalmente su ubicación, escala o estructura (Figura 4.8 con el eje a la derecha para mostrar su relación con la Figura 4.9): por ejemplo, una empresa de soporte de PC que solo brinda soporte a pequeñas empresas en una sola ciudad; o una empresa que solo brinda soluciones de punto de venta para tiendas de conveniencia. Estos proveedores de servicios suelen ser muy especializados y despliegan activos comerciales de acuerdo con el negocio específico y el perfil del cliente al que se han dirigido. Esto contrasta con el posicionamiento basado en la variedad, en el que se dispone de un catálogo limitado de servicios para cualquier tipo de cliente, o el posicionamiento basado en las necesidades, en el que se proporciona una amplia gama de servicios a un cliente o segmento de clientes específico.
    </p>
    <p>
      
    </p>
    <p>
      Una compañía de seguros se ofrece a iniciar el proceso de reclamación en el lugar del accidente. Lo hace enviando personal de manejo de reclamos al lugar del accidente con todos los recursos necesarios para el proceso de reclamos. Esta estrategia no solo brinda un valor distintivo a sus asegurados, sino que también acelera los procesos y reduce los costos administrativos de los casos prolongados. Pone un trabajo de oficinista en la primera línea en vehículos especialmente equipados con las aplicaciones comerciales necesarias. La propia compañía de seguros adopta una estrategia basada en el acceso para distinguirse de las aseguradoras de la competencia.
    </p>
    <p>
      
    </p>
    <p>
      A su vez, otros proveedores de servicios pueden competir para ganar el negocio de esta aseguradora progresista al ofrecer servicios de lugar de trabajo móvil que automaticen e integren los vehículos de procesamiento de reclamos con los sistemas administrativos. Los proveedores de servicios con conocimiento y experiencia en sistemas y aplicaciones móviles, similares a los que utilizan los servicios médicos de emergencia, tendrían una ventaja distintiva.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Posicionamiento basado en la demanda" FOLDED="true" ID="ID_443594328" CREATED="1660224206163" MODIFIED="1660224653960" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Este tipo de posicionamiento contiene características tanto de posicionamiento basado en la variedad como basado en las necesidades, ya que ofrece una amplia gama de servicios a un número potencialmente ilimitado de clientes. Ha sido posible gracias a la convergencia de infraestructura, aplicaciones y servicios. Un ejemplo de este tipo de posicionamiento son los servicios ‘cloud’. En este posicionamiento, las empresas utilizan TI para relacionarse directamente con una gran variedad de clientes y consumidores individuales. No existe un servicio estándar predefinido, sino una gama de opciones de servicio que se pueden combinar en cualquier número de paquetes - todos manejados directamente por el consumidor. Por ejemplo, los consumidores entran a un sitio web, y crean una cuenta. Ellos crean perfiles y seleccionan la combinación de servicios que se adapte a sus necesidades particulares. Por ejemplo, un librero en línea puede rastrear compras y preferencias y notificar automáticamente al consumidor sobre nuevos productos o precios especiales disponibles para ellos, ponerlos en contacto con otros consumidores con gustos similares, permitirles vender copias usadas de los libros, brindar servicios de envío y más.
    </p>
    <p>
      
    </p>
    <p>
      Las principales capacidades del proveedor de servicios están en poder tener una serie de servicios estándar en un catálogo, que se pueden seleccionar en cualquier combinación y vincular a cada consumidor. La demanda es muy volátil y el proveedor de servicios debe poder mover las cargas de trabajo para poder mantener el rendimiento. Operativamente, necesitan monitorear la capacidad y adaptarla a la demanda muy rápidamente. Uno de los enfoques clave es anticiparse y adelantarse a la demanda.
    </p>
    <p>
      
    </p>
    <p>
      Los proveedores de servicios pueden adoptar uno o más de estos tipos genéricos de posicionamiento (Figura 4.9). No existen reglas universales para estas estrategias de posicionamiento, simplemente planes y patrones que funcionan, o definiciones a cumplir. Sin embargo, una vez que se ha determinado una posición, el proveedor de servicios necesitará planes concretos para mantener posiciones estratégicas desde las cuales se logren la misión y los objetivos. Una posición sólida guía a la organización en lo que debe hacer y, lo que es igualmente importante, en lo que no debe hacer.
    </p>
    <p>
      
    </p>
    <p>
      El tipo de posicionamiento elegido determinará cómo se especializarán y desplegarán los activos de servicio. Cada tipo de posicionamiento tendrá patrones típicos de demanda generados por actividades comerciales, ciclos y eventos de los espacios de mercado objetivo. La arquitectura y la configuración de los activos de servicio deberán estructurarse adecuadamente para estos patrones de demanda. Esta es una oportunidad para consolidar, estabilizar, aprender y crecer hasta convertirse en un proveedor de servicios de alto rendimiento con enfoque. La especialización de los activos de servicio permite a los proveedores de servicios ofrecer mayores niveles de utilidad a los segmentos objetivo. Por otro lado, la especialización también puede exponer al proveedor de servicios al riesgo de no poder responder a cambios repentinos o drásticos en el mercado. Si el proveedor de servicios no mantiene un equilibrio entre la especialización y la agilidad, es posible que no se recupere de este tipo de cambio.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Nivel de especialización en activos" ID="ID_240185862" CREATED="1660224871911" MODIFIED="1660224977276" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Cuanto más especializado se vuelve un activo, menor es su utilidad para otros fines. Una terminal de punto de venta tiene una mayor especificidad de activos que una estación de trabajo de PC o un dispositivo de almacenamiento que se puede reutilizar. El nivel de especialización de los activos también se aplica a los activos de la organización y las personas. A los proveedores de servicios internos que nunca han atendido a más de un cliente les resulta difícil adaptarse a las fusiones y adquisiciones corporativas.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Menor especialización" ID="ID_317129138" CREATED="1660224987821" MODIFIED="1660225101376" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El posicionamiento basado en la demanda requiere menos especialización. De hecho, existe solo porque las infraestructuras convergentes hacen posible administrar las cargas de trabajo en múltiples aplicaciones y sistemas vinculados. Cuanto más convergente y genérica se vuelve la infraestructura, más fácil es intercambiar sistemas, mover cargas de trabajo de un área a otra, alojar servicios en cualquier parte de la organización y brindarlos en cualquier lugar donde los consumidores puedan acceder a Internet. La infraestructura se convierte en una inversión genérica y los activos de servicio se convierten en los módulos de los propios servicios.
    </p>
    <p>
      
    </p>
    <p>
      Una vez que se ha determinado una posición, se puede probar haciendo las siguientes preguntas:
    </p>
    <ul>
      <li>
        ¿Guía a la organización en la toma de decisiones entre recursos competitivos e inversiones en capacidad?
      </li>
      <li>
        ¿Ayuda a los gerentes a probar la idoneidad de un curso de acción en particular?
      </li>
      <li>
        ¿Establece límites claros dentro de los cuales el personal debe y no debe operar?
      </li>
      <li>
        ¿Permite la libertad de experimentar dentro de estas limitaciones?
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="4.1.5.10 Generación de la estrategia: Diseñar un plan" FOLDED="true" ID="ID_551571818" CREATED="1660169961642" MODIFIED="1660225579710" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un plan estratégico identifica cómo la organización logrará sus objetivos, visión y posición. El plan es un curso de acción deliberado hacia los objetivos estratégicos y describe cómo la organización se moverá de un punto a otro dentro de un escenario específico. Aunque un plan estratégico podría ser un solo documento, más a menudo podría comprender varios planes, especialmente cuando el proveedor de servicios busca más de una posición o más de un mercado.
    </p>
    <p>
      
    </p>
    <p>
      Los horizontes de planificación suelen ser a más largo plazo, aunque la duración puede variar según las organizaciones, las industrias y el contexto estratégico. Los planes típicamente se enfocan en presupuestos financieros, portafolio de servicios, desarrollo de nuevos servicios, inversiones en activos de servicio y planes de mejora.
    </p>
    <p>
      
    </p>
    <p>
      Aunque esta sección habla sobre la elaboración de un plan, en realidad la estrategia para una organización en particular es a menudo una colección de planes, cada uno de los cuales apunta a lograr un conjunto específico de objetivos o una posición específica. Por ejemplo, puede haber un plan destinado a aumentar la cantidad de servicios ofrecidos y un plan separado destinado a aumentar la cantidad de ubicaciones en las que la organización hace negocios. Es fundamental que estos planes estén vinculados y que sus objetivos estén alineados. No sería bueno que aumentara el número de ubicaciones, pero la organización no pudo ofrecer servicios clave a sus nuevos clientes.
    </p>
    <p>
      
    </p>
    <p>
      Los planes deben estar vinculados por la necesidad de lograr ciertos objetivos estratégicos. Por ejemplo, la creación de capacidad de infraestructura, la consolidación del personal en ubicaciones clave, la concesión de licencias para un nuevo conjunto de aplicaciones de software y el cumplimiento de un estándar de la industria pueden ser partes del mismo plan estratégico para alcanzar una posición distintiva.
    </p>
    <p>
      
    </p>
    <p>
      El plan debe ser un documento conciso y legible. Muchas estrategias han fallado simplemente porque eran demasiado complicadas para que cualquiera, fuera de las personas que crearon el plan, las entendiera. Las hojas de cálculo complejas y los modelos financieros deben colocarse en apéndices o documentos separados, y solo los resultados deben publicarse en el plan, con referencias a dónde se pueden encontrar los trabajos.
    </p>
    <p>
      
    </p>
    <p>
      En cierto sentido, se puede considerar que la gestión de servicios facilita un conjunto coordinado de planes con los que los proveedores de servicios planifican y ejecutan sus estrategias de servicios. La diferencia entre el éxito y el fracaso en el liderazgo y la dirección estratégicos depende en gran medida de qué tan bien se armó, se puso a trabajar y se controló la ejecución de este conjunto coordinado. Dos proveedores de servicios con el mismo conjunto de recursos pueden lograr diferentes grados de éxito simplemente debido a sus planes estratégicos.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Información confidencial" ID="ID_780116087" CREATED="1660225582902" MODIFIED="1660225688934" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Es importante señalar que los planes estratégicos son documentos confidenciales porque contienen información sensible sobre cómo la organización mantiene una posición diferenciada o competitiva y cuáles son sus prioridades de inversión. Al mismo tiempo, el plan también contiene información que la organización puede querer ser público, como la misión, visión y ciertos aspectos de su posicionamiento - por ejemplo, que tipo de servicio ofrecerá. El plan debe claramente identificar que información es confidencial y cual debería ser comunicada externamente, y a quién.
    </p>
    <p>
      
    </p>
    <p>
      Aunque los planes son confidenciales, no deben ocultarse a los miembros clave del personal. Es muy difícil ejecutar una estrategia si las personas que realizan el trabajo no comprenden por qué se realiza el trabajo. Una empresa tenía información sobre patrones de ventas y ciclos de demanda de compras en línea, pero se negó a compartirla con el administrador de capacidad porque era confidencial. El administrador de capacidad intentó pronosticar la demanda utilizando herramientas de modelado, pero no siempre pudo predecir los ciclos de gran demanda. La organización no estaba preparada para compartir información estratégica con un empleado clave, pero gastó millones en capacidad adicional para evitar el tiempo de inactividad ocasional.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Contenido típico de un plan estratégico" ID="ID_27256806" CREATED="1660225690722" MODIFIED="1660226477567" TEXT_SHORTENED="true">
<font BOLD="true"/>
<richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los contenidos típicos de un plan estratégico podrían incluir:
    </p>
    <ul>
      <li>
        <b>Resumen ejecutivo</b>&nbsp;Este debe delinear la estrategia a un alto nivel, brindando un resumen de la misión y la visión, las metas principales y las estrategias clave. La idea es que alguien que no haya estado involucrado en la redacción del plan pueda comprender rápidamente la intención general y el contenido del documento. En la mayoría de los casos, debería ser suficiente información para que un ejecutivo comprenda la dirección general y el impacto del plan, y lo dirija a información más detallada que sea relevante para él.
      </li>
      <li>
        <b>Autorización</b>&nbsp;Es importante saber quién firmó el plan, como esto indica propiedad del plan. Esto le dará peso al plan cuando se esté ejecutando, auditando o midiendo. También identificará a las partes interesadas clave para las revisiones y actualizaciones del plan.
      </li>
      <li>
        <b>Antecedentes</b>&nbsp;Esta es una breve descripción de los eventos que llevaron a la estrategia actual. Podría tratarse de una revisión periódica de la estrategia, pero es más probable que haya algunos factores desencadenantes específicos que iniciaron el plan actual. Estos pueden ser positivos o negativos, por ejemplo, una disminución o un crecimiento imprevistos en el negocio, demandas de nuevos clientes, fallas en la estrategia anterior que resultaron en fallas de alto perfil, un cambio en la economía, etc. Es muy&nbsp;&nbsp;importante para los ejecutivos entender el contexto y las razones para la nueva estrategia. Esto hace más fácil el soporte y la ejecución del plan.
      </li>
      <li>
        <b>Análisis situacional</b>&nbsp;Esta sección del plan podría combinarse con los antecedentes, pero contiene información más específica y detallada sobre la situación que dio origen a la estrategia actual. También describirá qué métodos se utilizaron para realizar el análisis (por ejemplo, evaluación comparativa o cumplimiento de estándares) y cualquier estrategia alternativa que se rechazó, junto con las razones del rechazo.
      </li>
      <li>
        <b>Declaraciones de visión, misión y valores</b>&nbsp;Esta sección describe la perspectiva general de la organización. Actúa como un patrón contra el cual se deben medir el resto del plan y las acciones resultantes. Aunque pueda parecer que muchos valores (como la integridad o la justicia) son intangibles, alientan a quienes ejecutan la estrategia a hacer que los valores sean parte de su proceso de toma de decisiones tácticas. Por ejemplo, una de las regiones en las que la organización desea generar nuevas oportunidades les exige pagar importantes sobornos a los funcionarios del gobierno. ¿Se apoyará eso?
      </li>
      <li>
        <b>Objetivos</b>&nbsp;Son los objetivos definidos en el apartado 4.1.5.6.
      </li>
      <li>
        <b>Estrategias</b>&nbsp;Esta sección describe el posicionamiento estratégico, los servicios y los hitos clave. Es importante saber cuál es la estrategia, así como saber que ha tenido éxito.
      </li>
      <li>
        <b>Anexos</b>&nbsp;Son documentos utilizados para generar y ejecutar la estrategia. Muchos de ellos son documentos de trabajo que se utilizan para afinar la estrategia a lo largo de la medición y evaluación de la estrategia. Los apéndices podrían incluir lo siguiente:

        <ul>
          <li>
            <i>Método y datos de análisis de la estrategia</i>&nbsp;Estos son los datos que se utilizaron para generar los objetivos y la estrategia. Es importante incluir esto para que cualquier suposición o datos incorrectos puedan ser detectados y la estrategia ajustada a tiempo.
          </li>
          <li>
            <i>Planes de acción</i>&nbsp;Estos son planes detallados sobre las acciones que se tomarán para ejecutar la estrategia, delineando roles y responsabilidades, o identificando proyectos que se han iniciado como resultado de la estrategia, junto con fechas, costos y especificaciones de que será entregado o alcanzado.
          </li>
          <li>
            <i>Planificación de recursos y presupuestos</i>&nbsp;Muchas organizaciones utilizarán la estrategia como entrada en los planes y presupuestos de recursos. Es importante que estos estén debidamente coordinados. En una organización más pequeña, estos podrían incluirse en el plan. En organizaciones más grandes, esto puede ser demasiado detallado, en cuyo caso la estrategia debe identificar qué documentos contienen esta información, dónde se almacenan y quién es responsable de ellos. Esto también ayudará a coordinar acciones para ejecutar la estrategia y asegurar que el las inversiones y los rendimientos previstos son alcanzables.
          </li>
          <li>
            <i>Responsabilidades del grupo/junta directivo de TI</i>&nbsp;A veces vale la pena separar las acciones que se requieren de los ejecutivos. Esto garantiza la accesibilidad y el enfoque de un grupo de gerentes que tienen que hacer malabarismos con múltiples responsabilidades.
          </li>
          <li>
            <i>Plan de comunicación</i>&nbsp;Contendrá información sobre quién necesita saber sobre el plan y qué necesitan saber. También contendrá detalles sobre actividades de educación o concientización y quién debe asistir a esos eventos. Un buen plan de comunicación también definirá cómo garantizar que los destinatarios hayan entendido el mensaje, incluidas encuestas, entrevistas y sesiones grupales en las que se les pide a las personas que expresen cómo entienden la estrategia y su impacto en ellos.
          </li>
        </ul>
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Ejemplo de una planificación inadecuada de presupuesto y recursos" ID="ID_960546210" CREATED="1660226486167" MODIFIED="1660226558038" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una compañía de seguros decidió una estrategia para trasladar todos sus servicios del procesamiento por lotes a servicios en línea en tiempo real durante tres años y con un costo de 80 millones de libras esterlinas. Seis años y 240 millones de libras más tarde, están completos en dos tercios y los rendimientos previstos aún no se han materializado. Lo que comenzó como una buena idea se ha convertido en una situación en la que la organización ha cambiado demasiado como para volver atrás, y la organización de TI simplemente tiene que reaccionar ante cualquier unidad de negocio que tenga la mayor influencia política o presupuesto.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Análisis estratégico" ID="ID_55219663" CREATED="1660226581365" MODIFIED="1660226927527" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El análisis estratégico incluye la identificación de los factores estratégicos de la industria relacionados a los espacios de mercado identificados. Una salida de esta actividad es un conjunto de factores críticos de éxito que necesitan ser alcanzados para que el proveedor de servicio pueda lograr ejecutar la estrategia. Como se indica en la sección 4.1.5.5, en el contexto de la gestión de la estrategia, estos factores críticos de éxito suelen ser una combinación de varios activos de servicio, como activos financieros, experiencia, competencias, propiedad intelectual, procesos, infraestructura y escala de operaciones.
    </p>
    <p>
      
    </p>
    <p>
      Una forma de identificar los factores críticos de éxito es mapear los activos del cliente y los arquetipos de servicio (Figura 4.10). Por ejemplo, en el cuidado de la salud, los proveedores de servicios de TI tienen un amplio conocimiento de los procedimientos hospitalarios, equipos médicos, interacciones entre médicos, clínicos y farmacéuticos, pólizas de seguros y normas de privacidad. Los proveedores de servicios presentes en espacios de mercado relacionados con la calidad de los resultados en el cuidado de la salud suelen tener médicos y clínicos en su nómina. Las estrategias de servicio para los espacios del mercado de la salud tienen en cuenta la necesidad de tratar con usuarios con habilidades altamente especializadas, equipos para propósitos especiales, baja tolerancia al error y la necesidad de equilibrar la seguridad con la usabilidad de los servicios. Estos son factores críticos de éxito para un grupo de espacios de mercado relacionados con el cuidado de la salud.
    </p>
    <p>
      
    </p>
    <p>
      Un subconjunto de estos factores críticos de éxito es compartido por otros espacios de mercado, como las aplicaciones militares. Por lo tanto, los factores críticos de éxito pueden abarcar más de un espacio de mercado. Representan oportunidades para aprovechar las economías de escala y alcance.
    </p>
    <p>
      
    </p>
    <p>
      Definir los factores críticos de éxito para la estrategia es importante porque forma la base para la ejecución. Los factores críticos de éxito identifican lo que existe y lo que se necesita construir. Proporcionan un conjunto de requisitos para que la ejecución de los planes sea exitosa.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5.11 Generación de la estrategia: Adoptar patrones de acción" FOLDED="true" ID="ID_82115672" CREATED="1660169985118" MODIFIED="1660227879452" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un patrón de acción es como una organización trabaja. Jerarquías formales muestran una visión de la organización, pero las interacciones entre estas jerarquías, el intercambio de información, el traspaso de unidades de trabajo y el intercambio de dinero contribuyen a una red de actividad que hace que las cosas se hagan.
    </p>
    <p>
      
    </p>
    <p>
      Los patrones de acción funcionan de dos maneras. Por un lado, la estrategia define patrones que los ejecutivos creen que serán medios eficientes y efectivos para alcanzar los objetivos. Estos patrones son discernibles y generalmente se definen y documentan en forma de sistemas de gestión, estructuras organizativas, políticas, procesos, procedimientos, presupuestos, cronogramas, etc. Los patrones también se definen en las interacciones formales entre el personal del proveedor de servicios y sus clientes. La organización invierte en herramientas y capacitación para garantizar que estos patrones sean admitidos y comprendidos, de modo que puedan ejecutarse según lo previsto.
    </p>
    <p>
      
    </p>
    <p>
      Por otro lado, los patrones de acción son una forma de lidiar con la naturaleza dinámica de las organizaciones. A medida que la organización y su entorno cambian, la estrategia debe adaptarse y fortalecerse. Las personas encuentran mejores formas de hacer las cosas, ven nuevas oportunidades y detectan nuevas necesidades de los clientes o tecnologías innovadoras. La estrategia debe ser capaz de tener esto en cuenta y evolucionar junto con la organización y su entorno. Estos patrones de acción son menos formales y, a menudo, no son tangibles. Por ejemplo, las personas de la organización utilizan redes informales en función de si alguien tiene un conocimiento tácito de alguna tarea o tema de la organización.
    </p>
    <p>
      
    </p>
    <p>
      Otro ejemplo de cómo los patrones de trabajo pueden cambiar o adaptar una estrategia es si se cometió un error al definir la organización, los procesos o las herramientas. Los patrones de trabajo pueden cambiar a medida que las personas de la organización encuentran una forma más exitosa de lograr los objetivos. En estos casos, los patrones de actuación se convierten en la forma en que una organización supera los obstáculos introducidos por la estrategia o cultura actual. De esta forma, los patrones de acción dan como resultado “estrategias emergentes”, o patrones de acción distintivos reforzados con el tiempo por el éxito repetido. Por ejemplo, en lugar de seguir un plan para reducir los costos del servicio a través de la subcontratación completa, el proveedor hace subcontrataciones una a la vez - probando la validez de la idea. Primeramente externalizando servicios de telecomunicaciones, luego el hosting de aplicaciones, etc, hasta que un patrón estratégico ha aparecido.
    </p>
    <p>
      
    </p>
    <p>
      Sin embargo, existe una delgada línea entre el comportamiento innovador y el caos. Los patrones de acción pueden resultar en una organización ágil y de aprendizaje, pero aún deben estar alineados con la estrategia general de la organización. Si no lo hacen, es responsabilidad de los ejecutivos cambiar el comportamiento o ajustar la estrategia. Un patrón de acción exitoso siempre debe estar debidamente documentado, el impacto en la estrategia cuantificado y, si es necesario, la estrategia misma actualizada.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Éxito en la aplicación de patrones" ID="ID_1714372910" CREATED="1660227881346" MODIFIED="1660227901099" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Si los patrones de acción se alinean adecuadamente con los objetivos de la organización y se ejecutan bien, es probable que sean una firma de la organización y una fuente de ventaja competitiva. Si bien las prácticas y estándares de la industria están disponibles para todos, los procesos de firma pueden realmente distinguir el valor proporcionado por un proveedor de servicios (Gratton y Ghoshal, 2005). Las mejores prácticas son patrones de acción que documentan la práctica prevaleciente en una industria o situación en particular. Una organización exitosa podrá aprovecharlos, pero una organización superior los usa para crear sus propios patrones únicos de acción, que maximizan tanto las mejores prácticas como la propia cultura y posición única de la organización.
    </p>
    <p>
      
    </p>
    <p>
      Si los patrones tienen éxito, se pueden replicar en otras partes del negocio, reduciendo los costos de desarrollo y aumentando la efectividad. Por ejemplo, un departamento de un proveedor de servicios encontró una forma de vender sus servicios a través de socios, pero ofreciéndolos ellos mismos. En efecto, crearon una fuerza de ventas completamente nueva, a una fracción del costo de emplear personal de ventas. En seis meses, tres departamentos más se habían posicionado para hacer lo mismo. Este principio de 'crear una vez, usar muchas veces' es un componente fundamental de muchos de los modelos que se encuentran en ITIL, por ejemplo, modelos de cambios, modelos de incidentes, modelos de operaciones, etc. Cuando los patrones de acción se convierten en sistemas y procesos, se colocan bajo gestión de configuración para que puedan estabilizarse, estandarizarse y mejorarse. Cada patrón formal de acción se convierte en una línea de base a partir de la cual se pueden realizar mejoras y nuevas oportunidades aparecen. De este modo la gestión de servicio puede ser vista como una red adaptativa de patrones a través de la cual los objetivos estratégicos son alcanzados.
    </p>
    <p>
      
    </p>
    <p>
      Los Tipos de patrones de acción son útiles en la definición y ejecución de las estrategias de gestión de servicio y se muestran en la tabla 4.3
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5.12 Ejecución de la estrategia" FOLDED="true" ID="ID_409557895" CREATED="1660170015055" MODIFIED="1660228174743" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Una vez que se ha acordado una estrategia, es necesario ponerla en práctica. La pregunta de cómo ejecutar la estrategia se responde en un conjunto de planes tácticos más detallados. Los planes tácticos describen qué enfoques y métodos se utilizarán para lograr la estrategia. Si una estrategia responde a la pregunta &quot;¿adónde vamos?&quot;, entonces las tácticas responden a la pregunta &quot;¿cómo llegaremos allí?&quot;.
    </p>
    <p>
      
    </p>
    <p>
      Todos los procesos de gestión de servicios tienen un papel que desempeñar en la ejecución de una estrategia, ya que se trata de lograr la visión, los objetivos y los planes definidos en la gestión de la estrategia. En un sentido muy real, las otras etapas del ciclo de vida del servicio tienen que ver con la ejecución de la estrategia. Además, todos los servicios y resultados comerciales deben estar en línea con la estrategia. Si no lo son, entonces el proceso de gestión de la estrategia fue ineficaz, no se actualizó con la suficiente regularidad o simplemente fue pasado por alto por partes de la organización.
    </p>
    <p>
      
    </p>
    <p>
      Si bien los procesos y funciones existentes ejecutarán la estrategia como parte de su mandato, muchas de las iniciativas deberán gestionarse como proyectos. Es habitual que una estrategia esté vinculada a un conjunto de proyectos, gestionados por una oficina de gestión de proyectos (PMO) y coordinados a través de la cartera de proyectos (ver apartado 4.2.4.9). Además, muchos de estos se iniciarán como planes de mejora del servicio (registrados en el registro de CSI) discutidos en detalle en ITIL Mejora Continua del Servicio. Para obtener más información sobre cómo establecer, desarrollar y mantener estructuras de apoyo adecuadas para carteras, programas y proyectos, consulte Oficinas de carteras, programas y proyectos (GC, 2008).
    </p>
    <p>
      
    </p>
    <p>
      En la sección 4.1.6.4 se incluye una descripción más detallada de las interfaces con otros procesos de gestión de servicios. En general, sin embargo, hay varias áreas genéricas a las que estos procesos contribuyen o que deben abordarse cuando se ejecuta la estrategia. Estos se ilustran en la Figura 4.3 y se analizan en las siguientes secciones.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Comunicar el plan estratégico" ID="ID_351273609" CREATED="1660228376662" MODIFIED="1660228593467" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Asegurarse de que el plan estratégico se comunique a las personas adecuadas es un requisito previo importante para implementar la estrategia. El plan generalmente se comunica de la siguiente manera:
    </p>
    <ul>
      <li>
        Distribuya una copia del plan a cada ejecutivo y parte interesada clave.
      </li>
      <li>
        Aunque partes del plan son confidenciales, puede ser útil producir un resumen del plan o una presentación para todos en la organización.
      </li>
      <li>
        Los miembros de la junta o los miembros del grupo directivo de TI deben recibir capacitación sobre cómo hablar sobre la estrategia, qué puntos enfatizar y cómo garantizar el cumplimiento en su área de la organización. Nunca se debe suponer que los ejecutivos que formaron parte del proceso de planificación estratégica sabrán comunicarlo. Esto se debe a que han sobrevivido a la producción del plan y dan por sentado la información y la redacción, donde una persona ajena al proceso podría encontrarlo confuso.
      </li>
      <li>
        Los aspectos clave de la estrategia, como la visión, la misión y los objetivos principales, deben estar presentes en todo el lugar de trabajo. carteles, salvapantallas, etc. que puedan recordar a los empleados la estrategia central en el trabajo del día a día.
      </li>
      <li>
        Las partes relevantes de la estrategia deben incluirse en las políticas, los procedimientos y los manuales de los empleados. Una gran organización requiere que cada gerente use objetivos estratégicos para definir metas e indicadores clave de desempeño (KPI) para su personal, que luego se usan como parte de la medición continua del desempeño.
      </li>
      <li>
        También podría ser útil proporcionar a los socios estratégicos clave, como proveedores e inversores, un resumen de los planes, con información sobre cómo esperan que los utilicen.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5.13 Ejecución de la estrategia: Otros procesos de gestión de servicio" FOLDED="true" ID="ID_1824123496" CREATED="1660170033795" MODIFIED="1660789657566" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Para un proveedor de servicios, ninguna estrategia puede ejecutarse sin poder administrar los servicios que brindará. Los procesos de gestión de servicios permiten al proveedor de servicios lograr la alineación entre los servicios y los resultados deseados de forma continua. Sin procesos de gestión de servicios, lo mejor que puede hacer el proveedor de servicios es reaccionar con prontitud a los clientes, pero a menudo no podrá seguir adelante para brindar servicios de alta calidad a lo largo del tiempo.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Otros procesos de gestión de servicio contribuyen con la ejecución de la estrategia en tres vías:" ID="ID_1785959808" CREATED="1660789661899" MODIFIED="1660789898052" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Proporcionan un sistema de gestión que formaliza cómo el proveedor de servicios gestionará los servicios. Por lo tanto, es una parte crucial de cómo se ejecutará la estrategia.
      </li>
      <li>
        La estrategia define una serie de oportunidades. Otros procesos de gestión de servicios definen los servicios para satisfacer esas oportunidades. Los procesos en la estrategia de servicio alinean la estrategia y la inversión de cada servicio con los objetivos estratégicos generales y la estrategia de inversión. Los procesos de diseño de servicios garantizan que los servicios estén diseñados para cumplir estos objetivos y que puedan gestionarse de forma eficaz. Los procesos de transición de servicios construyen, prueban y validan los servicios y definen exactamente qué información debe registrarse para garantizar que se puedan rastrear y administrar. Los procedimientos de operación del servicio aseguran que los servicios se entreguen y respalden adecuadamente. Los procesos de mejora continua del servicio (CSI) miden los logros reales de los servicios, identifican las brechas entre los requisitos y la entrega real, y recomiendan soluciones.
      </li>
      <li>
        Definen un plan de acción sobre cómo se gestionarán los servicios. Los procesos de gestión de servicios no solo gestionan servicios, también gestionan las actividades, las herramientas y las personas que prestan y dan soporte a los servicios. La gestión de servicios es un sistema de gestión que determina no solo qué servicios se utilizarán para lograr la estrategia, sino también cómo se gestionarán esos servicios.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Programas" ID="ID_1186632634" CREATED="1660789913704" MODIFIED="1660789957385" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Cuando no se hayan implementado formalmente otros componentes de la gestión del servicio, o cuando solo aborde un subconjunto de los aspectos de la gestión del servicio, la estrategia debe incluir un proyecto o programa para rectificar la situación. Este proyecto estratégico debe ser formalmente reconocido, aprobado y respaldado por los ejecutivos de la organización como parte de la estrategia general de la organización.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5.14 Ejecución de la estrategia: Alinear los activos con los resultados de los clientes" ID="ID_1306394749" CREATED="1660170085876" MODIFIED="1660790262454" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El proveedor de servicio necesita asegurar que sus activos de servicio están coordinados, controlados e implementados de tal forma que puedan proveer los niveles apropiados de servicio a los niveles acordados. La ejecución de la estrategia depende en la habilidad del proveedor de servicio para conocer que activos de servicio tiene, donde están localizados y como han sido implementados.
    </p>
    <p>
      
    </p>
    <p>
      En primer lugar, el proveedor de servicios debe poder articular los servicios que se prestan y a qué clientes. La cartera de servicios tiene esta información, junto con información sobre los resultados comerciales que permite cada servicio. La cartera de servicios también identifica quién es el propietario del servicio y quién está involucrado en la entrega y el soporte del servicio.
    </p>
    <p>
      
    </p>
    <p>
      En segundo lugar, el proveedor de servicios debe poder decidir cómo se prestarán estos servicios. Se producen modelos de arquitectura y servicio y procesos como la gestión de la capacidad, la gestión de la disponibilidad, la gestión del nivel de servicio, la gestión de los activos del servicio y la configuración trabajan juntos para definir el diseño y la configuración óptimos de los activos del servicio. El objetivo es aumentar el uso productivo de los activos, al mismo tiempo que se optimizan los costos.
    </p>
    <p>
      
    </p>
    <p>
      La forma en que los proveedores de servicios gestionan sus activos a menudo refleja la forma en que los clientes gestionan los suyos. Por ejemplo, un proveedor de servicios ofrece un servicio de correo electrónico inalámbrico. Esto aumenta el rendimiento de uno de los tipos de activos de clientes más críticos y costosos: los gerentes y el personal. Mediante el uso de correo electrónico inalámbrico, el cliente implementa estos activos de una manera que aprovecha al máximo sus capacidades productivas.
    </p>
    <p>
      
    </p>
    <p>
      De esta manera, los gerentes de ventas pasan más tiempo en el sitio con los clientes, los técnicos se envían rápidamente para cubrir las fallas de los equipos en el campo y los miembros del personal administrativo se concentran en ubicaciones estratégicas para mejorar la efectividad operativa. Para respaldar al cliente, el proveedor de servicios configura e implementa sus activos de una manera que respalda de manera efectiva las propias implementaciones del cliente. Puede requerir el diseño, implementación, operación y mantenimiento de mensajería segura y de alta disponibilidad en teléfonos inalámbricos o computadoras. Lo que importa es que los empleados del cliente puedan coordinar las actividades comerciales, acceder a las aplicaciones comerciales y controlar los procesos comerciales.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="4.1.5.15 Ejecución de la estrategia: Optimizar los factores críticos del éxito" ID="ID_1559903853" CREATED="1660170118331" MODIFIED="1660790513663" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El término &quot;factor crítico de éxito&quot; se refiere a cualquier aspecto de una estrategia, proceso, proyecto o iniciativa que necesita ser presentada en orden para que sea exitosa.
    </p>
    <p>
      
    </p>
    <p>
      Los factores críticos de éxito pueden ser habilidades específicas, herramientas, circunstancias, finanzas, apoyo ejecutivo o la finalización de otra actividad o proyecto.
    </p>
    <p>
      
    </p>
    <p>
      Las publicaciones principales de ITIL describen los factores críticos de éxito que se requieren para que cada proceso y cada etapa del ciclo de vida tenga éxito.
    </p>
    <p>
      
    </p>
    <p>
      En ITIL Service Strategy se define un nivel adicional de factores críticos de éxito. En este contexto, los factores críticos de éxito se relacionan específicamente con aquellas áreas que deben existir para que la organización pueda competir en una industria o espacio de mercado específico. La evaluación de la estrategia identificó una serie de factores estratégicos de la industria. Los factores críticos de éxito en este contexto son cualquier aspecto o propiedad de la organización que le permita lograr o cumplir con los factores estratégicos de la industria.
    </p>
    <p>
      
    </p>
    <p>
      En esta etapa de ejecución de la estrategia, la organización, los servicios, los procesos, las habilidades, las herramientas, etc. existentes se compararán con los factores estratégicos de la industria y se identificarán las deficiencias. Si la organización no está posicionada para cumplir con un factor estratégico de la industria, se iniciará un proyecto para implementar los factores críticos de éxito requeridos. Si la organización ha estado en el negocio durante algún tiempo, es probable que la mayoría de los factores críticos de éxito ya existan, pero es posible que deban optimizarse para hacer frente a los cambios en el entorno externo o interno.
    </p>
    <p>
      
    </p>
    <p>
      Cada proyecto debe estar vinculado a la estrategia, y formalmente aprobado y financiado por los ejecutivos de la organización. Si esto no se hace, la importancia y la relevancia del proyecto podrían perderse y el proyecto perdería prioridad.
    </p>
    <p>
      
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="4.1.5.16 Ejecución de la estrategia: Priorizar las inversiones" FOLDED="true" ID="ID_695687906" CREATED="1660170148015" MODIFIED="1660790692188" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Cada nuevo servicio o proyecto requerirá financiación. No es raro que se establezca una estrategia y luego se modifique después de revisar el análisis detallado de la inversión, simplemente porque no se han reservado suficientes fondos o porque el rendimiento anticipado no respalda la inversión. La gestión de la cartera de servicios garantizará que cada nuevo servicio propuesto, o cualquier cambio estratégico a un servicio existente, se analice para determinar el nivel de inversión requerido y el rendimiento propuesto de esa inversión.
    </p>
    <p>
      
    </p>
    <p>
      La PMO se asegurará de que se haga lo mismo con cualquier proyecto que se haya propuesto como parte de la ejecución de la estrategia. La gestión de la cartera de servicios también identificará si algún servicio depende de los proyectos propuestos. Esto asegurará que cualquier decisión de cancelar un proyecto se tome con una comprensión completa del impacto, y no solo por la necesidad de reducir costos.
    </p>
    <p>
      
    </p>
    <p>
      Una vez que se ha realizado un análisis de inversión para todos los nuevos servicios y proyectos y cambios estratégicos en los servicios existentes, se someten a los ejecutivos de la organización para una decisión final. Todos los proyectos y servicios serán aprobados y apoyados por los ejecutivos, junto con un compromiso de financiación. Si se retira la financiación en el futuro, los ejecutivos deberán hacerlo directamente y podrán evaluar el impacto de su decisión en la ejecución de la estrategia.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Priorizando las inversiones estratégicas basado en las necesidades del cliente" ID="ID_1991820817" CREATED="1660790695870" MODIFIED="1660791116064" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Un problema común que tienen los proveedores de servicios es priorizar las inversiones y la atención gerencial en el conjunto correcto de oportunidades. Las mejores oportunidades para los proveedores de servicios se encuentran en áreas donde una necesidad importante del cliente sigue sin satisfacerse adecuadamente.
    </p>
    <p>
      
    </p>
    <p>
      Esto se ilustra en la Figura 4.11 (según Ulwick, 2005). En este diagrama se ilustra una gama de requisitos y servicios del cliente. La parte inferior izquierda del gráfico muestra los requisitos que tendrían poco impacto comercial si se cumplieran y que no se cumplen bien (o no se cumplen en absoluto). La parte superior derecha del gráfico muestra los requisitos que tienen un alto impacto en el negocio cuando se cumplen y se están cumpliendo muy bien.
    </p>
    <p>
      
    </p>
    <p>
      Cualquier servicio que encaje en el triángulo de la Figura 4.11 brindaría el nivel de servicio adecuado para el grado de impacto en el negocio. Cualquier servicio a la izquierda del triángulo brindaría un nivel de servicio demasiado alto (a un costo potencialmente más alto que el necesario). ). Cualquier servicio a la derecha del triángulo es inadecuado para los requisitos comerciales.
    </p>
    <p>
      
    </p>
    <p>
      Las carteras de servicios deben ampliarse para respaldar las áreas de oportunidad a la derecha del gráfico. Por lo general, esto significa que existe la necesidad de que los servicios brinden niveles adicionales de utilidad y garantía. Sin embargo, los gerentes no deben pasar por alto los costos y riesgos en tales áreas. Por lo general, existen fuertes razones por las cuales ciertas necesidades de los clientes permanecen insatisfechas (ejemplo, podría costar más de lo que se espera recibir, o la tecnología podría simplemente no existir para cumplir esa necesidad). Rendimiento e innovación disruptiva es usualmente necesaria para entregar valor satisfactoriamente en áreas de oportunidad sub atendidas.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5.17 Medición y evaluación" ID="ID_70399947" CREATED="1660170169379" MODIFIED="1660791463168" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Esta etapa de la gestión de la estrategia es realizada por varias áreas diferentes, que incluyen:
    </p>
    <ul>
      <li>
        <b>Clientes y usuarios</b>&nbsp;Los clientes y usuarios de los servicios a menudo serán los primeros en determinar si una estrategia se está logrando o no, ya que están monitoreando el desempeño de los resultados del negocio.
      </li>
      <li>
        <b>Procesos de gestión de servicios</b>&nbsp;Si cada proceso se ha diseñado para ejecutar y habilitar la estrategia de la organización, sus métricas se pueden utilizar para medir la eficacia de la estrategia.
      </li>
      <li>
        <b>Mejora continua del servicio</b>&nbsp;La medición continua del desempeño de los servicios y los procesos de gestión del servicio mostrarán cualquier desviación de los resultados previstos. Estos se pueden relacionar con áreas estratégicas específicas y las oportunidades de mejora se pueden registrar en el registro CSI para que posteriormente se considere su inclusión en los planes de mejora del servicio.
      </li>
      <li>
        <b>Los Ejecutivos de la Organización</b>&nbsp;Los ejecutivos necesitarán un tablero para indicar el desempeño de la organización en relación con su estrategia. Podrán determinar si hay áreas que no están funcionando al nivel requerido, o que requieren más de los niveles de inversión acordados, e investigarlos con más detalle.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="4.1.5.18 Medición y evaluación: Mejora continua del servicio" FOLDED="true" ID="ID_1853481383" CREATED="1660170185319" MODIFIED="1660791872191" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las actividades de CSI miden y evalúan el logro de la estrategia a lo largo del tiempo. CSI contribuye a la medición y evaluación de la estrategia de dos maneras principales.
    </p>
    <p>
      
    </p>
    <p>
      En primer lugar, las actividades de CSI identifican áreas que no se están desempeñando según las expectativas y, por lo tanto, amenazan &quot;el logro de la estrategia&quot;. Las actividades de CSI proporcionan información sobre las fases de generación y ejecución de la estrategia del proceso. Específicamente, brindan retroalimentación sobre:
    </p>
    <ul>
      <li>
        Adhesión a las políticas definidas como parte del posicionamiento.
      </li>
      <li>
        Si se están cumpliendo los planes, en términos de tiempo, presupuesto y objetivos. Aunque las actividades de CSI no son responsables de si los proyectos se completan a tiempo, según el presupuesto y las especificaciones, pueden cuantificar el impacto general de las desviaciones de los planes. Esta retroalimentación se puede utilizar para modificar los planes o acelerar las acciones para completar los proyectos.
      </li>
      <li>
        Si los patrones de acción identificados fueron apropiados y si se están utilizando de manera efectiva.
      </li>
      <li>
        Logro de resultados y desempeño de los activos que los respaldan. Esta información se puede utilizar para mejorar la alineación de los activos para salidas, o para mejorar el desempeño en general de esos activos.
      </li>
      <li>
        Definición efectiva y consecución de factores críticos de éxito.
      </li>
      <li>
        Consecución de los objetivos de retorno de la inversión de cada proyecto, iniciativa o servicio.
      </li>
    </ul>
    <p>
      En segundo lugar, las actividades de CSI establecen la línea de base para la próxima ronda de evaluaciones de estrategia. Dado que la organización existe (y es en sí misma) un entorno en constante cambio, la estrategia debe evaluarse y revisarse continuamente. Las actividades de CSI ayudan en este proceso al monitorear e informar sobre los cambios en los aspectos relevantes del entorno interno, la capacidad de expandir las oportunidades de espacio de mercado y la capacidad de cumplir con los factores estratégicos de la industria.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="La importancia de los clientes y usuarios en las mediciones de servicios" ID="ID_1632986303" CREATED="1660791909645" MODIFIED="1660792070811" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Aunque los proveedores de servicios confían en las métricas y las mediciones para ser proactivos en la prestación y el soporte de los servicios, la realidad es que existe un conjunto significativo de métricas sobre las que no tienen control. Independientemente de qué tan bien se hayan instrumentado los servicios y qué tan bien se entreguen, el usuario del servicio aún los medirá, a menudo utilizando criterios y técnicas que son ajenos al proveedor del servicio.
    </p>
    <p>
      
    </p>
    <p>
      Por ejemplo, un minorista en línea realizó más de 100 000 transacciones por día. Todos los clientes pagaron con tarjeta de crédito y todas las transacciones fueron procesadas por una empresa de servicios financieros de terceros. Aproximadamente el 1% de todas las transacciones se perdió entre el minorista y la empresa de servicios financieros.
    </p>
    <p>
      
    </p>
    <p>
      A la empresa de servicios financieros (el proveedor del servicio) le resultó imposible detectar las transacciones faltantes, porque nunca llegaron. Se llegó a un acuerdo mediante el cual la empresa minorista (su cliente) realizó una conciliación de todas las transacciones al final del día y proporcionó la información faltante para el procesamiento nocturno.
    </p>
    <p>
      
    </p>
    <p>
      Los proveedores de servicios deben ser conscientes de que, en muchos casos, no podrán medir algo que es importante para el cliente. En estos casos, lo importante es que el cliente asuma la responsabilidad de realizar la medición, y el proveedor del servicio acordará una respuesta adecuada a las excepciones.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Publicación CSI" ID="ID_1896254696" CREATED="1660791880409" MODIFIED="1660791905210" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Como hay una publicación completa dedicada al tema, esta sección no proporcionará detalles, excepto para señalar las principales áreas cubiertas. La mejora continua del servicio de ITIL proporciona una guía detallada sobre las siguientes áreas:
    </p>
    <ul>
      <li>
        Definición de lo que debe medirse. Esta área se enfoca en comprender qué elementos son importantes para lograr la estrategia y si, de hecho, se pueden medir.
      </li>
      <li>
        Definir cómo medir estos elementos, dónde obtener datos y qué herramientas se requieren para realizar la medición.
      </li>
      <li>
        Establecimiento de un seguimiento para permitir la recopilación de datos.
      </li>
      <li>
        Definir cómo se van a informar las medidas, a quién y qué se espera que hagan con los informes.
      </li>
      <li>
        Identificar oportunidades de mejora registrándolas en el registro del CSI.
      </li>
      <li>
        Elaborar y ejecutar planes de mejora del servicio que resuelvan las desviaciones de la estrategia.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5.19 Medición y evaluación: Expansión y crecimiento" FOLDED="true" ID="ID_1303772267" CREATED="1660170211892" MODIFIED="1660792410538" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      A medida que la organización logra su estrategia existente, mejora su capacidad de prestar servicios a sus espacios de mercado existentes. Esto plantea preguntas como: '¿Podemos brindar los mismos servicios a nuevos clientes? ‘¿Podemos proporcionar nuevos servicios a los clientes existentes? y '¿Podemos ofrecer nuevos servicios a nuevos clientes?' Una vez que las estrategias de servicio están vinculadas a los espacios de mercado, es más fácil tomar decisiones sobre carteras de servicios, diseños, operaciones y mejoras a largo plazo. Las inversiones en activos de servicio, como conjuntos de habilidades, conocimientos, procesos e infraestructura, están impulsadas por los factores críticos de éxito para un mercado o espacio determinado. El crecimiento y la expansión de cualquier negocio son menos riesgosos cuando están anclados en capacidades centrales y un desempeño demostrado.
    </p>
    <p>
      
    </p>
    <p>
      Las estrategias de expansión exitosas a menudo se basan en aprovechar los activos de servicio existentes y las carteras de clientes para impulsar un nuevo crecimiento y rentabilidad. La figura 4.12 ilustra cómo una organización expande su negocio moviéndose a espacios de mercado adyacentes. En este caso, la organización puede utilizar los activos de servicio existentes (recursos y capacidades) para aprovechar las oportunidades relacionadas con aquellos a los que están sirviendo actualmente.
    </p>
    <p>
      
    </p>
    <p>
      En la Figura 4.12, la organización ha optado por expandirse a una serie de espacios de mercado. Siguiendo los ejemplos utilizados en la sección 3.4.4, la organización ha optado por pasar de U2.A7 (conectar e integrar aplicaciones) a U2.A6 y U2.A8 (conectar e integrar información e infraestructura además de aplicaciones). En este ejemplo, la organización ha desarrollado un enfoque sólido para administrar, operar y mantener aplicaciones, y ha encontrado una manera de utilizar ese enfoque para otras tecnologías.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="La exposición de costos y riesgos" ID="ID_191717263" CREATED="1660792413559" MODIFIED="1660792574323" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La exposición a costos y riesgos es mucho menor en este enfoque en comparación con las expansiones ad hoc, que son de naturaleza oportunista. Esto se debe a que la expansión a espacios de mercado adyacentes aprovecha los activos de servicio que son comunes en todos los espacios de mercado. Esto significa que las inversiones adicionales están cubiertas en espacios de mercado nuevos y existentes. Si por cualquier motivo fracasa la ampliación o no se materializan oportunidades de negocio, se podrá rescatar la inversión para las nuevas inversiones realizadas. Para reducir aún más los riesgos de las estrategias de expansión, lo mejor es aprovechar la presencia en espacios de mercado que han logrado un crecimiento suficiente. El crecimiento y la madurez podrían significar mejorar los resultados en los espacios de mercado existentes o expandir la cartera a otros espacios de mercado con un alto potencial de éxito.
    </p>
    <p>
      
    </p>
    <p>
      La expansión y el crecimiento también ocurren dentro de un solo espacio de mercado o cliente, por ejemplo, al expandirse a nuevos clientes en los espacios de mercado que ya atiende la organización; o ampliando los servicios ofrecidos a los clientes existentes. La expansión dentro de un espacio de mercado único podría implicar:
    </p>
    <ul>
      <li>
        Ampliaciones de los contratos existentes
      </li>
      <li>
        Aumentos en la demanda de los clientes por un servicio existente
      </li>
      <li>
        Proporcionar servicios complementarios (por ejemplo, proporcionar soporte de vida temprana para todas las nuevas versiones de software).
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Expansión y crecimiento de un negocio dentro un solo cliente o espacio de mercado" ID="ID_1535141144" CREATED="1660792582737" MODIFIED="1660792743544" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La Figura 4.13 ilustra cómo es posible expandir y hacer crecer un negocio dentro de un solo cliente o espacio de mercado. En el primer ejemplo, el negocio se expande al proporcionar servicios adicionales al mismo cliente. Cada nueva oportunidad que surge dentro de ese cliente es una nueva dirección estratégica, ya que requerirá nuevos servicios, o aplicar los mismos servicios a una nueva área dentro del cliente. En este ejemplo, la capacidad histórica de la organización para comprender y cumplir con los requisitos únicos del cliente es lo más importante para el crecimiento. El mayor riesgo es que la organización no tenga las habilidades y capacidades adecuadas para diseñar y brindar cada nuevo servicio. La falta de activos de servicio apropiados es, por lo tanto, una limitación potencial.
    </p>
    <p>
      
    </p>
    <p>
      En el segundo ejemplo, la organización atrae nuevos clientes con requisitos similares a los clientes existentes, ofreciéndoles servicios similares. En este caso, el historial de calidad del servicio de la organización es más importante para el crecimiento. El mayor riesgo en este tipo de crecimiento es que la organización no pueda hacer frente a la mayor demanda de servicios y la capacidad se convierta en una limitación importante.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.5.20 Gestión estratégica para proveedores de servicio de TI internos" FOLDED="true" ID="ID_1352218594" CREATED="1660170236339" MODIFIED="1660793194510" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      El procesos descrito en la sección 4.1.5 aplica directamente a las organizaciones proveedores de servicio externas, pero esto también aplica a los proveedores internas (Tipo I y Tipo II). Las organizaciones internas de TI a menudo cometen el error de pensar que no desempeñan un papel estratégico en la organización y que deben limitar sus actividades a la planificación y ejecución tácticas. En muchas organizaciones, incluso los miembros del personal técnico superior no tienen conocimiento de la estrategia organizacional y son tomados por sorpresa por las fluctuaciones en la actividad comercial y la demanda.
    </p>
    <p>
      
    </p>
    <p>
      Sin embargo, TI es una parte estratégica de la mayoría de los negocios y es importante que la estrategia de la organización de TI esté estrechamente alineada y medida con la estrategia comercial. Esto se hace de dos maneras:
    </p>
    <ul>
      <li>
        Asegurar que la estrategia de TI esté estrechamente vinculada a la estrategia de negocios. Por ejemplo, los planes de acción diseñados durante la estrategia comercial incluirán acciones para los proveedores de servicios de TI internos. Además, muchas de las actividades en la ejecución de la estrategia dependen de TI.
      </li>
      <li>
        Siguiendo los mismos pasos que en la sección 4.1.5 para definir la estrategia de TI, pero utilizando un alcance más limitado, insumos específicos y parámetros definidos.
      </li>
    </ul>
    <p>
      Mientras que la estrategia general de la organización es responsabilidad de sus ejecutivos, la estrategia de los proveedores de servicios internos es responsabilidad de los ejecutivos de TI, dirigidos por el CIO. En organizaciones más grandes, es probable que la gestión de la estrategia sea ejecutada por una persona o equipo dedicado que informe al CIO.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Evaluación estratégica para proveedores de servicio internos" ID="ID_71293505" CREATED="1660793196641" MODIFIED="1660793437130" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La evaluación estratégica es similar al proceso de gestión de estrategias descrito anteriormente, pero define su entorno de forma más específica y utiliza la información recopilada por otras unidades de negocio para proporcionar información sobre sus entornos externos. Por ejemplo, es poco probable que TI necesite realizar una investigación sobre el entorno socioeconómico si ya se ha hecho durante la evaluación estratégica del negocio. Los altos ejecutivos de TI deben obtener esta información y asegurarse de que el grupo directivo de TI (o al menos los gerentes de TI senior) comprendan el significado y las implicaciones de esta información.
    </p>
    <p>
      
    </p>
    <p>
      El propósito de la evaluación estratégica es determinar la actual situación de la organización TI y que cambios podrían impactar en un futuro deseable. La evaluación también resaltará las áreas las cuales limitarán o evitarán el progreso hacia las metas actuales o para adaptarse al cambio.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="El ambiente interno que debe ser evaluado por las organizaciones TI incluye:" ID="ID_917859763" CREATED="1660793461178" MODIFIED="1660793893518" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        <b>La estrategia de negocio de la organización</b>&nbsp;Este es el marco más importante para la gestión de la estrategia de TI, ya que proporciona los parámetros dentro de los cuales TI debe trabajar. Al mismo tiempo, las nuevas tecnologías y prácticas en la industria pueden hacer posible que la organización amplíe los límites de su estrategia existente. Por ejemplo, las soluciones en la nube hacen posible que la empresa cambie su posicionamiento, reelabore su cartera de servicios y llegue a nuevos clientes. De esta manera, la estrategia de TI ayuda a moldear la estrategia comercial, incluso mientras la estrategia comercial la moldea.
      </li>
      <li>
        <b>Servicios existentes</b>&nbsp;Estos reflejan el posicionamiento actual, la diferenciación y la estrategia de la organización de TI, y también formarán una base para el crecimiento potencial.
      </li>
      <li>
        <b>Tecnologías existentes</b>&nbsp;Incluyendo arquitecturas actuales, cartera de aplicaciones, activos, recursos, etc. Esta información ayudará a la organización de TI a comprender cuáles son sus capacidades y, si se requiere crecimiento, dónde deberá desarrollarse.
      </li>
      <li>
        <b>Gestión de servicios de TI</b>&nbsp;Una evaluación del nivel actual de madurez de la gestión de servicios de TI resaltará cualquier limitación o límite para definir cómo se definen, construyen y gestionan los servicios. Por ejemplo, si la gestión financiera de los servicios de TI solo puede realizar un seguimiento de los gastos generales y asignarlos a las unidades de negocio, la organización de TI tendrá dificultades para cuantificar el valor de los servicios individuales. La estrategia de TI puede necesitar cambiar la forma en que se rastrean y reportan los costos de TI.
      </li>
      <li>
        <b>Recursos humanos</b>&nbsp;Las habilidades y conocimientos actuales de las personas que trabajan en la organización son clave para que una oportunidad pueda ser aprovechada o no.
      </li>
      <li>
        <b>Relación con las unidades de negocio</b>&nbsp;Para los proveedores de servicios internos, este es un ejercicio interesante, porque ya han evaluado las unidades de negocio como clientes; ahora también necesitan evaluar las dependencias entre TI y otras unidades de negocios como socios en la misma organización, cada uno esforzándose por alcanzar los mismos objetivos. Esta parte de la evaluación se centrará en la calidad de la relación entre las unidades de negocio. Por ejemplo, muchos departamentos tienen que pasar tanto tiempo tratando de venderse a sí mismos a otras unidades de negocio así como lo hacen a clientes externos.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="El ambiente externo que debe ser evaluado por las organizaciones TI específicamente incluye:" ID="ID_893300130" CREATED="1660793847024" MODIFIED="1660794524110" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        <b>Otras organizaciones</b>&nbsp;Cómo otras organizaciones están administrando y usando TI
      </li>
      <li>
        <b>Tasas de gasto de TI de la industria</b>&nbsp;Esto no debe usarse para establecer un objetivo de gasto, pero es un buen indicador de cómo se gasta el dinero y por qué. (Puede haber una tendencia que indique un cambio en el entorno; por ejemplo, un mayor gasto en virtualización puede indicar que más organizaciones están recurriendo a una infraestructura convergente para lograr una mayor eficiencia o agilidad).
      </li>
      <li>
        <b>Estrategias de proveedores y hojas de ruta de productos</b>&nbsp;Es importante saber si la tecnología que respalda su estrategia actual continuará haciéndolo. Los cambios en el catálogo de servicios o productos del proveedor pueden ser el resultado de cambios en la concesión de licencias, fusiones y adquisiciones (donde el conjunto de productos actual ya no es compatible y se impone una compra alternativa), la tecnología se vuelve obsoleta, la desagregación (por ejemplo, cuando los componentes de un conjunto de productos ahora tienen que comprarse por separado), etc. Las organizaciones de TI que no tengan en cuenta las estrategias de los proveedores podrían enfrentarse a gastos inesperados o interrupciones.
      </li>
      <li>
        <b>Socios</b>&nbsp;Es importante que se identifiquen los compromisos y obligaciones existentes hacia y desde los socios. Estos pueden necesitar ser cambiados, o pueden poner un límite sobre qué nivel de cambio estratégico es posible.
      </li>
      <li>
        <b>Tendencias tecnológicas</b>&nbsp;Por ejemplo, ¿cuáles son las implicaciones de las tecnologías convergentes? ¿Facilitarán la entrega y el soporte de servicios u ofrecerán nuevas oportunidades, o ambas cosas?
      </li>
      <li>
        <b>Clientes</b>&nbsp;Esta es una situación compleja para los proveedores de servicios internos porque hay dos tipos de clientes a los que atienden. Por un lado, brindan servicios a otras unidades de negocio ('clientes internos'), quienes a su vez los utilizan para brindar servicios a 'clientes externos'. TI tiene que evaluar las necesidades de los clientes de su unidad de negocio en función de los resultados de negocio que la unidad de negocio está tratando de lograr y cómo TI permite esos resultados. Esto ayudará a TI a asegurar que sus planes tienen una cobertura adecuada de las necesidades de los clientes. Sin embargo, la mayoría de las organizaciones de TI también brindan servicios estratégicos (a menudo generadores de ingresos) directamente a clientes externos, por ejemplo, pedidos en línea o presentación de declaraciones de impuestos. En un sentido muy real, estos son clientes tanto de TI como de otras unidades comerciales, y la estrategia de ambas debería reflejar esto.
      </li>
      <li>
        <b>Estándares o requisitos reglamentarios para TI</b>&nbsp;Aunque la legislación como SOX o Basilea II está dirigida al negocio, TI tiene un papel muy importante que desempeñar para garantizar su implementación y el cumplimiento de la organización. Otros estándares, como ISO/IEC 27001, son directamente aplicables a TI, y si este es un requisito, el costo del cumplimiento y la certificación debe reflejarse tanto en las estrategias comerciales como de TI.
      </li>
      <li>
        <b>Operaciones</b>&nbsp;Se evalúa la madurez, eficacia y eficiencia actuales de las operaciones de TI. ITIL se puede utilizar como una &quot;lista de verificación&quot; para garantizar que los procesos y funciones necesarios para respaldar la estrategia estén implementados.
      </li>
      <li>
        <b>La relación entre el desarrollo de aplicaciones y las operaciones</b>&nbsp;Una relación disfuncional entre estos dos grupos es más que un simple conflicto político; representa una organización que aún no ha alcanzado un equilibrio entre el enfoque en la funcionalidad y el enfoque en la capacidad de administración (o utilidad y garantía). Aunque ambos son esenciales, muchas organizaciones están llenas de situaciones en las que cada grupo trata de apropiarse de la relación con los clientes (y sus presupuestos).
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Generación de estrategias para proveedores de servicio internos" ID="ID_1225642554" CREATED="1660794534700" MODIFIED="1660794886038" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Idealmente, la generación de una estrategia de TI debe hacerse en conjunto con la planificación estratégica y táctica del negocio (ver Figura 4.2). A medida que la empresa define su perspectiva y posición, esta información debe comunicarse a los ejecutivos de TI que preparan la estrategia de TI.
    </p>
    <p>
      
    </p>
    <p>
      La(s) organización(es) de TI pueden entonces comenzar a formular su propia perspectiva y posiciones, y comenzar a proporcionar información sobre el desarrollo de la estrategia comercial. A medida que la estrategia de negocio va elaborando sus planes, estos son comunicados a los ejecutivos de TI, quienes los prueban y validan, u ofrecen más alternativas. Al mismo tiempo que se hace esto para TI, también se hace para otras unidades comerciales, como fabricación, marketing, etc. Todas las unidades comerciales participan en la comunicación continua y el refinamiento de la estrategia de cada una.
    </p>
    <p>
      
    </p>
    <p>
      Una vez que se hayan tomado las decisiones estratégicas comerciales y se hayan finalizado y coordinado los planes estratégicos con todas las unidades comerciales, el plan estratégico de TI se someterá a una verificación para garantizar que sea coherente con la estrategia comercial.
    </p>
    <p>
      
    </p>
    <p>
      Se produce un plan estratégico de TI y se definen los patrones de acción necesarios para asegurar su éxito. A medida que la estrategia comercial se traduzca en planes tácticos para la ejecución, el plan estratégico de TI y los patrones de acción se validarán nuevamente para garantizar que los planes comerciales sean compatibles. Los planes tácticos de TI se redactan, se comparan con los planes tácticos comerciales y se finalizan.
    </p>
    <p>
      
    </p>
    <p>
      El plan estratégico de TI será similar en estructura y contenido al plan estratégico de negocios, pero su contenido será específico para la organización de TI.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Ejecución de la estrategia para proveedores de servicio internos" ID="ID_1467116690" CREATED="1660794891165" MODIFIED="1660794947409" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Al igual que con los proveedores de servicios externos, la ejecución de estrategias será una combinación de las actividades normales de los procesos de gestión de servicios y el inicio de una serie de proyectos, generalmente coordinados a través de la PMO.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Medición y evaluación para proveedores de servicio internos" ID="ID_748930400" CREATED="1660794949555" MODIFIED="1660795301205" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La mejora continua del servicio es el principal mecanismo de medición y evaluación, utilizando las mismas técnicas y procesos que para los proveedores de servicios externos.
    </p>
    <p>
      
    </p>
    <p>
      Sin embargo, las actividades relacionadas con el crecimiento y la expansión del negocio son muy diferentes. En los proveedores de servicios externos, el objetivo es identificar nuevos espacios de mercado y clientes que puedan expandir la estrategia (y en las organizaciones comerciales, los ingresos) de la organización. En el sector público, la capacidad de demostrar valor por dinero es fundamental.
    </p>
    <p>
      
    </p>
    <p>
      Los proveedores de servicios internos no deben tratar de hacer crecer su negocio, sino que deben tratar de encontrar mejores formas de lograr los resultados comerciales deseados de la organización para la que trabajan, o de reducir el costo de la provisión de servicios. El concepto de expandirse a espacios de mercado adyacentes, o encontrar nuevos clientes en espacios de mercado existentes, podría ser útil para enmarcar el pensamiento de TI, pero a veces esto resulta en organizaciones de TI que tratan de implementar sus propias estrategias, en lugar de habilitar que de la organización es soportado.
    </p>
    <p>
      
    </p>
    <p>
      TI es, sin embargo, una parte muy importante del crecimiento y expansión del negocio, desde que el negocio usa servicios TI para incrementar sus capacidades. Cada vez que el negocio decide crecer o expandir, TI será parte del proceso - sea directamente a través del proceso de gestión&nbsp;&nbsp;estratégico del negocio, o indirectamente a través de la gestión del portafolio de servicios.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="¿Cuándo un proveedor de servicios cambia la estrategia de negocio?" ID="ID_1718553318" CREATED="1660795303487" MODIFIED="1660795404281" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Aunque el papel de los proveedores de servicios internos es apoyar la estrategia de la organización de la que forman parte, hay momentos en los que pueden tomar la iniciativa en la definición de la estrategia empresarial.
    </p>
    <p>
      
    </p>
    <p>
      Esto sucede cuando se pone a disposición una nueva tecnología que permite a la empresa cambiar su estrategia, abriendo nuevos mercados, posibilitando nuevos servicios o cambiando la forma en que funciona la organización.
    </p>
    <p>
      
    </p>
    <p>
      Algunos ejemplos han incluido el correo electrónico, Internet, Intercambios Electrónicos de Datos (¡ED!) y computación en la nube. Si la organización de TI comprende y se integra en el negocio, está muy bien posicionada para investigar y proponer estas innovaciones como base de una nueva estrategia comercial. Este tipo de investigación generalmente se realiza como parte del proceso de gestión de la capacidad (consulte ITIL Diseño de servicios).
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="4.1.6 Disparadores, entradas, salidas e interfaces" FOLDED="true" ID="ID_176793295" CREATED="1660795414673" MODIFIED="1660795448362">
<node TEXT="4.1.6.1 Disparadores" ID="ID_914983558" CREATED="1660795456337" MODIFIED="1660868357325" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los desencadenantes de la gestión de la estrategia para los servicios de TI incluyen:
    </p>
    <ul>
      <li>
        <b>Ciclos de planificación anual</b>&nbsp;La gestión de la estrategia para los servicios de TI se utiliza para revisar y planificar anualmente.
      </li>
      <li>
        <b>Nueva oportunidad de negocio</b>&nbsp;La gestión de estrategias para servicios de TI se utiliza para analizar, establecer objetivos, perspectivas, posiciones, planes y patrones para nuevas oportunidades de negocio o de servicio.
      </li>
      <li>
        <b>Cambios en entornos internos o externos</b>&nbsp;La gestión de la estrategia para los servicios de TI evaluará el impacto de los cambios ambientales en los planes estratégicos y tácticos existentes.
      </li>
      <li>
        <b>Fusiones o adquisiciones</b>&nbsp;La fusión o adquisición de otra empresa dará lugar a un análisis detallado y la definición de la estrategia de la nueva organización.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="4.1.6.2 Entradas" ID="ID_863019408" CREATED="1660795467763" MODIFIED="1660878349634" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las entradas para la gestión de estrategias para servicios de TI incluyen:
    </p>
    <ul>
      <li>
        Planes existentes
      </li>
      <li>
        Investigación sobre aspectos del entorno por parte de organizaciones de investigación especializadas
      </li>
      <li>
        Estrategias de proveedores y hojas de ruta de productos, que indican el impacto (y las posibles oportunidades) de la tecnología nueva o cambiante
      </li>
      <li>
        Entrevistas con clientes y planes estratégicos para indicar posibles requisitos futuros
      </li>
      <li>
        Cartera de servicios para indicar los compromisos de servicio actuales y planificados para el futuro
      </li>
      <li>
        Informes de servicio para indicar la eficacia de la estrategia
      </li>
      <li>
        Informes de auditoría que indican el cumplimiento con (o desviación de) la estrategia de la organización.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="4.1.6.3 Salidas" ID="ID_1266342240" CREATED="1660795475666" MODIFIED="1660878499196" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los resultados de la gestión de la estrategia para los servicios de TI incluyen:
    </p>
    <ul>
      <li>
        Planes estratégicos: en este contexto, especialmente la estrategia de servicio
      </li>
      <li>
        Planes tácticos que identifican cómo se ejecutará la estrategia
      </li>
      <li>
        Cronogramas de revisión de la estrategia y documentación
      </li>
      <li>
        Declaraciones de misión y visión
      </li>
      <li>
        Políticas que muestran cómo se deben ejecutar los planes, cómo se diseñarán, harán la transición, operarán y mejorarán los servicios
      </li>
      <li>
        Requerimientos estratégicos para nuevos servicios, y entrada en la cual los servicios existentes necesitan ser cambiados. La gestión de la estrategia para los servicios de TI también articulará qué resultados comerciales deben alcanzarse y cómo los servicios lo lograrán.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="4.1.6.4 Interfaces" ID="ID_1037323575" CREATED="1660795486724" MODIFIED="1660879053476" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Las principales estrategias de gestión para las interfaces de servicios de TI incluyen:
    </p>
    <ul>
      <li>
        La gestión de la estrategia para los servicios de TI interconecta y dirige todos los procesos de gestión de servicios, ya sea directa o indirectamente, el Capítulo 8 define esto en detalle. En lugar de discutir las interfaces con cada proceso, esta sección destacará las principales interfaces, como se muestra en la Figura 4.3,
      </li>
      <li>
        La gestión de la estrategia para los servicios de TI proporciona las pautas y el marco dentro del cual el se definirá y gestionará la cartera de servicios. Específicamente, la gestión de la estrategia para los servicios de TI proporciona los objetivos, las políticas y los límites que se deben utilizar para evaluar cada nuevo servicio o cambio estratégico en un servicio existente. La cartera de servicios proporciona a la gestión de estrategias para los servicios de TI información importante sobre el tipo de servicios que se encuentran actualmente en la cartera de servicios o en el catálogo de servicios, y qué objetivos estratégicos se han diseñado para cumplir. Esto ayudará en la evaluación estratégica y también en la evaluación de los espacios de mercado actuales y futuros.
      </li>
      <li>
        La gestión de estrategias para los servicios de TI proporciona información a la gestión financiera para indicar qué tipos de rendimientos se requieren y dónde se deben realizar las inversiones. La gestión financiera, a su vez, proporciona la información financiera y las herramientas para permitir que la gestión estratégica de los servicios de TI priorice acciones y planes.
      </li>
      <li>
        Aunque la gestión de la estrategia para los servicios de TI no define los requisitos detallados del diseño del servicio, proporciona información para el diseño del servicio. Específicamente, identifica las políticas que deben tenerse en cuenta al diseñar servicios, las restricciones dentro de las cuales los equipos de diseño deben trabajar y una clara priorización del trabajo. Los procesos de diseño de servicios proporcionarán información sobre la gestión de la estrategia de los servicios de TI para permitir la medición y evaluación de los servicios que se están diseñando.
      </li>
      <li>
        La gestión de estrategias para los servicios de TI permite que la transición del servicio priorice y evalúe los servicios creados para garantizar que cumplan con la intención original y los requisitos estratégicos de los servicios. Si se detecta alguna variación durante la transición del servicio, será necesario retroalimentarla a la gestión de la estrategia para los servicios de TI para que se pueda revisar la estrategia existente o para que se pueda tomar una decisión sobre la prioridad y la validez del servicio.
      </li>
      <li>
        La gestión del conocimiento juega un papel importante en la estructuración de la información que se utiliza para tomar decisiones estratégicas. Permite a los planificadores estratégicos comprender el entorno existente, su historia y su dinámica, y tomar decisiones informadas sobre el futuro.
      </li>
      <li>
        Aunque la gestión de la estrategia para los servicios de TI está bastante alejada de las operaciones diarias, existen algunos vínculos importantes, especialmente en términos de la ejecución de las prioridades estratégicas, y en la capacidad de medir si la estrategia se está cumpliendo. Las herramientas y procesos operativos deben asegurar que se hayan alineado con los objetivos estratégicos y los resultados comerciales deseados. Adicionalmente, el monitoreo de los entornos operativos debe estar instrumentado para que la ejecución de las actividades operativas indique si la estrategia es efectiva o no. Por ejemplo, si un objetivo estratégico es que una nueva oportunidad puede resultar en 10.000 nuevos clientes por mes, la actividad operativa requerida para satisfacer esta demanda debe coincidir con lo previsto.
      </li>
      <li>
        La mejora continua del servicio ayudará a evaluar si la estrategia se ha ejecutado de manera efectiva y si ha cumplido sus objetivos (es decir, las actividades de CSI medirán el cumplimiento de los planes y políticas estratégicos, y también medirán si se lograron los resultados esperados). Cualquier desviación será reportada a la gerencia de estrategia de los servicios de TI, que trabajará en la mejora del proceso o en el ajuste de la estrategia.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.7 Gestión de la Información" ID="ID_1070473739" CREATED="1660795499333" MODIFIED="1660879956878" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      La documentación y la información necesarias para una gestión estratégica eficaz de los servicios de TI se han descrito a lo largo de la sección 4.1, pero a continuación se ofrece una descripción general de las principales fuentes:
    </p>
    <ul>
      <li>
        Información sobre el entorno externo, incluida la investigación de mercado, la investigación de las prácticas de los competidores, las tendencias económicas y políticas, las direcciones e innovaciones tecnológicas, los puntos de referencia de la industria, los requisitos reglamentarios, etc. Es de especial importancia cualquier factor que indique que la organización necesita invertir para poder cumplir con un factor industrial estratégico particular.
      </li>
      <li>
        Resultados de las evaluaciones del entorno interno, que incluyen informes formales de evaluación, autoevaluación, entrevistas, sondeos de opinión de los empleados, etc.
      </li>
      <li>
        Información sobre las necesidades y niveles de satisfacción del cliente, capacidades de servicio actuales y niveles de desempeño.
      </li>
      <li>
        Gestión de la estrategia para la documentación de los servicios de TI, incluidos los planes estratégicos, tácticos y operativos. Los planes del período previo de planificación proveen una entrada valiosa para el período actual. Todos los planes son importantes para asegurar actividades concertadas entre las unidades de negocio. Estas proveen una herramienta valiosa en la educación del equipo, y la gestión de su desempeño hacia el cumplimiento de los objetivos organizacionales.
      </li>
      <li>
        La cartera de servicios proporciona información valiosa sobre las capacidades y los recursos actuales disponibles en la organización y cómo se implementan actualmente. También proporciona información sobre la inversión futura propuesta, que deberá evaluarse a la luz de los cambios de una estrategia anterior a una nueva estrategia.
      </li>
      <li>
        La información de gestión financiera está “evaluando espacios de mercado potenciales y si es factible que el proveedor de servicios invierta en servicios destinados a esas oportunidades.
      </li>
      <li>
        La gestión de las relaciones comerciales y la gestión de la demanda son fuentes críticas de información sobre lo que necesitan los clientes, por qué lo necesitan y cómo lo utilizarán. Esta información es útil, no solo para definir la posición y los planes, sino también para comprender los patrones de acción que asegurarán el éxito de la estrategia.
      </li>
      <li>
        La mejora continua del servicio proporciona retroalimentación que es vital para evaluar la efectividad de las estrategias y que permite que la gestión de estrategias para los servicios de TI realice cambios en las estrategias tan rápido como sea necesario, lo que hace que el proveedor de servicios sea ágil y receptivo.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="4.1.8 Factores críticos del éxito e indicadores de desempeño clave" FOLDED="true" ID="ID_186101973" CREATED="1660879960957" MODIFIED="1660881442841" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Esta sección proporciona ejemplos de factores críticos de éxito (CSF). Estas son las condiciones que deben existir, o las cosas que deben suceder, para que el proceso de gestión de la estrategia de servicios de TI se considere exitoso. Cada CSF incluirá ejemplos de indicadores clave de rendimiento (KPI). Estas son métricas que se utilizan para evaluar factores que son cruciales para el éxito del proceso. Los KPI, a diferencia de las métricas generales, deben estar relacionados con los CSF.
    </p>
    <p>
      
    </p>
    <p>
      La siguiente lista incluye algunos CSFs de muestra para la gestión de estrategias para servicios de TI. Cada organización debe identificar los CSF apropiados en función de sus objetivos para el proceso. Cada CSF de muestra va seguido de una pequeña cantidad de KPI típicos que soportan el CSF. Estos KPIs no deben ser adoptados sin una consideración cuidadosa. Cada organización debe desarrollar KPIs que sean apropiados para su nivel de madurez, sus CSFs y sus circunstancias particulares. Los logros en comparación con los KPIs deben monitorearse y usarse para identificar oportunidades de mejora, que deben registrarse en el registro de CSI para su evaluación y posible implementación.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="CSF Acceso a información estructurada sobre los entornos internos y externos en los que se desenvuelve el proveedor de servicios, y que puede ser utilizada para definir los componentes de la estrategia de la organización." ID="ID_564378798" CREATED="1660881074133" MODIFIED="1660881228436" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        KPI Cada espacio de mercado en la estrategia está respaldado por evidencia documentada de la oportunidad.
      </li>
      <li>
        KPI Cada hallazgo o recomendación en la evaluación estratégica se basa en información validada.
      </li>
      <li>
        KPI Los pronósticos y los resultados de la investigación externa se validan al final del período de planificación y se determina que tienen una precisión del 5 %.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="CSF La capacidad de identificar restricciones en la capacidad del proveedor de servicios para cumplir con los resultados comerciales y para entregar y administrar servicios - y la capacidad de eliminar estas restricciones o reducir su impacto." ID="ID_1551595865" CREATED="1660881193654" MODIFIED="1660881274112" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        KPI Número de acciones correctivas tomadas para eliminar las restricciones y el resultado de esas acciones en el logro de los objetivos estratégicos.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="CSF El proveedor de servicios tiene una comprensión clara de su perspectiva, y se revisa periódicamente para garantizar la pertinencia continua." ID="ID_329312257" CREATED="1660881230131" MODIFIED="1660881275433" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        KPI Las declaraciones de visión y misión se han definido y todos los miembros del personal han recibido capacitación sobre lo que significan en términos de sus funciones y trabajos dentro de la organización.
      </li>
      <li>
        KPI Cada unidad de negocio cuenta con un plan estratégico que muestra claramente cómo las actividades de la unidad de negocio están vinculadas a los objetivos, visión y misión de la organización.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="CSF El proveedor de servicio tiene un entendimiento claro de como está posicionado para asegurar la ventaja competitiva. La estrategia de servicio claramente identifica cuales servicios son entregados a cada espacio de mercado, y como el proveedor de servicio mantendrá su ventaja competitiva." ID="ID_1549151995" CREATED="1660881291832" MODIFIED="1660881330897" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        KPI Cada plan estratégico y táctico contiene una declaración de cómo los contenidos del plan respaldan la ventaja competitiva del proveedor de servicios.
      </li>
      <li>
        KPI (para proveedores de servicios externos) La organización gana un porcentaje definido de todos los acuerdos comerciales propuestos dentro del espacio de mercado identificado.
      </li>
      <li>
        KPI (para proveedores de servicios internos) La financiación está disponible para respaldar las iniciativas estratégicas y las unidades de negocio pueden demostrar un retorno de la inversión.
      </li>
      <li>
        KPI Cada servicio en la cartera de servicios tiene una declaración sobre los resultados comerciales que cumple y se mide en términos de estos resultados.
      </li>
      <li>
        KPI Cada servicio en la cartera de servicios tiene una explicación sobre a qué espacios de mercado se entregó y qué oportunidades encontrará. Se definió un conjunto de métricas y se informó sobre ellas, que muestran que el servicio está cumpliendo con la oportunidad identificada.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="CSF La capacidad de producir, almacenar, mantener y comunicar documentos de planificación estratégica." ID="ID_66583900" CREATED="1660881345033" MODIFIED="1660881369831" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        Los resultados de la auditoría de KPI muestran que cada parte interesada tiene una copia actualizada del documento de planificación apropiado.
      </li>
      <li>
        KPI Las partes interesadas pueden proporcionar una descripción general del contenido de los documentos de estrategia relevantes para su unidad de negocio.
      </li>
      <li>
        KPI Todos los documentos están bajo control de documentos y se han realizado cambios a los documentos a través de las medidas de control de cambios apropiadas.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="CSF La capacidad de traducir planes estratégicos en planes tácticos y operativos que son ejecutados por cada unidad organizacional." ID="ID_8600205" CREATED="1660881382722" MODIFIED="1660881413475" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        KPI Cada líder de unidad organizativa puede identificar los planes para su unidad y proporcionar una descripción general del contenido del plan.
      </li>
      <li>
        KPI Cada plan táctico y operativo se identifica por los planes estratégicos que respaldan, y los cambios en la estrategia se gestionan a través del control de cambios para garantizar que los planes tácticos y operacionales se encuentran alineados.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="CSF Cambios a los ambientes internos y externos son identificados y los ajustes realizados a las estrategias y documentos relacionados." ID="ID_1360446267" CREATED="1660881446177" MODIFIED="1660881471144" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        KPI Número de objetivos estratégicos que no son cumplidos - Identificados por las actividades CSI.
      </li>
      <li>
        KPI Desviación de actividades y patrones identificadas en la estrategia.
      </li>
      <li>
        KPI Número de cambios identificados en ambientes internos y externos, comparado con el número de cambios realizados a documentos estratégicos.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="4.1.9 Retos y Riesgos" FOLDED="true" ID="ID_228382129" CREATED="1660880009494" MODIFIED="1660880021486">
<node TEXT="4.1.9.1 Retos" ID="ID_475700488" CREATED="1660880025064" MODIFIED="1660881619513" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los desafíos para la gestión de la estrategia de los servicios de TI incluyen:
    </p>
    <ul>
      <li>
        La gestión de la estrategia de los servicios de TI se lleva a cabo en el nivel incorrecto de la organización - debe ser impulsada por los altos ejecutivos y cada unidad de la organización debe continuar con la producción de un plan estratégico, táctico y operativo. plan que es un subconjunto de la estrategia empresarial.
      </li>
      <li>
        Falta de información precisa sobre el medio externo.
      </li>
      <li>
        Falta de apoyo por parte de las partes interesadas.
      </li>
      <li>
        Falta de las herramientas apropiadas o falta de comprensión de cómo usar las herramientas y técnicas identificadas en esta sección.
      </li>
      <li>
        Ausencia de mecanismos y procedimientos adecuados de control documental.
      </li>
      <li>
        Los objetivos operativos deben coincidir con los objetivos estratégicos. De lo contrario, los gerentes operativos se esforzarán por lograr objetivos que no respaldan la estrategia.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="4.1.9.2 Riesgos" ID="ID_1906998996" CREATED="1660880032323" MODIFIED="1660881804757" TEXT_SHORTENED="true"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Los riesgos para la gestión de la estrategia de los servicios de TI incluyen:
    </p>
    <ul>
      <li>
        Un modelo de gobierno defectuoso que permite a los gerentes decidir si implementar todos los aspectos de una estrategia, o que les permite desviarse de la estrategia para objetivos a corto plazo.
      </li>
      <li>
        Las prioridades a corto plazo anulan las directivas de la estrategia. Por ejemplo, un déficit en las ventas de un trimestre puede alentar a los gerentes a implementar incentivos y medidas que alejan a la organización de sus políticas y objetivos estratégicos declarados.
      </li>
      <li>
        Tomar decisiones estratégicas cuando falta información sobre los entornos internos o externos, o usar información incorrecta o engañosa (es decir, información que no ha sido validado). Es más fácil evaluar si la información es precisa que determinar si está completa. La organización debe considerar seriamente cómo verificar sus fuentes y debe incluir una verificación de validez como parte de la revisión estratégica.
      </li>
      <li>
        El riesgo de elegir la estrategia equivocada. Es muy importante evaluar y retroalimentar el proceso de gestión del servicio en todas las etapas para que las decisiones incorrectas puedan detectarse temprano y corregirse.
      </li>
      <li>
        Las estrategias se ven como un ejercicio que ocurre una vez al año y que no tiene relación con lo que sucede durante el resto del año. Es fundamental alinear las métricas de cada unidad organizativa con las de las estrategias, y garantizar que tanto los gerentes como el personal estén informados sobre los contenidos y objetivos de las estrategias con el nivel de detalle adecuado. Las revisiones de desempeño deben estar vinculadas al logro de los objetivos estratégicos.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="4.2 Gestión del portafolio de servicios" ID="ID_1084044628" CREATED="1660880053876" MODIFIED="1660880067528"/>
</node>
</node>
<node TEXT="Diseno del servicio" FOLDED="true" POSITION="right" ID="ID_1765363838" CREATED="1657826901225" MODIFIED="1657830798652">
<edge COLOR="#ff00ff"/>
<font SIZE="10"/>
<node TEXT="Proposito" ID="ID_1513099385" CREATED="1657826920662" MODIFIED="1657827061860"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Es la fase del ciclo de vida del servicio donde la estrategia se convierte en un plan para la entrega de los objetivos del negocio
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Transicion del servicio" FOLDED="true" POSITION="right" ID="ID_547281625" CREATED="1657827107733" MODIFIED="1657830798652">
<edge COLOR="#00ffff"/>
<font SIZE="10"/>
<node TEXT="Proposito" ID="ID_1839602533" CREATED="1657827115650" MODIFIED="1657827387596"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Provee la guia para el desarrollo y mejora de las capacidades para introducir nuevos servicios o cambios en los existentes en los ambientes soportados.
    </p>
    <p>
      
    </p>
    <p>
      Describe como transicionar una organizacion desde un estado a otro mientras se mantiene controlado el riesgo y el soporte del conocimiento organizacional que sustente la decision.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Operacion del servicio" FOLDED="true" POSITION="right" ID="ID_1804621284" CREATED="1657827394604" MODIFIED="1657830798652">
<edge COLOR="#7c0000"/>
<font SIZE="10"/>
<node TEXT="Proposito" ID="ID_840216334" CREATED="1657827402871" MODIFIED="1657827522958"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Describe las mejores practicas para gestionar los servicios en ambientes soportados. Incluye una guia para alcanzar la efectividad y eficiencia en la entrega y soporte de servicios para asegurar el valor para cliente, los usuarios, y el proveedor de servicio.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Mejora continua del servicio" FOLDED="true" POSITION="right" ID="ID_1796701282" CREATED="1657827526246" MODIFIED="1657830798652">
<edge COLOR="#00007c"/>
<font SIZE="10"/>
<node TEXT="Proposito" ID="ID_1706288466" CREATED="1657827537122" MODIFIED="1657827654963"><richcontent CONTENT-TYPE="xml/" TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Provee una guia en la creacion y mantenimiento de valor para clientes a traves de la mejor estrategia, diseno, transicion y operacion de servicios.
    </p>
    <p>
      
    </p>
    <p>
      Combina principios, practicas y metodos de la gestion de la calidad, gestion de cambios y mejora de la capacidad.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</map>
